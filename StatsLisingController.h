//
//  StatsLisingController.h
//  NellPad
//
//  Created by Rakesh Kumar on 05/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatsLisingController : UIViewController<UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate,UIActionSheetDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@property(strong,nonatomic) UITableView *tbleUsers;
@property(nonatomic,strong) UIView *viewForAgentsListBG, *viewForAgentsList;

@property(nonatomic,strong) NSMutableArray *arrStatsData;
@property(nonatomic,strong) UITableView *tblStatsData;
@property(nonatomic,strong) UITextField *txtVSortSelection, *textSortingCompany;
@property(nonatomic,strong) UIButton *btnDropDown;
@property(nonatomic,strong) NSMutableArray *arrRecommendations;
@property(nonatomic,strong) NSString *strSelectedTag;
@property (strong,nonatomic) NSDictionary *responseDict;
@property(nonatomic,strong) UIActionSheet *actionSheetSort;

@property(nonatomic,strong) NSString *statsString;
@property(nonatomic,strong) NSArray *searchResults;

-(void)refreshTableStats;
-(void) DataReload;
@end
