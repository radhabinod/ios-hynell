//
//  CompaniesListVC.m
//  NellPat
//
//  Created by Ram Kumar on 04/04/16.
//  Copyright © 2016 Gagan Joshi. All rights reserved.
//

#import "CompaniesListVC.h"
#import "PatrawinCasesVC.h"
#import "CasesVC.h"
#import "PatrawinCase.h"
#import "RecommendationsListBO.h"
#import "PetrawinCasesList.h"
#import "CompanyList.h"
#import "CompanyBO.h"
#import "RecommendationsList.h"
#import "DataSyncBO.h"
#import <objc/message.h>
#import "CustomerIDVC.h"


@interface CompaniesListVC ()
{
    UIView *view;
    UITextField *sendTo;
    UIButton *dropdownButton;
    NSArray *caseArray;
    NSArray *arrRecommendation;
    UISearchBar *searchBar1;
    UISearchDisplayController *searchDisplayController;
    NSArray *searchResults;
    
    NSMutableArray *arrayListOfCompaniesName, *arrayListOfCompaniesId;
    CompanyBO *companyBo;
    
    CasesVC *obj_CaseVC;
    NSArray *Recommendation_TextArray;
    NSArray *arrayForTable;
    NSMutableArray *unique;
    UIRefreshControl *refresh;
    DataSyncBO *dataBO;
    
    NSSet *uniqueCompany;
    NSMutableArray *compArray;
    NSArray *comName;
    NSArray *commonCases;
    
    NSMutableArray *sortedArray;
    NSMutableArray *sortedCommonArray;
    NSMutableArray *finalArrayValueZero;
    
    
}

@end

@implementation CompaniesListVC
@synthesize casesArray,arrRecommendations,actionSheetSort,textSortingCompany,viewForAgentsListBG,viewForAgentsList,search,grantedSearch,statsString,statsCases,finalArrayValueZeroStat;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    obj_CaseVC  = [[CasesVC alloc] init];
    //search = [[NSArray alloc]init];
    commonCases = [[NSArray alloc]init];
    
   // NSLog(@"stats cases in companies:%@",statsCases);
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.arrRecommendations = [[NSMutableArray alloc]init];
      // comName = [[NSArray alloc]init];
    //searchResults = [[NSArray alloc] init];
   // arrayListOfCompaniesName = [[NSMutableArray alloc] init];
  //  arrayListOfCompaniesId = [[NSMutableArray alloc] init];
    
    
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]){
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
        }
    }
    self.title  = @"Companies list";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont systemFontOfSize: 18], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    
    [self configureView];

   
    
    self.casesArray = [[NSMutableArray alloc]init];
 
    if ([statsString isEqualToString:@"Statistics"])
    {
        [self getCompany];
    }
    
   if ([statsString isEqualToString:@"All Cases"]) {
       
       [self fetchFromDatabase];
       [self getCompany];
   }
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

}


- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    
        [super viewWillAppear:animated];
    
    
}
//-(void)refreshTable
//{
//    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"CaseListdownloaded" object:nil];
//    DataSyncBO *refreshData = [[DataSyncBO alloc]init];
//    [refreshData caseList];
//    [self fetchCompanyData];
//    
//}

-(void) DataReload
{
    [self fetchFromDatabase];
    [refresh endRefreshing];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

//-(void) CompanyReload
//{
//    [self fetchCompanyData];
//    [refresh endRefreshing];
//    [[NSNotificationCenter defaultCenter]removeObserver:self];
//}

//-(void)assigningDataCompany:(NSDictionary *)companyDict
//{
//    NSArray *arrCompanyData=[responseDict objectForKey:@"data"];
//    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
//    
//    arrayListOfCompaniesName=[NSMutableArray new];
//    
//    if ([strKey isEqualToString:@"0"])
//    {
//        for (CompanyList *companyList in arrCompanyData) {
//            
//            CompanyBO *objectCompanyBo=[[CompanyBO alloc]init];
//            
//            objectCompanyBo.strCompany_Id = companyList.companyID;
//            objectCompanyBo.strCompany_name = companyList.companyName;
//            
//            [arrayListOfCompaniesName addObject:objectCompanyBo];
//        }
//    }
//}


#pragma mark - Call For List Of Companies

//- (void) callForListOfCompanies
//{
//    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
//    inputPram.methodType = WebserviceMethodTypePost;
//    inputPram.relativeWebservicePath = @"listofcompanies";
//    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
//    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
//    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error)
//     {
//         if (error)
//         {
//             if ([error isErrorOfKindNoNetwork])
//                 
//                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//             
//             return ;
//         }
//         
//         error = nil;
//         
//         responseDict = nil;
//         
//         responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
//         
//         NSLog(@"Response Dict = %@", responseDict);
//         
//         arrayListOfCompaniesId = [[responseDict objectForKey:@"data"] valueForKey:@"company_id"];
//         
//         //         arrayListOfCompaniesName = [[self.responseDict objectForKey:@"data"] valueForKey:@"company_name"];
//         
//         arrayListOfCompaniesName = responseDict[@"data"];
//     }];
//}


-(void)configureView
{
    
    [self.view endEditing:YES];
    
    searchBar1 = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, IS_IPAD ? 768 : 320, 40)];
    
    searchBar1.placeholder = @"Search Company";
    
    searchBar1.delegate = self;
    //   [searchBar1 setImage:[UIImage imageNamed:@""] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self.view addSubview:searchBar1];
    for (UIView *searchBarSubview in [searchBar1 subviews]) {
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            @try {
                
                [(UITextField *)searchBarSubview setBorderStyle:UITextBorderStyleLine];
            }
            @catch (NSException * e) {
                // ignore exception
            }
        }
    }
    self.caseTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, searchBar1.frame.size.height+searchBar1.frame.origin.y, deviceWidth, deviceHeight-100) style:UITableViewStylePlain];
    
    self.caseTableView.delegate=self;
    self.caseTableView.dataSource=self;
    self.caseTableView.backgroundColor=[UIColor whiteColor];
    self.caseTableView.rowHeight = IS_IPAD ? 60 : 50.0f;
    self.caseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.caseTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.caseTableView];
    self.view.layer.borderColor = UIColorFromRGB(0xF0F0F0).CGColor;
    
    
    
    
}

//- (void) buttonDropdownCompanyName
//{
//    [self ShowViewForAgentsList:YES];
//}



//-(void)ShowViewForAgentsList:(BOOL)isShow
//{
//    [self.view endEditing:YES];
//    
//    if (self.viewForAgentsListBG)
//    {
//        [self.viewForAgentsListBG.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//        [self.viewForAgentsListBG removeFromSuperview];
//        self.viewForAgentsListBG=nil;
//    }
//    if(isShow)
//    {
//        self.viewForAgentsListBG=[[UIView alloc]initWithFrame:CGRectMake(0, IS_VERSION_GRT_7 ? 20:0, deviceWidth,IS_VERSION_GRT_7?deviceHeight-20:deviceHeight)];
//        self.viewForAgentsListBG.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
//        [APP_DELEGATE.window addSubview:self.viewForAgentsListBG];
//        
//        NSLog(@"values=%f %f %f %f",self.view.bounds.origin.x,self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height);
//        
//        self.viewForAgentsList=[[UIView alloc]initWithFrame:CGRectMake(10,50, deviceWidth-20,CGRectGetHeight(self.view.bounds)-75)];
//        
//        UIView *blueBar=[[UIView alloc]init];
//        blueBar.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
//        blueBar.frame=CGRectMake(0, 0, deviceWidth-20, 43);
//        
//        UILabel *cases=[[UILabel alloc]init];
//        cases.frame=CGRectMake(0, 0, blueBar.frame.size.width, 40);
//        cases.text=@"Choose Company";
//        cases.textAlignment=NSTextAlignmentCenter;
//        cases.textColor=[UIColor whiteColor];
//        [blueBar addSubview:cases];
//        
//        [self.viewForAgentsList addSubview:blueBar];
//        [self.viewForAgentsList bringSubviewToFront:blueBar];
//        
//        self.viewForAgentsList.backgroundColor = [UIColor whiteColor] ;
//        [self.viewForAgentsListBG addSubview:self.viewForAgentsList];
//        
//        //        CGFloat height= IS_IPAD ? self.arrUsersList.count * 55 : self.arrUsersList.count * 44;
//        //        CGFloat  heightOfTable;
//        //        CGFloat BoundHeight=CGRectGetHeight(self.view.bounds)-60;
//        //        if (height> BoundHeight)
//        //        {
//        //            heightOfTable=BoundHeight;
//        //        }
//        //        else heightOfTable=height;
//        //
//        _tbleUsers=[[UITableView alloc]initWithFrame:CGRectMake(0, 43, deviceWidth-20, IS_IPAD ? 870 : 425) style:UITableViewStylePlain];
//        _tbleUsers.delegate=self;
//        _tbleUsers.dataSource=self;
//        
//        _tbleUsers.rowHeight = IS_IPAD ? 55 : 44;
//        
//        _tbleUsers.backgroundColor=[UIColor clearColor];
//        _tbleUsers.backgroundView=nil;
//        _tbleUsers.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
//        
//        _tbleUsers.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
//        
//        [self.viewForAgentsList addSubview:_tbleUsers];
//        
//        UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
//        btnCross.frame = CGRectMake(0, 30, 24, 24);
//        [btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
//        [btnCross addTarget:self action:@selector(btnCross_Clicked) forControlEvents:UIControlEventTouchUpInside];
//        [self.viewForAgentsListBG addSubview:btnCross];
//        btnCross = nil;
//    }
//}

//-(void)btnCross_Clicked
//{
//    [self ShowViewForAgentsList:NO];
//}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}

// use this method to search data in list =====================================================================================================================

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] > 0) {
        
        //        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.case_type contains[c] %@", searchText];
        
        NSMutableArray *sResult = @[].mutableCopy;
        
        for (PatrawinCase *cs in sortedCommonArray) {
            NSRange r = [cs.customer_name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (r.length > 0) {
               // NSLog(@"case is's%@", cs.customer_name);
                [sResult addObject:cs];
            }
        }
        
        if (sortedArray) {
            sortedArray = nil;
        }
        
        sortedArray = [NSArray arrayWithArray:sResult];
        
        sResult = nil;
    } else {
        sortedArray = [NSArray arrayWithArray:sortedCommonArray];
    }
    //    [searchResults addObjectsFromArray:[self.casesArray filteredArrayUsingPredicate:resultPredicate]];
    
    [self.caseTableView reloadData];
}

#pragma mark - UISearchField Delegates

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
}

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.case_type contains[c] %@", searchText];
//
//    searchResults = [self.casesArray filteredArrayUsingPredicate:resultPredicate];
//
////    [searchResults addObjectsFromArray:[self.casesArray filteredArrayUsingPredicate:resultPredicate]];
//
//    NSLog(@"Search Array = %@", searchResults);
//}

//- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//{
//    [self filterContentForSearchText:searchText
//                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                      objectAtIndex:[self.searchDisplayController.searchBar
//                                                     selectedScopeButtonIndex]]];
//}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}



#pragma mark - Table view Datasource & Delegate Methods 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==_caseTableView)
    {
        return IS_IPAD ? 61 : 71.0f;
    }
    else
        return IS_IPAD ? 55 : 44;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
        return sortedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
    }
    cell.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (tableView == _caseTableView)
    {
        if (sortedArray.count==0)
        {
            UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?380: 160,IS_IPAD? 768: 320, 30)];
            
            if (responseDict.count==0)
            {
                [lblNoRecord setText:@""];
            }
            else
            {
                [lblNoRecord setText:@"No Record Available."];
            }
            [lblNoRecord setTextColor:[UIColor lightGrayColor]];
            lblNoRecord.textAlignment = NSTextAlignmentCenter;
            [lblNoRecord setBackgroundColor:[UIColor clearColor]];
            [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
            [cell.contentView addSubview:lblNoRecord];
            cell.backgroundColor=[UIColor clearColor];
        }
        else
        {
            
            PatrawinCase *cid = [sortedArray objectAtIndex:indexPath.row];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            cell.textLabel.text= [NSString stringWithFormat:@"%@",cid.customer_name];
//            if (cid.customer_name==nil) {
//                
//            }
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"Customer Id: %@",cid.customer_id];
            
//            NSString *hh=cid.customer_id;
//            if([hh isEqualToString:@"0"] || [hh isEqualToString:@""])
//            {
//                cell.textLabel.text = @"Others";
//
//            }
            
            cell.textLabel.textColor= [UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
            
            cell.textLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 18 : 16.0];
            
            cell.detailTextLabel.textColor=[UIColor darkGrayColor];
            
            cell.detailTextLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 16 : 12.0];
        }
        cell.accessoryView = [[ UIImageView alloc ]
                              initWithImage:[UIImage imageNamed:@"arrowMessage"]];
        
        UIView *_lineDiv=[[UIView alloc] init];
        _lineDiv.frame=CGRectMake(0, IS_IPAD ? 60 : 70.0f, self.view.frame.size.width, 1.0);
        _lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
        [cell addSubview:_lineDiv];
    }
    
//    else
//    {
//        cell.textLabel.font = [UIFont systemFontOfSize:IS_IPAD ? 25 : 15];
//        
//        companyBo=[arrayListOfCompaniesName objectAtIndex:indexPath.row];
//        cell.textLabel.text = (![companyBo.strCompany_name isEqual:[NSNull null]]) ? companyBo.strCompany_name  : @"";
//    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CustomerIDVC* customerIDTable = [[CustomerIDVC alloc] init];
    PatrawinCase *cusId=[sortedArray objectAtIndex:indexPath.row];
    customerIDTable.customerID = cusId.customer_id;
    customerIDTable.statisticCases = statsCases;
    customerIDTable.statsScreenType=statsString;
    [self.navigationController pushViewController:customerIDTable animated:YES];
}


-(void)getCompany
{
 
    if ([statsString isEqualToString:@"Statistics"]) {
        search=grantedSearch;
        commonCases=grantedSearch;
        
        PatrawinCase *cwithzero = [[PatrawinCase alloc]init];
        cwithzero.customer_id=@"0";
        cwithzero.customer_name=@"Other";
        cwithzero.comment=@"";
        cwithzero.comment_case=@"";
        
        cwithzero.keyword=@"";
        cwithzero.agent_id=@"";
        cwithzero.country_code=@"";
        cwithzero.title=@"";
        
        cwithzero.figure=@"";
        cwithzero.deadline=@"";
        cwithzero.Recommendation_Text=@"";
        cwithzero.case_type=@"";
        
        cwithzero.arrived_date=@"";
        cwithzero.case_id=@"";
        cwithzero.link=@"";
        cwithzero.figureSmall=@"";
        
        cwithzero.account_cost=@"";
        cwithzero.patent_number=@"";
        cwithzero.status=@"";
        cwithzero.Recommendation_Status=@"";

        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"customer_name"
                                                     ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sorted = [search sortedArrayUsingDescriptors:sortDescriptors];
        sortedArray=[sorted mutableCopy];
        NSArray *sorted1  =[commonCases sortedArrayUsingDescriptors:sortDescriptors];
        sortedCommonArray=[sorted1 mutableCopy];
        if(finalArrayValueZeroStat.count>0)
        {
            [sortedArray addObject:cwithzero];
            [sortedCommonArray addObject:cwithzero];
        }
        
    }
    
    if ([statsString isEqualToString:@"All Cases"]) {
        
        
        NSMutableArray *finalArray = [[NSMutableArray alloc]init];
        NSMutableArray *finalArrayValueZero = [[NSMutableArray alloc]init];
        

        //    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        //    dict[@"customer_id"] = @"0";
        //    dict[@"customer_name"] = @"Others";
        //    NSMutableArray *finalAr = [[NSMutableArray alloc]init];
        //
        //            [finalAr addObject:dict];
        
        uniqueCompany= [NSSet setWithArray:[self.casesArray valueForKey:@"customer_id"]];
        comName=[[NSMutableArray alloc]init];
        compArray = [[NSMutableArray alloc]init];
        compArray=[[uniqueCompany allObjects] mutableCopy];
        for(int i=0;i<compArray.count;i++)
        {
            for(int j=0;j<self.casesArray.count;j++)
            {
                PatrawinCase *cid=[self.casesArray objectAtIndex:j];
                NSString *hh=cid.customer_id;
                if([hh isEqualToString:@"0"] || [hh isEqualToString:@""])

                {
                    [finalArrayValueZero addObject:cid];
                    NSLog(@"cid null *****");
                    break;
                }
                else if([hh isEqualToString:[compArray objectAtIndex:i]])
                {
                    [finalArray addObject:cid];
                    break;
                }
                
//              // PatrawinCase *cid;
//             //   NSString *hh=cid.customer_id;
//                NSString *otherCompany = @"Others";
//                if([hh isEqualToString:@"0"])
//                {
//                    [finalArray addObject:otherCompany];
//                    break;
//                }

                
                
            }
        }
       // NSMutableSet *set = [NSMutableSet setWithArray:finalArrayValueZero];
       // [set addObjectsFromArray:finalArray];
        PatrawinCase *cwithzero = [[PatrawinCase alloc]init];
        cwithzero.customer_id=@"0";
        cwithzero.customer_name=@"Other";
        cwithzero.comment=@"";
       cwithzero.comment_case=@"";
        
        cwithzero.keyword=@"";
        cwithzero.agent_id=@"";
        cwithzero.country_code=@"";
        cwithzero.title=@"";
        
        cwithzero.figure=@"";
        cwithzero.deadline=@"";
        cwithzero.Recommendation_Text=@"";
        cwithzero.case_type=@"";
        
        cwithzero.arrived_date=@"";
        cwithzero.case_id=@"";
        cwithzero.link=@"";
        cwithzero.figureSmall=@"";
        
        cwithzero.account_cost=@"";
        cwithzero.patent_number=@"";
        cwithzero.status=@"";
        cwithzero.Recommendation_Status=@"";
        
        
       // [finalArray addObject:cwithzero];
      //  NSArray *arrayCombine = [set allObjects];
        comName = [[NSArray alloc]initWithArray:finalArray];

        
        
       search=comName;
        commonCases=comName;
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"customer_name"
                                                     ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        NSArray *sorted = [search sortedArrayUsingDescriptors:sortDescriptors];
        sortedArray=[sorted mutableCopy];
        NSArray *sorted1  =[commonCases sortedArrayUsingDescriptors:sortDescriptors];
        sortedCommonArray=[sorted1 mutableCopy];
        if(finalArrayValueZero.count>0)
        {
         [sortedArray addObject:cwithzero];
        [sortedCommonArray addObject:cwithzero];
        }
    }
//    for(int i=0;i<comName.count;i++)
//    {
//        PatrawinCase *cid=[comName objectAtIndex:i];
//   // NSLog(@"%d comp name: %@",i,cid.customer_id);
//    }
    
    
}


//-(void)getCompany
//{
//    NSMutableArray *finalArray = [[NSMutableArray alloc]init];
//    
//    //    NSMutableDictionary *others = [[NSMutableDictionary alloc]init];
//    //    [others setObject:@"0" forKey:@"customer_id"];
//    //    [others setObject:@"Other" forKey:@"customer_name"];
//    //    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//    //    dict[@"customer_id"] = @"0";
//    //    dict[@"customer_name"] = @"Others";
//    //    NSMutableArray *finalAr = [[NSMutableArray alloc]init];
//    //
//    //            [finalAr addObject:dict];
//    
//    uniqueCompany= [NSSet setWithArray:[self.casesArray valueForKey:@"customer_id"]];
//    comName=[[NSMutableArray alloc]init];
//    compArray = [[NSMutableArray alloc]init];
//    compArray=[[uniqueCompany allObjects] mutableCopy];
//    for(int i=0;i<compArray.count;i++)
//    {
//        for(int j=0;j<self.casesArray.count;j++)
//        {
//            PatrawinCase *cid=[self.casesArray objectAtIndex:j];
//            NSString *hh=cid.customer_id;
//            if([hh isEqualToString:[compArray objectAtIndex:i] ])
//            {
//                [finalArray addObject:cid];
//                break;
//            }
//            
//        }
//    }
//    
//    comName = [[NSArray alloc]initWithArray:finalArray];
//    //search=nil;
//    //search=grantedSearch;
//    if ([statsString isEqualToString:@"Statistics"]) {
//        search=grantedSearch;
//        commonCases=grantedSearch;
//    }
//    
//    if ([statsString isEqualToString:@"All Cases"]) {
//        
//        
//        search=comName;
//        commonCases=comName;
//    }
//    
//    //    for(int i=0;i<comName.count;i++)
//    //    {
//    //        PatrawinCase *cid=[comName objectAtIndex:i];
//    //   // NSLog(@"%d comp name: %@",i,cid.customer_id);
//    //    }
//    
//}


-(void)fetchFromDatabase
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"PetrawinCasesList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    
    NSEntityDescription *entityDesc1 = [NSEntityDescription entityForName:@"RecommendationsList" inManagedObjectContext:context];
    NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
    [request1 setEntity:entityDesc1];
    
    NSError *error1;
    NSArray *fetchedArray1 = [context executeFetchRequest:request1 error:&error1];
    
    responseDict =[NSMutableDictionary new];
    [responseDict setValue:fetchedArray forKey:@"data"];
    [responseDict setValue:fetchedArray1 forKey:@"recommendation_list"];
    
    //NSLog(@"The array is %i", fetchedArray1.count);
    
    if (fetchedArray.count == 0) {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"CaseListdownloaded" object:nil];
        dataBO=[DataSyncBO new];
        [dataBO caseList];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(sortCasesDescending) withObject:self waitUntilDone:YES];
    }
}



-(void)sortCasesDescending
{
    
    [self.casesArray removeAllObjects];
    [self.arrRecommendations removeAllObjects];
    
    caseArray = responseDict[@"data"];
    arrRecommendation=responseDict[@"recommendation_list"];
    
    //Recommendation_TextArray = [caseArray valueForKey:@"Recommendation_Text"];
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ([strKey isEqualToString:@"1"])
    {
        NSMutableArray *unsortedArr = [NSMutableArray array];
        for(PetrawinCasesList *caseDict in caseArray)
        {
            PatrawinCase *patCase = [[PatrawinCase alloc] init];
            patCase.account_cost=caseDict.accountCost;
            patCase.agent_id =caseDict.agentId ;
            patCase.arrived_date =[NSString stringWithFormat:@"%@",caseDict.arrivedDate];
            //patCase.case_id =[NSString stringWithFormat:@"%@",caseDict.caseId];
            patCase.case_id = caseDict.caseId;
            patCase.comment_case =caseDict.caseComment;
            patCase.case_type =caseDict.caseType;
            patCase.comment =caseDict.comment;
            patCase.country_code =caseDict.countrycode;
            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
            patCase.customer_name=caseDict.customerName;
            patCase.deadline=caseDict.deadline;
            patCase.figure=caseDict.figure;
            patCase.figureSmall = caseDict.figuresmall;
            patCase.link=caseDict.link;
            patCase.patent_number =caseDict.patentNo;
            patCase.title =caseDict.title;
            patCase.status =caseDict.status;
            patCase.Recommendation_Text = caseDict.recommendationText;
            
            //            for(RecommendationsList *dictRecommendation in arrRecommendation)
            //            {
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];;
            patCase.objRecommendationListBO=objRecommendationListBO;
            [unsortedArr addObject:patCase];
            //            }
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        self.casesArray = [sortedArray mutableCopy];
        sortedArray = nil;
        
    }
    else if ([strKey isEqualToString:@"0"])
    {
        for (RecommendationsList *dictRecommendation in arrRecommendation)
        {
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = dictRecommendation.recommendation_ID;
            objRecommendationListBO.strRecommendationText = dictRecommendation.recommendation_Text;
            objRecommendationListBO.strRecommendationStatus = dictRecommendation.recommendation_Status;
            [self.arrRecommendations addObject:objRecommendationListBO];
        }
        
        NSMutableArray *unsortedArr = [NSMutableArray array];
        for(PetrawinCasesList *caseDict in caseArray)
        {
            PatrawinCase *patCase = [[PatrawinCase alloc] init];
            patCase.account_cost=caseDict.accountCost;
            patCase.agent_id = caseDict.agentId;
            patCase.arrived_date = [NSString stringWithFormat:@"%@",caseDict.arrivedDate];
            //patCase.case_id = [NSString stringWithFormat:@"%@",caseDict.caseId];
            patCase.case_id = caseDict.caseId;
            patCase.comment_case = caseDict.caseComment;
            patCase.case_type = caseDict.caseType;
            patCase.comment = caseDict.comment;
            patCase.country_code = caseDict.countrycode;
            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
            patCase.customer_name=caseDict.customerName;
            patCase.deadline=caseDict.deadline;
            patCase.figure=caseDict.figure;
            patCase.figureSmall = caseDict.figuresmall;
            patCase.link=caseDict.link;
            patCase.patent_number = caseDict.patentNo;
            patCase.title = caseDict.title;
            patCase.status = caseDict.status;
            patCase.Recommendation_Text = caseDict.recommendationText;
            
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];;
            patCase.objRecommendationListBO=objRecommendationListBO;
            
            [unsortedArr addObject:patCase];
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        self.casesArray = [sortedArray mutableCopy];
        sortedArray = nil;
        
        
    }
    //    else
    //    {
    //        NSMutableArray *unsortedArr = [NSMutableArray array];
    //        for(PetrawinCasesList *caseDict in caseArray)
    //        {
    //            PatrawinCase *patCase = [[PatrawinCase alloc] init];
    //            patCase.account_cost=caseDict.accountCost;
    //            patCase.agent_id =caseDict.agentId;
    //            patCase.arrived_date = [NSString stringWithFormat:@"%@",caseDict.arrivedDate];
    //            //patCase.case_id = [NSString stringWithFormat:@"%@",caseDict.caseId];
    //            patCase.case_id = caseDict.caseId;
    //            patCase.comment_case = caseDict.caseComment;
    //            patCase.case_type = caseDict.caseType;
    //            patCase.comment = caseDict.comment;
    //            patCase.country_code = caseDict.countrycode;
    //            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
    //            patCase.customer_name=caseDict.customerName;
    //            patCase.deadline=caseDict.deadline;
    //            patCase.figure=caseDict.figure;
    //            patCase.figureSmall = caseDict.figuresmall;
    //            patCase.link=caseDict.link;
    //            patCase.patent_number = caseDict.patentNo;
    //            patCase.title = caseDict.title;
    //            patCase.status = caseDict.status;
    //
    //            patCase.Recommendation_Text = caseDict.recommendationText;
    //
    //            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
    //            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
    //            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
    //            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];
    //            patCase.objRecommendationListBO=objRecommendationListBO;
    //
    //            [unsortedArr addObject:patCase];
    //        }
    //        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
    //        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    //        self.casesArray = [sortedArray mutableCopy];
    //        sortedArray = nil;
    //    }
    
    searchResults = [NSArray arrayWithArray:self.casesArray];
    [self.caseTableView reloadData];
    
    //   [self sortCasesDescendings];
}

//
//
//
//-(void)fetchCompanyData
//{
//    AppDelegate *appDelegate;
//    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSManagedObjectContext *context = [appDelegate managedObjectContext];
//    
//    //Company Data.
//    
//    NSEntityDescription *entityDesc2 = [NSEntityDescription entityForName:@"CompanyList" inManagedObjectContext:context];
//    NSFetchRequest *request2 = [[NSFetchRequest alloc] init];
//    [request2 setEntity:entityDesc2];
//    
//    NSError *error2;
//    NSArray *fetchedArray2 = [context executeFetchRequest:request2 error:&error2];
//    [responseDict setValue:fetchedArray2 forKey:@"data"];
//    
//    if (fetchedArray2.count == 0)
//    {
//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(CompanyReload) name:@"CompanyListdownloaded" object:nil];
//        dataBO=[DataSyncBO new];
//        [dataBO callForListOfCompanies];
//    }
//    else
//    {
//        [self performSelectorOnMainThread:@selector(assigningDataCompany:) withObject:responseDict waitUntilDone:YES];
//    }
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
