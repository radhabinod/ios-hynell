//
//  PetrawinCasesList.m
//  NellPat
//
//  Created by Macbook on 6/17/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import "PetrawinCasesList.h"


@implementation PetrawinCasesList

@dynamic accountCost;
@dynamic agentId;
@dynamic arrivedDate;
@dynamic caseComment;
@dynamic caseId;
@dynamic caseType;
@dynamic comment;
@dynamic countrycode;
@dynamic customerId;
@dynamic customerName;
@dynamic deadline;
@dynamic figure;
@dynamic figuresmall;
@dynamic link;
@dynamic patentNo;
@dynamic pcNewDL;
@dynamic recommendationID;
@dynamic recommendationStatus;
@dynamic recommendationText;
@dynamic status;
@dynamic title;
@dynamic keyword;
@dynamic deadlineCode;

@end
