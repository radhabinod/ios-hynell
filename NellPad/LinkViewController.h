//
//  LinkViewController.h
//  NellPad
//
//  Created by Rakesh Kumar on 30/04/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkViewController : UIViewController<UIWebViewDelegate>
@property(nonatomic,strong) UIWebView *webView;
@property(nonatomic,strong) NSString *strUrl;
@property(assign) BOOL isDeadline;
@property(nonatomic,strong) NSString *screenType;

@end
