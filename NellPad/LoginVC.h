//
//  LogVC.h
//  NellPad
//
//  Created by Herry Makker  on 12/5/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController <UITextFieldDelegate, UIWebViewDelegate, UIGestureRecognizerDelegate,UIScrollViewDelegate>
{
    UILabel *lblForgotPassword;
    UIButton *btnForgotPassword;

    IBOutlet UIView *loginView;
    IBOutlet UIView *forgotPasswordView;
    NSDictionary *responseDict;
}
@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UITextField *forgotEmailField;


- (IBAction)btn_Submit_Clicked:(id)sender;
- (IBAction)btnLoginClicked:(id)sender;
- (IBAction)btnForgotPasswordClicked:(id)sender;


@end
