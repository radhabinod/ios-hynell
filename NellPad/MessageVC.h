//
//  MessageVC.h
//  NellPad
//
//  Created by Sanjeev Kumar on 12/19/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "Message.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <QuickLook/QuickLook.h>


@interface MessageVC : UIViewController <UITextFieldDelegate, MFMailComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate,UITextViewDelegate,UIActionSheetDelegate,QLPreviewControllerDataSource,QLPreviewControllerDelegate>
{
    NSDictionary *responseDictDropDown;
    NSDictionary *responseChatDict;
    NSString *emailBody;

}
@property(nonatomic,strong)UILabel *lblMessageReciever;
@property(strong, nonatomic) UITableView *tblViewChat;
@property(nonatomic,strong)  NSMutableArray *arrChatMessages;
@property(nonatomic,strong) Message *objMessageBO;
@property(nonatomic,strong)UITextField *sendTo;
@property(nonatomic,strong)UIButton *btnDropDown;
@property(nonatomic,strong)NSDictionary *responseDictReplyMessage;
@property(nonatomic,strong)UITextView *replyTextView;
@property(nonatomic,strong)UIView *replyView;
@property(nonatomic,strong) UIActionSheet *actionSheetSort;
@property(nonatomic,strong)NSMutableArray *payLoadArrayCopy;
@property(nonatomic,strong) UIView *viewForAgentsListBG;
@property(nonatomic,strong) UIView *viewForAgentsList;
@property(nonatomic,strong) NSMutableArray *arrUsersList;
@property(nonatomic,strong) UITableView *tblUsers;
@property(nonatomic,strong) NSString *strSelectedAgentID;

@end
