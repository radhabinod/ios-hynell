//
//  IPManagementVC.m
//  NellPad
//
//  Created by Herry Makker on 12/9/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

static NSString *kCustomCell = @"customCell";

#import "IPManagementVC.h"
#import "CustomDetailCell.h"
#import "InboxVC.h"
#import "PatrawinCasesVC.h"
#import "TimeRegistrationVC.h"
#import "StatsViewController.h"
#import "SettingsVC.h"
#import "UpdatedCasesVC.h"
#include <objc/message.h>

@interface IPManagementVC ()
{
    NSString *myString;
}

@end

@implementation IPManagementVC
@synthesize arrData, arrImg, scrollView,emailForChngePass;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title =@"IP Management";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    myString = [prefs stringForKey:@"emailForChangePassView"];
    
        NSLog(@" ^^^^^^^^^^^^^^^^^%@",myString);


    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
       
        }
    }
    
   // self.view.backgroundColor = UIColorFromRGB(0xF0F0F0);
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize: 18], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
   // [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,deviceWidth,deviceHeight)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.scrollEnabled=YES;
    self.scrollView.backgroundColor=UIColorFromRGB(0xF0F0F0);
    self.scrollView.userInteractionEnabled=YES;
    [self.view addSubview: self.scrollView];
    
    
    if ([strKey isEqualToString:@"0"])
    {
        UIButton *btnInbox = [UIButton buttonWithType:UIButtonTypeCustom];
        btnInbox.frame = CGRectMake(12, 40, self.view.frame.size.width/2-18,142);
        btnInbox.backgroundColor = [UIColor clearColor];
        [btnInbox setImage:[UIImage imageNamed:@"blueinboxbg.png"] forState:UIControlStateNormal];
        [btnInbox addTarget:self action:@selector(InboxtActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnInbox];
        
        
        UIButton *btnStatistic = [UIButton buttonWithType:UIButtonTypeCustom];
        btnStatistic.frame = CGRectMake(btnInbox.frame.size.width+btnInbox.frame.origin.x+10, 40, self.view.frame.size.width/2-18,142);
        btnStatistic.backgroundColor = [UIColor clearColor];
        [btnStatistic setImage:[UIImage imageNamed:@"bluestatisticsbg.png"] forState:UIControlStateNormal];
        [btnStatistic addTarget:self action:@selector(StatisticActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnStatistic];
        
        
        UIButton *btnCase = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCase.frame = CGRectMake(12, btnStatistic.frame.size.height+btnStatistic.frame.origin.y+10, self.view.frame.size.width/2-18,142);
        btnCase.backgroundColor = [UIColor clearColor];
        [btnCase setImage:[UIImage imageNamed:@"cases@2x.png"] forState:UIControlStateNormal];
        [btnCase addTarget:self action:@selector(CaseActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnCase];
        
        
        UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSetting.frame = CGRectMake(btnCase.frame.size.width+btnCase.frame.origin.x+10, btnStatistic.frame.size.height+btnStatistic.frame.origin.y+10, self.view.frame.size.width/2-18,142);
        btnSetting.backgroundColor = [UIColor clearColor];
        [btnSetting setImage:[UIImage imageNamed:@"bluesettingsbg.png"] forState:UIControlStateNormal];
        [btnSetting addTarget:self action:@selector(SettingActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnSetting];
        
        
        
        UIButton *btnTime = [UIButton buttonWithType:UIButtonTypeCustom];
        btnTime.frame = CGRectMake(12, btnSetting.frame.size.height+btnSetting.frame.origin.y+10, self.view.frame.size.width/2-18,142);
        btnTime.backgroundColor = [UIColor clearColor];
        [btnTime setImage:[UIImage imageNamed:@"bluetimeRegistrationbg.png"] forState:UIControlStateNormal];
        [btnTime addTarget:self action:@selector(TimeActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnTime];
        
    }
    else if([strKey isEqualToString:@"1"])
    {
        UIButton *btnInbox = [UIButton buttonWithType:UIButtonTypeCustom];
        btnInbox.frame = CGRectMake(12, 40, self.view.frame.size.width/2-18,142);
        btnInbox.backgroundColor = [UIColor clearColor];
        [btnInbox setImage:[UIImage imageNamed:@"blueinboxbg.png"] forState:UIControlStateNormal];
        [btnInbox addTarget:self action:@selector(InboxtActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnInbox];
        
        
        UIButton *btnStatistic = [UIButton buttonWithType:UIButtonTypeCustom];
        btnStatistic.frame = CGRectMake(btnInbox.frame.size.width+btnInbox.frame.origin.x+10, 40, self.view.frame.size.width/2-18,142);
        btnStatistic.backgroundColor = [UIColor clearColor];
        [btnStatistic setImage:[UIImage imageNamed:@"bluestatisticsbg.png"] forState:UIControlStateNormal];
        [btnStatistic addTarget:self action:@selector(StatisticActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnStatistic];
        
        
        UIButton *btnCase = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCase.frame = CGRectMake(12, btnStatistic.frame.size.height+btnStatistic.frame.origin.y+10, self.view.frame.size.width/2-18,142);
        btnCase.backgroundColor = [UIColor clearColor];
        [btnCase setImage:[UIImage imageNamed:@"cases@2x.png"] forState:UIControlStateNormal];
        [btnCase addTarget:self action:@selector(CaseActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnCase];
        
        
        UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSetting.frame = CGRectMake(btnCase.frame.size.width+btnCase.frame.origin.x+10, btnStatistic.frame.size.height+btnStatistic.frame.origin.y+10, self.view.frame.size.width/2-18,142);
        btnSetting.backgroundColor = [UIColor clearColor];
        [btnSetting setImage:[UIImage imageNamed:@"bluesettingsbg.png"] forState:UIControlStateNormal];
        [btnSetting addTarget:self action:@selector(SettingActionCall:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btnSetting];
        
    }
    
//    NSString *stringStatus = [USER_DEFAULTS objectForKey:@"user_status"];
//    
//    if ([stringStatus isEqualToString:@"1"] || [stringStatus isEqualToString:@"2"] )
//    {

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBacks) ];
    
    
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_notification@2x.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toUpdatedCasesList) ];
  
   // }
    
//    
//    
//    int Xpos=0;
//    int Ypos=0;
//    Xpos=5;
//    if ([strKey isEqualToString:@"0"])
//    {
//    Ypos=30;
//    }
//    else{
//    Ypos=55;
//    }
//    NSLog(@"device =%d",deviceWidth);
//    
//    for (int i=0; i<arrData.count; i++)
//    {
//        UIButton *btnTemp = [UIButton buttonWithType:UIButtonTypeCustom];
//        btnTemp.frame = CGRectMake(Xpos, Ypos, [UIImage imageNamed:@"blueinboxbg.png"].size.width,[UIImage imageNamed:@"blueinboxbg.png"].size.height);
//        btnTemp.backgroundColor = [UIColor yellowColor];
//        btnTemp.clipsToBounds = YES;
//        btnTemp.titleLabel.textColor = UIColorFromRGB(0x2a2a2a);
//        btnTemp.titleLabel.font = [UIFont fontWithName:@"Avenir" size:IS_IPAD ? 30 : 20];
//        btnTemp.tag = i+1;
//        [btnTemp setImage:[UIImage imageNamed:[arrImg objectAtIndex:i]] forState:UIControlStateNormal];
//        [btnTemp addTarget:self action:@selector(pressedtheScreenbutton:) forControlEvents:UIControlEventTouchUpInside];
//        [self.scrollView addSubview:btnTemp];
//        
//        UILabel *labelUnderBtn=[[UILabel alloc]initWithFrame:CGRectMake(Xpos-18, Ypos+90, 130, 30)];
//        labelUnderBtn.text = [arrData objectAtIndex:i];
//        labelUnderBtn.font=[UIFont systemFontOfSize:IS_IPAD ? 18 : 16.0];
//        labelUnderBtn.textColor = [UIColor darkGrayColor];
//        labelUnderBtn.textAlignment=NSTextAlignmentCenter;
//        [self.scrollView addSubview:labelUnderBtn];
//        
//        if ([strKey isEqualToString:@"0"])
//        {
//        if(btnTemp.tag%2==0)
//        {
//            Xpos=45;
//            Ypos+= IS_IPAD?110:125;
//        }
//        else
//        {
//            Xpos+=145;
//        }
//        }
//        else{
//            
//            if(btnTemp.tag%2==0)
//            {
//                Xpos=45;
//                Ypos+= IS_IPAD?110:155;
//            }
//            else
//            {
//                Xpos+=145;
//            }
//            
//        }
//        
//        btnTemp=nil;
//    }

    // self.scrollView.contentSize=CGSizeMake(deviceWidth, 600);
    //[self.scrollView setContentSize:CGSizeMake(deviceWidth, 650)];
}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton=YES;
    self.navigationController.navigationBarHidden=NO;

}


-(void)toUpdatedCasesList
{
    UpdatedCasesVC* updatedCasesVC = [[UpdatedCasesVC alloc] init];
    [self.navigationController pushViewController:updatedCasesVC animated:YES];
}



- (void) buttonBacks
{
//    NSString *stringStatus = [USER_DEFAULTS objectForKey:@"user_status"];
//    
//    if ([stringStatus isEqualToString:@"1"] || [stringStatus isEqualToString:@"2"] )
//    {
        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:YES];
    //}

}
-(IBAction)InboxtActionCall:(id)sender{
    
    InboxVC* calledme = [[InboxVC alloc] init];
    [self.navigationController pushViewController:calledme animated:YES];
    
}
-(IBAction)StatisticActionCall:(id)sender{
  
    StatsViewController *objStatsVC=[[StatsViewController alloc]init];
    [self.navigationController pushViewController:objStatsVC animated:YES];
}
-(IBAction)CaseActionCall:(id)sender{
  //  NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    PatrawinCasesVC* casesTable = [[PatrawinCasesVC alloc] init];
    
//    if([strKey isEqualToString:@"0"])
//    {
//    [casesTable refreshTable];
//    }
//    if([strKey isEqualToString:@"1"])
//    {
//    }
    [self.navigationController pushViewController:casesTable animated:YES];
}
-(IBAction)SettingActionCall:(id)sender{
   
//    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
//    NSLog(@"strKey=%@",strKey);
//    if([strKey isEqualToString:@"0"])
//    {
//        TimeRegistrationVC* displayContext = [[TimeRegistrationVC alloc] init];
//        [self.navigationController pushViewController:displayContext animated:YES];
//    }
//    else
//    {
//        SettingsVC *objSettingVC=[[SettingsVC alloc]init];
//        [self.navigationController pushViewController:objSettingVC animated:YES];
//    }
    
    UIStoryboard * story=[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    SettingsVC *objSettingVC=[story instantiateViewControllerWithIdentifier:@"SettingsVC"];
    objSettingVC.emailForChngePass = myString;
    
    [self.navigationController pushViewController:objSettingVC animated:YES];

    
}
-(IBAction)TimeActionCall:(id)sender
{
   
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    if([strKey isEqualToString:@"0"])
    {
        TimeRegistrationVC* displayContext = [[TimeRegistrationVC alloc] init];
        [self.navigationController pushViewController:displayContext animated:YES];
    }
    else
    {
        UIStoryboard * story=[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        SettingsVC *objSettingVC=[story instantiateViewControllerWithIdentifier:@"SettingsVC"];
         objSettingVC.emailForChngePass = myString;
        [self.navigationController pushViewController:objSettingVC animated:YES];
    }
}
- (void)pressedtheScreenbutton :(id)sender
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    if([sender tag] == 2)
    {
        PatrawinCasesVC* casesTable = [[PatrawinCasesVC alloc] init];
        [self.navigationController pushViewController:casesTable animated:YES];
    }
    if([sender tag] == 3)
    {
        StatsViewController *objStatsVC=[[StatsViewController alloc]init];
        [self.navigationController pushViewController:objStatsVC animated:YES];
    }
    if([sender tag] ==4)
    {
        if([strKey isEqualToString:@"0"])
        {
        TimeRegistrationVC* displayContext = [[TimeRegistrationVC alloc] init];
        [self.navigationController pushViewController:displayContext animated:YES];
        }
        else
        {
       //     SettingsVC *objSettingVC=[[SettingsVC alloc]init];
            UIStoryboard * story=[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
            
            SettingsVC *objSettingVC=[story instantiateViewControllerWithIdentifier:@"SettingsVC"];
            objSettingVC.emailForChngePass = myString;
            [self.navigationController pushViewController:objSettingVC animated:YES];
        }

    }
    if([sender tag] == 5)
    {
//        SettingsVC *objSettingVC=[[SettingsVC alloc]init];
//        

        UIStoryboard * story=[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        SettingsVC *objSettingVC=[story instantiateViewControllerWithIdentifier:@"SettingsVC"];
        objSettingVC.emailForChngePass = myString;
        [self.navigationController pushViewController:objSettingVC animated:YES];

        
        
        
        
        
        //        if([strKey isEqualToString:@"0"])
//        {
//        }
//        else if([strKey isEqualToString:@"1"])
//        {
//            SettingsVC *objSettingVC=[[SettingsVC alloc]init];
//            [self.navigationController pushViewController:objSettingVC animated:YES];
//        }

    }
    if ([sender tag]==6)
    {
        UIStoryboard * story=[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        SettingsVC *objSettingVC=[story instantiateViewControllerWithIdentifier:@"SettingsVC"];
        objSettingVC.emailForChngePass = myString;
        [self.navigationController pushViewController:objSettingVC animated:YES];
    }
}

@end
