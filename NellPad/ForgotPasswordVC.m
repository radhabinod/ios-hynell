//
//  ForgotPasswordVC.m
//  NellPat
//
//  Created by anish on 12/12/15.
//  Copyright © 2015 Gagan Joshi. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "Constants.h"
@interface ForgotPasswordVC (){
    
    @private
    UIScrollView *_scrollView;
      CGFloat heightofKeyboard;
    UITextField *_txtEmail;
    
     NSDictionary *responseForgotMail;
     NSDictionary *responseDict;
}

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    self.title = @"Forgot Password";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize: 18], NSFontAttributeName, nil]];
    
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-65);
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height-65);
    
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.delegate=self;
    
    UIImageView *locimage=[[UIImageView alloc]init];
    locimage.frame=CGRectMake(0, 0, [UIImage imageNamed:@"lockIcon.png"].size.width, [UIImage imageNamed:@"lockIcon.png"].size.height);
    locimage.center=CGPointMake(self.view.center.x, 110);
    locimage.image=[UIImage imageNamed:@"lockIcon.png"];
    locimage.backgroundColor=[UIColor clearColor];
    [_scrollView addSubview:locimage];
    
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, locimage.frame.size.height+locimage.frame.origin.y+20, self.view.frame.size.width, 50) bckgroundColor:[UIColor clearColor] title:@"Enter your email address to receive forgot password link" font:[UIFont fontWithName:helveticaRegular size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor darkGrayColor];
    _lblIAM.numberOfLines=0;
    [_scrollView addSubview:_lblIAM];

    
    UIImageView *emailImageView=[[UIImageView alloc]init];
    emailImageView.frame=CGRectMake(30,_lblIAM.frame.size.height+_lblIAM.frame.origin.y+50, [UIImage imageNamed:@"emailIcon.png"].size.width, [UIImage imageNamed:@"emailIcon.png"].size.height);
    emailImageView.image=[UIImage imageNamed:@"emailIcon.png"];
    emailImageView.backgroundColor=[UIColor clearColor];
    [_scrollView addSubview:emailImageView];
    
    
    _txtEmail = [self creareTextField:CGRectMake(50,_lblIAM.frame.size.height+_lblIAM.frame.origin.y+30, self.view.frame.size.width-90, 45) backgroundColor:[UIColor clearColor] placeHolder:@"Email" placeHolderColor:[UIColor darkGrayColor] textColor:[UIColor lightGrayColor]];
    _txtEmail.delegate = self;
    _txtEmail.returnKeyType = UIReturnKeyNext;
    _txtEmail.placeholder=@"Email";
    [_txtEmail setFont:[UIFont fontWithName:helveticaRegular size:15]];
    
    
    UIView *_line=[[UIView alloc] init];
    _line.frame=CGRectMake(55, _txtEmail.frame.size.height+_txtEmail.frame.origin.y, _txtEmail.frame.size.width, 1);
    _line.backgroundColor=[UIColor lightGrayColor];
    [_scrollView addSubview:_line];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _txtEmail.leftView = paddingView;
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    _txtEmail.keyboardType = UIKeyboardTypeEmailAddress;
    [_txtEmail setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtEmail.borderStyle = UITextBorderStyleNone;
    _txtEmail.textAlignment = NSTextAlignmentLeft;
    _txtEmail.textColor=[UIColor darkGrayColor];
    [_scrollView addSubview:_txtEmail];
    
    
    UIButton *_btnLoginIn = [self createButton:CGRectMake(0, _txtEmail.frame.size.height+_txtEmail.frame.origin.y+40, [UIImage imageNamed:@"Button.png"].size.width, [UIImage imageNamed:@"Button.png"].size.height) bckgroundColor:[UIColor clearColor] image:nil title:@"Submit" font:[UIFont fontWithName:helveticaRegular size:20.0] titleColor:[UIColor blackColor]];
    _btnLoginIn.center=CGPointMake(self.view.center.x, _txtEmail.frame.size.height+_txtEmail.frame.origin.y+50);
    [_btnLoginIn addTarget:self action:@selector(submitButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [_btnLoginIn setBackgroundImage:[UIImage imageNamed:@"Button.png"] forState:UIControlStateNormal];
    [_btnLoginIn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_scrollView addSubview:_btnLoginIn];
    

    
    
    [self.view addSubview:_scrollView];
    
    
    
    
    
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDidTapped)] ;
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

    
}


- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}

-(void)submitButtonAction{
      [self.view endEditing:YES];
    
    [self.view endEditing:YES];
    
    BOOL success = [CommonMethods validateEmail:_txtEmail.text];
    if (success)
        [self forgotPassword];
    else
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"Please enter a valid email address."];
    
}
-(void)forgotPassword
{
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"";
    [inputParams.dict_postParameters setObject:GET_VALID_STRING(_txtEmail.text)
                                        forKey:@"email"];
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error) {
             if ([error isErrorOfKindNoNetwork])
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         }
         error = nil;
         responseForgotMail = [NSJSONSerialization JSONObjectWithData:response
                                                              options:NSJSONReadingMutableContainers
                                                                error:&error];
         [self performSelectorOnMainThread:@selector(submitEmail) withObject:self waitUntilDone:YES];
         
     }];
    
}
-(void)submitEmail
{
    if ([responseDict[@"log"] isEqualToString:@"Password sent successfully."])
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:responseDict[@"log"]];
    else
        [CommonMethods showAlertWithTitle:@"Error!!" andMessage:responseDict[@"error"]];
    
}

-(void)scrollViewDidTapped{
    [self.view endEditing:YES];
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI

-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}

//---------------------------------------------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UITextField UI
//--------------------------------------------------------------------------------------------------------------------------------------------------
-(UITextField*)creareTextField:(CGRect)frame backgroundColor:(UIColor*)backgroundColor placeHolder:(NSString*)placeHolder placeHolderColor:(UIColor*)placeHolderColor textColor:(UIColor*)textColor
//---------------------------------------------------------------------------------------------------------------------------------------------------
{
    UITextField *_txtField = [[UITextField alloc]initWithFrame:frame];
    _txtField.backgroundColor = [UIColor clearColor];
    _txtField.delegate = self;
    _txtField.returnKeyType = UIReturnKeyNext;
    _txtField.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtField.font=[UIFont fontWithName:helveticaLight size:18];
    _txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _txtField.keyboardType = UIKeyboardTypeEmailAddress;
    _txtField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtField.borderStyle = UITextBorderStyleNone;
    _txtField.textAlignment = NSTextAlignmentLeft;
    _txtField.textColor = textColor;
    return _txtField;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)                                                       name:UIKeyboardWillHideNotification
                                               object:nil];
    
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- viewWillDisappear
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        
    {
        
        if (screenSize.height > 480.0f)
            
        {
            heightofKeyboard = 240;
        }
        else{
            heightofKeyboard = 220;
            
        }
    }
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, heightofKeyboard, 0);
    
    [_scrollView setContentInset:insets];
    [_scrollView setScrollIndicatorInsets:insets];
    
}
-(void)keyboardWillHide
{
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [_scrollView setContentInset:insets];
    [_scrollView setScrollIndicatorInsets:insets];
    
}
#pragma mark- TouchBegan
//-----------------------------------------------------------------------------------------------------------------------------------------------
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.view endEditing:YES];
}

#pragma mark- TextField Delegates
/********************************************** TextField Delegates ******
 *******************************/
-(void)textFieldDidBeginEditing:(UITextField *)textField
//----------------------------------------------------------------------------------------------------------------------------------------
{
    //iPhone 4s
    if (kDeviceHeight==480.0) {
        [UIView animateWithDuration:0.2f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             // _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y-50, _scrollView.frame.size.width, _scrollView.frame.size.height);
                             
                         }
                         completion:nil];
    }else{
        [UIView animateWithDuration:0.2f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             // _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y-10, _scrollView.frame.size.width, _scrollView.frame.size.height);
                         }
                         completion:nil];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
