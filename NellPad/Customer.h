//
//  Customer.h
//  NellPad
//
//  Created by Atul Thakur on 4/2/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Customer : NSObject
{
    
}
@property(nonatomic, strong)NSString *customer_id;
@property(nonatomic, strong)NSString *customer_name;
@end
