//
//  InboxVC.m
//  NellPad
//
//  Created by Herry Makker on 12/9/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import "InboxVC.h"
#import "CustomPickerCell.h"
#import "MessageVC.h"
#import "Message.h"
#import "ComposeSheetVC.h"
#import "IPManagementVC.h"
#import <objc/message.h>

@interface InboxVC ()
{
    
    NSMutableArray * sortingArray;
}
@end

@implementation InboxVC
@synthesize actionSheetSort,arrMessages,btnDropDown,tblMessages,txtFSendTo,responseDict,viewForPopUp,viewForPopUpBG;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title  = @"Inbox";
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
            
            
        }
    }
    //dashboard
    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    button.frame = CGRectMake(0, 0, 25, 16);
//    [button setBackgroundImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
//    [button addTarget:self.navigationController.parentViewController action:@selector(backbuttonaction) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *customBarItem2 = [[UIBarButtonItem alloc] initWithCustomView:button];
//    self.navigationItem.leftBarButtonItem =customBarItem2;
//
//    self.view.backgroundColor=[UIColor whiteColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize: 18], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
   // [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    
    self.navigationController.navigationBar.translucent = NO;
    
    self.arrMessages=[[NSMutableArray alloc]init];
    
    NSString *actionSheetTitle = @"Choose Sorting Type";
    NSString *other1 = @"Sort By Latest";
    NSString *other2 = @"Sort By Oldest";
    NSString *other3 = @"Sort By Unread";
    NSString *cancelTitle = @"Cancel";
    
    //sortingArray =[[NSMutableArray alloc]initWithObjects:other1,other2,other3, nil];
    
     actionSheetSort = [[UIActionSheet alloc] initWithTitle:actionSheetTitle delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:other1, other2, other3, nil];
  
    
    [self DesignDropDownView];
    
     self.view.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

}


- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
  //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}

-(void)backbuttonaction{
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([buttonTitle isEqualToString:@"Sort By Latest"])
    {
     txtFSendTo.text = @"   Sort By Latest";
        [self sortInboxinDescending];
      //  NSLog(@"index essss ");
    }
    else if ([buttonTitle isEqualToString:@"Sort By Oldest"])
    {
        txtFSendTo.text = @"   Sort By Oldest";
        [self sortInboxinAscending];
    }
    else if ([buttonTitle isEqualToString:@"Sort By Unread"])
    {
        txtFSendTo.text = @"   Sort By Unread";
        [self sortInboxByUnread];
    }
    else if ([buttonTitle isEqualToString:@"Cancel"])
    {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    UIBarButtonItem *btnRight=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"compose-top"] style:UIBarButtonItemStylePlain target:self action:@selector(btnComposeClk)];
    
    self.navigationItem.rightBarButtonItem=btnRight;
    [self CallForGetInboxMessages];
}

-(void)btnComposeClk
{
   ComposeSheetVC *obj_ComposeVC = [[ComposeSheetVC alloc] init];
    obj_ComposeVC.strScreenType=@"Inbox";
    [self.navigationController pushViewController:obj_ComposeVC animated:YES];
}
-(void)DesignDropDownView
{
    
    UIView *_bgview=[[UIView alloc]init];
    _bgview.frame=CGRectMake(0, 0, self.view.frame.size.width, 70);
    _bgview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:_bgview];
    
    
    txtFSendTo = [[UITextField alloc] initWithFrame:CGRectMake(20, 20, deviceWidth-40, 30)];
    txtFSendTo.userInteractionEnabled = NO;
    txtFSendTo.text = @"   Sort By Latest";
    txtFSendTo.clipsToBounds = YES;
    txtFSendTo.textColor = [UIColor darkGrayColor];
    txtFSendTo.layer.borderColor = UIColorFromRGB(0xA9A9A9).CGColor;
    txtFSendTo.layer.borderWidth = 0.5f;
    txtFSendTo.layer.cornerRadius = 5.0;
    txtFSendTo.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    txtFSendTo.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    txtFSendTo.delegate = self;
    [self.view addSubview:txtFSendTo];
//
//
    btnDropDown = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDropDown setBackgroundImage:[UIImage imageNamed:@"searchBox.png"] forState:UIControlStateNormal];
    btnDropDown.frame = CGRectMake(20, 20, deviceWidth-40, 30);
    btnDropDown.layer.cornerRadius = 5.0;
    [btnDropDown addTarget:self action:@selector(btndropDownClk) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDropDown];
    
    
    self.tblMessages = [[UITableView alloc] initWithFrame:CGRectMake(0, _bgview.frame.size.height+_bgview.frame.origin.y, deviceWidth, IS_IPAD?deviceHeight-120: deviceHeight-80-_bgview.frame.size.height) style:UITableViewStylePlain];
    self.tblMessages.delegate = self;
    self.tblMessages.dataSource = self;
    
    self.tblMessages.rowHeight = IS_IPAD ? 60 : 50.0f;
    
    self.tblMessages.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tblMessages.backgroundColor=[UIColor clearColor];
    //self.tblMessages.backgroundColor=[UIColor redColor];
    
    self.tblMessages.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.tblMessages];
    

//    UIButton *btnCompose = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnCompose.frame=CGRectMake(0, self.view.frame.size.height-113, self.view.frame.size.width/3-1, 50);
//    [btnCompose setImage:[UIImage imageNamed:@"composeButton.png"] forState:UIControlStateNormal];
//    btnCompose.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
//    [self.view addSubview:btnCompose];
//    
//    UIButton *btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnShare.frame=CGRectMake(btnCompose.frame.size.width+btnCompose.frame.origin.x+1, self.view.frame.size.height-113, self.view.frame.size.width/3-1, 50);
//    [btnShare setImage:[UIImage imageNamed:@"shareButton.png"] forState:UIControlStateNormal];
//    btnShare.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
//    [self.view addSubview:btnShare];
//    
//    UIButton *btnReply = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnReply.frame=CGRectMake(btnShare.frame.size.width+btnShare.frame.origin.x+1, self.view.frame.size.height-113, self.view.frame.size.width/3, 50);
//    [btnReply setImage:[UIImage imageNamed:@"replyButton.png"] forState:UIControlStateNormal];
//    btnReply.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
//    [self.view addSubview:btnReply];
    

}

-(void)btndropDownClk
{
    [actionSheetSort showInView:self.view];
}

- (void) callForCompanyIdData
{
    
}

-(void)CallForGetInboxMessages
{
    [self.arrMessages removeAllObjects];
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"getinbox";
    
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];

    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             
             return;
         }
         else
         {
             error = nil;
             self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
             
             [self performSelectorOnMainThread:@selector(sortInboxinDescending) withObject:self waitUntilDone:YES];
         }
     }];
}

-(void)sortInboxinDescending
{
    
    NSArray *arrTemp = [[self.responseDict objectForKey:@"data"]  mutableCopy];
    NSMutableArray *unsortedArr = [NSMutableArray array];
    
    for (NSDictionary *messageDict in arrTemp)
    {
        Message *objMessage = [[Message alloc] init];

        objMessage.strMessage_Id = [[messageDict objectForKey:@"Message_Id"] isKindOfClass:[NSString class]]?[messageDict objectForKey:@"Message_Id"]:@"";
        objMessage.strSenderName = [[messageDict objectForKey:@"Name"] isKindOfClass:[NSString class]]?[messageDict objectForKey:@"Name"]:@"";
        objMessage.strRead_Status= [[messageDict objectForKey:@"Read_Status"]isKindOfClass:[NSString class]]?[messageDict objectForKey:@"Read_Status"]:@"";
        objMessage.strConversation_Id=[[messageDict objectForKey:@"conversation_id"] isKindOfClass:[NSString class]]?[messageDict objectForKey:@"conversation_id"]:@"";
        objMessage.strDateTime = [[messageDict objectForKey:@"datetime"]isKindOfClass:[NSString class]]?[messageDict objectForKey:@"datetime"]:@"";
        objMessage.strMessage = [[messageDict objectForKey:@"message"] isKindOfClass:[NSString class]]?[messageDict objectForKey:@"message"]:@"";
        objMessage.strRelation_Id = [[messageDict objectForKey:@"relation_id"] isKindOfClass:[NSString class]]?[messageDict objectForKey:@"relation_id"]:@"";
        objMessage.strSubject = [[messageDict objectForKey:@"subject"] isKindOfClass:[NSString class]]?[messageDict objectForKey:@"subject"]:@"";
        objMessage.you = [messageDict objectForKey:@"you"] ;
        [unsortedArr addObject:objMessage];
    }

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"strDateTime" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    self.arrMessages = [sortedArray mutableCopy];
    sortedArray = nil;
    [self.tblMessages reloadData];
}

-(void)sortInboxinAscending
{
    NSMutableArray *unsortedArray = [self.arrMessages mutableCopy];

    [self.arrMessages removeAllObjects];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"strDateTime" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    self.arrMessages = [sortedArray mutableCopy];
    sortedArray = nil;
    
    [self.tblMessages reloadData];
}

-(void)sortInboxByUnread
{
    NSMutableArray *unsortedArr = [self.arrMessages mutableCopy];

    [self.arrMessages removeAllObjects];
    NSSortDescriptor *unreadDescriptor = [[NSSortDescriptor alloc] initWithKey:@"strRead_Status" ascending:YES ];
    NSArray *sortDescriptors = [NSArray arrayWithObject:unreadDescriptor];
    NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    self.arrMessages = [sortedArray mutableCopy];
    sortedArray = nil;
    [self.tblMessages reloadData];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==200) {
return IS_IPAD ? 60 : 44;
    
    }
    
    return IS_IPAD ? 71 : 66.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
//    if (tableView.tag==200) {
//        return [sortingArray count];
//    }
    
    
    if (self.arrMessages.count==0)
    {
        return 1;
    }
    
  return self.arrMessages.count;
    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag!=200) {
        
    
    static NSString *CellIdentifier2 = @"Cell2";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier2];
        
        cell.detailTextLabel.font =[UIFont systemFontOfSize:14];
        
        cell.backgroundColor=[UIColor clearColor];
    }
    
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    tblMessages.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (self.arrMessages.count==0)
    {
        UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?375: 160,IS_IPAD? 768: 320, 30)];
        
        if (self.responseDict.count==0)
        {
            [lblNoRecord setText:@""];
        }
        else
        {
        
            [lblNoRecord setText:@"No Record Available."];
        
        }
        
        [lblNoRecord setTextColor:[UIColor lightGrayColor]];
        
        lblNoRecord.textAlignment = NSTextAlignmentCenter;
        
        [lblNoRecord setBackgroundColor:[UIColor clearColor]];
        
        [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
        
        [cell.contentView addSubview:lblNoRecord];
        
    }
    else
    {
     
        //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        cell.backgroundColor=[UIColor clearColor];

        Message *objMessage = self.arrMessages[indexPath.row];
        
        UILabel *lbl_SubjectHD = [[UILabel alloc] initWithFrame:CGRectMake(50, 13, IS_IPAD ? 75 : 65, 20)];
        
        lbl_SubjectHD.backgroundColor = [UIColor clearColor];
        
        lbl_SubjectHD.text= @"Subject:";
        
        lbl_SubjectHD.textAlignment=NSTextAlignmentLeft;
        
        lbl_SubjectHD.textColor=[UIColor blackColor];
        
        lbl_SubjectHD.font= IS_IPAD ? FONT_NORMAL(18) : FONT_NORMAL(16.0);
        
        [cell.contentView addSubview:lbl_SubjectHD];
        
        UILabel *lbl_SubjectValue = [[UILabel alloc] initWithFrame:CGRectMake(lbl_SubjectHD.frame.size.width+lbl_SubjectHD.frame.origin.x, 13,175, 20)];
        
        lbl_SubjectValue.backgroundColor = [UIColor clearColor];
        
        lbl_SubjectValue.text= [NSString stringWithFormat:@"%@",objMessage.strSubject];
        
        lbl_SubjectValue.textAlignment=NSTextAlignmentLeft;
        
        lbl_SubjectValue.textColor=[UIColor darkGrayColor];
        
        lbl_SubjectValue.font=FONT_NORMAL(16.0);
        
        [cell.contentView addSubview:lbl_SubjectValue];
        
        
    if( [objMessage.you isEqualToString:@"0"] && [objMessage.strRead_Status isEqualToString:@"0"])
       {
        cell.backgroundColor = [UIColor colorWithRed:0.8 green:0.898 blue:1 alpha:1];
       }
        
        UIImageView *imgV=[[UIImageView alloc]init];
        imgV.frame=CGRectMake(11, 17, [UIImage imageNamed:@"MessageRecieved.png"].size.width, [UIImage imageNamed:@"MessageRecieved.png"].size.width);
        int youValue=[objMessage.you intValue];
        
        if (youValue==0)
        {
            [imgV setImage:[UIImage imageNamed:@"MessageRecieved.png"]];
        }
        else if (youValue==1)
        {
            [imgV setImage:[UIImage imageNamed:@"MessageReply.png"]];
        }
        [cell.contentView addSubview:imgV];
        
        UILabel *lbl_MessageValue = [[UILabel alloc] initWithFrame:CGRectMake(imgV.frame.size.width+imgV.frame.origin.x+10, IS_IPAD ? 35 : 35, 240, 20)];
        
        lbl_MessageValue.backgroundColor = [UIColor clearColor];
        
        lbl_MessageValue.text=objMessage.strMessage;
        
        lbl_MessageValue.font = FONT_NORMAL(13.0);
        
        lbl_MessageValue.textAlignment=NSTextAlignmentLeft;
        
        lbl_MessageValue.textColor=[UIColor lightGrayColor];
        
        [cell.contentView addSubview:lbl_MessageValue];
        
    }
    cell.accessoryView = [[ UIImageView alloc ]
                            initWithImage:[UIImage imageNamed:@"arrowMessage"]];
    
    UIView *_lineDiv=[[UIView alloc] init];
    
    _lineDiv.frame=CGRectMake(0, IS_IPAD ? 70 : 65.0f, self.view.frame.size.width, 1.0);
    
    _lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
    
    [cell addSubview:_lineDiv];
    
    return cell;
    }
    else
    {
    static  NSString *CellIdentifier=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
    }
    
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UILabel *lbl_NameHD = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 170, 30)];
   // lbl_NameHD.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
    // lbl_NameHD.backgroundColor=[UIColor redColor];
    lbl_NameHD.font = IS_IPAD ? FONT_NORMAL(18) : FONT_NORMAL(16.0);
    lbl_NameHD.textAlignment=NSTextAlignmentCenter;
    lbl_NameHD.textColor=[UIColor blackColor];
    [cell.contentView addSubview:lbl_NameHD];
    

   // lbl_NameHD.text=[sortingArray objectAtIndex:indexPath.row];
    
        
    return cell;
    }
}


#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath
{
    if (tableView.tag!=200) {
        
    
    if (self.arrMessages.count==0)
    {
        
    }
    else
    {
        Message *objMessage = self.arrMessages[indexPath.row];
        
        MessageVC* objMessageController = [[MessageVC alloc] init];
        objMessageController.objMessageBO=[[Message alloc]init];
        
        objMessageController.objMessageBO=objMessage;
        
        [self.navigationController pushViewController:objMessageController animated:YES];
    }
        
    }
//    else
//    {
//       /// [tableView deselectRowAtIndexPath:indexPath animated:YES];
//        [self showCustomViewForPopUpForArray:nil toShow:NO];
//
//    txtFSendTo.text=[NSString stringWithFormat:@"   %@",[sortingArray objectAtIndex:indexPath
//                                                              .row]];
//        
//        if (indexPath.row==1) {
//            [self sortInboxinAscending];
//        }
//        else if (indexPath.row==0)
//        {
//            [self sortInboxinDescending];
//
//        }
//        else if (indexPath.row==2)
//        {
//            [self sortInboxByUnread];
//
//        
//        }
//    }
}


#pragma mark -
- (BOOL) shouldAutorotate
{
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait)
    {
        return YES;
    }
    return NO;
}
- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}



- (void) tableView:(UITableView *) tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *objMessage=[self.arrMessages objectAtIndex:indexPath.row];
    
    NSString *strConversation=objMessage.strConversation_Id;
    
    NSString *strRelation = objMessage.strRelation_Id;
    
    [self CallToDeleteConversationForConversation:strConversation relation:strRelation withIndexpathValue:indexPath.row];
}


#pragma mark- CallToDeleteConversation

- (void) CallToDeleteConversationForConversation:(NSString *) strConversationId relation: (NSString *) strRelationId withIndexpathValue:(NSInteger) selectedIndexPathRow
{
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    
    inputParams.relativeWebservicePath = @"deleteConversationMessagesByConversationId";
    
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    
    [inputParams.dict_postParameters setObject:strConversationId forKey:@"conversationId"];
    
    [inputParams.dict_postParameters setObject:strRelationId forKey:@"relationId"];
    
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return;
             
             return;
         }
         else
         {
             error = nil;
          
             self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
             
              NSString *strMessage=[[self.responseDict objectForKey:@"data"] objectForKey:@"log"];
             
             if ([strMessage isEqualToString:@"Conversation deleted successfully."])
             {
                 [self.arrMessages removeObjectAtIndex:selectedIndexPathRow];
                 [tblMessages reloadData];
             }
         }
     }];
}





#pragma mark Popup


//-(void)showCustomViewForPopUpForArray:(NSMutableArray *)arrData toShow:(BOOL)isshow
//{
//    if(self.viewForPopUpBG)
//    {
//        [self.viewForPopUpBG.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//        [self.viewForPopUpBG removeFromSuperview];
//        self.viewForPopUpBG=nil;
//    }
//    if (isshow)
//    {
//        self.viewForPopUpBG=[[UIView alloc]initWithFrame:CGRectMake(0, IS_VERSION_GRT_7 ? 20:0, deviceWidth,IS_VERSION_GRT_7? deviceHeight-20:deviceHeight)];
//        self.viewForPopUpBG.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
//        [APP_DELEGATE.window addSubview:self.viewForPopUpBG];
//        
//        UIView *blueBar=[[UIView alloc]init];
//        blueBar.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
//        blueBar.frame=CGRectMake(60, 149, self.view.frame.size.width-120, 43);
//        
//        UILabel *cases=[[UILabel alloc]init];
//        cases.frame=CGRectMake(0, 0, blueBar.frame.size.width, 40);
//        cases.text=@"Choose Sorting Type";
//        cases.textAlignment=NSTextAlignmentCenter;
//        cases.textColor=[UIColor whiteColor];
//        [blueBar addSubview:cases];
//        
//        [self.viewForPopUpBG addSubview:blueBar];
//        
//        self.viewForPopUp=[[UIView alloc]initWithFrame:CGRectMake(60,190, deviceWidth-120,IS_IPAD ? 870 : deviceHeight-75-60-300)];
//        self.viewForPopUp.backgroundColor = [UIColor whiteColor] ;
//        [self.viewForPopUpBG addSubview:self.viewForPopUp];
//        
//        UITableView *tblForRecommendations=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, deviceWidth-120, IS_IPAD ? 870 : deviceHeight-75-60-300) style:UITableViewStylePlain];
//        tblForRecommendations.tag=200;
//        tblForRecommendations.delegate=self;
//        tblForRecommendations.dataSource=self;
//        
//        tblForRecommendations.rowHeight = IS_IPAD ? 60 : 44;
//        
//        tblForRecommendations.backgroundColor=[UIColor clearColor];
//        tblForRecommendations.backgroundView=nil;
//        //tblForRecommendations.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
//        
//        tblForRecommendations.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
//        
//        [self.viewForPopUp addSubview:tblForRecommendations];
//        
//        
//        UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
//        btnCross.frame = CGRectMake(52, 140, 24, 24);
//        [btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
//        [btnCross addTarget:self action:@selector(btnCross_Clicked) forControlEvents:UIControlEventTouchUpInside];
//        [self.viewForPopUpBG addSubview:btnCross];
//        btnCross = nil;
//    }
//}
//-(void)btnCross_Clicked
//{
//    [self showCustomViewForPopUpForArray:nil toShow:NO];
//}



@end