//
//  RecommendationsList.m
//  NellPat
//
//  Created by Macbook on 6/12/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import "RecommendationsList.h"


@implementation RecommendationsList

@dynamic recommendation_ID;
@dynamic recommendation_Text;
@dynamic recommendation_Status;

@end
