//
//  Message.h
//  NellPad
//
//  Created by Atul Thakur on 3/26/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreGraphics;


@interface Message : NSObject
{
    
}
@property(strong, nonatomic) NSString *strRelation_Id;
@property(strong, nonatomic) NSString *strDateTime;
@property(strong, nonatomic) NSString *strMessage_Id;
@property(strong, nonatomic) NSString *strRead_Status;
@property(strong, nonatomic) NSString *strSenderId;
@property(strong, nonatomic) NSString *strSubject;
@property(strong, nonatomic) NSString *strMessage;
@property(nonatomic,strong)  NSString *strConversation_Id;
@property(strong, nonatomic) NSString *you;
@property(nonatomic,strong) NSString *strSenderName;
@property(nonatomic, readonly) CGFloat height;
@property(nonatomic, readonly) CGFloat width;

@end
