//
//  Message.m
//  NellPad
//
//  Created by Atul Thakur on 3/26/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "Message.h"
#import "CommonMethods.h"


@implementation Message
@synthesize height = _height;
@synthesize width = _width;
@synthesize strDateTime,strMessage,strMessage_Id,strRead_Status,strRelation_Id,strSenderId,strSubject,strConversation_Id,strSenderName,you;
-(CGFloat)height
{
    if (!_height)
        _height = [self calculateHeight];
        return _height;
}

-(CGFloat)calculateHeight
{
    CGFloat height;
    CGSize maximumLabelSize = CGSizeMake(180,9999);
    CGSize expectedLabelSize = [CommonMethods   calculateSizeForMaxSize:maximumLabelSize forText:self.strMessage WithFont:[UIFont systemFontOfSize:14.0]];
    height = ceilf(expectedLabelSize.height);
    return height;
}

-(CGFloat)calculateWidth
{
    CGFloat width;
    CGSize maximumLabelSize = CGSizeMake(180,9999);
    CGSize expectedLabelSize = [CommonMethods   calculateSizeForMaxSize:maximumLabelSize forText:self.self.strMessage WithFont:[UIFont systemFontOfSize:14.0]];
    width = ceilf(expectedLabelSize.width);
    return width + 35;
}

-(CGFloat)width
{
    if (!_width)
        _width = [self calculateWidth];
    return _width;
}

@end
