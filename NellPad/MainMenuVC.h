//
//  MainMenuVC.h
//  NellPad
//
//  Created by Herry Makker on 12/5/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SettingsVC;
@interface MainMenuVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate>
{
    
 NSMutableIndexSet *expandedSections;
   
    UILabel *theLabel;
    SettingsVC *settingsVC;
    
}

@property (retain, nonatomic) UITableView *menuTableView;
@property (retain, nonatomic) UIView *menuView;
@property (retain,nonatomic)  UIView *leftView;
@property (retain, nonatomic) UIView *contentView;
@property (nonatomic,retain)  UIWebView *webView;
@property (nonatomic, weak) NSTimer *myTimer;


//-(IBAction)toggleMenu;
@property (retain, nonatomic) NSArray *arrayTable;


@end
