
/*
 ********************CashOutGiftCardApp Server URLs********************
 */

#define kBaseUrl @"http://103.21.59.150/thoughtapp/api/api.php"

#define helveticaBold @"Helvetica-Bold"
#define helveticaThin @"HelveticaNeue-Thin"
#define helveticaLight @"HelveticaNeue-Light"
#define helveticaRegular @"Helvetica"


#define kAppName @"Mind Your Focus"
#define kDeviceHeight [[UIScreen mainScreen] bounds].size.height

#define kHeaderHeight 64

#define kRed 36.0/255.0
#define kGreen 41.0/255.0
#define kBlue 48.0/255.0
#define API   [ApiMaster singleton]
#define JSONObjectFromData(d) [NSJSONSerialization JSONObjectWithData:data options:0 error:nil]
#define NSStringFromNSData(d) [[NSString alloc] initWithBytes:[d bytes] length:[d length] encoding:NSUTF8StringEncoding]


#define FacebookKey @"865469603499728"

/*------------------------NSUserdefault---------------------------------*/

#define kDeviceToken @"Device Token"
#define kLoginData @"LoginData"
#define kEmotionId @"EmotionId"

#define AppDelegateClass ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define kInformationArrayData @"InformationArrayData"

/*
 **************************************************************************************************************************
 **************************************************************************************************************************
 **************************************************************************************************************************
 
 Alerts
 **************************************************************************************************************************
 **************************************************************************************************************************
 **************************************************************************************************************************
 
 */

#define kAlertInternetConnection @"Please connect to internet."
#define kAlertServerConnection @"Unable to connect to server. Please try again later."
#define kAlerPhoneSettings @"No Facebook Account. There are no Facebook accounts configured. You can add or create a Facebook account in the main Settings section of your phone device."
#define kAlerTwitterPhoneSettings @"No Twitter Account. There are no Twitter accounts configured. You can add or create a Twitter account in the main Settings section of your phone device."
#define kAlerTwitterPhonePermisionError @"No Permission granted. Please provide permissions access your twitter account."

//No Permission granted", @"Please provide permissions access your twitter account.
