//
//  CustomNavigationController.h
//  NellPat
//
//  Created by Sumit Jain on 6/6/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController

@end
