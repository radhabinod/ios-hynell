//
//  PatrawinCasesVC.h
//  NellPad
//
//  Created by Herry Makker on 12/9/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CasesVC;
@class PatrawinCase;
@interface PatrawinCasesVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,UIActionSheetDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
{
    NSMutableDictionary *responseDict;
}
@property(strong, nonatomic) UITableView *caseTableView;
@property(strong, nonatomic) NSMutableArray *casesArray;
@property(nonatomic,strong) NSMutableArray *arrRecommendations;
@property(nonatomic,strong) UIActionSheet *actionSheetSort;
@property(nonatomic,strong) UITextField *textSortingCompany;
@property(nonatomic,strong) UIView *viewForAgentsListBG, *viewForAgentsList;
@property(strong,nonatomic) UITableView *tbleUsers;


-(void)refreshTable;
-(void) DataReload;

-(void)fetchFromDatabase;
-(void)sortCasesDescending;

@end
