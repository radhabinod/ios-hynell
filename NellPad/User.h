//
//  User.h
//  NellPad
//
//  Created by Atul Thakur on 4/2/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property(nonatomic, strong)NSString *user_id;
@property(nonatomic, strong)NSString *user_name;
@property(nonatomic, strong)NSString *user_Email;
@property(nonatomic, strong)NSString *user_type;

@end
