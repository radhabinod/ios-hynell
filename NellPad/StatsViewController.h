//
//  StatsViewController.h
//  NellPad
//
//  Created by Rakesh Kumar on 05/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatsViewController : UIViewController<CPTBarPlotDataSource,CPTBarPlotDelegate>
@property(nonatomic,strong) NSMutableArray *arrData;
@property (nonatomic, strong) CPTGraphHostingView *hostView;
@property (nonatomic, strong) CPTBarPlot *barPlot;
@property (strong,nonatomic) NSDictionary *responseDict;
@property(nonatomic,strong) UIScrollView *scrollView;


-(void)initPlot;
-(void)configurePlots;
-(void)configureAxeswithMaxYValue:(int)MaxmValue;
-(void)configureGraphwithMaxYValue:(int)MaxmValue;
@end
