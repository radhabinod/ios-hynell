
//
//  SingleRecentCaseVC.m
//  Hynell
//
//  Created by Ram Kumar on 18/10/16.
//  Copyright © 2016 Gagan Joshi. All rights reserved.
//

#import "SingleRecentCaseVC.h"

#import "ComposeSheetVC.h"
#import "PatrawinCase.h"
#import "TimeRegistrationVC.h"
#import "LinkViewController.h"
#import "RecommendationsListBO.h"
#import "UIImageView+AFNetworking.h"
#import "PatrawinCasesVC.h"
#import "PetrawinCasesList.h"
#import "DataSyncBO.h"
#import "StasDetailList.h"
#import "StatsLisingController.h"
#import <objc/message.h>

#import "UpdatedCasesVC.h"

@interface SingleRecentCaseVC ()
{
    UITextView *txtViewComment;
    UIImageView *imageFigure;
    UIButton *btn_Image;
    
    //  Using In Action Sheet.
    int buttonValue;
    int buttonSelected;
    UIButton * btnLeft;
    UIButton * btnRight;
    UITableView *tblForRecommendations;
    NSString *statusData;
    PatrawinCasesVC *PatrawinCasesObject;
    NSMutableArray *singleCaseDetail;
    
    UIActionSheet *actionsheet;
    BOOL select;

}

@end

@implementation SingleRecentCaseVC

@synthesize arrDataHdng,strSelectedIndex,arrayAllCases,lbl_CaseName,viewForCaseData,scrollView,ViewForRecommendations,responseDict,arrRecommendationListBO,ViewForRecommendationsBG,strGlobalRecommendationId,recommendation_TexPC,casesScreentype,singleCaseId;

NSString *TXTFileName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"Cases";
    
    select=NO;
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]){
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
        }
        
    }
    
    PatrawinCasesObject = [[PatrawinCasesVC alloc]init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont systemFontOfSize: 16], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    
    
    self.strGlobalRecommendationId=[[NSString alloc]init];

    
    [self configureView];
    [self designToolBar];
    
    UITapGestureRecognizer *gst=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hidekeyboard)];
    gst.delegate=self;
    gst.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:gst];
    
    self.view.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    // self.scrollView.backgroundColor=[UIColor orangeColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

    
}

- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
     NSLog(@"view is disappear****");

    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshUpdateCasesList" object:self userInfo:nil];

}






-(void)viewWillAppear:(BOOL)animated
{
    
    [self refreshCaseWhenViewAppear];
    
    UIBarButtonItem *btnRefresh=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"refresh-top"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshCase)];
    
    
    UIBarButtonItem *btnShare=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"shareButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(shareCase)];
    
    // self.navigationItem.rightBarButtonItem=@[btnRefresh,btnShare];
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnShare, btnRefresh, nil]];
    
    
    [super viewWillAppear:animated];
    
    
    buttonSelected = 0;
    [self showCustomViewForCase:YES];
    btnLeft.enabled=[self enableDisableLeftButtons];
    
    btnRight.enabled=[self enableDisableRightButtons];
    

}



- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
   

}


-(void)refreshCaseWhenViewAppear{
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ([strKey isEqualToString:@"1"])
    {
        
        [self refreshCustomerCase];
        [self updateCaseStatus];
    }
    else if ([strKey isEqualToString:@"0"])
    {
        
        [self refreshAgentCase];
        
        [self updateCaseStatus];
        
    }
    
    
}


-(void)refreshCase{
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ([strKey isEqualToString:@"1"])
    {
        
        [self refreshCustomerCase];
    }
    else if ([strKey isEqualToString:@"0"])
    {
        
        [self refreshAgentCase];
        
    }

}





-(void)updateCaseStatus
{
    //NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"setStatusForlatestupdatedcases";
    
    
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    
    [inputParams.dict_postParameters setObject:singleCaseId forKey:@"case_id"];

    
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         }
         
         else
         {
             error = nil;
             
             NSDictionary *responseDicts = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
             NSDictionary *data = responseDicts[@"data"];
             
             NSString *msg = data[@"log"];
             
             NSLog(@"message is %@",msg);
//                                           [[[UIAlertView alloc]initWithTitle:@"Hynell" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
 
         }
     }];
    
}




-(void)shareCase
{
    PatrawinCase *patCase = [self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    
    
    RecommendationsListBO *ObjRecommmendationBO=patCase.objRecommendationListBO;
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    // NSString *strDueDateNote_comment;
    
    
    NSString *dataCases;
    NSString *sts;
    
    if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"1"]) {
        sts = @"Approved";
    }
    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"0"])
    {
        sts = @"Waiting for approval";
        
    }
    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"2"])
    {
        sts = @"Rejected";
        
    }
    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"3"])
    {
        sts = @"Waiting";
        
    }
    
    
    if ([strKey isEqualToString:@"1"])
    {
        dataCases=[NSString stringWithFormat:@"Case Id: %@  \n Title: %@ \n Country: %@ \n Agent: %@ \n Customer: %@  \n Case Type:%@ \n Keyword:%@  \n Status:%@ \n Acc Cost: %@ \n Comment: %@  \n Deadline: %@  \n Deadline Code: %@ \n Recommendation Text: %@  \n Recommendation Status: %@",patCase.case_id,patCase.title,patCase.country_code,patCase.agent_id,patCase.customer_name,patCase.case_type,patCase.keyword,patCase.status,patCase.account_cost,patCase.comment_case,patCase.deadline,patCase.deadline_code,patCase.Recommendation_Text,sts];
    }
    else if ([strKey isEqualToString:@"0"])
    {
        dataCases=[NSString stringWithFormat:@"Case Id: %@  \n Title: %@ \n Country: %@ \n Agent: %@ \n Customer: %@ \n Due Date Note: %@  \n Case Type:%@ \n Keyword:%@  \n Status:%@ \n Acc Cost: %@ \n Comment: %@  \n Deadline: %@  \n Deadline Code: %@ \n Recommendation Text: %@  \n Recommendation Status: %@",patCase.case_id,patCase.title,patCase.country_code,patCase.agent_id,patCase.customer_name,patCase.comment,patCase.case_type,patCase.keyword,patCase.status,patCase.account_cost,patCase.comment_case,patCase.deadline,patCase.deadline_code,patCase.Recommendation_Text,sts];
    }
    
    
    
    
    
    NSArray *objectsToShare = @[dataCases];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,UIActivityTypeMessage,UIActivityTypePostToTwitter,UIActivityTypePostToFacebook];
    
    controller.excludedActivityTypes = excludeActivities;
    
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
}


-(void)hidekeyboard
{
    UITextView *txtf=(UITextView *)[self.scrollView viewWithTag:kTxtVComment];
    [txtf resignFirstResponder];
    // [self.scrollView setContentOffset:CGPointZero animated:YES];
}

-(void)configureView
{
    UIImageView *imgV=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, deviceWidth, 55)];
    //imgV.image=[UIImage imageNamed:@"top-grey-bg.png"];
    imgV.backgroundColor=[UIColor whiteColor];
    imgV.userInteractionEnabled=YES;
    [self.view addSubview:imgV];
    
    btnLeft=[[UIButton alloc]initWithFrame:CGRectMake(10, 15, 18, 30)];
    btnLeft.tag=kBtnLeft;
    [btnLeft setBackgroundImage:[UIImage imageNamed:@"ArrowPrevback.png"] forState:UIControlStateNormal];
    [btnLeft setBackgroundColor:[UIColor clearColor]];
    [btnLeft addTarget:self action:@selector(btnLeftClk) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnLeft];
    
    lbl_CaseName = [[UILabel alloc] initWithFrame:CGRectMake(50, 15, deviceWidth-100, 30)];
    lbl_CaseName.backgroundColor = [UIColor clearColor];
    lbl_CaseName.font = IS_IPAD ? FONT_NORMAL(17) : FONT_NORMAL(14.0);
    lbl_CaseName.textAlignment=NSTextAlignmentCenter;
    lbl_CaseName.adjustsFontSizeToFitWidth=YES;
    lbl_CaseName.textColor=UIColorFromRGB(0x4174DA);
    [self.view addSubview:lbl_CaseName];
    
    btnRight=[[UIButton alloc]initWithFrame:CGRectMake(deviceWidth-28, 15, 18, 30)];
    [btnRight setBackgroundImage:[UIImage imageNamed:@"arrowMessageinbox.png"] forState:UIControlStateNormal];
    [btnRight setBackgroundColor:[UIColor clearColor]];
    btnRight.tag=kBtnRight;
    [btnRight addTarget:self action:@selector(btnRightclk) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnRight];
    
    btnLeft.enabled=[self enableDisableLeftButtons];
    
    btnRight.enabled=[self enableDisableRightButtons];
}

-(void)UpdateValuesForCases
{
    [arrDataHdng removeAllObjects];
    
    arrDataHdng=nil;
    
    PatrawinCase *objTemp = [self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    
     NSString *strTemp=[objTemp.case_id substringWithRange:NSMakeRange(1, 1)];
    
    NSString *strKey = [USER_DEFAULTS valueForKey:@"Customer"];
    
    if ( [strKey isEqualToString:@"0"])
    {

        arrDataHdng=[[NSMutableArray alloc]initWithObjects:@"Title",@"Country",@"Agent",@"Customer",@"Due Date Note",@"Case Type",@"Case Image",@"Keyword",@"Status", @"Acc. Cost",@"Comment", @"Deadline",@"Deadline Code",@"Recommendation",@"Recomm. Status", nil];
        
        // }
        
    }
    else if ([strKey isEqualToString:@"1"])
    {

        arrDataHdng=[[NSMutableArray alloc]initWithObjects:@"Title",@"Country",@"Agent",@"Customer",@"Case Type",@"Case Image",@"Keyword",@"Status", @"Acc. Cost",@"Comment", @"Deadline",@"Deadline Code",@"Recommendation",@"Recomm. Status", nil];
        // }
    }
}











#pragma mark-

#pragma mark showCustomView

-(void)showCustomViewForCase:(BOOL)isShow
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    [self UpdateValuesForCases];
    
    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
    
    if(self.viewForCaseData)
    {
        [self.viewForCaseData.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.viewForCaseData removeFromSuperview];
        self.viewForCaseData=nil;
    }
    
    //  self.view.backgroundColor=[UIColor yellowColor];
    
    if ( [strKey isEqualToString:@"0"])
    {
        if (isShow)
        {
            
            self.viewForCaseData=[[UIView alloc]initWithFrame:CGRectMake(0, 60, deviceWidth, deviceHeight-177)];
            self.viewForCaseData.backgroundColor=[UIColor clearColor];
            [self.view addSubview:self.viewForCaseData];
            
            self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,deviceWidth,deviceHeight-185)];
            self.scrollView.showsVerticalScrollIndicator=NO;
            self.scrollView.delegate=self;
            self.scrollView.scrollEnabled=YES;
            [self.scrollView setBounces:NO];
            self.scrollView.backgroundColor = [UIColor colorWithRed:0.898 green:0.898 blue:0.898 alpha:1];
            //old bg color =  [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:0.8]
            self.scrollView.userInteractionEnabled=YES;
            [self.viewForCaseData addSubview: self.scrollView];
            
            int Xpos=0;
            int Ypos=0;
            Ypos=20;
            
            for (int i=0; i<arrDataHdng.count; i++)
            {
                Xpos=1;
                
                UILabel *lbl_Line = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, deviceWidth-8, 1)];
                
                UILabel *lbl_Line1 = [[UILabel alloc] initWithFrame:CGRectMake(14, Ypos, self.view.frame.size.width-28,IS_IPAD?101: 45)];
                
                lbl_Line1.backgroundColor = [UIColor whiteColor];
                lbl_Line1.textColor=[UIColor lightGrayColor];
                //lbl_Line1.alpha=0.8;
                [self.scrollView addSubview:lbl_Line1];
                
                
                Ypos+=lbl_Line.frame.size.height;
                Xpos+=1;
                
                UILabel *lbl_HD= [[UILabel alloc] initWithFrame:CGRectMake(Xpos+16, Ypos-3, IS_IPAD?200: 160,IS_IPAD?100: 50)];
                lbl_HD.numberOfLines=0;
                
                lbl_HD.backgroundColor = [UIColor clearColor];
                lbl_HD.text=[NSString stringWithFormat:@"%@:",[arrDataHdng objectAtIndex:i]];
                lbl_HD.font = IS_IPAD ? FONT_BOLD(20) : FONT_BOLD(13.0);
                lbl_HD.textAlignment=NSTextAlignmentLeft;
                //    lbl_HD.textColor=UIColorFromRGB(0x4174DA);
                lbl_HD.adjustsFontSizeToFitWidth=YES;
                [self.scrollView addSubview:lbl_HD];
                
                Xpos=IS_IPAD?303:79;
                
                UILabel *lbl_Line2 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?100: 50)];
                lbl_Line2.backgroundColor = [UIColor whiteColor];
                lbl_Line2.textColor=[UIColor grayColor];
                // [self.scrollView addSubview:lbl_Line2];
                
                Xpos+=1;
                
                UILabel *lbl_Values = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55, Ypos, IS_IPAD?449: 170,IS_IPAD?100: 43)];
                lbl_Values.backgroundColor = [UIColor clearColor];
                lbl_Values.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                lbl_Values.textAlignment=NSTextAlignmentLeft;
                lbl_Values.numberOfLines=0;
                lbl_Values.tag=KlblCountry+i;
                lbl_Values.textColor=[UIColor blackColor];
                [self.scrollView addSubview:lbl_Values];
                
                if (i==6)
                {
                    if ([objTemp.figure isEqualToString:@""]) {
                        btn_Image=nil;
                        btn_Image.hidden= YES;
                    }else
                        
                        btn_Image=[[UIButton alloc]initWithFrame:CGRectMake(Xpos+55, Ypos, IS_IPAD?459: 170,IS_IPAD?100:43)];
                    [btn_Image setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    btn_Image.backgroundColor=[UIColor clearColor];
                    btn_Image.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    btn_Image.titleLabel.font=IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                    [btn_Image setTitle:@"View Image" forState:UIControlStateNormal];
                    [btn_Image setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                    btn_Image.tag=kBtnImage;
                    [btn_Image addTarget:self action:@selector(btnImageClick) forControlEvents:UIControlEventTouchUpInside];
                    [self.scrollView addSubview:btn_Image];
                    

                }
                
                
                if (i==8)
                {
                    UIButton *btn_figure=[[UIButton alloc]initWithFrame:CGRectMake(Xpos+55, Ypos,IS_IPAD?459: 170,IS_IPAD?100:50)];
                    [btn_figure setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    btn_figure.backgroundColor=[UIColor clearColor];
                    btn_figure.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    btn_figure.titleLabel.font=IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                    btn_figure.tag=kBtnFigure;
                    [btn_figure addTarget:self action:@selector(btnFigureClk) forControlEvents:UIControlEventTouchUpInside];
                    [self.scrollView addSubview:btn_figure];
                    
                    imageFigure = [[UIImageView alloc] initWithFrame:CGRectMake(175, 180, 50, 30)];
                    
                    [self.scrollView addSubview:imageFigure];
                }
                
                if (i==10)
                {
                    txtViewComment=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+55, Ypos,IS_IPAD?459:170,IS_IPAD?100:42)];
                    txtViewComment.delegate=self;
                    txtViewComment.tag=kTxtVComment;
                    txtViewComment.returnKeyType=UIReturnKeyDefault;
                    txtViewComment.keyboardType=UIKeyboardTypeDefault;
                    txtViewComment.backgroundColor=[UIColor colorWithRed:0.902 green:0.902 blue:0.98 alpha:1];
                    txtViewComment.textColor=[UIColor blackColor];
                    txtViewComment.editable=YES;
                    txtViewComment.userInteractionEnabled=YES;
                    [txtViewComment setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                    [self.scrollView addSubview: txtViewComment];
                }
                
                // else if (i==arrDataHdng.count-1)
                
                else  if (i==13)
                {
                    
                    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
                    
                    UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+46,IS_IPAD?Ypos+15: Ypos+5.0,IS_IPAD?400:152,IS_IPAD?70: 40)];
                    txtViewRecommendation.delegate=self;
                    txtViewRecommendation.tag=kTxtVRecommendation;
                    txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                    txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                    txtViewRecommendation.backgroundColor=[UIColor clearColor];
                    txtViewRecommendation.textColor=[UIColor blackColor];
                    txtViewRecommendation.editable=NO;
                    [txtViewRecommendation setTextAlignment:NSTextAlignmentLeft];
                    txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText; // this is applicable for both patrav cases & stats cases
                    
                    if ([txtViewRecommendation.text isEqualToString:@""])
                    {
                        
                        txtViewRecommendation.text=objTemp.Recommendation_Text;
                    }
                    
                    
                    [txtViewRecommendation setTextContainerInset:UIEdgeInsetsMake(4, 4, 0, 2)];
                    //                    txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                    //                    txtViewRecommendation.layer.cornerRadius=2.0;
                    //                    txtViewRecommendation.layer.borderWidth=1.0;
                    txtViewRecommendation.userInteractionEnabled=YES;
                    [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                    [self.scrollView addSubview: txtViewRecommendation];
                    
                    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
                    btn.frame=CGRectMake(IS_IPAD?Xpos+420: Xpos+197,IS_IPAD?Ypos+35.5:Ypos+10.5, 25, 25);
                    [btn setImage:[UIImage imageNamed:@"recomended"] forState:UIControlStateNormal];
                    [btn setBackgroundColor:[UIColor clearColor]];
                    btn.tag=kBtnSelectRecommendation;
                    [btn addTarget:self action:@selector(btnSelectRecommendationClk) forControlEvents:UIControlEventTouchUpInside];
                    [self.scrollView addSubview:btn];
                    
                }
      
                
                else if (i==arrDataHdng.count-1)
                {
                    
                    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
                    
                    
                    if ( [ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"2"])
                    {
                        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                        statusLabel.backgroundColor = [UIColor clearColor];
                        statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                        statusLabel.textAlignment=NSTextAlignmentLeft;
                        statusLabel.numberOfLines=0;
                        statusLabel.text =@"Rejected";
                        statusLabel.tag=kStsVRecommendation;
                        statusLabel.textColor=[UIColor redColor];
                        [self.scrollView addSubview:statusLabel];
                        
                        
                    }
                    
                    if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"1"] )
                    {
                        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                        statusLabel.backgroundColor = [UIColor clearColor];
                        statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                        statusLabel.textAlignment=NSTextAlignmentLeft;
                        statusLabel.numberOfLines=0;
                        statusLabel.text =@"Approved";
                        statusLabel.tag=kStsVRecommendation;
                        statusLabel.textColor=[UIColor colorWithRed:0 green:1 blue:0 alpha:1];
                        [self.scrollView addSubview:statusLabel];
                        
                    }
                    
                    
                    if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"3"])
                    {
                        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                        statusLabel.backgroundColor = [UIColor clearColor];
                        statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                        statusLabel.textAlignment=NSTextAlignmentLeft;
                        statusLabel.numberOfLines=0;
                        statusLabel.text =@"Waiting";
                        statusLabel.tag=kStsVRecommendation;
                        statusLabel.textColor=[UIColor colorWithRed:1 green:0.749 blue:0 alpha:1];
                        [self.scrollView addSubview:statusLabel];
                        
                    }
                    
                    
                    if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"0"] || [ObjRecommmendationBO.strRecommendationText isEqualToString:@""])
                    {
                        
                        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                        statusLabel.backgroundColor = [UIColor clearColor];
                        statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                        statusLabel.textAlignment=NSTextAlignmentLeft;
                        statusLabel.numberOfLines=0;
                        // statusLabel.text =@"Waiting for approval";
                        statusLabel.text =nil;
                        statusLabel.tag=kStsVRecommendation;
                        statusLabel.textColor=[UIColor colorWithRed:0.333 green:0.706 blue:0.863 alpha:1];
                        [self.scrollView addSubview:statusLabel];
                        
                        if (![ObjRecommmendationBO.strRecommendationText isEqualToString:@""])
                            
                        {
                            UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                            statusLabel.backgroundColor = [UIColor clearColor];
                            statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                            statusLabel.textAlignment=NSTextAlignmentLeft;
                            statusLabel.numberOfLines=0;
                            statusLabel.text =@"Waiting for approval";
                            statusLabel.tag=kStsVRecommendation;
                            statusLabel.textColor=[UIColor colorWithRed:0 green:1 blue:0 alpha:1];
                            [self.scrollView addSubview:statusLabel];
                            
                        }
                        
                        
                    }
                    
                }
                

                
                Xpos+=lbl_Values.frame.size.width+10;
                Ypos-=1;
                
                UILabel *lbl_Line3 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?101: 51)];
                lbl_Line3.backgroundColor = [UIColor lightGrayColor];
                lbl_Line3.textColor=[UIColor redColor];
                //  [self.scrollView addSubview:lbl_Line3];
                
                Ypos+=IS_IPAD?100:50;
                
            }
            
            

            
            
            UILabel *lbl_LastLine = [[UILabel alloc] initWithFrame:CGRectMake(4, Ypos, deviceWidth-8, 1)];
            lbl_LastLine.backgroundColor = [UIColor lightGrayColor];
            lbl_LastLine.textColor=[UIColor grayColor];
            //  [self.scrollView addSubview:lbl_LastLine];
            
            [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+30)];
            
            [self getValuesForLabelsForSelectedCase:objTemp];
            //     [self GetRecommendationStatus:objTemp];
            
            
            
        }
        
    }
    
    else if ( [strKey isEqualToString:@"1"])
    {
        if (isShow)
        {
            self.viewForCaseData=[[UIView alloc]initWithFrame:CGRectMake(0, 50,  deviceWidth, deviceHeight-177)];
            self.viewForCaseData.backgroundColor=[UIColor clearColor];
            [self.view addSubview:self.viewForCaseData];
            
            self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0, deviceWidth,deviceHeight-177)];
            self.scrollView.showsVerticalScrollIndicator=NO;
            self.scrollView.scrollEnabled=YES;
            self.scrollView.backgroundColor =[UIColor colorWithRed:0.898 green:0.898 blue:0.898 alpha:1];
            self.scrollView.delegate=self;
            self.scrollView.userInteractionEnabled=YES;
            [self.viewForCaseData addSubview: self.scrollView];
            
            int Xpos=0;
            int Ypos=0;
            Ypos=20;
            
            for (int i=0; i<arrDataHdng.count; i++)
            {
                Xpos=17;
                UILabel *lbl_Line = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, deviceWidth-8, 1)];
                lbl_Line.backgroundColor = [UIColor lightGrayColor];
                lbl_Line.textColor=[UIColor grayColor];
                //   [self.scrollView addSubview:lbl_Line];
                
                
                UILabel *lbl_Line1 = [[UILabel alloc] initWithFrame:CGRectMake(14, Ypos, self.view.frame.size.width-28,IS_IPAD?101: 45)];
                lbl_Line1.backgroundColor = [UIColor whiteColor];
                lbl_Line1.textColor=[UIColor lightGrayColor];
                //lbl_Line1.alpha=0.8;
                [self.scrollView addSubview:lbl_Line1];
                
                Ypos+=lbl_Line.frame.size.height;
                Xpos+=1;
                
                UILabel *lbl_HD= [[UILabel alloc] initWithFrame:CGRectMake(Xpos+2, Ypos-3, IS_IPAD?298: 160,IS_IPAD?100: 50)];
                lbl_HD.numberOfLines=0;
                
                lbl_HD.backgroundColor = [UIColor clearColor];
                lbl_HD.text=[NSString stringWithFormat:@"%@:",[arrDataHdng objectAtIndex:i]];
                lbl_HD.font = IS_IPAD ? FONT_BOLD(20) : FONT_BOLD(13.0);
                lbl_HD.textAlignment=NSTextAlignmentLeft;
                //  lbl_HD.textColor=UIColorFromRGB(0x4174DA);
                lbl_HD.adjustsFontSizeToFitWidth=YES;
                [self.scrollView addSubview:lbl_HD];
                
                Xpos=IS_IPAD?303: 79;
                UILabel *lbl_Line2 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1, IS_IPAD?100: 50)];
                lbl_Line2.backgroundColor = [UIColor lightGrayColor];
                lbl_Line2.textColor=[UIColor blackColor];
                ////    [self.scrollView addSubview:lbl_Line2];
                
                Xpos+=1;
                
                UILabel *lbl_Values = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55, Ypos, IS_IPAD?449: 170,IS_IPAD?100: 43)];
                lbl_Values.backgroundColor = [UIColor clearColor];
                lbl_Values.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                lbl_Values.textAlignment=NSTextAlignmentLeft;
                lbl_Values.numberOfLines=0;
                lbl_Values.tag=KlblTitle+i;
                lbl_Values.textColor=[UIColor blackColor];
                [self.scrollView addSubview:lbl_Values];
                
                if (i==5)
                {
                    
                    if ([objTemp.figure isEqualToString:@""]) {
                        btn_Image=nil;
                        btn_Image.hidden= YES;
                    }
                    else
                        btn_Image=[[UIButton alloc]initWithFrame:CGRectMake(Xpos+55, Ypos, IS_IPAD?459: 170,IS_IPAD?100:43)];
                    btn_Image.backgroundColor=[UIColor clearColor];
                    btn_Image.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    btn_Image.titleLabel.font=IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                    [btn_Image setTitle:@"View Image" forState:UIControlStateNormal];
                    [btn_Image setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                    btn_Image.tag=kBtnImage;
                    [btn_Image addTarget:self action:@selector(btnImageClick) forControlEvents:UIControlEventTouchUpInside];
                    [self.scrollView addSubview:btn_Image];
                    
                    //                    if ([objTemp.figure isEqualToString:@""]) {
                    //                        btn_Image=nil;
                    //                        [btn_Image setTitle:@"No Image" forState:UIControlStateNormal];
                    //                        [btn_Image setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    //
                    //
                    //                    }
                    
                }
                
                if (i==7)
                {
                    UIButton *btn_figure=[[UIButton alloc]initWithFrame:CGRectMake(Xpos+55, Ypos, IS_IPAD?459: 170,IS_IPAD?100:50)];
                    [btn_figure setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    btn_figure.backgroundColor=[UIColor clearColor];
                    btn_figure.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    btn_figure.titleLabel.font=IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                    btn_figure.tag=kBtnFigure;
                    [btn_figure addTarget:self action:@selector(btnFigureClk) forControlEvents:UIControlEventTouchUpInside];
                    [self.scrollView addSubview:btn_figure];
                }
                
                if (i==9)
                {
                    txtViewComment=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+55, IS_IPAD?Ypos+10:Ypos, IS_IPAD?439:170,IS_IPAD?100:43)];
                    txtViewComment.delegate=self;
                    txtViewComment.tag=kTxtVComment;
                    txtViewComment.returnKeyType=UIReturnKeyDefault;
                    txtViewComment.keyboardType=UIKeyboardTypeDefault;
                    txtViewComment.backgroundColor = [UIColor clearColor];
                    // txtViewComment.backgroundColor=[UIColor colorWithRed:0.902 green:0.902 blue:0.98 alpha:1];
                    txtViewComment.textColor=[UIColor blackColor];
                    txtViewComment.textAlignment=NSTextAlignmentLeft;
                    txtViewComment.editable=NO;
                    // txtViewComment.layer.borderColor=[UIColor grayColor].CGColor;
                    //txtViewComment.layer.cornerRadius=2.0;
                    // txtViewComment.layer.borderWidth=1.0;
                    //  [txtViewComment setBackgroundColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f]];
                    txtViewComment.userInteractionEnabled=YES;
                    [txtViewComment setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                    [self.scrollView addSubview: txtViewComment];
                }
                
                //  if (i==arrDataHdng.count-1)
                if (i==12)
                {
                    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
                    
                    if ([ObjRecommmendationBO.strRecommendationText isEqualToString:@""] || [ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"2"])
                    {
                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+51, IS_IPAD?Ypos+15: Ypos, IS_IPAD?399:148,IS_IPAD?100: 43)];
                        txtViewRecommendation.delegate=self;
                        txtViewRecommendation.tag=kTxtVRecommendation;
                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
                        txtViewRecommendation.textColor=[UIColor blackColor];
                        [txtViewRecommendation setTextAlignment:NSTextAlignmentLeft];
                        txtViewRecommendation.editable=NO;
                        txtViewRecommendation.userInteractionEnabled=YES;
                        txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                        //                        txtViewRecommendation.layer.cornerRadius=2.0;
                        //                        txtViewRecommendation.layer.borderWidth=1.0;
                        [txtViewComment setBackgroundColor:[UIColor clearColor]];
                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
                        [self.scrollView addSubview: txtViewRecommendation];
                        
                    }
                    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"1"])
                    {
                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+51, IS_IPAD?Ypos+15: Ypos, IS_IPAD?399:148,IS_IPAD?100: 43)];
                        txtViewRecommendation.delegate=self;
                        txtViewRecommendation.tag=kTxtVRecommendation;
                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
                        txtViewRecommendation.textColor=[UIColor blackColor];
                        [txtViewRecommendation setTextAlignment:NSTextAlignmentLeft];
                        txtViewRecommendation.editable=NO;
                        // [txtViewRecommendation setTextContainerInset:UIEdgeInsetsMake(15, 0, 0, 0)];
                        txtViewRecommendation.userInteractionEnabled=YES;//(4, 10, 0, 2)];
                        txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                        
                        //                        txtViewRecommendation.layer.cornerRadius=2.0;
                        //                        txtViewRecommendation.layer.borderWidth=1.0;
                        [txtViewComment setBackgroundColor:[UIColor clearColor]];
                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
                        [self.scrollView addSubview: txtViewRecommendation];
                        
                        
                    }
                    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"3"])
                    {
                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+51, IS_IPAD?Ypos+15: Ypos, IS_IPAD?399:148,IS_IPAD?100: 43)];
                        txtViewRecommendation.delegate=self;
                        txtViewRecommendation.tag=kTxtVRecommendation;
                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
                        txtViewRecommendation.textColor=[UIColor blackColor];
                        txtViewRecommendation.editable=NO;
                        [txtViewRecommendation setTextAlignment:NSTextAlignmentLeft];
                        txtViewRecommendation.userInteractionEnabled=YES;
                        //txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                        //  txtViewRecommendation.layer.cornerRadius=2.0;
                        //  txtViewRecommendation.layer.borderWidth=1.0;
                        [txtViewComment setBackgroundColor:[UIColor clearColor]];
                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
                        [self.scrollView addSubview: txtViewRecommendation];
                        
                        
                        UIButton *btnApproved = [[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+384:Xpos+195,IS_IPAD? Ypos+42:Ypos+9, 25, 25)];
                        buttonValue = 0;
                        //[btnApproved setTitle:@"" forState:UIControlStateNormal];
                        btnApproved.tag = kbtnRecom;
                        [btnApproved setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [btnApproved setBackgroundImage:[UIImage imageNamed:@"recomended"] forState:UIControlStateNormal];
                        [btnApproved addTarget:self action:@selector(selectedActionSheet) forControlEvents:UIControlEventTouchUpInside];
                        [self.scrollView addSubview:btnApproved];
  
                        
                    }
                    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"0"])
                    {
                        
                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+51, IS_IPAD?Ypos+10:Ypos,IS_IPAD?374:148,IS_IPAD?100: 43)];
                        txtViewRecommendation.delegate=self;
                        txtViewRecommendation.textAlignment=NSTextAlignmentLeft;
                        txtViewRecommendation.tag=kTxtVRecommendation;
                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
                        txtViewRecommendation.textColor=[UIColor blackColor];
                        txtViewRecommendation.editable=NO;
                        //[txtViewRecommendation setTextContainerInset:UIEdgeInsetsMake(4, 10, 0, 2)];
                        txtViewRecommendation.userInteractionEnabled=YES;
                        //    txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                        //                        txtViewRecommendation.layer.cornerRadius=2.0;
                        //                        txtViewRecommendation.layer.borderWidth=1.0;
                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
                        [self.scrollView addSubview: txtViewRecommendation];
                        
                        
                        //Show Action sheet for button...
                        UIButton *btnApproved = [[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+384:Xpos+195,IS_IPAD? Ypos+42:Ypos+9, 25, 25)];
                        buttonValue = 1;
                        [btnApproved setBackgroundImage:[UIImage imageNamed:@"recomended"] forState:UIControlStateNormal];
                        btnApproved.tag = kbtnRecom;
                        [btnApproved setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [btnApproved addTarget:self action:@selector(selectedActionSheet) forControlEvents:UIControlEventTouchUpInside];
                        [self.scrollView addSubview:btnApproved];
                        
                                      }
                }
                
                
                
                else if (i==arrDataHdng.count-1)
                {
                    
                    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
                    
                    if ( [ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"2"])
                    {
                        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                        statusLabel.backgroundColor = [UIColor clearColor];
                        statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                        statusLabel.textAlignment=NSTextAlignmentLeft;
                        statusLabel.numberOfLines=0;
                        statusLabel.text =@"Rejected";
                        statusLabel.tag=kStsVRecommendation;
                        //statusLabel.textColor=[UIColor colorWithRed:1 green:0.4 blue:0.4 alpha:1];
                        statusLabel.textColor = [UIColor redColor];
                        [self.scrollView addSubview:statusLabel];
                        
                    }
                    
                    if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"1"])
                    {
                        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                        statusLabel.backgroundColor = [UIColor clearColor];
                        statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                        statusLabel.textAlignment=NSTextAlignmentLeft;
                        statusLabel.numberOfLines=0;
                        statusLabel.text =@"Approved";
                        statusLabel.tag=kStsVRecommendation;
                        statusLabel.textColor=[UIColor colorWithRed:0 green:1 blue:0 alpha:1];
                        [self.scrollView addSubview:statusLabel];
                        
                    }
                    
                    
                    if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"3"])
                    {
                        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                        statusLabel.backgroundColor = [UIColor clearColor];
                        statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                        statusLabel.textAlignment=NSTextAlignmentLeft;
                        statusLabel.numberOfLines=0;
                        statusLabel.text =@"Waiting";
                        statusLabel.tag=kStsVRecommendation;
                        statusLabel.textColor=[UIColor colorWithRed:1 green:0.749 blue:0 alpha:1];
                        [self.scrollView addSubview:statusLabel];
                        
                    }
                    if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"0"] || [ObjRecommmendationBO.strRecommendationText isEqualToString:@""])
                    {
                        
                        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                        statusLabel.backgroundColor = [UIColor clearColor];
                        statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                        statusLabel.textAlignment=NSTextAlignmentLeft;
                        statusLabel.numberOfLines=0;
                        // statusLabel.text =@"Waiting for approval";
                        statusLabel.text =nil;
                        statusLabel.tag=kStsVRecommendation;
                        statusLabel.textColor=[UIColor colorWithRed:0.333 green:0.706 blue:0.863 alpha:1];
                        [self.scrollView addSubview:statusLabel];
                        
                        if (![ObjRecommmendationBO.strRecommendationText isEqualToString:@""])
                            
                        {
                            UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+55,IS_IPAD?Ypos+15: Ypos+2.5,IS_IPAD?400:152,IS_IPAD?70: 40)];
                            statusLabel.backgroundColor = [UIColor clearColor];
                            statusLabel.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                            statusLabel.textAlignment=NSTextAlignmentLeft;
                            statusLabel.numberOfLines=0;
                            statusLabel.text =@"Waiting for approval";
                            // statusLabel.text =nil;
                            statusLabel.tag=kStsVRecommendation;
                            statusLabel.textColor=[UIColor colorWithRed:0 green:1 blue:0 alpha:1];
                            [self.scrollView addSubview:statusLabel];
                            
                        }
                    }
                    
                }
                
                
                
                
                
                
                Xpos+=lbl_Values.frame.size.width+10;
                Ypos-=1;
                
                UILabel *lbl_Line3 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?101: 51)];
                lbl_Line3.backgroundColor = [UIColor lightGrayColor];
                lbl_Line3.textColor=[UIColor grayColor];
                //    [self.scrollView addSubview:lbl_Line3];
                
                Ypos+=IS_IPAD?100: 50;
                
            }
            
            UILabel *lbl_LastLine = [[UILabel alloc] initWithFrame:CGRectMake(4, Ypos, deviceWidth-8, 1)];
            lbl_LastLine.backgroundColor = [UIColor lightGrayColor];
            lbl_LastLine.textColor=[UIColor grayColor];
            //  [self.scrollView addSubview:lbl_LastLine];
            
            [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+30)];
            
            [self getValuesForLabelsForSelectedCase:objTemp];
            
            
        }
    }
}


-(void)selectedActionSheet
{
    
    //UIActionSheet *actionsheet;
    
    switch (buttonValue)
    {
        case 0:
            actionsheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Accept" otherButtonTitles:@"Decline", nil];
            actionsheet.tag = 0;
            break;
            
        case 1:
            actionsheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Accept" otherButtonTitles:@"Decline",@"Wait", nil];
            actionsheet.tag = 1;
            break;
        default:
            break;
    }
    
    [actionsheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
    
    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
    statusData = ObjRecommmendationBO.strRecommendationStatus;
    
    switch (buttonIndex)
    {
        case 0:
            buttonSelected = 1;
            //  ObjRecommmendationBO.strRecommendationStatus= @"1";
            statusData=@"1";
            
            break;
            
        case 1:
            buttonSelected = 2;
            // ObjRecommmendationBO.strRecommendationStatus= @"2";
            statusData=@"2";
            
            break;
            
        case 2:
            buttonSelected = 3;
            //    ObjRecommmendationBO.strRecommendationStatus= @"3";
            statusData=@"3";
            
            break;
        default:
            break;
    }
}

#pragma mark-

#pragma mark ButtonAction

//=============================================================================================================================================

-(void)btnFigureClk
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ( [strKey isEqualToString:@"0"])
    {
        
        if ([[arrDataHdng objectAtIndex:8] isEqualToString:@"Status"])
        {
            
            PatrawinCase *objTemp=[arrayAllCases objectAtIndex:[strSelectedIndex integerValue]];
            NSString *strWebUrl=objTemp.link;
            
            LinkViewController *objLinkVC=[[LinkViewController alloc]init];
            objLinkVC.strUrl=strWebUrl;
            [self.navigationController pushViewController:objLinkVC animated:YES];
        }
        else if([[arrDataHdng objectAtIndex:3] isEqualToString:@"Figure"])
        {
            UIView *viewImageFullSize = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  deviceWidth-20, IS_IPAD ? 875 : deviceHeight-75)];
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,  deviceWidth-20, IS_IPAD ? 875 : deviceHeight-75)];
            
            image.image=  [UIImage imageNamed:@"infoBlue"];
            [viewImageFullSize addSubview:image];
            
            [self.view addSubview:viewImageFullSize];
            
            NSLog(@"Show Images");
        }
        
    }
    else if ([strKey isEqualToString:@"1"])
    {
        if ([[arrDataHdng objectAtIndex:7] isEqualToString:@"Status"])
        {
            PatrawinCase *objTemp=[arrayAllCases objectAtIndex:[strSelectedIndex integerValue]];
            NSString *strWebUrl=objTemp.link;
            
            LinkViewController *objLinkVC=[[LinkViewController alloc]init];
            objLinkVC.strUrl=strWebUrl;
            [self.navigationController pushViewController:objLinkVC animated:YES];
        }
        else if([[arrDataHdng objectAtIndex:3] isEqualToString:@"Figure"])
        {
        }
    }
    
}


-(void)btnImageClick
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ( [strKey isEqualToString:@"0"])
    {
        
        if ([[arrDataHdng objectAtIndex:6] isEqualToString:@"Case Image"])
        {
            PatrawinCase *objTemp=[arrayAllCases objectAtIndex:[strSelectedIndex integerValue]];
            NSString *strWebUrl=objTemp.figure;
            
            LinkViewController *objLinkVC=[[LinkViewController alloc]init];
            objLinkVC.screenType=@"Image";
            objLinkVC.strUrl=strWebUrl;
            [self.navigationController pushViewController:objLinkVC animated:YES];
        }
        else if([[arrDataHdng objectAtIndex:3] isEqualToString:@"Figure"])
        {
            UIView *viewImageFullSize = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  deviceWidth-20, IS_IPAD ? 875 : deviceHeight-75)];
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,  deviceWidth-20, IS_IPAD ? 875 : deviceHeight-75)];
            
            image.image=  [UIImage imageNamed:@"infoBlue"];
            [viewImageFullSize addSubview:image];
            
            [self.view addSubview:viewImageFullSize];
            
            //NSLog(@"Show Images");
        }
        
    }
    else if ([strKey isEqualToString:@"1"])
    {
        if ([[arrDataHdng objectAtIndex:5] isEqualToString:@"Case Image"])
        {
            PatrawinCase *objTemp=[arrayAllCases objectAtIndex:[strSelectedIndex integerValue]];
            NSString *Image_Url=objTemp.figure;
            
            LinkViewController *objLinkVC=[[LinkViewController alloc]init];
            objLinkVC.strUrl=Image_Url;
            objLinkVC.screenType=@"Image";
            [self.navigationController pushViewController:objLinkVC animated:YES];
        }
        else if([[arrDataHdng objectAtIndex:3] isEqualToString:@"Figure"])
        {
        }
    }
    
    
}

-(void)btnLeftClk
{
    int SelectedArrow=[self.strSelectedIndex intValue] -1;
    self.strSelectedIndex =[NSString stringWithFormat:@"%d",SelectedArrow];
    
    // NSLog(@"Array index :%i",SelectedArrow);
    statusData=nil;
    [self enableDisableLeftButtons];
    [self enableDisableRightButtons];
    [self showCustomViewForCase:YES];
    
    
}

-(void)btnRightclk
{
    int SelectedArrow=[self.strSelectedIndex intValue]+1;
    self.strSelectedIndex =[NSString stringWithFormat:@"%d",SelectedArrow];
    statusData=nil;
    //  NSLog(@"Array index :%i",SelectedArrow);
    
    [self enableDisableRightButtons];
    [self enableDisableLeftButtons];
    [self showCustomViewForCase:YES];
    
}


-(BOOL)enableDisableLeftButtons
{
    UIButton *btnLeft=(UIButton *)[self.view viewWithTag:kBtnLeft];
    btnLeft.enabled=[self.strSelectedIndex  integerValue]<1?NO:YES;
    
    return btnLeft.enabled;
}

-(BOOL)enableDisableRightButtons
{
    UIButton *btnRight=(UIButton *)[self.view viewWithTag:kBtnRight];
    btnRight.enabled=[self.strSelectedIndex  integerValue]==arrayAllCases.count-1?NO:YES;
    
    return btnRight.enabled;
}

-(void)btnSelectRecommendationClk
{

    [self ViewForRecomendationsShow:YES];
}

-(void)btnCross_Clicked
{
    [self ViewForRecomendationsShow:NO];
}

-(void)designToolBar
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if([strKey isEqualToString:@"0"])
    {
        UIButton *btnCompose = [[UIButton alloc] initWithFrame:CGRectMake(0, deviceHeight-125, deviceWidth/4, 65)];
        btnCompose.tag = kBtnCompose;
        btnCompose.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnCompose addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnCompose setImage:[UIImage imageNamed:@"compose-top"] forState:UIControlStateNormal];
        [self.view addSubview:btnCompose];
        
        UIButton *btnShare = [[UIButton alloc] initWithFrame:CGRectMake(btnCompose.frame.size.width+1, deviceHeight -125, deviceWidth/4, 65)];
        btnShare.tag = kBtnShare;
        btnShare.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnShare addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnShare setImage:[UIImage imageNamed:@"share1"] forState:UIControlStateNormal];
        [self.view addSubview:btnShare];
        
        UIButton *btntempRegistration = [[UIButton alloc] initWithFrame:CGRectMake(btnShare.frame.size.width+1+btnShare.frame.origin.x, deviceHeight-125, deviceWidth/4, 65)];
        btntempRegistration.tag = kBtnRegistration;
        btntempRegistration.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btntempRegistration addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btntempRegistration setImage:[UIImage imageNamed:@"time_registration.png"] forState:UIControlStateNormal];
        [self.view addSubview:btntempRegistration];
        
        UIButton *btnupdate = [[UIButton alloc] initWithFrame:CGRectMake(btntempRegistration.frame.size.width+1+btntempRegistration.frame.origin.x, deviceHeight-125, deviceWidth/4, 65)];
        btnupdate.tag = kBtnUpdate;
        btnupdate.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnupdate addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnupdate setImage:[UIImage imageNamed:@"caseupload"] forState:UIControlStateNormal];
        [self.view addSubview:btnupdate];
    }
    
    else if ([strKey isEqualToString:@"1"])
    {
        UIButton *btnCompose = [[UIButton alloc] initWithFrame:CGRectMake(0,deviceHeight-125,deviceWidth/3+1, 65)];
        btnCompose.tag = kBtnCompose;
        btnCompose.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnCompose addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnCompose setImage:[UIImage imageNamed:@"compose-top"] forState:UIControlStateNormal];
        [self.view addSubview:btnCompose];
        
        UIButton *btnShare = [[UIButton alloc] initWithFrame:CGRectMake(btnCompose.frame.size.width+1,deviceHeight-125, deviceWidth/3+1, 65)];
        btnShare.tag = kBtnShare;
        btnShare.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnShare addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnShare setImage:[UIImage imageNamed:@"share1"] forState:UIControlStateNormal];
        [self.view addSubview:btnShare];
        
        UIButton *btnupdate = [[UIButton alloc] initWithFrame:CGRectMake(btnShare.frame.origin.x + btnShare.frame.size.width+1, deviceHeight-125, deviceWidth/3+1, 65)];
        btnupdate.tag = kBtnUpdate;
        btnupdate.backgroundColor =[UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnupdate addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnupdate setImage:[UIImage imageNamed:@"caseupload"] forState:UIControlStateNormal];
        [self.view addSubview:btnupdate];
        
    }
}

-(void)openPreviewcontrollerToShare
{
    //[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.173 green:0.463 blue:0.886 alpha:1]];
    
    PatrawinCase *objCasesTemp=[self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    
    NSMutableString *txtString=[[NSMutableString alloc]init];
    
    [txtString appendString:[NSString stringWithFormat:@"Country: %@",objCasesTemp.country_code]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Title: %@",objCasesTemp.title]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Status: %@",objCasesTemp.status]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Comment: %@",objCasesTemp.comment_case]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Deadline: %@",objCasesTemp.deadline]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    NSLog(@"txt:%@", txtString);
    NSLog(@" array all values %@",arrayAllCases);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Casesdata.txt"]];
    [fileManager createFileAtPath:fullPath contents:[txtString dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    TXTFileName = [documentsDirectory stringByAppendingPathComponent:@"Casesdata.txt"];
    
    QLPreviewController *previewController = [[QLPreviewController alloc] init];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    //    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    //    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    previewController.dataSource = self;
    // [self presentViewController:previewController animated:NO completion:nil];
    [self presentViewController:previewController animated:NO completion:^(void){
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        
        UIView *view = [[[previewController.view.subviews lastObject] subviews] lastObject];
        if ([view isKindOfClass:[UINavigationBar class]])
        {
            ((UINavigationBar *)view).translucent = NO;
            
        }
        
    }];
}


#pragma mark - QLPreviewControllerDataSource Methods

-(NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
    return 1;
}

-(id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    return  [NSURL fileURLWithPath:TXTFileName];
}

-(void)previewControllerWillDismiss:(QLPreviewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)openMail
{
    PatrawinCase *objCasesTemp=[self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    
    NSMutableString *txtString=[[NSMutableString alloc]init];
    
    
    [txtString appendString:[NSString stringWithFormat:@"Country: %@",objCasesTemp.country_code]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Title: %@",objCasesTemp.title]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Status: %@",objCasesTemp.status]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Comment: %@",objCasesTemp.comment]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Deadline: %@",objCasesTemp.deadline]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Casesdata.txt"]];
    [fileManager createFileAtPath:fullPath contents:[txtString dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    TXTFileName = [documentsDirectory stringByAppendingPathComponent:@"Casesdata.txt"];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@""];
        NSArray *toRecipients = @[];
        [mailer setToRecipients:toRecipients];
        [mailer addAttachmentData:[NSData dataWithContentsOfFile:fullPath] mimeType:@".text" fileName:@"Casesdata"];
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        [CommonMethods showAlertWithTitle:NSLocalizedString(@"Alert.Title.NoMail", @"") andMessage:NSLocalizedString(@"Alert.Message.NoMail", @"")];
    }
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark -

-(void)toolBarBtnClicked:(UIButton *)sender
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    switch (sender.tag)
    {
        case kBtnCompose:
        {
            ComposeSheetVC *obj_ComposeVC = [[ComposeSheetVC alloc] init];
            obj_ComposeVC.strScreenType=@"Cases";
            PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
            obj_ComposeVC.strSubject=objTemp.case_id;
            [self.navigationController pushViewController:obj_ComposeVC animated:YES];
        }
            break;
            
        case kBtnShare:
        {
            [self openPreviewcontrollerToShare];
        }
            break;
            
            
        case kBtnRegistration:
        {
            if([strKey isEqualToString:@"0"])
            {
                TimeRegistrationVC *obj_TimeVC = [[TimeRegistrationVC alloc] init];
                PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
                obj_TimeVC.strCustomerIdValue=objTemp.customer_id;
                obj_TimeVC.strCustomerNameValue=objTemp.customer_name;
                obj_TimeVC.strCaseValue=objTemp.case_id;
                
                [self.navigationController pushViewController:obj_TimeVC animated:YES];
            }
        }
            break;
            
            
        case kBtnUpdate:
        {
            if ([strKey isEqualToString:@"1"])
            {
                [[[UIAlertView alloc]initWithTitle:@"Update" message:@"Do you really want to update recommendation changes?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil]show];
                
                // [self CallForCustomerUpdate];
            }
            else if ([strKey isEqualToString:@"0"])
            {
                [[[UIAlertView alloc]initWithTitle:@"Update" message:@"Do you really want to update comment/recommendation changes?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil]show];
                //[self CallForAgentUpdate];
            }
        }
            break;
            
            
            
        default:
            break;
    }
}





-(void)refreshAgentCase
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    UITextView *txtV=(UITextView *)[self.scrollView viewWithTag:kTxtVRecommendation];
    //    //  UILabel *stsTxtLabel=(UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
    UITextField *txtfComment=(UITextField *)[self.scrollView viewWithTag:kTxtVComment];
    
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"petrawincasedetail";
    
    PatrawinCase *patCase=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    //  patCase.comment_case=txtfComment.text;
    // patCase.Recommendation_Text=txtV.text;
    
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    
    if ([strKey isEqualToString:@"1"])
    {
        [inputPram.dict_postParameters setObject:@"2" forKey:@"status_id"];
    }
    
    else if ([strKey isEqualToString:@"0"])
    {
        [inputPram.dict_postParameters setObject:@"1" forKey:@"status_id"];
    }
    
    
    // NSString *Str1 = [NSString stringWithUTF8String:[txtfComment.text cStringUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"%@", Str1);
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(patCase.customer_id) forKey:@"user_id"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(patCase.case_id) forKey:@"case_id"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtfComment.text) forKey:@"Comment"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtV.text) forKey:@"recommendation_text"];
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(self.strGlobalRecommendationId) forKey:@"recommendation_id"];
    
    
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
        if (error)
        {
            if ([error isErrorOfKindNoNetwork])
                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return ;
            return ;
        }
        else{
            error = nil;
            self.responseDict=nil;
            responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
            
            
            singleCaseDetail = self.responseDict[@"data"];
            //[singleCaseDetail addObjectsFromArray:self.responseDict[@"data"]];
            // NSString *title = [singleCaseDetail valueForKey:@"title"];
            // NSLog(@"Single case data%@", [singleCaseDetail valueForKey:@"title"]);
            
            //NSArray *singleCaseRecom = self.responseDict[@"recommendation_list"];
            // NSLog(@"Single case recommendation%@", singleCaseRecom);
            
            for (int i=0; i<[singleCaseDetail count]; i++) {
                
                
                [self getValuesFromDatabase:[singleCaseDetail objectAtIndex:i]];
                
                
                NSLog(@"Single case data%@",[patCase valueForKey:@"customer_name"]);
                
            }
            
        }
    }];
    
}





-(void)refreshCustomerCase{
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    //UITextView *txtV=(UITextView *)[self.scrollView viewWithTag:kTxtVRecommendation];
    //    //  UILabel *stsTxtLabel=(UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
    //  UITextField *txtfComment=(UITextField *)[self.scrollView viewWithTag:kTxtVComment];
    
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"petrawincasedetail";
    
    PatrawinCase *patCase=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    //patCase.comment_case=txtfComment.text;
    // patCase.Recommendation_Text=txtV.text;
    
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    
    if ([strKey isEqualToString:@"1"])
    {
        [inputPram.dict_postParameters setObject:@"2" forKey:@"status_id"];
    }
    
    else if ([strKey isEqualToString:@"0"])
    {
        [inputPram.dict_postParameters setObject:@"1" forKey:@"status_id"];
    }
    
    
    // NSString *Str1 = [NSString stringWithUTF8String:[txtfComment.text cStringUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"%@", Str1);
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(patCase.customer_id) forKey:@"user_id"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(patCase.case_id) forKey:@"case_id"];
    //[inputPram.dict_postParameters setObject:GET_VALID_STRING(txtfComment.text) forKey:@"Comment"];
    // [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtV.text) forKey:@"recommendation_text"];
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(self.strGlobalRecommendationId) forKey:@"recommendation_id"];
    
    
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
        if (error)
        {
            if ([error isErrorOfKindNoNetwork])
                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return ;
            return ;
        }
        else{
            error = nil;
            self.responseDict=nil;
            responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
            
            
            singleCaseDetail = self.responseDict[@"data"];
            //[singleCaseDetail addObjectsFromArray:self.responseDict[@"data"]];
            // NSString *title = [singleCaseDetail valueForKey:@"title"];
            // NSLog(@"Single case data%@", [singleCaseDetail valueForKey:@"title"]);
            
            //NSArray *singleCaseRecom = self.responseDict[@"recommendation_list"];
            // NSLog(@"Single case recommendation%@", singleCaseRecom);
            
            for (int i=0; i<[singleCaseDetail count]; i++) {
                
                
                [self getValuesFromDatabase:[singleCaseDetail objectAtIndex:i]];
                
                
                NSLog(@"Single case data %@",[patCase valueForKey:@"Recommendation_Status"]);
                
            }
            
        }
    }];
    
}


-(void)getValuesFromDatabase:(NSDictionary *)dict{
    
    
    
    PatrawinCase *patCase = [[PatrawinCase alloc]init];
    patCase.account_cost=dict[@"account_cost"];
    patCase.agent_id = dict[@"agent_id"];
    patCase.arrived_date = dict[@"arrived_date"];
    patCase.case_id = dict[@"case_id"];
    patCase.comment_case = dict[@"Case_Comment"];
    patCase.case_type = dict[@"case_type"];
    patCase.comment = dict[@"comment"];
    patCase.country_code = dict[@"country_code"];
    patCase.customer_id=dict[@"customer_id"];
    patCase.customer_name=dict[@"customer_name"];
    patCase.deadline=dict[@"new_deadline"];
    patCase.deadline_code = dict[@"deadline_code"];
    patCase.figure=dict[@"figure"];
    patCase.figureSmall =dict[@"figuresmall"];
    patCase.link=dict[@"link"];
    patCase.patent_number = dict[@"patent_number"];
    patCase.title = dict[@"title"];
    patCase.status = dict[@"status"];
    patCase.keyword = dict[@"keyword"];
    patCase.Recommendation_Text = dict[@"Recommendation_Text"];
    patCase.Recommendation_Status = dict[@"Recommendation_Status"];
    
    
    
    
    //  if ([casesScreentype isEqualToString:@"All Cases"]) {
    
    
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"StasDetailList" inManagedObjectContext:context]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"caseId  = %@", patCase.case_id];
    NSLog(@"Statss single case id:%@",predicate);
    [request setPredicate:predicate];
    //NSError *error = nil;
    
    
    NSArray *sCases =[context executeFetchRequest:request error:nil];
    for(StasDetailList *stat in sCases) {
        stat.caseComment = dict[@"Case_Comment"];
        stat.recommendationText=dict[@"Recommendation_Text"];
        stat.comment = dict[@"comment"];
        
        stat.caseType = dict[@"case_type"];
        stat.keyword=dict[@"keyword"];
        
        stat.customerName = dict[@"customer_name"];
        stat.agentId=dict[@"agent_id"];
        
        stat.figure = dict[@"figure"];
        stat.deadline=dict[@"new_deadline"];
        stat.deadlineCode=dict[@"deadline_code"];
        
        stat.title = dict[@"title"];
        stat.countryCode=dict[@"country_code"];
        
        stat.status = dict[@"status"];
        stat.accountCost=dict[@"account_cost"];
        
        stat.recommendationStatus=[NSNumber numberWithInteger: [patCase.Recommendation_Status integerValue]];
        [context save:nil];
        
        //  }
        
        
        //        StasDetailList *statList=[[context executeFetchRequest:request error:nil] lastObject];
        //        statList.caseComment = dict[@"Case_Comment"];
        //        statList.recommendationText=dict[@"Recommendation_Text"];
        //        statList.recommendationStatus=[NSNumber numberWithInteger: [patCase.Recommendation_Status integerValue]];
        //        statList.status=dict[@"status"];
        //        statList.deadline=dict[@"new_deadline"];
        //        [context save:nil];
        
        
    }
    
    
    // if ([casesScreentype isEqualToString:@"Statistics"]) {
    
    AppDelegate *appDelegate1;
    appDelegate1 = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context1 = [appDelegate1 managedObjectContext];
    
    NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
    [request1 setEntity:[NSEntityDescription entityForName:@"PetrawinCasesList" inManagedObjectContext:context1]];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"caseId  = %@", patCase.case_id];
    [request1 setPredicate:predicate1];
    // NSError *error1=nil;
    NSLog(@"PetrawinCasesList single case id:%@",predicate1);
    
    
    PetrawinCasesList *pCase=[[context1 executeFetchRequest:request1 error:nil] lastObject];
    pCase.caseComment = dict[@"Case_Comment"];
    pCase.recommendationText=dict[@"Recommendation_Text"];
    pCase.comment = dict[@"comment"];
    
    pCase.caseType = dict[@"case_type"];
    pCase.keyword=dict[@"keyword"];
    
    pCase.customerName = dict[@"customer_name"];
    pCase.agentId=dict[@"agent_id"];
    
    pCase.figure = dict[@"figure"];
    pCase.deadline=dict[@"new_deadline"];
    pCase.deadlineCode = dict[@"deadline_code"];
    
    pCase.title = dict[@"title"];
    pCase.countrycode=dict[@"country_code"];
    
    pCase.status = dict[@"status"];
    pCase.accountCost=dict[@"account_cost"];
    
    pCase.recommendationStatus=[NSNumber numberWithInteger: [patCase.Recommendation_Status integerValue]];
    [context1 save:nil];
    
    //  }
    
    
    
    
    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    objTemp.Recommendation_Text=patCase.Recommendation_Text;
    objTemp.comment_case=patCase.comment_case;
    objTemp.deadline=patCase.deadline; // update all other values necessary
    objTemp.case_type=patCase.case_type;
    
    objTemp.title=patCase.title;
    objTemp.country_code=patCase.country_code;
    objTemp.agent_id=patCase.agent_id; // update all other values necessary
    objTemp.customer_name=patCase.customer_name;
    
    objTemp.figure=patCase.figure;
    objTemp.keyword=patCase.keyword;
    objTemp.status=patCase.status; // update all other values necessary
    objTemp.account_cost=patCase.account_cost;
    
    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
    ObjRecommmendationBO.strRecommendationStatus = patCase.Recommendation_Status;
    
    //[self getValuesForLabelsForSelectedCase:patCase];
    [self showCustomViewForCase:YES];
    
    
}



-(void)CallForAgentUpdate
{
    NSString *oldComent;
    NSString *oldRecom;
    UIAlertView *alertForChkUpdate;
    
    
    UITextView *txtV=(UITextView *)[self.scrollView viewWithTag:kTxtVRecommendation];
    //  UILabel *stsTxtLabel=(UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
    UITextField *txtfComment=(UITextField *)[self.scrollView viewWithTag:kTxtVComment];
    
    if ( [txtfComment.text length]<=0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please add comments." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else if ([txtV.text length]<=0 )
    {
        [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please Select Recommendation." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else
    {
        
        
        PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
        RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
        // NSLog(@"Recomm ID **** %@",objTemp.Recommendation_ID);
        
        oldComent = objTemp.comment_case;
        oldRecom = objTemp.Recommendation_Text;
        
        NSLog(@"comment data ********** :%@",oldComent);
        NSLog(@"recom data ********** :%@",oldRecom);
        
        alertForChkUpdate = [[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Nothing to update." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
        //   [alertForChkUpdate show];
        
        
        //                if (select==YES) {
        //                    alertForChkUpdate=nil;
        //                }
        
        if (select==NO) {
            if ([txtfComment.text isEqualToString:oldComent])
            {
                
                [alertForChkUpdate show];
                
                
            }
            /// select=YES;
        }
        
        if (select==NO || select==YES) {
            
            
            if (![txtfComment.text isEqualToString:oldComent] || select==YES)
            {
                
                
                
                
                //                else if ([txtV.text isEqualToString:oldRecom])
                //                {
                //                    [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please Select Recommendation." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                //                }
                
                // else{
                WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
                inputPram.methodType = WebserviceMethodTypePost;
                inputPram.relativeWebservicePath = @"sendcommentandrecommendation";
                
                
                
                
                //=====================================================================================================================================================
                //  if ([casesScreentype isEqualToString:@"All Cases"]) {
                
                AppDelegate *appDelegate1;
                appDelegate1 = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                NSManagedObjectContext *context1 = [appDelegate1 managedObjectContext];
                
                
                NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
                [request1 setEntity:[NSEntityDescription entityForName:@"StasDetailList" inManagedObjectContext:context1]];
                NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"caseId  = %@", objTemp.case_id];
                [request1 setPredicate:predicate1];
                
                NSArray *sCases =[context1 executeFetchRequest:request1 error:nil];
                for(StasDetailList *stat in sCases) {
                    stat.caseComment = txtfComment.text;
                    
                    
                    if (select==YES) {
                        stat.recommendationText = txtV.text;
                        stat.recommendationStatus =[NSNumber numberWithInteger:0];
                    }
                    
                    else  if (select==NO) {
                        
                        stat.recommendationStatus =[NSNumber numberWithInteger:[ObjRecommmendationBO.strRecommendationStatus integerValue]];
                        stat.recommendationID=[NSNumber numberWithInteger:[ObjRecommmendationBO.strRecommendationId integerValue]];
                        
                    }
                    //                        else{
                    //                        txtV.text=objTemp.Recommendation_Text;
                    //                        stat.recommendationStatus =[NSNumber numberWithInteger:0];
                    //                        }
                    
                    [context1 save:nil];
                }
                //    }
                
                //=====================================================================================================================================================
                
                //  if ([casesScreentype isEqualToString:@"Statistics"]) {
                
                AppDelegate *appDelegate;
                appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                NSManagedObjectContext *context = [appDelegate managedObjectContext];
                
                
                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                [request setEntity:[NSEntityDescription entityForName:@"PetrawinCasesList" inManagedObjectContext:context]];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"caseId  = %@", objTemp.case_id];
                [request setPredicate:predicate];
                
                PetrawinCasesList *pCase=[[context executeFetchRequest:request error:nil] lastObject];
                pCase.caseComment = txtfComment.text;
                
                if (select==YES) {
                    pCase.recommendationText = txtV.text;
                    pCase.recommendationStatus =[NSNumber numberWithInteger:0];
                }
                else if (select==NO)
                {
                    pCase.recommendationStatus =[NSNumber numberWithInteger:[ObjRecommmendationBO.strRecommendationStatus integerValue]];
                    pCase.recommendationID=[NSNumber numberWithInteger:[ObjRecommmendationBO.strRecommendationId integerValue]];
                    
                }
                
                
                ObjRecommmendationBO=objTemp.objRecommendationListBO;
                ObjRecommmendationBO.strRecommendationStatus =[NSString stringWithFormat:@"%@", pCase.recommendationStatus];
                
                [context save:nil];
                
                
                //   }
                
                objTemp.comment_case=txtfComment.text;
                objTemp.Recommendation_Text=txtV.text;
                
                
                NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
                
                NSString *Str1 = [NSString stringWithUTF8String:[txtfComment.text cStringUsingEncoding:NSUTF8StringEncoding]];
                NSLog(@"%@", Str1);
                
                NSLog(@"%@", txtfComment.text);
                
                [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
                [inputPram.dict_postParameters setObject:GET_VALID_STRING(objTemp.customer_id) forKey:@"user_id"];
                [inputPram.dict_postParameters setObject:GET_VALID_STRING(objTemp.case_id) forKey:@"case_id"];
                [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtfComment.text) forKey:@"comment"];
                
                if (select==YES)
                {
                    [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtV.text) forKey:@"recommendation_text"];
                    
                }
                
                [inputPram.dict_postParameters setObject:GET_VALID_STRING(self.strGlobalRecommendationId) forKey:@"recommendation_id"];
                
                //  NSLog(@"%@", [inputPram.dict_postParameters valueForKey:@"recommendation_text"]);
                // NSLog(@"%@", [inputPram.dict_postParameters valueForKey:@"recommendation_id"]);
                
                [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
                    if (error)
                    {
                        if ([error isErrorOfKindNoNetwork])
                            [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                        return ;
                    }
                    
                    error = nil;
                    self.responseDict=nil;
                    self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
                    
                    
                    NSString *strMessage=[[self.responseDict objectForKey:@"data"] objectForKey:@"log"];
                    
                    if ([strMessage isEqualToString:@"Updated Successfully."])
                    {
                        UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        alertVw.tag=677;
                        [alertVw show];
                    }
                    
                    if ([strMessage isEqualToString:@"Sent Successfully."])
                    {
                        
                        if (select==YES) {
                            
                            if (![objTemp.Recommendation_Status isEqualToString:@""]) {
                                
                                
                                UILabel *stsTxtLabel=(UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
                                stsTxtLabel.text = @"Waiting for approval";
                                stsTxtLabel.textColor = [UIColor greenColor];
                                
                                
                            }
                        }
                        UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        alertVw.tag=677;
                        [alertVw show];
                        
                        select=NO;
                        
                    }
                }];
                //}
            }
        }//
        // select=NO;
    }
    
}

-(void)CallForCustomerUpdate
{
    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
    
    if ([ObjRecommmendationBO.strRecommendationText isEqualToString:@""] || [ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"2"])
    {
        [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"No recommendation to update" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"1"])
    {
        [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Nothing to update" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else
    {
        
        if(buttonSelected == 0)
        {
            [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"Please select atleast one action for recommendation."];
        }
        else
        {
            WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
            inputPram.methodType = WebserviceMethodTypePost;
            inputPram.relativeWebservicePath = @"updateStatusOfUserRecommendationSendByAgent";
            
            NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
            
            [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
            
            switch (buttonSelected) {
                case 1:
                    [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"1") forKey:@"status"];
                    break;
                case 2:
                    [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"2") forKey:@"status"];
                    break;
                case 3:
                    [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"3") forKey:@"status"];
                    break;
                default:
                    break;
            }
            
            PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
            RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
            
            if (statusData != nil) {
                ObjRecommmendationBO.strRecommendationStatus = statusData;
            }
            else{
                
            }
            
            
            
            // if ([casesScreentype isEqualToString:@"All Cases"]) {
            AppDelegate *appDelegate1;
            appDelegate1 = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            NSManagedObjectContext *context1 = [appDelegate1 managedObjectContext];
            
            
            NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
            [request1 setEntity:[NSEntityDescription entityForName:@"StasDetailList" inManagedObjectContext:context1]];
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"caseId  = %@", objTemp.case_id];
            [request1 setPredicate:predicate1];
            
            NSArray *sCases =[context1 executeFetchRequest:request1 error:nil];
            for(StasDetailList *stat in sCases) {
                stat.recommendationStatus=[NSNumber numberWithInteger: [ObjRecommmendationBO.strRecommendationStatus integerValue]];
                [context1 save:nil];
            }
            
            //    }
            
            //     if ([casesScreentype isEqualToString:@"Statistics"]) {
            
            AppDelegate *appDelegate;
            appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            
            
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:[NSEntityDescription entityForName:@"PetrawinCasesList" inManagedObjectContext:context]];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"caseId  = %@", objTemp.case_id];
            [request setPredicate:predicate];
            
            PetrawinCasesList *pCase=[[context executeFetchRequest:request error:nil] lastObject];
            pCase.recommendationStatus=[NSNumber numberWithInteger: [ObjRecommmendationBO.strRecommendationStatus integerValue]];
            [context save:nil];
            
            
            //    }
            
            
            
            
            //   PatrawinCasesObject = [PatrawinCasesVC new];
            // [PatrawinCasesObject refreshTable];
            
            [inputPram.dict_postParameters setObject:GET_VALID_STRING(ObjRecommmendationBO.strRecommendationId) forKey:@"Id"];
            
            
            [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
                if (error)
                {
                    if ([error isErrorOfKindNoNetwork])
                        [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                    return ;
                }
                
                error = nil;
                self.responseDict=nil;
                self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
                NSString *strMessage=[[self.responseDict objectForKey:@"data"] objectForKey:@"log"];
                if ([strMessage isEqualToString:@"Status Updated Successfully."])
                {
                    [self setText:ObjRecommmendationBO.strRecommendationStatus];
                    
                    [self showCustomViewForCase:YES];
                    
                    UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    alertVw.tag=677;
                    [alertVw show];
                    
                }
            }];
        }
    }
}


- (void) alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger) buttonIndex
{
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        
    } else if(buttonIndex == 1) {
        
        if ([strKey isEqualToString:@"1"])
        {
            
            [self CallForCustomerUpdate];
        }
        else if ([strKey isEqualToString:@"0"])
        {
            [self CallForAgentUpdate];
        }
        
    }
    
    
    
    if (alertView.tag == 677)
    {
        switch (buttonIndex)
        {
            case 0:

                break;
                
            default:
                break;
        }
    }
}



-(void)setText:(NSString *)RecmStatusID
{
    if ([RecmStatusID isEqualToString:@"1"]) {
        
        UILabel*recommendationSts = (UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
        recommendationSts.text = @"Approved";
        recommendationSts.textColor = [UIColor greenColor];
        
        
    }
    if ([RecmStatusID isEqualToString:@"2"]) {
        
        UILabel*recommendationSts = (UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
        recommendationSts.text = @"Rejected";
        recommendationSts.textColor = [UIColor redColor];
        
    }
    if ([RecmStatusID isEqualToString:@"3"]) {
        
        UILabel*recommendationSts = (UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
        recommendationSts.text = @"Waiting";
        recommendationSts.textColor = [UIColor colorWithRed:1 green:0.749 blue:0 alpha:1];
        
    }
}



- (void) ViewForRecomendationsShow:(BOOL) isShow
{
    if (self.ViewForRecommendationsBG)
    {
        [self.ViewForRecommendationsBG.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self.ViewForRecommendationsBG removeFromSuperview];
        
        self.ViewForRecommendationsBG=nil;
    }
    if(isShow)
    {
        self.ViewForRecommendationsBG=[[UIView alloc]initWithFrame:CGRectMake(0, IS_VERSION_GRT_7 ? 20:0,  deviceWidth,IS_VERSION_GRT_7? deviceHeight-20:deviceHeight)];
        
        self.ViewForRecommendationsBG.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
        
        [APP_DELEGATE.window addSubview:self.ViewForRecommendationsBG];
        
        UIView *blueBar=[[UIView alloc]init];
        blueBar.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
        blueBar.frame=CGRectMake(10, 49, self.view.frame.size.width-20, 40);
        
        UILabel *cases=[[UILabel alloc]init];
        cases.frame=CGRectMake(0, 0, blueBar.frame.size.width, 40);
        cases.text=@"Recommendation List";
        cases.textAlignment=NSTextAlignmentCenter;
        cases.textColor=[UIColor whiteColor];
        [blueBar addSubview:cases];
        
        [self.ViewForRecommendationsBG addSubview:blueBar];
        
        self.ViewForRecommendations=[[UIView alloc]initWithFrame:CGRectMake(10,90, deviceWidth-20, IS_IPAD ? 875 : deviceHeight-75-60)];
        
        self.ViewForRecommendations.backgroundColor = [UIColor whiteColor] ;
        
        [self.ViewForRecommendationsBG addSubview:self.ViewForRecommendations];
        
        
        tblForRecommendations=[[UITableView alloc]initWithFrame:CGRectMake(0, 0,  deviceWidth-20, IS_IPAD ? 875 : deviceHeight-75-60) style:UITableViewStylePlain];
        
        tblForRecommendations.delegate=self;
        
        tblForRecommendations.dataSource=self;
        
        tblForRecommendations.rowHeight = IS_IPAD ? 60 : 44;
        
        tblForRecommendations.backgroundColor=[UIColor clearColor];
        
        tblForRecommendations.backgroundView=nil;
        
        tblForRecommendations.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        
        tblForRecommendations.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        [self.ViewForRecommendations addSubview:tblForRecommendations];
        
        
        UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btnCross.frame = CGRectMake(0, 30, 24, 24);
        
        [btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
        
        [btnCross addTarget:self action:@selector(btnCross_Clicked) forControlEvents:UIControlEventTouchUpInside];
        
        [self.ViewForRecommendationsBG addSubview:btnCross];
        
        btnCross = nil;
    }
}

#pragma mark - getValuesForLabelsForSelectedCase


- (void) getValuesForLabelsForSelectedCase:(PatrawinCase *) selectedPatCase
{
    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    // objTemp.Recommendation_Status = selectedPatCase.Recommendation_Status;
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    if ( [strKey isEqualToString:@"0"])
    {
        
        UILabel *lblCountryValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+1];
        lblCountryValue.text= [NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.country_code)];
        objTemp.country_code =lblCountryValue.text;
        
        UILabel *lblTitleValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry];
        lblTitleValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.title)];
        objTemp.title =lblTitleValue.text;
        
        UILabel *lblAgentValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+2];
        lblAgentValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.agent_id)];
        objTemp.agent_id =lblAgentValue.text;
        
        UILabel *lblCustomerValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+3];
        lblCustomerValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.customer_name)];
        objTemp.customer_name =lblCustomerValue.text;
        
        UILabel *lblAccCostValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+9];
        lblAccCostValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.account_cost)];
        objTemp.account_cost = lblAccCostValue.text;
        
        UILabel *lblCaseTypeValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+5];
        lblCaseTypeValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.case_type)];
        objTemp.case_type = lblCaseTypeValue.text;
        
        UILabel *lblKeywordValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+7];
        lblKeywordValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.keyword)];
        objTemp.keyword = lblKeywordValue.text;
        
        UILabel *lblDeadlineCode=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+12];
        lblDeadlineCode.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.deadline_code)];
        objTemp.deadline_code = lblDeadlineCode.text;
        
        UILabel *lblCommentDueDateNote=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+4];
        lblCommentDueDateNote.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.comment)];
        objTemp.comment = lblCommentDueDateNote.text;
        
        
        
        lbl_CaseName.text=selectedPatCase.case_id;
        
        
        if ([[arrDataHdng objectAtIndex:8] isEqualToString:@"Status"])
        {
            UIButton *btn=(UIButton *)[self.viewForCaseData viewWithTag:kBtnFigure];
            [btn setTitle:GET_VALID_STRING(selectedPatCase.status) forState:UIControlStateNormal];
            objTemp.status = btn.titleLabel.text;
            btn.backgroundColor = [UIColor clearColor];
            //  [btn.titleLabel setTextAlignment:NSTextAlignmentLeft];
            
            UIImageView *btnApproved = [[UIImageView alloc]initWithFrame:CGRectMake(btn.frame.origin.x+btn.frame.size.width-28,btn.frame.origin.y+10, 25, 25)];
            //  buttonValue = 0;
            //[btnApproved setTitle:@"" forState:UIControlStateNormal];
            //  [btnApproved setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btnApproved setImage:[UIImage imageNamed:@"infoBlue"]];
            
            [self.scrollView addSubview:btnApproved];
            
            
        }
        else if([[arrDataHdng objectAtIndex:2] isEqualToString:@"Figure"])
        {
            
            [imageFigure setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.figureSmall)]]];
            
            //            UILabel *lblStatusValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+3];
            //            lblStatusValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.status)];
        }
        
        UITextView *txtVCommentValue=(UITextView *)[self.viewForCaseData viewWithTag:kTxtVComment];
        txtVCommentValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.comment_case)];
        objTemp.comment_case =txtVCommentValue.text;
        
        UILabel *lblDeadlineValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblCountry+11];
        lblDeadlineValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.deadline)];
        objTemp.deadline = lblDeadlineValue.text;
        
        [lblDeadlineValue setTextAlignment:NSTextAlignmentLeft];
        //   [lblDeadlineValue setBackgroundColor:[UIColor redColor]];
        
        if (![lblDeadlineValue.text isEqualToString:@""]) {
            
            
            
            if ([[arrDataHdng objectAtIndex:4] isEqualToString:@"Deadline"]||[[arrDataHdng objectAtIndex:11] isEqualToString:@"Deadline"]) {
                
                UIButton *btnApproved1 = [[UIButton alloc]initWithFrame:CGRectMake(lblDeadlineValue.frame.origin.x+lblDeadlineValue.frame.size.width-28,lblDeadlineValue.frame.origin.y+8, 25, 25)];
                [btnApproved1 setBackgroundImage:[UIImage imageNamed:@"infoBlue"] forState:UIControlStateNormal];
                
                [btnApproved1 addTarget:self action:@selector(btnDeadlineClk) forControlEvents:UIControlEventTouchUpInside];
                
                [self.scrollView addSubview:btnApproved1];
                
            }
            
        }
        
        
        
        if ([[arrDataHdng objectAtIndex:13] isEqualToString:@"Recommendation"])
        {
            
            UITextView *recommendationtext =(UITextView *)[self.viewForCaseData viewWithTag:kTxtVRecommendation];
            recommendationtext.text = selectedPatCase.Recommendation_Text;
            objTemp.Recommendation_Text =recommendationtext.text;
            
        }
        
        
        
    //=============================================================================================================================================
        
    }
    
    
    
    
     //        //=============================================================================================================================================
    //
    //    }
    //
    
    
    
    
    else if ( [strKey isEqualToString:@"1"])
    {
        UILabel *lblTitleValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle];
        lblTitleValue.text= [NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.title)];
        objTemp.title = lblTitleValue.text;
        
        UILabel *lblCountryValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+1];
        lblCountryValue.text= [NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.country_code)];
        objTemp.country_code =lblCountryValue.text;
        
        UILabel *lblAgentValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+2];
        lblAgentValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.agent_id)];
        objTemp.agent_id = lblAgentValue.text;
        
        UILabel *lblCustomerValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+3];
        lblCustomerValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.customer_name)];
        objTemp.customer_name = lblCustomerValue.text;
        
        UILabel *lblCaseTypeValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+4];
        lblCaseTypeValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.case_type)];
        objTemp.case_type = lblCaseTypeValue.text;
        
        UILabel *lblKeywordValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+6];
        lblKeywordValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.keyword)];
        objTemp.keyword = lblKeywordValue.text;
        
        NSString *recomString = selectedPatCase.Recommendation_Status;
        
        
        if ([recomString isEqualToString:@"0"]) {
            UILabel*recommendationSts = (UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
            recommendationSts.text = @"Waiting for Approval";
            recommendationSts.textColor = [UIColor greenColor];
            
            UIButton *recommendatiouttonn = (UIButton *)[self.scrollView viewWithTag:kbtnRecom];
            buttonValue = 1;
            recommendatiouttonn.hidden=NO;
            
        }
        
        if ([recomString isEqualToString:@"1"]) {
            
            UILabel*recommendationSts = (UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
            recommendationSts.text = @"Approved";
            recommendationSts.textColor = [UIColor greenColor];
            
            
        }
        if ([recomString isEqualToString:@"2"]) {
            
            UILabel*recommendationSts = (UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
            recommendationSts.text = @"Rejected";
            recommendationSts.textColor = [UIColor redColor];
            
            
        }
        if ([recomString isEqualToString:@"3"]) {
            
            UILabel*recommendationSts = (UILabel *)[self.scrollView viewWithTag:kStsVRecommendation];
            recommendationSts.text = @"Waiting";
            recommendationSts.textColor = [UIColor colorWithRed:1 green:0.749 blue:0 alpha:1];
            
            UIButton *recommendatiouttonn = (UIButton *)[self.scrollView viewWithTag:kbtnRecom];
            buttonValue = 0;
            
            recommendatiouttonn.hidden=NO;
            
            
            
        }
        objTemp.Recommendation_Status = recomString;
        
        
        
        lbl_CaseName.text=selectedPatCase.case_id;
        
        if ([[arrDataHdng objectAtIndex:7] isEqualToString:@"Status"])
        {
            UIButton *btn=(UIButton *)[self.viewForCaseData viewWithTag:kBtnFigure];
            [btn setTitle:GET_VALID_STRING(selectedPatCase.status) forState:UIControlStateNormal];
            objTemp.status = btn.titleLabel.text;
            
            UIImageView *btnApproved = [[UIImageView alloc]initWithFrame:CGRectMake(btn.frame.origin.x+btn.frame.size.width-28,btn.frame.origin.y+10, 25, 25)];
            //  buttonValue = 0;
            //[btnApproved setTitle:@"" forState:UIControlStateNormal];
            //  [btnApproved setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btnApproved setImage:[UIImage imageNamed:@"infoBlue"]];
            
            [self.scrollView addSubview:btnApproved];
            
            
        }
        else if([[arrDataHdng objectAtIndex:3] isEqualToString:@"Figure"])
        {
            UILabel *lblStatusValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+3];
            lblStatusValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.status)];
        }
        
        UILabel *lblAccCostValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+8];
        lblAccCostValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.account_cost)];
        objTemp.account_cost = lblAccCostValue.text;
        
        if (selectedPatCase.comment_case.length == 0)
        {
            UITextView *txtf=(UITextView *)[self.scrollView viewWithTag:kTxtVComment];
            txtf.hidden = YES;
        }
        else
        {
            txtViewComment = (UITextView *)[self.viewForCaseData viewWithTag:kTxtVComment];
            txtViewComment.hidden = NO;
            txtViewComment.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.comment_case)];
            objTemp.comment_case =txtViewComment.text;
            
        }
        
        UILabel *lblDeadlineValue = (UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+10];
        lblDeadlineValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.deadline)];
        objTemp.deadline = lblDeadlineValue.text;
        [lblDeadlineValue setTextAlignment:NSTextAlignmentLeft];
        
        UILabel *lblDeadlineCode = (UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+11];
        lblDeadlineCode.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedPatCase.deadline_code)];
        objTemp.deadline_code = lblDeadlineCode.text;
        [lblDeadlineCode setTextAlignment:NSTextAlignmentLeft];
        
        
        //        [lblDeadlineValue setBackgroundColor:[UIColor redColor]];
        if (![lblDeadlineValue.text isEqualToString:@""]) {
            
            
            if ([[arrDataHdng objectAtIndex:4] isEqualToString:@"Deadline"]||[[arrDataHdng objectAtIndex:10] isEqualToString:@"Deadline"]) {
                
                UIButton *btnApproved1 = [[UIButton alloc]initWithFrame:CGRectMake(lblDeadlineValue.frame.origin.x+lblDeadlineValue.frame.size.width-28,lblDeadlineValue.frame.origin.y+8, 25, 25)];
                //  buttonValue = 0;
                //[btnApproved setTitle:@"" forState:UIControlStateNormal];
                //  [btnApproved setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [btnApproved1 setBackgroundImage:[UIImage imageNamed:@"infoBlue"] forState:UIControlStateNormal];
                
                [btnApproved1 addTarget:self action:@selector(btnDeadlineClk) forControlEvents:UIControlEventTouchUpInside];
                
                [self.scrollView addSubview:btnApproved1];
                
                
                
            }
        }
        
        if ([[arrDataHdng objectAtIndex:12]isEqualToString:@"Recommendation"])
        {
            
            UITextView *recommendationtext =(UITextView *)[self.viewForCaseData viewWithTag:kTxtVRecommendation];
            recommendationtext.text = selectedPatCase.Recommendation_Text;
            objTemp.Recommendation_Text =recommendationtext.text;
            
        }
        
        
        
    }
}


-(void)btnDeadlineClk
{
    
    LinkViewController *objLinkVC=[[LinkViewController alloc]init];
    objLinkVC.strUrl=@"http://app.hynell.se/html/deadline.html";
    [self.navigationController pushViewController:objLinkVC animated:YES];
    objLinkVC.isDeadline=YES;
    
    
}


#pragma mark - UITableView DataSource Methods

- (CGFloat) tableView:(UITableView *) tableView heightForRowAtIndexPath:(NSIndexPath *) indexPath
{
    return IS_IPAD ? 60 : 44;
}

- (NSInteger) tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger) section
{
    
    if (self.arrRecommendationListBO.count == 0)
    {
        return 1;
    }
    else{
        return self.arrRecommendationListBO.count;
    }
}

- (UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.backgroundColor = [UIColor clearColor];
    }
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (self.arrRecommendationListBO.count==0)
    {
        UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?380: 230,IS_IPAD? 768: 320, 30)];
        
        [lblNoRecord setText:@"No Record Available."];
        
        [lblNoRecord setTextColor:[UIColor lightGrayColor]];
        
        lblNoRecord.textAlignment = NSTextAlignmentCenter;
        
        [lblNoRecord setBackgroundColor:[UIColor clearColor]];
        
        [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
        
        [cell.contentView addSubview:lblNoRecord];
        
        cell.backgroundColor=[UIColor clearColor];
    }
    else if(tableView==tblForRecommendations)
    {
        RecommendationsListBO *objRecommendation=self.arrRecommendationListBO[indexPath.row];
        NSString *strRecommendationText= objRecommendation.strRecommendationText;
        
        cell.textLabel.frame=CGRectMake(5, 10, 290, 30);
        cell.textLabel.text = strRecommendationText;
        cell.textLabel.textAlignment=NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor darkGrayColor];
        // cell.textLabel.backgroundColor=[UIColor yellowColor];
        cell.textLabel.font = [UIFont systemFontOfSize:IS_IPAD ? 25 : 15];
    }
    return cell;
}

- (BOOL) shouldAutorotate
{
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait)
    {
        return YES;
    }
    return NO;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark - UITableView Delegates

- (void) tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.arrRecommendationListBO.count == 0)
    {
        
    }
    else
    {
        RecommendationsListBO *objRecommendationBO = [self.arrRecommendationListBO objectAtIndex:indexPath.row];
        [self ViewForRecomendationsShow:NO];
        
        UITextView *txtV=(UITextView *)[self.scrollView viewWithTag:kTxtVRecommendation];
        
        txtV.hidden=NO;
        
        txtV.text=objRecommendationBO.strRecommendationText;
        
        self.strGlobalRecommendationId=objRecommendationBO.strRecommendationId;
        select = YES;
        
        UIButton *btn=(UIButton *)[self.scrollView viewWithTag:kBtnSelectRecommendation];
        
        btn.selected=YES;
    }
}


#pragma mark- UITextView Delegates

- (BOOL) textViewShouldBeginEditing:(UITextView *) textView
{
    if (IS_IPHONE)
    {
        NSString *strKey = [USER_DEFAULTS valueForKey:@"Customer"];
        
        if ([strKey isEqualToString:@"0"])
        {
            UITextView *txtf=(UITextView *)[self.scrollView viewWithTag:kTxtVComment];
            
            int y = txtf.frame.origin.y-80;
            
            if (textView.tag == kTxtVComment )
            {
                [self.scrollView setContentOffset:CGPointMake(0,IS_IPHONE_5? (y):(y)) animated:YES];
            }
        }
        else if ([strKey isEqualToString:@"1"])
        {
            
        }
    }
    return YES;
}

- (BOOL) textView:(UITextView *) textView shouldChangeTextInRange:(NSRange) range replacementText:(NSString *) text
{
    if (IS_IPHONE)
    {
        if ([text isEqualToString:@"\n"])
        {
            [self.scrollView setContentOffset:CGPointZero animated:YES];
            
            [textView resignFirstResponder];
            
            return YES;
        }
    }
    return YES;
}




#pragma mark - ScrollView Delegate Methods

- (void) scrollViewWillEndDragging:(UIScrollView *) scrollView withVelocity:(CGPoint) velocity targetContentOffset:(inout CGPoint *) targetContentOffset
{
    [self.view endEditing:YES];
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}



@end
