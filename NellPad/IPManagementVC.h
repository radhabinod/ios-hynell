//
//  IPManagementVC.h
//  NellPad
//
//  Created by Herry Makker on 12/9/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPManagementVC : UIViewController
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) NSArray *arrData;
@property(nonatomic,strong) NSArray *arrImg;


@property(nonatomic,strong) NSString *emailForChngePass;
@end
