//
//  CalendarVC.m
//  NellPad
//
//  Created by Herry Makker on 12/5/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import "WebReferenceVC.h"

@interface WebReferenceVC ()

@end

@implementation WebReferenceVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webView = [[UIWebView  alloc]initWithFrame:self.view.frame];
    self.webView.autoresizesSubviews = YES;
    self.webView.autoresizingMask  = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    [self.webView setDelegate:self];
    [self.view addSubview:self.webView];
}


- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


-(void)viewWillAppear:(BOOL)animated
{
    
    
    NSURL *url = [NSURL URLWithString:kApp_URL];
    NSURLRequest *requestAddress = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestAddress];
}

-(void)loadView
{
    UIView *view;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        view=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 504)];
    else
        view=[[UIView alloc] initWithFrame:CGRectMake(0,0, 768, 1024)];

    
    self.title  = @"Legal";
    [self.navigationController.navigationBar setTranslucent:NO];
    [[UINavigationBar appearance] setTitleTextAttributes:
                                [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIColor whiteColor],
                                 NSForegroundColorAttributeName,
                                 [UIFont fontWithName:@"Avenir" size:22.0],NSFontAttributeName, nil]];
    
    view.backgroundColor=[UIColor grayColor];
    self.view = view;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
