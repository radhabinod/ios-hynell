//
//  TimeRegistrationVC.h
//  NellPad
//
//  Created by Sanjeev Kumar on 12/23/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kbtnCase 1233
#define kbtnCode 1234
#define kbtnDay 1236
#define kbtnTime 1237
#define kTxtFieldCase 23445
#define kbtnDone 4398
#define kbtnCancel 9889
@interface TimeRegistrationVC : UIViewController <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate>
{
}

@property (strong,nonatomic) UIDatePicker* datetimeSelection;
@property (strong, nonatomic) UIPickerView *timePicker;
@property (strong,nonatomic) NSDictionary *responseDict;
@property (nonatomic,strong) NSMutableArray *arrCasesData;

@property(nonatomic,strong) NSMutableArray *arrCodesData;
@property(nonatomic,strong) NSMutableArray *arrDataHdng;
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) NSString *strCaseValue;
@property(nonatomic,strong) UIToolbar *keyBoardToolBar;
@property(nonatomic,strong) NSString *strGlobalCompany_id;
@property(nonatomic,strong) NSString *strDatePickerSelected;
@property(nonatomic,strong) UIView *viewForCaseData;
@property(nonatomic,strong) NSString *strViewSelected;
@property(nonatomic,strong) UIView *viewForPopUpBG;
@property(nonatomic,strong) UIView *viewForPopUp;
@property(nonatomic,strong) NSString *strTypeOfData;
@property(nonatomic,strong) NSString *strCustomerNameValue;
@property(nonatomic,strong) NSString *strCustomerIdValue;
@property(nonatomic,strong) NSMutableArray *arrSelectedCodes;


-(void)showCustomViewForCase:(BOOL)isShow;
-(void)showCustomView:(BOOL)isShow;


@end
