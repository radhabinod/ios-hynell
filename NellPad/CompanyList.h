//
//  CompanyList.h
//  NellPat
//
//  Created by Macbook on 6/19/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CompanyList : NSManagedObject

@property (nonatomic, retain) NSString * companyID;
@property (nonatomic, retain) NSString * companyName;

@end
