//
//  StasList.h
//  NellPat
//
//  Created by Macbook on 6/10/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StasList : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * number;

@end
