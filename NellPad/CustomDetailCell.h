//
//  CustomDetailCell.h
//  Fieldo
//
//  Created by Herry Makker on 11/13/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomDetailCell : UITableViewCell



@property (strong, nonatomic) UILabel *labelKey;
@property (strong, nonatomic) UILabel *labelValue;

@end