//
//  PatrawinCase.h
//  NellPad
//
//  Created by Atul Thakur on 4/11/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecommendationsListBO.h"

@interface PatrawinCase : NSObject

@property(nonatomic,strong) NSString *account_cost;
@property(nonatomic,strong) NSString *comment_case;
@property(strong, nonatomic) NSString *agent_id;
@property(strong, nonatomic) NSString *arrived_date;
@property(strong, nonatomic) NSString *case_id;
@property(strong, nonatomic) NSString *case_type;
@property(strong, nonatomic) NSString *comment;
@property(strong, nonatomic) NSString *country_code;
@property(nonatomic,strong) NSString *customer_id;
@property(nonatomic,strong) NSString *customer_name;

@property(strong, nonatomic) NSString *deadline;
@property(strong, nonatomic) NSString *figure;
@property (nonatomic, strong) NSString * keyword;
@property (nonatomic, strong) NSString * deadline_code;

@property(strong, nonatomic) NSString *figureSmall;

@property(nonatomic,strong) NSString *link;
@property(strong, nonatomic) NSString *patent_number;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *title;
@property (nonatomic, retain) NSNumber * Recommendation_ID;
@property(strong, nonatomic) NSString *Recommendation_Text;
@property(strong, nonatomic) NSString *Recommendation_Status;
@property(nonatomic,strong) NSMutableArray *arrCodesData;

@property(nonatomic,strong) RecommendationsListBO *objRecommendationListBO;



@end
