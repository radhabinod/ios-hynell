//
//  CompanyBO.h
//  NellPad
//
//  Created by Rakesh Kumar on 29/04/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompanyBO : NSObject
@property(nonatomic,strong) NSString *strCompany_Id;
@property(nonatomic,strong) NSString *strCompany_name;
@end
