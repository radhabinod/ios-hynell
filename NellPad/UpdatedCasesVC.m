//
//  UpdatedCasesVC.m
//  Hynell
//
//  Created by Ram Kumar on 18/10/16.
//  Copyright © 2016 Gagan Joshi. All rights reserved.
//

#import "UpdatedCasesVC.h"

#import "CasesVC.h"
#import "PatrawinCase.h"
#import "RecommendationsListBO.h"
#import "PetrawinCasesList.h"
#import "CompanyList.h"
#import "CompanyBO.h"
#import "RecommendationsList.h"
#import "DataSyncBO.h"
#import "CompaniesListVC.h"
#import "SingleRecentCaseVC.h"

@interface UpdatedCasesVC ()

@end

@implementation UpdatedCasesVC

{
 //   CasesVC *obj_CaseVC;
     DataSyncBO *dataBO;
    SingleRecentCaseVC *singleCaseVCObject;
    NSArray *allCasesArray;
    NSMutableArray *finalArray;
    NSArray *arrRecommendation;
    NSMutableArray *updatedCaseArray;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"Updated Cases";

   // obj_CaseVC  = [[CasesVC alloc] init];
    
    singleCaseVCObject  = [[SingleRecentCaseVC alloc] init];
    self.view.backgroundColor = [UIColor whiteColor];
    updatedCaseArray = [[NSMutableArray alloc]init];
    
    [self configureView];

//    [self recentlyUpdateCases];
    [self fetchFromDatabase];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(ViewReceiveToggleAuthUINotification:)
     name:@"RefreshUpdateCasesList"
     object:nil];


    
}


- (void) ViewReceiveToggleAuthUINotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"RefreshUpdateCasesList"]) {
        
        
        [self recentlyUpdateCases];

    }
    
}


- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];

    
}





- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


-(void)fetchFromDatabase
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"PetrawinCasesList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    
    NSEntityDescription *entityDesc1 = [NSEntityDescription entityForName:@"RecommendationsList" inManagedObjectContext:context];
    NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
    [request1 setEntity:entityDesc1];
    
    NSError *error1;
    NSArray *fetchedArray1 = [context executeFetchRequest:request1 error:&error1];
    
    responseDict =[NSMutableDictionary new];
    [responseDict setValue:fetchedArray forKey:@"data"];
    [responseDict setValue:fetchedArray1 forKey:@"recommendation_list"];
    
    //NSLog(@"The array is %i", fetchedArray1.count);
    
    
   
    
    if (fetchedArray.count == 0) {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"CaseListdownloaded" object:nil];
        dataBO=[DataSyncBO new];
        [dataBO caseList];
        
       
    }
    else
    {
        [self performSelectorOnMainThread:@selector(sortCasesDescendings) withObject:self waitUntilDone:YES];
        [self recentlyUpdateCases];

    }
    
    
}

-(void) DataReload
{
    [self fetchFromDatabase];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}




-(void)recentlyUpdateCases
{
    //NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"getlatestupdatedcases";
    
    
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         }
         
         else
         {
             error = nil;

             [updatedCaseArray removeAllObjects];
             NSDictionary *responseDicts = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
             
            updatedCaseArray = responseDicts[@"data"];
            NSLog(@"cases list is**** :%lu",(unsigned long)updatedCaseArray.count);
       
             
            _updateCasesArray = [[NSMutableArray alloc]init];
             if (updatedCaseArray.count)
             {
                 
                 for (NSDictionary *caseDict in updatedCaseArray) {
                     
                     NSString *casesID =caseDict[@"case_id"];
                     [_updateCasesArray addObject:casesID];
                     
                 }
                 
                 NSLog(@"cases list is**** :%@",_updateCasesArray);
                                 
            }
             else{

           [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"No Case Found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];

                 
             }
             [_caseTableView reloadData];
             
         }
     }];
    
}





-(void)configureView
{
    [self.view endEditing:YES];
    self.caseTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, deviceWidth, deviceHeight-65) style:UITableViewStylePlain];
    
    self.caseTableView.delegate=self;
    self.caseTableView.dataSource=self;
    self.caseTableView.backgroundColor=[UIColor whiteColor];
    self.caseTableView.rowHeight = IS_IPAD ? 60 : 50.0f;
    self.caseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.caseTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.caseTableView];
    self.view.layer.borderColor = UIColorFromRGB(0xF0F0F0).CGColor;
    
}



#pragma mark - Table view Datasource & Delegate Methods 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==_caseTableView)
    {
        return IS_IPAD ? 61 : 71.0f;
    }
    else
        return IS_IPAD ? 55 : 44;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _updateCasesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
    }
    cell.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];

    
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    //    if (self.casesArray.count==0)
    if (tableView == _caseTableView)
    {
        if (_updateCasesArray.count==0)
        {
            UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?380: 160,IS_IPAD? 768: 320, 30)];
            
            if (responseDict.count==0)
            {
                [lblNoRecord setText:@""];
            }
            else
            {
                [lblNoRecord setText:@"No Record Available."];
            }
            [lblNoRecord setTextColor:[UIColor lightGrayColor]];
            lblNoRecord.textAlignment = NSTextAlignmentCenter;
            [lblNoRecord setBackgroundColor:[UIColor clearColor]];
            [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
            [cell.contentView addSubview:lblNoRecord];
            cell.backgroundColor=[UIColor clearColor];
        }
        
        else
        {
           // PatrawinCase *patCase = searchResults[indexPath.row];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            cell.textLabel.text=[NSString stringWithFormat:@"Case: %@",[_updateCasesArray objectAtIndex:indexPath.row]];
            
            
            cell.textLabel.textColor= [UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
            
            cell.textLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 18 : 16.0];
            
            cell.detailTextLabel.textColor=[UIColor darkGrayColor];
            
            cell.detailTextLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 16 : 12.0];
        }
        
        cell.accessoryView = [[ UIImageView alloc ]
                              initWithImage:[UIImage imageNamed:@"arrowMessage"]];
        
        UIView *_lineDiv=[[UIView alloc] init];
        _lineDiv.frame=CGRectMake(0, IS_IPAD ? 60 : 70.0f, self.view.frame.size.width, 1.0);
        _lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
        [cell addSubview:_lineDiv];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if(tableView == self.caseTableView)
    {
        if (_updateCasesArray.count==0)
        {
            
        }
        else
        {
            
            NSLog(@"case id by index %@",[_updateCasesArray objectAtIndex:indexPath.row]);
            
            
            NSMutableArray *sResult = [[NSMutableArray alloc]init];
            
            for(PatrawinCase *patCases in finalArray)
            {
                NSRange r = [patCases.case_id rangeOfString:[_updateCasesArray objectAtIndex:indexPath.row] options:NSCaseInsensitiveSearch];
                
                if (r.length > 0) {
                    [sResult addObject:patCases];
                    NSLog(@"single array %@",sResult);
                }
            }
            
            NSArray *searchResults = [NSArray arrayWithArray:sResult];
            int index= 0;

            singleCaseVCObject.strSelectedIndex=[NSString stringWithFormat:@"%d",index];
            singleCaseVCObject.arrayAllCases = [[NSMutableArray alloc]initWithArray:searchResults];
            singleCaseVCObject.singleCaseId = [_updateCasesArray objectAtIndex:indexPath.row];
            
            if ([strKey isEqualToString:@"1"])
            {
                
            }
            else if ([strKey isEqualToString:@"0"])
            {
                singleCaseVCObject.arrRecommendationListBO=self.arrRecommendations;
               // NSLog(@"%i", singleCaseVCObject.arrRecommendationListBO.count);
            }
            else{
                NSLog(@"Logic testing");
            }
           [self.navigationController pushViewController:singleCaseVCObject animated:YES];
        }
    }
}




-(void)sortCasesDescendings
{
    
    finalArray = [[NSMutableArray alloc]init];
    self.arrRecommendations = [[NSMutableArray alloc]init];
    
    [finalArray removeAllObjects];
    [self.arrRecommendations removeAllObjects];
    
    allCasesArray = responseDict[@"data"];
    arrRecommendation=responseDict[@"recommendation_list"];
    
   NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
   
    
    if ([strKey isEqualToString:@"1"])
    {
        NSMutableArray *unsortedArr = [NSMutableArray array];
        for(PetrawinCasesList *caseDict in allCasesArray)
        {
            PatrawinCase *patCase = [[PatrawinCase alloc] init];
            patCase.account_cost=caseDict.accountCost;
            patCase.agent_id =caseDict.agentId;
            patCase.arrived_date =[NSString stringWithFormat:@"%@",caseDict.arrivedDate];
            //patCase.case_id =[NSString stringWithFormat:@"%@",caseDict.caseId];
            patCase.case_id = caseDict.caseId;
            patCase.comment_case =caseDict.caseComment;
            patCase.case_type =caseDict.caseType;
            patCase.comment =caseDict.comment;
            patCase.country_code =caseDict.countrycode;
            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
            patCase.customer_name=caseDict.customerName;
            patCase.deadline=caseDict.deadline;
            patCase.figure=caseDict.figure;
            patCase.figureSmall = caseDict.figuresmall;
            patCase.link=caseDict.link;
            patCase.patent_number =caseDict.patentNo;
            patCase.title =caseDict.title;
            patCase.status =caseDict.status;
            patCase.Recommendation_Text = caseDict.recommendationText;
            patCase.keyword = caseDict.keyword;
            patCase.deadline_code=caseDict.deadlineCode;
            
            //            for(RecommendationsList *dictRecommendation in arrRecommendation)
            //            {
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];;
            patCase.objRecommendationListBO=objRecommendationListBO;
            [unsortedArr addObject:patCase];
            //            }
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        finalArray = [sortedArray mutableCopy];
        sortedArray = nil;
        
    }

    
    
   else if ([strKey isEqualToString:@"0"])
    {
    for (RecommendationsList *dictRecommendation in arrRecommendation)
    {
        RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
        objRecommendationListBO.strRecommendationId = dictRecommendation.recommendation_ID;
        objRecommendationListBO.strRecommendationText = dictRecommendation.recommendation_Text;
        // objRecommendationListBO.strRecommendationStatus = dictRecommendation.recommendation_Status;
        [self.arrRecommendations addObject:objRecommendationListBO];
    }
    
    NSMutableArray *unsortedArr = [NSMutableArray array];
    for(PetrawinCasesList *caseDict in allCasesArray)
    {
        PatrawinCase *patCase = [[PatrawinCase alloc] init];
        patCase.account_cost=caseDict.accountCost;
        patCase.agent_id = caseDict.agentId;
        patCase.arrived_date = [NSString stringWithFormat:@"%@",caseDict.arrivedDate];
        //patCase.case_id = [NSString stringWithFormat:@"%@",caseDict.caseId];
        patCase.case_id = caseDict.caseId;
        patCase.comment_case = caseDict.caseComment;
        patCase.case_type = caseDict.caseType;
        patCase.comment = caseDict.comment;
        patCase.country_code = caseDict.countrycode;
        patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
        patCase.customer_name=caseDict.customerName;
        patCase.deadline=caseDict.deadline;
        patCase.figure=caseDict.figure;
        patCase.figureSmall = caseDict.figuresmall;
        patCase.link=caseDict.link;
        patCase.patent_number = caseDict.patentNo;
        patCase.title = caseDict.title;
        patCase.status = caseDict.status;
        patCase.Recommendation_Text = caseDict.recommendationText;
        patCase.keyword = caseDict.keyword;
        patCase.deadline_code = caseDict.deadlineCode;
        patCase.Recommendation_ID = caseDict.recommendationID;
        
        
        RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
        objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
        objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
        objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];;
        patCase.objRecommendationListBO=objRecommendationListBO;
        [unsortedArr addObject:patCase];
    }
    
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        finalArray = [sortedArray mutableCopy];
        sortedArray = nil;
           }
    
   else
   {
       NSMutableArray *unsortedArr = [NSMutableArray array];
       for(PetrawinCasesList *caseDict in allCasesArray)
       {
           PatrawinCase *patCase = [[PatrawinCase alloc] init];
           patCase.account_cost=caseDict.accountCost;
           patCase.agent_id =caseDict.agentId;
           patCase.arrived_date = [NSString stringWithFormat:@"%@",caseDict.arrivedDate];
           //patCase.case_id = [NSString stringWithFormat:@"%@",caseDict.caseId];
           patCase.case_id = caseDict.caseId;
           patCase.comment_case = caseDict.caseComment;
           patCase.case_type = caseDict.caseType;
           patCase.comment = caseDict.comment;
           patCase.country_code = caseDict.countrycode;
           patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
           patCase.customer_name=caseDict.customerName;
           patCase.deadline=caseDict.deadline;
           patCase.figure=caseDict.figure;
           patCase.figureSmall = caseDict.figuresmall;
           patCase.link=caseDict.link;
           patCase.patent_number = caseDict.patentNo;
           patCase.title = caseDict.title;
           patCase.status = caseDict.status;
           patCase.keyword = caseDict.keyword;
           patCase.Recommendation_Text = caseDict.recommendationText;
           
           RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
           objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
           objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
           objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];
           patCase.objRecommendationListBO=objRecommendationListBO;
           
           [unsortedArr addObject:patCase];
       }
       NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
       NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
       NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
       finalArray = [sortedArray mutableCopy];
       sortedArray = nil;
   }

    
    

    [self.caseTableView reloadData];


        }







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}



@end
