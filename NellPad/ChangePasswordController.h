//
//  ChangePasswordController.h
//  NellPad
//
//  Created by Rakesh Kumar on 12/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kTxtFEmail 8997

@interface ChangePasswordController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    UITextField *txtFieldTemp;
    
    int ii;
}

@property(nonatomic,strong) NSMutableArray *arrData;
@property(nonatomic,strong) UIScrollView *scrollBar;
@property (strong,nonatomic) NSDictionary *responseDict;
@property(nonatomic,strong) NSString *emailForChngePass;


@end
