//
//  PatrawinCasesVC.m
//  NellPad
//
//  Created by Herry Makker on 12/9/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import "PatrawinCasesVC.h"
#import "CasesVC.h"
#import "PatrawinCase.h"
#import "RecommendationsListBO.h"
#import "PetrawinCasesList.h"
#import "CompanyList.h"
#import "CompanyBO.h"
#import "RecommendationsList.h"
#import "DataSyncBO.h"
#import "CompaniesListVC.h"


#import <objc/message.h>

@interface PatrawinCasesVC ()
{
    UIView *view;
    UITextField *sendTo;
    UIButton *dropdownButton;
    NSArray *caseArray;
    NSArray *arrRecommendation;
    UISearchBar *searchBar1;
    UISearchDisplayController *searchDisplayController;
    NSArray *searchResults;
    
    NSString *other4;
    
    NSMutableArray *arrayListOfCompaniesName, *arrayListOfCompaniesId;
    CompanyBO *companyBo;
    
    CasesVC *obj_CaseVC;
    NSArray *Recommendation_TextArray;
    NSArray *arrayForTable;
    NSMutableArray *unique;
    UIRefreshControl *refresh;
    DataSyncBO *dataBO;
}
@end

@implementation PatrawinCasesVC
@synthesize casesArray,arrRecommendations,actionSheetSort,textSortingCompany,viewForAgentsListBG,viewForAgentsList;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    obj_CaseVC  = [[CasesVC alloc] init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.arrRecommendations = [[NSMutableArray alloc]init];
    
    searchResults = [[NSArray alloc] init];
    arrayListOfCompaniesName = [[NSMutableArray alloc] init];
    arrayListOfCompaniesId = [[NSMutableArray alloc] init];

    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]){
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
        }
    }
    self.title  = @"All Cases";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont systemFontOfSize: 18], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    
    
     NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    NSString *actionSheetTitle = @"Choose Sorting Type";
  
    NSString *other1 = @"Sort By Latest";
    
    NSString *other2 = @"Sort By Oldest";
    
    NSString *other3 = @"Sort By Deadline";
    
    if ([strKey isEqualToString:@"0"])
    {
         other4 = @"Sort By Company";
    }
    if ([strKey isEqualToString:@"1"])
    {
        other4 = nil;
    }
    
    NSString *cancelTitle = @"Cancel";
    
    actionSheetSort = [[UIActionSheet alloc] initWithTitle:actionSheetTitle delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:other1, other2,other3,other4, nil];

    [self configureView];
    
    self.casesArray = [[NSMutableArray alloc]init];
   
    
    [self fetchFromDatabase];

    
    //refresh control
    refresh = [[UIRefreshControl alloc]init];
    [self.caseTableView addSubview:refresh];
    [refresh addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    //[self refreshTable];
    
    
    UIBarButtonItem *btnRefresh=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"refresh-top"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshAllCases)];
    
    self.navigationItem.rightBarButtonItem=btnRefresh;


    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

    
}

- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];

   // [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}

-(void)refreshAllCases
{
    [[[UIAlertView alloc]initWithTitle:@"Update Cases!" message:@"Do you want to update cases?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] show ];

    
}

#pragma mark - uialert view methods==========

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        [refresh endRefreshing];
        
    } else if(buttonIndex == 1) {
        
         //[self refreshTable];
       // [self.casesArray removeAllObjects];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"CaseListdownloaded" object:nil];
        DataSyncBO *refreshData = [[DataSyncBO alloc]init];
        [refreshData caseList];

    }
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}



-(void)refreshTable
{
    
    [[[UIAlertView alloc]initWithTitle:@"Update Cases!" message:@"Do you want to update cases?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] show ];

//    [self.casesArray removeAllObjects];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"CaseListdownloaded" object:nil];
//    DataSyncBO *refreshData = [[DataSyncBO alloc]init];
//    [refreshData caseList];
    
}

-(void) DataReload
{
     [self fetchFromDatabase];
     [refresh endRefreshing];
     [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)dropDown
{
    [self.view endEditing:YES];
    
    [actionSheetSort showInView:self.view];
}



-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];

    if ([buttonTitle isEqualToString:@"Sort By Latest"])
    {
        sendTo.text = @"   Sort By Latest";
        [self sortCasesDescending];
    }
    else if ([buttonTitle isEqualToString:@"Sort By Oldest"])
    {
        sendTo.text = @"   Sort By Oldest";
        [self sortCasesAscending];
    }
    else if([buttonTitle isEqualToString:@"Sort By Deadline"])
    {
        sendTo.text = @"   Sort By Deadline";
       [self sortCasesByDeadline];
    }
    else if([buttonTitle isEqualToString:@"Sort By Company"])
    {
        sendTo.text = @"   Sort By Company";
        
        CompaniesListVC* companiesTable = [[CompaniesListVC alloc] init];
        companiesTable.statsString = @"All Cases";
        [self.navigationController pushViewController:companiesTable animated:YES];
        
    }
    else if ([buttonTitle isEqualToString:@"Cancel"])
    {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
}




-(void)configureView
{

    [self.view endEditing:YES];
    
    UIView *_bgview=[[UIView alloc]init];
    _bgview.frame=CGRectMake(0, 0, self.view.frame.size.width, 70);
    _bgview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:_bgview];
    
    sendTo = [[UITextField alloc] init];
    sendTo.frame = CGRectMake(20, 20, deviceWidth-40, 30);
   // sendTo.layer.cornerRadius = 5.0;
    sendTo.userInteractionEnabled = NO;
    sendTo.layer.borderWidth = 0.5f;
    sendTo.text = @"   Sort By Latest";
    sendTo.textColor = [UIColor darkGrayColor];
    sendTo.clipsToBounds = YES;
    sendTo.layer.borderColor = UIColorFromRGB(0xA9A9A9).CGColor;
    sendTo.delegate = self;
    sendTo.backgroundColor=[UIColor clearColor];
    sendTo.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    sendTo.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:sendTo];
    
    dropdownButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dropdownButton.frame = CGRectMake(20, 20, deviceWidth-40, 30);
   // dropdownButton.layer.cornerRadius = 5.0;
    UIImage *image=[UIImage imageNamed:@"searchBox.png"];
    [dropdownButton setBackgroundImage:image forState:UIControlStateNormal];
    //dropdownButton.imageEdgeInsets = UIEdgeInsetsMake(0., dropdownButton.frame.size.width - (image.size.width + 15.), 0., 0.);

    dropdownButton.clipsToBounds = YES;
    [dropdownButton addTarget:self action:@selector(dropDown) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:dropdownButton];
    
    
    searchBar1 = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 60, IS_IPAD ? 768 : 320, 40)];
    
    searchBar1.placeholder = @"Search Cases";
    
    searchBar1.delegate = self;
 //   [searchBar1 setImage:[UIImage imageNamed:@""] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self.view addSubview:searchBar1];
    for (UIView *searchBarSubview in [searchBar1 subviews]) {
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            @try {
                
                [(UITextField *)searchBarSubview setBorderStyle:UITextBorderStyleLine];
            }
            @catch (NSException * e) {
                // ignore exception
            }
        }
    }
    self.caseTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, searchBar1.frame.size.height+searchBar1.frame.origin.y, deviceWidth, deviceHeight-160) style:UITableViewStylePlain];
  
    self.caseTableView.delegate=self;
    self.caseTableView.dataSource=self;
    self.caseTableView.backgroundColor=[UIColor whiteColor];
    self.caseTableView.rowHeight = IS_IPAD ? 60 : 50.0f;
    self.caseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.caseTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.caseTableView];
    self.view.layer.borderColor = UIColorFromRGB(0xF0F0F0).CGColor;
    
    
    
}



- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] > 0) {
        
//        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.case_type contains[c] %@", searchText];
        
        NSMutableArray *sResult = @[].mutableCopy;
        
        for (PatrawinCase *cs in self.casesArray) {
            
            NSRange r = [cs.case_id rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (r.length > 0) {
               // NSLog(@"case is's%@", cs.case_id);
                [sResult addObject:cs];
                
            }
        }
        
        if (searchResults) {
            searchResults = nil;
        }
        
        searchResults = [NSArray arrayWithArray:sResult];
        
        sResult = nil;
    } else {
        searchResults = [NSArray arrayWithArray:self.casesArray];
    }
//    [searchResults addObjectsFromArray:[self.casesArray filteredArrayUsingPredicate:resultPredicate]];
    
    [self.caseTableView reloadData];
}



#pragma mark - UISearchField Delegates

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
}

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.case_type contains[c] %@", searchText];
//
//    searchResults = [self.casesArray filteredArrayUsingPredicate:resultPredicate];
//    
////    [searchResults addObjectsFromArray:[self.casesArray filteredArrayUsingPredicate:resultPredicate]];
//    
//    NSLog(@"Search Array = %@", searchResults);
//}

//- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//{
//    [self filterContentForSearchText:searchText
//                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                      objectAtIndex:[self.searchDisplayController.searchBar
//                                                     selectedScopeButtonIndex]]];
//}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

-(void)caseList
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];

    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"petrawincasesofagent";
    
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    
    if ([strKey isEqualToString:@"1"])
    {
        [inputParams.dict_postParameters setObject:@"2" forKey:@"status_id"];
    }
    else if ([strKey isEqualToString:@"0"])
    {
        [inputParams.dict_postParameters setObject:@"1" forKey:@"status_id"];
    }

    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         }
         else
         {
             error = nil;
          
             responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
             
             NSLog(@"Response Dict = %@", responseDict);
             
           [self performSelectorOnMainThread:@selector(sortCasesDescending) withObject:self waitUntilDone:YES];
         }
     }];
}



-(void)sortCasesDescending
{
    
    [casesArray removeAllObjects];
    [self.arrRecommendations removeAllObjects];
    
    caseArray = responseDict[@"data"];
    arrRecommendation=responseDict[@"recommendation_list"];
    
    //Recommendation_TextArray = [caseArray valueForKey:@"Recommendation_Text"];
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ([strKey isEqualToString:@"1"])
    {
        NSMutableArray *unsortedArr = [NSMutableArray array];
        for(PetrawinCasesList *caseDict in caseArray)
        {
            PatrawinCase *patCase = [[PatrawinCase alloc] init];
            patCase.account_cost=caseDict.accountCost;
            patCase.agent_id =caseDict.agentId;
            patCase.arrived_date =[NSString stringWithFormat:@"%@",caseDict.arrivedDate];
            //patCase.case_id =[NSString stringWithFormat:@"%@",caseDict.caseId];
            patCase.case_id = caseDict.caseId;
            patCase.comment_case =caseDict.caseComment;
            patCase.case_type =caseDict.caseType;
            patCase.comment =caseDict.comment;
            patCase.country_code =caseDict.countrycode;
            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
            patCase.customer_name=caseDict.customerName;
            patCase.deadline=caseDict.deadline;
            patCase.figure=caseDict.figure;
            patCase.figureSmall = caseDict.figuresmall;
            patCase.link=caseDict.link;
            patCase.patent_number =caseDict.patentNo;
            patCase.title =caseDict.title;
            patCase.status =caseDict.status;
            patCase.Recommendation_Text = caseDict.recommendationText;
            patCase.keyword = caseDict.keyword;
            patCase.deadline_code=caseDict.deadlineCode;
            
            //            for(RecommendationsList *dictRecommendation in arrRecommendation)
            //            {
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];;
            patCase.objRecommendationListBO=objRecommendationListBO;
            [unsortedArr addObject:patCase];
            //            }
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        self.casesArray = [sortedArray mutableCopy];
        sortedArray = nil;
        
    }
    else if ([strKey isEqualToString:@"0"])
    {
        for (RecommendationsList *dictRecommendation in arrRecommendation)
        {
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = dictRecommendation.recommendation_ID;
            objRecommendationListBO.strRecommendationText = dictRecommendation.recommendation_Text;
           // objRecommendationListBO.strRecommendationStatus = dictRecommendation.recommendation_Status;
            [self.arrRecommendations addObject:objRecommendationListBO];
        }
        
        NSMutableArray *unsortedArr = [NSMutableArray array];
        for(PetrawinCasesList *caseDict in caseArray)
        {
            PatrawinCase *patCase = [[PatrawinCase alloc] init];
            patCase.account_cost=caseDict.accountCost;
            patCase.agent_id = caseDict.agentId;
            patCase.arrived_date = [NSString stringWithFormat:@"%@",caseDict.arrivedDate];
            //patCase.case_id = [NSString stringWithFormat:@"%@",caseDict.caseId];
            patCase.case_id = caseDict.caseId;
            patCase.comment_case = caseDict.caseComment;
            patCase.case_type = caseDict.caseType;
            patCase.comment = caseDict.comment;
            patCase.country_code = caseDict.countrycode;
            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
            patCase.customer_name=caseDict.customerName;
            patCase.deadline=caseDict.deadline;
            patCase.figure=caseDict.figure;
            patCase.figureSmall = caseDict.figuresmall;
            patCase.link=caseDict.link;
            patCase.patent_number = caseDict.patentNo;
            patCase.title = caseDict.title;
            patCase.status = caseDict.status;
            patCase.Recommendation_Text = caseDict.recommendationText;
            patCase.keyword = caseDict.keyword;
            patCase.deadline_code = caseDict.deadlineCode;
            patCase.Recommendation_ID = caseDict.recommendationID;
            
            
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];;
            patCase.objRecommendationListBO=objRecommendationListBO;
            [unsortedArr addObject:patCase];
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        self.casesArray = [sortedArray mutableCopy];
        sortedArray = nil;
        
        
    }
        else
        {
            NSMutableArray *unsortedArr = [NSMutableArray array];
            for(PetrawinCasesList *caseDict in caseArray)
            {
                PatrawinCase *patCase = [[PatrawinCase alloc] init];
                patCase.account_cost=caseDict.accountCost;
                patCase.agent_id =caseDict.agentId;
                patCase.arrived_date = [NSString stringWithFormat:@"%@",caseDict.arrivedDate];
                //patCase.case_id = [NSString stringWithFormat:@"%@",caseDict.caseId];
                patCase.case_id = caseDict.caseId;
                patCase.comment_case = caseDict.caseComment;
                patCase.case_type = caseDict.caseType;
                patCase.comment = caseDict.comment;
                patCase.country_code = caseDict.countrycode;
                patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
                patCase.customer_name=caseDict.customerName;
                patCase.deadline=caseDict.deadline;
                patCase.figure=caseDict.figure;
                patCase.figureSmall = caseDict.figuresmall;
                patCase.link=caseDict.link;
                patCase.patent_number = caseDict.patentNo;
                patCase.title = caseDict.title;
                patCase.status = caseDict.status;
                 patCase.keyword = caseDict.keyword;
                patCase.Recommendation_Text = caseDict.recommendationText;
    
                RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
                objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
                objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
                objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];
                patCase.objRecommendationListBO=objRecommendationListBO;
    
                [unsortedArr addObject:patCase];
            }
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
            self.casesArray = [sortedArray mutableCopy];
            sortedArray = nil;
        }
    
    searchResults = [NSArray arrayWithArray:self.casesArray];
    [self.caseTableView reloadData];
    
}


-(void)sortCasesAscending
{
    NSMutableArray *unsortedArray = [self.casesArray mutableCopy];
    [self.casesArray removeAllObjects];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    self.casesArray = [sortedArray mutableCopy];
    sortedArray = nil;
    
    searchResults = [NSArray arrayWithArray:self.casesArray];
    [self.caseTableView reloadData];
}

-(void)sortCasesByDeadline
{
    NSMutableArray *unsortedArray = [self.casesArray mutableCopy];
    [self.casesArray removeAllObjects];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deadline" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    NSMutableArray *sortedwithoutZero = [[NSMutableArray alloc]init];
    for(int i=0;i<sortedArray.count;i++)
    {
        PatrawinCase *c=[sortedArray objectAtIndex:i];
        if(![c.deadline isEqualToString:@""])
        {
            [sortedwithoutZero addObject:c];
        }
    }
    self.casesArray = [sortedwithoutZero mutableCopy];
    sortedwithoutZero=nil;
    sortedArray = nil;
    
    searchResults = [NSArray arrayWithArray:self.casesArray];
    [self.caseTableView reloadData];
}

#pragma mark - Table view Datasource & Delegate Methods 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==_caseTableView)
    {
        return IS_IPAD ? 61 : 71.0f;
    }
    else
        return IS_IPAD ? 55 : 44;
    
    
   // return IS_IPAD ? 61 : 71.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
    
//    if (tableView == _caseTableView)
//    {
//        if (searchResults.count==0)
//        {
//            return 1;
//        }
        return searchResults.count;
//    }
//    else
//    {
//        return [arrayListOfCompaniesName count];
//    }

    
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
    }
    cell.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
//    if ([arrayForTable count]>0)
//    {
////        NSLog(@" table view content  = %@",[arrayForTable lastObject]);// this doesn't log
//        PetrawinCasesList *patCase = [searchResults objectAtIndex:indexPath.row];
//    
//        
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//     
//        cell.textLabel.text = [NSString  stringWithFormat:@"Case : %@ ", patCase.caseId];
//        cell.textLabel.textColor= UIColorFromRGB(0x4174DA);
//        cell.textLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 20 : 16.0];
//    
//        cell.detailTextLabel.text = patCase.title;
//        cell.detailTextLabel.textColor=[UIColor darkGrayColor];
//        cell.detailTextLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 20 : 16.0];
  
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
//    if (self.casesArray.count==0)
    if (tableView == _caseTableView)
    {
    if (searchResults.count==0)
    {
        UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?380: 160,IS_IPAD? 768: 320, 30)];
        
        if (responseDict.count==0)
        {
            [lblNoRecord setText:@""];
        }
        else
        {
            [lblNoRecord setText:@"No Record Available."];
        }
        [lblNoRecord setTextColor:[UIColor lightGrayColor]];
        lblNoRecord.textAlignment = NSTextAlignmentCenter;
        [lblNoRecord setBackgroundColor:[UIColor clearColor]];
        [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
        [cell.contentView addSubview:lblNoRecord];
        cell.backgroundColor=[UIColor clearColor];
    }
        
    else
    {
        PatrawinCase *patCase = searchResults[indexPath.row];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.textLabel.text=[NSString stringWithFormat:@"Case: %@",GET_VALID_STRING(patCase.case_id)];
        
        cell.detailTextLabel.text=GET_VALID_STRING(patCase.title);
        
        cell.textLabel.textColor= [UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
        
        cell.textLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 18 : 16.0];
        
        cell.detailTextLabel.textColor=[UIColor darkGrayColor];
        
        cell.detailTextLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 16 : 12.0];
    }
        
    cell.accessoryView = [[ UIImageView alloc ]
                          initWithImage:[UIImage imageNamed:@"arrowMessage"]];

    UIView *_lineDiv=[[UIView alloc] init];
    _lineDiv.frame=CGRectMake(0, IS_IPAD ? 60 : 70.0f, self.view.frame.size.width, 1.0);
    _lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
    [cell addSubview:_lineDiv];
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];

    if(tableView == self.caseTableView)
    {
//        if (self.casesArray.count==0)
        if (searchResults.count==0)
        {
            
        }
        else
        {
            NSLog(@"array list %@",searchResults);
            obj_CaseVC.strSelectedIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
//          obj_CaseVC.arrayAllCases=[[NSMutableArray alloc]initWithArray:self.casesArray];
            obj_CaseVC.arrayAllCases = [[NSMutableArray alloc]initWithArray:searchResults];
            obj_CaseVC.casesScreentype = @"All Cases";
            
            if ([strKey isEqualToString:@"1"])
            {
                
            }
            else if ([strKey isEqualToString:@"0"])
            {
                obj_CaseVC.arrRecommendationListBO=self.arrRecommendations;
                NSLog(@"%i", obj_CaseVC.arrRecommendationListBO.count);
            }
            else{
                NSLog(@"Logic testing");
            }
    
            [self.navigationController pushViewController:obj_CaseVC animated:YES];
        }
    }
}

-(void)fetchFromDatabase
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"PetrawinCasesList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];

    NSEntityDescription *entityDesc1 = [NSEntityDescription entityForName:@"RecommendationsList" inManagedObjectContext:context];
    NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
    [request1 setEntity:entityDesc1];
    
    NSError *error1;
    NSArray *fetchedArray1 = [context executeFetchRequest:request1 error:&error1];

    responseDict =[NSMutableDictionary new];
    [responseDict setValue:fetchedArray forKey:@"data"];
    [responseDict setValue:fetchedArray1 forKey:@"recommendation_list"];
   
    //NSLog(@"The array is %i", fetchedArray1.count);
    
    if (fetchedArray.count == 0) {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"CaseListdownloaded" object:nil];
        dataBO=[DataSyncBO new];
        [dataBO caseList];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(sortCasesDescending) withObject:self waitUntilDone:YES];
    }
}






@end
