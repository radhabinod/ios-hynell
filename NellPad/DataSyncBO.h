//
//  DataSyncBO.h
//  NellPat
//
//  Created by Macbook on 6/10/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataSyncBO : NSObject
{
    NSArray *staticsArray;
}
-(void)LoginCall;
-(void) caseList;
-(void)CallForStatsData:(NSString*)selectedStr;
-(void)CallForStaticsData;
- (void) callForListOfCompanies;
- (void)deleteEntitiesWRTSelectedStr:(NSString *)nameEntity withstr:(NSString *) selectedStr;
- (void)deleteAllEntities:(NSString *)nameEntity;

-(void)ParseAndSaveData :(NSDictionary *) caseDict;

@end
