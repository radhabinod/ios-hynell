
//
//  MainMenuVC.m
//  NellPad
//
//  Created by Herry Makker on 12/5/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#define kTitleKey    @"title"
#define kValueKey    @"value"
#define kImageKey    @"image"
#define kWebAddKey   @"webAdd"


#import "MainMenuVC.h"
#import "ProjectVC.h"
#import "LoginVC.h"
#import "MBProgressHUD.h"
#import "LoginVC.h"
#import "IPManagementVC.h"
#import <objc/message.h>


@interface MainMenuVC () {
    BOOL orientationNeed;
    UIButton *button;
    CGRect contentFrame;
    CGRect menuFrame;
    float menuWidth;
    CGRect masterRect;
    
     UIAlertView *downloadAlert;
//    UIView *view;
}

@end

@implementation MainMenuVC
@synthesize myTimer;
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.frame = CGRectMake(0, 0, 120, 143);
//    
//   // hud.mode = MBProgressHUDModeAnnularDeterminate;
//    hud.mode=MBProgressHUDModeDeterminate;
//    NSString *strloadingText = [NSString stringWithFormat:@"Loading Data."];
//    NSString *strloadingText2 = [NSString stringWithFormat:@" Please Wait.\r 1-2 Minutes"];
//    
//    NSLog(@"the loading text will be %@",strloadingText);
//    hud.labelText = strloadingText;
//    hud.detailsLabelText=strloadingText2;
    //  hud.userInteractionEnabled = YES;
  //  hud.labelText = @"Loading..";
//    hud.dimBackground = YES;
}


-(void)hideLoadingView
{
  //  [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (id)init
{
    self = [super init];
    if (self)
    {
    
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            self.view.frame = CGRectMake(0, 0, 320, 568 );
            menuWidth = 255.0;
        }
        else
        {
            self.view.frame = CGRectMake(0, 0, 768, 1024);
            menuWidth = 660.0;
        }
        
        masterRect = self.view.bounds;
//=======================================================================================================================
        
        self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, masterRect.size.width, masterRect.size.height)];
       // self.leftView.backgroundColor = UIColorFromRGB(0x4174DA);
        self.leftView.backgroundColor = UIColorFromRGB(0x2C76E2);
        self.leftView.layer.shadowColor = [[UIColor colorWithRed: 0.333 green: 0.333 blue: 0.333 alpha: 1] CGColor];
        self.leftView.layer.borderWidth = 0.5;
        self.leftView.layer.borderColor = [[UIColor colorWithRed: 0.333 green: 0.333 blue: 0.333 alpha: 1] CGColor];
        
      //  self.view.backgroundColor=UIColorFromRGB(0x4174DA);

   //     self.view.backgroundColor=   UIColorFromRGB(0xFFFFFF);
//        view = [[UIView alloc] init];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        theLabel = [[UILabel alloc] init];
        theLabel.text = @"Home";
        theLabel.textAlignment=NSTextAlignmentCenter;
        theLabel.textColor = UIColorFromRGB(0xFFFFFF);
        //        view.backgroundColor=UIColorFromRGB(0x4174DA);
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            menuFrame = CGRectMake(65.0, 0.0, menuWidth, masterRect.size.height);
            contentFrame = CGRectMake(0.0, 64.0, masterRect.size.width, masterRect.size.height - 64.0);
//            view.frame = CGRectMake(0, 0, 320, 64);
            theLabel.frame = CGRectMake(10, 25, 300, 30);
            theLabel.font = [UIFont boldSystemFontOfSize:18];
            self.menuTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 64.0, menuWidth, masterRect.size.height)];
            [button setFrame:CGRectMake(275, 20, 40, 40)];
            
        } else {
            
            menuFrame = CGRectMake(130.0, 0.0, menuWidth, masterRect.size.height);
            contentFrame = CGRectMake(0.0, 128.0, masterRect.size.width, masterRect.size.height - 128.0);
//            view.frame = CGRectMake(0, 0, 768, 70);
            theLabel.frame = CGRectMake(230, 12, 300, 60);
            theLabel.font = [UIFont boldSystemFontOfSize:18];
            self.menuTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 64.0, menuWidth, masterRect.size.height)] ;
            
            self.menuTableView.rowHeight = 55;
            
            [button setFrame:CGRectMake(690, 0, 80, 80)];
        }
        
        [self.leftView addSubview:theLabel];
        self.menuTableView.dataSource = self;
        self.menuTableView.delegate = self;
        self.menuView = [[UIView alloc] initWithFrame:menuFrame];
        self.menuView.layer.shadowColor = [[UIColor colorWithRed: 0.333 green: 0.333 blue: 0.333 alpha: 1] CGColor];
        self.menuView.layer.borderWidth = 0.5;
        self.menuView.layer.borderColor=[[UIColor colorWithRed: 0.333 green: 0.333 blue: 0.333 alpha: 1] CGColor];
        
        [self.menuView addSubview:self.menuTableView];
//        [self.menuView addSubview:view];
        
        
        self.webView = [[UIWebView  alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            
            if (IS_IPHONE_5)
            {
                self.webView.frame = contentFrame;
            }
            else
            {
                self.webView.frame = CGRectMake(0, 70, 320, 410);
            }
            
            self.webView.autoresizesSubviews = YES;
            
            self.webView.autoresizingMask  =(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
        }
        
        else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            self.webView.frame = CGRectMake(0, 64, 768, 960);
        }
        self.webView.autoresizesSubviews = YES;
        self.webView.autoresizingMask  =(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
        [self.webView setDelegate:self];
        [self.leftView addSubview:self.webView];
//====================================================================================================================
       // self.webView.backgroundColor=UIColorFromRGB(0x4174DA);
         self.webView.backgroundColor=UIColorFromRGB(0x2C76E2);
    /// change URL
        
        NSString* urlAddress  = @"http://www.hynell.se/";
       // NSString* urlAddress  = @"";

        NSURL* url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestAddress = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:requestAddress];
        
        
        [self.view addSubview:self.menuView];
        [self.view insertSubview:self.leftView aboveSubview:self.menuView];
        
        [button setImage:[UIImage imageNamed:@"Out.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.leftView addSubview:button];
        
    }
    return self;
}


- (void)viewDidLoad
{
    orientationNeed = NO;
    [super viewDidLoad];
    
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    
    NSArray *arrayImages = @[@"about.png",@"who_we_are.png",@"what_we_do.png",@"telephone.png"];
    NSArray *array2 = @[@"What's Hynell",@"Who We Are",@"What We Do",@"Contact Us"];
    
//    NSArray *webAddArray = @[@"http://www.nellpat.se/mobil/?page_id=12", @"http://www.nellpat.se/mobil/?page_id=18", @"http://www.nellpat.se/mobil/?page_id=16", @"http://www.nellpat.se/mobil/?page_id=20", @"http://www.nellpat.se/mobil/?page_id=22"];
    
    NSArray *webAddArray = @[@"http://www.hynell.se/our-services/", @"http://www.hynell.se/co-workers/", @"http://www.hynell.se/patents/", @"http://www.hynell.se/contact/varmland/"];

    
    NSMutableDictionary *itemTwo = [@{ kTitleKey : @"Information", kValueKey : array2 , kImageKey :arrayImages, kWebAddKey :webAddArray} mutableCopy];
  //  NSMutableDictionary *itemThree = [@{ kTitleKey :@"Patent Search" } mutableCopy];
   // NSMutableDictionary *itemFour = [@{ kTitleKey :@"International Patent Search" } mutableCopy];
    NSMutableDictionary *itemFour = [@{ kTitleKey : @"IP Management" } mutableCopy];
    NSMutableDictionary *itemThree = [@{ kTitleKey :@"IPR Control" } mutableCopy];

    self.arrayTable= @[itemTwo, itemThree, itemFour/*,itemFive,itemSix*/];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    
    [self willRotateToInterfaceOrientation:UIInterfaceOrientationPortrait duration:0.0f];
}

#pragma mark UIViewControllerRotation Methods 

- (BOOL)shouldAutorotate
{
    if (orientationNeed)
    {
        return YES;
    }
    else if ([self interfaceOrientation] != UIInterfaceOrientationPortrait)
    {
        return YES;
    }
    
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    if (orientationNeed)
    {
        return UIInterfaceOrientationPortrait | UIInterfaceOrientationLandscapeRight;
    }
    return UIInterfaceOrientationPortrait;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    if (orientationNeed)
    {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait)
        {
            NSLog(@"Portrait");
            
            if (IS_IPHONE_5)
            {
                self.leftView.frame = CGRectMake(0.0, 0.0, masterRect.size.width, masterRect.size.height);
                
                theLabel.frame = CGRectMake(10, 25, 300, 30);
                
                [button setFrame:CGRectMake(275, 20, 40, 40)];
                
                self.menuView.frame = menuFrame;
                
                self.menuTableView.frame = CGRectMake(0.0, 64.0, menuWidth, masterRect.size.height) ;
                
                self.webView.frame = contentFrame;
            }
            else if (IS_IPHONE)
            {
                self.leftView.frame = CGRectMake(0.0, 0.0, masterRect.size.width, masterRect.size.height);
                
                theLabel.frame = CGRectMake(10, 25, 300, 30);
                
                [button setFrame:CGRectMake(275, 20, 40, 40)];
                
                self.menuView.frame = CGRectMake(250, 64, 250, 320);
                
                self.menuTableView.frame = CGRectMake(0.0, 64.0, menuWidth, masterRect.size.height) ;
                
                self.webView.frame = CGRectMake(0, 64, 320, 416);
            }
            else
            {
                self.leftView.frame = CGRectMake(0.0, 0.0, masterRect.size.width, masterRect.size.height);
                
                theLabel.frame = CGRectMake(240, 25, 300, 30);
                
                [button setFrame:CGRectMake(715, 20, 40, 40)];
                
                self.menuView.frame = menuFrame;
                
                self.menuTableView.frame = CGRectMake(0.0, 64.0, menuWidth, masterRect.size.height) ;
                
                self.webView.frame = CGRectMake(0, 64, 768, 960);

            }
          
        }
        else
        {
            NSLog(@"Landscape");

            if (IS_IPHONE_5)
            {
                self.leftView.frame = CGRectMake(0.0, 0.0, 568, 568);
                
                theLabel.frame = CGRectMake(135, 12, 300, 60);
                
                [button setFrame:CGRectMake(IS_IPAD ? 700 : 500, 0, 80, 80)];
                
                self.menuView.frame = CGRectMake(318, 64, 250, 320);
                
                self.menuTableView.frame = CGRectMake(0, 0.0, 250, 320) ;
                
                self.webView.frame = CGRectMake(0, 64, 568, 256);

            }
           else if (IS_IPHONE)
           {
               self.leftView.frame = CGRectMake(0.0, 0.0, 568, 568);
               
               theLabel.frame = CGRectMake(90, 12, 300, 60);
               
               [button setFrame:CGRectMake(415, 0, 80, 80)];
               
               self.menuView.frame = CGRectMake(318, 64, 250, 320);
               
               self.menuTableView.frame = CGRectMake(0, 0.0, 250, 320) ;
               
               self.webView.frame = CGRectMake(0, 64, 480, 256);
           }
            else
            {
            self.leftView.frame = CGRectMake(0.0, 0.0, 1025, 768);
            
            theLabel.frame = CGRectMake(363, 25, 300, 30);
            
            [button setFrame:CGRectMake(IS_IPAD ? 940 : 500, 0, 80, 80)];
            
            self.menuView.frame = CGRectMake(700, 64, 320, 980);

            self.menuTableView.frame = CGRectMake(0.0, 0.0, 340, 960) ;
            
            self.webView.frame = CGRectMake(0, 64, 1024, 704);
            }
        }
    }
    else
    {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait)
        {
            NSLog(@"Portrait");
            
            if (IS_IPHONE)
            {
                self.leftView.frame = CGRectMake(0.0, 0.0, masterRect.size.width, masterRect.size.height);
                
                theLabel.frame = CGRectMake(10, 25, 300, 30);
                
                [button setFrame:CGRectMake(275, 20, 40, 40)];
                
                self.menuView.frame = menuFrame;
                
                self.menuTableView.frame = CGRectMake(0.0, 64.0, menuWidth, masterRect.size.height) ;
                
                self.webView.frame = contentFrame;
            }
            else
            {
                self.leftView.frame = CGRectMake(0.0, 0.0, masterRect.size.width, masterRect.size.height);
                
                theLabel.frame = CGRectMake(240, 25, 300, 30);
                
                [button setFrame:CGRectMake(715, 20, 40, 40)];
                
                self.menuView.frame = menuFrame;
                
                self.menuTableView.frame = CGRectMake(0.0, 64.0, menuWidth, masterRect.size.height) ;
                
                self.webView.frame = CGRectMake(0, 64, 768, 960);
                
            }
            
        }

    }
}

#pragma mark TableView Delegate Methods 

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (section >= 1)
        return NO;
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            return 5;
        }
        
        return 1;
    }
    
    return 1;
}

#pragma mark TableView DataSource Methods 
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    // Configure the cell...
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            cell.textLabel.text = self.arrayTable[indexPath.section][kTitleKey];
            cell.indentationLevel = 0.0;
            cell.textLabel.font = [UIFont systemFontOfSize:IS_IPAD ? 20 : 16];
            cell.backgroundColor = [UIColor whiteColor];
            cell.accessoryType = UITableViewCellAccessoryNone;
            if ([expandedSections containsIndex:indexPath.section])
            {
                //show accessory if it contains indices
            }
            else
            {
                //hide accessory if it contains indices
            }
        }
        else
        {
            cell.textLabel.text = self.arrayTable[indexPath.section][kValueKey][indexPath.row-1];
            UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
            imageview.image=[UIImage imageNamed:self.arrayTable[indexPath.section][kImageKey][indexPath.row-1]];
            if (IS_IPAD)
            {
                imageview.frame=CGRectMake(600, 5, 32, 32);
                [cell.contentView addSubview:imageview];
            }
            else
            {
                cell.accessoryView = imageview;
                
            }
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.indentationLevel=2.0;
            cell.textLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 20 : 16];
            cell.backgroundColor = UIColorFromRGB(0xFFFFFF);
        }
    }
    else
    {
        cell.textLabel.text = self.arrayTable[indexPath.section][kTitleKey];
        cell.indentationLevel=0.0;
        cell.textLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 20 : 16];
        cell.backgroundColor = UIColorFromRGB(0xFFFFFF);
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    orientationNeed = NO;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        
        if (!indexPath.row)
        {
            // only first row toggles exapand/collapse
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSections removeIndex:section];
            }
            else
            {
                [expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            for (int i = 1; i < rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i inSection:section];
                
                [tmpArray addObject:tmpIndexPath];
            }
            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray withRowAnimation:UITableViewRowAnimationTop];
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray  withRowAnimation:UITableViewRowAnimationTop];
            }
        }
        else
        {
            
            theLabel.text = self.arrayTable[indexPath.section][kValueKey][indexPath.row - 1];
            NSString *urlPart = self.arrayTable[indexPath.section][kWebAddKey][indexPath.row - 1];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlPart]];
            
            if(UIDeviceOrientationIsLandscape(self.interfaceOrientation))
            {
                if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
                {
                    objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
                    
                    
                }
            }
            
            NSURLRequest *requestAddress = [NSURLRequest requestWithURL:url];
            [self.webView loadRequest:requestAddress];
            [self tableView:tableView didSelectRowAtIndexPath:0];
            [self toggleMenu];
            
        }
    }
    else if(indexPath.section == 1)
        {
                    orientationNeed = YES;
                    theLabel.text = @"IPR Control";
                    NSString* urlAddress  = @"https://hynell.iprcontrol.com/WebForms/Login.aspx";
                    NSURL* url = [NSURL URLWithString:urlAddress];
                    NSURLRequest *requestAddress = [NSURLRequest requestWithURL:url];
                    [self.webView loadRequest:requestAddress];
                    [self toggleMenu];
                    //        [self hideLoadingView];

            
        }
//    else if(indexPath.section == 1)
//    {
//        orientationNeed = YES;
//        theLabel.text = @"Patent Search";
//        NSString* urlAddress  = @"http://was.prv.se/spd/search?lang=sv";
//        NSURL* url = [NSURL URLWithString:urlAddress];
//        NSURLRequest *requestAddress = [NSURLRequest requestWithURL:url];
//        [self.webView loadRequest:requestAddress];
//        [self toggleMenu];
//        //        [self hideLoadingView];
//        
//    }
    
//    else if(indexPath.section == 2)
//    {
//        orientationNeed = YES;
//        theLabel.text = @"International Patent Search";
//        // http://was.prv.se/spd/search?lang=sv
//        NSString* urlAddress  = @"http://worldwide.espacenet.com/";
//        NSURL* url = [NSURL URLWithString:urlAddress];
//        NSURLRequest *requestAddress = [NSURLRequest requestWithURL:url];
//        [self.webView loadRequest:requestAddress];
//        [self toggleMenu];
//        //        [self hideLoadingView];
//        
//    }
    else if(indexPath.section==2)
    {
        NSString *stringStatus = [USER_DEFAULTS objectForKey:@"user_status"];
        NSString *strAppVersion =[USER_DEFAULTS objectForKey:@"ios_version"];
        float b = [strAppVersion floatValue];
        NSLog(@"App Version ###########%@",strAppVersion);
        
        
        NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        float a = [version floatValue];
        NSLog(@"Current App Version ----------------%@",version);
        NSLog(@"Current App build version +++++++++++++%@",build);
        
        if ([stringStatus isEqualToString:@"1"] || [stringStatus isEqualToString:@"2"] )
        {
           // if ([[USER_DEFAULTS objectForKey:@"ios_version"] integerValue] > [version integerValue])
                if ( a < b )

            {
                downloadAlert =  [[UIAlertView alloc]initWithTitle:@"Latest Version" message:[NSString stringWithFormat:@"Download latest version %@ of this application.",strAppVersion] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Download", nil];
          //      myTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(cancelAlert) userInfo:nil repeats:NO];

                               [downloadAlert show];
            }
            //else if ([[USER_DEFAULTS objectForKey:@"ios_version"] integerValue] == [strAppVersion integerValue])
            else
            {
                downloadAlert = nil;
            }

            
            IPManagementVC *ipVC = [[IPManagementVC alloc] init];
            [self.navigationController pushViewController:ipVC animated:YES];
        }
        else
        {
            
            LoginVC *loginObj=[[LoginVC alloc]init];
            [self.navigationController pushViewController:loginObj animated:YES];
            
//            LoginVC *loginVC = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//            [self.navigationController pushViewController:loginVC animated:YES];
        }
    }
}





#pragma mark - UIAlertView methods for download latest app by its version on "apple store" 

//- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
////  [myTimer invalidate];
//    // Process pressed button
//}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
      
       //    [myTimer invalidate];
        
//        UIMutableUserNotificationAction* declineAction = [[UIMutableUserNotificationAction alloc] init];
//        [declineAction setIdentifier:@"decline_action_id"];
//        [declineAction setTitle:@"Decline"];
//        [declineAction setActivationMode:UIUserNotificationActivationModeBackground];
//        [declineAction setDestructive:NO];
//        
//        UIMutableUserNotificationAction* replyAction = [[UIMutableUserNotificationAction alloc] init];
//        [replyAction setIdentifier:@"reply_action_id"];
//        [replyAction setTitle:@"Reply"];
//        [replyAction setActivationMode:UIUserNotificationActivationModeForeground];
//        [replyAction setDestructive:NO];
//        
//        UIMutableUserNotificationCategory* declineReplyCategory = [[UIMutableUserNotificationCategory alloc] init];
//        [declineReplyCategory setIdentifier:@"custom_category_id"];
//        [declineReplyCategory setActions:@[replyAction, declineAction] forContext:UIUserNotificationActionContextDefault];
//        
//        NSSet* categories = [NSSet setWithArray:@[declineReplyCategory]];
//        UIUserNotificationSettings* settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:categories];
//        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//        
//        UILocalNotification* notification = [[UILocalNotification alloc] init];
//        [notification setFireDate:[NSDate dateWithTimeIntervalSinceNow:2]];
//        [notification setAlertBody:@"Latest version is available now"];
//        [notification setCategory:@"custom_category_id"];
//        [[UIApplication sharedApplication] scheduleLocalNotification:notification];

        
        
        
        
    } else if(buttonIndex == 1)
    {
        
      //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/"]];
        
        NSString *appName = [NSString stringWithString:[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"]];
        //NSString *appName = @"Microsoft word";
        NSURL *appStoreURL = [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.com/app/%@",[appName stringByReplacingOccurrencesOfString:@" " withString:@""]]];
     //   itms-apps://itunes.com/app/
        
        [[UIApplication sharedApplication] openURL:appStoreURL];


         NSLog(@"app name :%@  %@",appName,appStoreURL);
        
//        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
//        NSString* appID = infoDictionary[@"CFBundleIdentifier"];
//        NSLog(@"Current App Bundle ID %@",appID);
//        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
//        NSData* data = [NSData dataWithContentsOfURL:url];
//        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//        
//        if ([lookup[@"resultCount"] integerValue] == 1)
//        {
//            
//            NSLog(@"app update required");
//            NSString* appStoreVersion = lookup[@"results"][0][@"version"];
//            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
//            if (![appStoreVersion isEqualToString:currentVersion]){
//                NSLog(@"app update required1");
//                NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
//               // return YES;
//                
//            }
//        }
       // [self needsUpdate];
        
        
    }
}





//- (void)cancelAlert {
//    [downloadAlert dismissWithClickedButtonIndex:-1 animated:YES];
//    NSLog(@"Alert show again *********");
//    //[downloadAlert show];
//}





//-(BOOL) needsUpdate{
//    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
//    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
//    NSLog(@"Current App Bundle ID  %@",appID);
//    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
//    NSData* data = [NSData dataWithContentsOfURL:url];
//    NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//    
//    if ([lookup[@"resultCount"] integerValue] == 1)
//    {
//        
//        NSLog(@"app update required");
//        NSString* appStoreVersion = lookup[@"results"][0][@"version"];
//        NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
//        if (![appStoreVersion isEqualToString:currentVersion]){
//            NSLog(@"app update required1");
//            NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
//            return YES;
//            
//        }
//    }
//    return NO;
//}


//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
//{
//    UIApplicationState appState = UIApplicationStateActive;
//    if ([application respondsToSelector:@selector(applicationState)])
//        appState = application.applicationState;
//    
//    if (appState == UIApplicationStateActive)
//    {
//        //        Don't open Url.
//    }
//    else
//    {
//        //        Open Url.
//         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];
//    }
//}
//
//
//
//- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void(^)())completionHandler
//{
//    
//    
//    
//    if([notification.category isEqualToString:@"custom_category_id"])
//    {
//        if([identifier isEqualToString:@"decline_action_id"])
//        {
//            NSLog(@"Decline was pressed");
//            
//        }
//        else if([identifier isEqualToString:@"reply_action_id"])
//        {
//            NSLog(@"Reply was pressed");
//          //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/"]];
//           // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];
//           
//
//        }
//    }
//    
//    
//    //	Important to call this when finished
//    completionHandler();
//}





- (void) changeView
{
    
}

- (void) toggleMenu
{
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self animateView];
                     }
                     completion:^(BOOL finished){
                     }];
}

- (void) animateView
{
    if(self.leftView.frame.origin.x == 0) //Menu is hidden
    {
        CGRect newFrame = CGRectOffset(self.leftView.frame, -(self.menuView.frame.size.width), 0.0);
        
        self.leftView.frame = newFrame;
        
        theLabel.text = @"Hynell";
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (IS_IPHONE_5)
            {
                if(UIDeviceOrientationIsLandscape(self.interfaceOrientation))
                {
                    theLabel.frame = CGRectMake(550, 25, 300, 30);
                    
                    //                [self.view bringSubviewToFront:self.menuView];
                }
                else
                {
                    theLabel.frame = CGRectMake(290, 25, 300, 30);
                }
            }
            else
            {
                if(UIDeviceOrientationIsLandscape(self.interfaceOrientation))
                {
                    theLabel.frame = CGRectMake(500, 25, 300, 30);
                    
                    //                [self.view bringSubviewToFront:self.menuView];
                }
                else
                {
                    theLabel.frame = CGRectMake(290, 25, 300, 30);
                }
            }
            
        }
        else
        {
            if(UIDeviceOrientationIsLandscape(self.interfaceOrientation))
            {
                theLabel.frame = CGRectMake(1030, 25, 300, 30);
            }
            else
            {
                theLabel.frame = CGRectMake(895, 25, 300, 30);
            }

//            theLabel.frame = CGRectMake(950, 12, 300, 60);
        }
        
        
    }
    else
    {
        [self.menuTableView reloadData];
        
        CGRect newFrame = CGRectOffset(self.leftView.frame, self.menuView.frame.size.width, 0.0);
        
        self.leftView.frame = newFrame;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (IS_IPHONE_5)
            {
                if(UIDeviceOrientationIsLandscape(self.interfaceOrientation))
                {
                    theLabel.frame = CGRectMake(130, 25, 300, 30);
                }
                else
                {
                    theLabel.frame = CGRectMake(10, 25, 300, 30);
                }
            }
            else
            {
                if(UIDeviceOrientationIsLandscape(self.interfaceOrientation))
                {
                    theLabel.frame = CGRectMake(90, 25, 300, 30);
                }
                else
                {
                    theLabel.frame = CGRectMake(10, 25, 300, 30);
                }
            }
        }
        else
        {
            if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))
            {
                theLabel.frame = CGRectMake(363, 25, 300, 30);
            }
            else
            {
                theLabel.frame = CGRectMake(230, 25, 300, 30);
            }
            
        }
        theLabel.text = @"Home";

    }
    
}

#pragma mark WebView Delegate Methods 

- (void) webViewDidStartLoad:(UIWebView *) webView
{
    SHOW_LOADING();
    
//     MBProgressHUD * hud= [MBProgressHUD showHUDAddedTo:SCREEN_WINDOW animated:NO];
//    hud.frame = CGRectMake(0, 0, 120, 143);
//    
//    // hud.mode = MBProgressHUD\ModeAnnularDeterminate;
//    hud.mode=MBProgressHUDModeDeterminate;
//    NSString *strloadingText = [NSString stringWithFormat:@"Loading Data."];
//    NSString *strloadingText2 = [NSString stringWithFormat:@" Please Wait.\r 1-2 Minutes"];
//    
//    NSLog(@"the loading text will be %@",strloadingText);
//    hud.labelText = strloadingText;
//    hud.detailsLabelText=strloadingText2;

}
- (void) webViewDidFinishLoad:(UIWebView *) webView
{
    HIDE_LOADING();
}

- (void) webView:(UIWebView *) webView didFailLoadWithError:(NSError *) error
{
    HIDE_LOADING();
    
    NSLog(@"error: %@",error.localizedDescription);
}


-(void)loadView
{
    CGRect rect = CGRectMake(0, 64, 320, 480);
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (screenSize.height > 480.0f)
            rect=CGRectMake(0, 64, 320, 568);
    }
    
    UIView *view = [[UIView alloc] initWithFrame:rect];
    view.backgroundColor = UIColorFromRGB(0x2C76E2);
  //  view.backgroundColor = [UIColor redColor];
    
    self.view = view;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
