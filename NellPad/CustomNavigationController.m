//
//  CustomNavigationController.m
//  NellPat
//
//  Created by Sumit Jain on 6/6/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "CustomNavigationController.h"

@implementation CustomNavigationController

- (BOOL)shouldAutorotate
{
    if ([[self myTopViewController] respondsToSelector:@selector(shouldAutorotate)])
    {
        return [[self myTopViewController] shouldAutorotate];
    }
    
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    if ([[self myTopViewController] respondsToSelector:@selector(preferredInterfaceOrientationForPresentation)]) {
        return [[self myTopViewController] preferredInterfaceOrientationForPresentation];
    }
    
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations {
    if ([[self myTopViewController] respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return [[self myTopViewController] supportedInterfaceOrientations];
    }

    return UIInterfaceOrientationPortrait;
}

- (UIViewController*)myTopViewController {
    return [self topViewController];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
    
    [super pushViewController:viewController animated:animated];
}

@end
