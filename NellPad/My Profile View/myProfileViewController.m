//
//  myProfileViewController.m
//  NellPat
//
//  Created by Anit Kumar on 11/12/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "myProfileViewController.h"

@interface myProfileViewController ()
{
    NSDictionary *responseDict;
    
    UILabel *nameLable;
    UILabel *emailLable;
    UILabel *statusLable;
    
    UILabel *nameShowLable;
    UILabel *emailShowLable;
    UILabel *statusShowLable;
    
}

@end

@implementation myProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title =@"My Profile";
    
    
    self.view.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    
    UIImageView *profile=[[UIImageView alloc]init];
    profile.frame=CGRectMake(0, 0, [UIImage imageNamed:@"settingprofileIcon.png"].size.width, [UIImage imageNamed:@"settingprofileIcon.png"].size.height);
    profile.backgroundColor=[UIColor clearColor];
    profile.center=CGPointMake(self.view.center.x, 70);
    profile.image=[UIImage imageNamed:@"settingprofileIcon.png"];
    [self.view addSubview:profile];
    
    UIView *firstview=[[UIView alloc]init];
    firstview.frame=CGRectMake(20,profile.frame.size.height+profile.frame.origin.y+30, self.view.frame.size.width-40, 50);
    firstview.backgroundColor=[UIColor colorWithRed:207.0/255.0 green:208.0/255.0 blue:209.0/255.0 alpha:1.0];
    
    nameLable=[[UILabel alloc]init];
    nameLable.frame=CGRectMake(10, 10, 70, 30);
    nameLable.textAlignment=NSTextAlignmentLeft;
    nameLable.text=@"Name :";
    nameLable.backgroundColor=[UIColor clearColor];
    [firstview addSubview:nameLable];
    
   
    nameShowLable=[[UILabel alloc]init];
    nameShowLable.frame=CGRectMake(nameLable.frame.size.width+nameLable.frame.origin.x+30, 10, 160, 30);
    nameShowLable.textAlignment=NSTextAlignmentRight;
    nameShowLable.backgroundColor=[UIColor clearColor];
    [firstview addSubview:nameShowLable];

    [self.view addSubview:firstview];
    
    UIView *firstview2=[[UIView alloc]init];
    firstview2.frame=CGRectMake(20,firstview.frame.size.height+firstview.frame.origin.y+5, self.view.frame.size.width-40, 50);
    firstview2.backgroundColor=[UIColor colorWithRed:207.0/255.0 green:208.0/255.0 blue:209.0/255.0 alpha:1.0];
    
    emailLable=[[UILabel alloc]init];
    emailLable.frame=CGRectMake(10, 10, 70, 30);
    emailLable.textAlignment=NSTextAlignmentLeft;
    emailLable.text=@"Email :";
    emailLable.backgroundColor=[UIColor clearColor];
    [firstview2 addSubview:emailLable];
    
    emailShowLable=[[UILabel alloc]init];
    emailShowLable.frame=CGRectMake(emailLable.frame.size.width+emailLable.frame.origin.x+20, 10, 170, 30);
    emailShowLable.textAlignment=NSTextAlignmentRight;
    emailShowLable.backgroundColor=[UIColor clearColor];
    [firstview2 addSubview:emailShowLable];
    
    [self.view addSubview:firstview2];
    
    UIView *firstview3=[[UIView alloc]init];
    firstview3.frame=CGRectMake(20,firstview2.frame.size.height+firstview2.frame.origin.y+5, self.view.frame.size.width-40, 50);
    firstview3.backgroundColor=[UIColor colorWithRed:207.0/255.0 green:208.0/255.0 blue:209.0/255.0 alpha:1.0];
    statusLable=[[UILabel alloc]init];
    statusLable.frame=CGRectMake(10, 10, 70, 30);
    statusLable.textAlignment=NSTextAlignmentLeft;
    statusLable.text=@"User :";
    statusLable.backgroundColor=[UIColor clearColor];
    [firstview3 addSubview:statusLable];
    
    statusShowLable=[[UILabel alloc]init];
    statusShowLable.frame=CGRectMake(statusLable.frame.size.width+statusLable.frame.origin.x+20, 10, 170, 30);
    statusShowLable.textAlignment=NSTextAlignmentRight;
    statusShowLable.textColor=[UIColor blackColor];
    statusShowLable.backgroundColor=[UIColor clearColor];
    [firstview3 addSubview:statusShowLable];
    
    
    [self.view addSubview:firstview3];
    
    
    
   [self callUserProfileWebService];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];
    
}

- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];

    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
    
}

-(void)callUserProfileWebService{

    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"getUserDetails";
    [inputPram.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    
    
    
    [WebserviceManager callWebserviceWithInputParame:inputPram
                                          completion:^(id response, NSError *error) {
                                              if (error) {
                                                  if ([error isErrorOfKindNoNetwork])
                                                      [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                                                  return ;
                                                  
                                                  return;
                                              }
                                              
                                              error = nil;
                                              responseDict = [NSJSONSerialization JSONObjectWithData:response
                                                                                             options:NSJSONReadingMutableContainers
                                                                                               error:&error];
                                              if ([responseDict count])
                                                  [self performSelectorOnMainThread:@selector(showProfileData) withObject:responseDict waitUntilDone:YES];
                                              
                                          }];
    
}
-(void)showProfileData{

    nameShowLable.text = [NSString stringWithUTF8String:[[[[responseDict objectForKey:@"data"] objectAtIndex:0] objectForKey:@"Name"]cStringUsingEncoding:NSUTF8StringEncoding]];
    emailShowLable.text = [[[responseDict objectForKey:@"data"] objectAtIndex:0] objectForKey:@"Email"];
    statusShowLable.text = [[[responseDict objectForKey:@"data"] objectAtIndex:0] objectForKey:@"Status"];
    
    NSLog(@"nameShowLable.text=%@",nameShowLable.text);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
