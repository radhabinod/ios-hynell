//
//  ProjectVC.m
//  NellPad
//
//  Created by Herry Makker on 12/5/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import "ProjectVC.h"

@interface ProjectVC ()

@end

@implementation ProjectVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)loadView
{
    UIView *view;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        view=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 504)];
    else
        view=[[UIView alloc] initWithFrame:CGRectMake(0,0, 768, 1024)];
    view.backgroundColor = [UIColor redColor];
    self.view=view;
}


@end
