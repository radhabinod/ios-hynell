//
//  CustomerIDVC.m
//  NellPat
//
//  Created by Ram Kumar on 05/04/16.
//  Copyright © 2016 Gagan Joshi. All rights reserved.
//

#import "CustomerIDVC.h"
#import "CompaniesListVC.h"
#import "PatrawinCasesVC.h"
#import "CasesVC.h"
#import "PatrawinCase.h"
#import "RecommendationsListBO.h"
#import "PetrawinCasesList.h"
#import "CompanyList.h"
#import "CompanyBO.h"
#import "RecommendationsList.h"
#import "DataSyncBO.h"
#import <objc/message.h>
#import "StasList.h"
#import "CustomerIDVC.h"

@interface CustomerIDVC ()
{
    UIView *view;
    UITextField *sendTo;
    UIButton *dropdownButton;
    NSArray *caseArray;
    NSArray *arrRecommendation;
    UISearchBar *searchBar1;
    UISearchDisplayController *searchDisplayController;
    NSArray *searchResults;
    
    NSMutableArray *arrayListOfCompaniesName, *arrayListOfCompaniesId;
    CompanyBO *companyBo;
    
    CasesVC *obj_CaseVC;
    NSArray *TextArray;
    NSArray *arrayForTable;
    NSMutableArray *unique;
    UIRefreshControl *refresh;
    DataSyncBO *dataBO;
    
    NSSet *uniqueCompany;
    NSString *CustomerData;
    NSArray *casesIds;
    NSArray *search;
}

@end

@implementation CustomerIDVC
@synthesize customerID,casesArray,arrRecommendations,actionSheetSort,textSortingCompany,viewForAgentsListBG,viewForAgentsList,statisticCases,statsScreenType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    search=[[NSArray alloc]init];
    CustomerData = [NSString stringWithFormat:@"%@",customerID];
   
    
//    NSLog(@"data cust:%@",CustomerData);
//    NSLog(@"stats cases in customr id view:%@",statisticCases);
//    NSLog(@"stats screen type:%@",statsScreenType);

    
    self.title  = @"Customer ID";
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]){
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
        }
    }
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont systemFontOfSize: 18], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];

    obj_CaseVC  = [[CasesVC alloc] init];

    // casesIds = [[NSMutableArray alloc]init];
    self.casesArray = [[NSMutableArray alloc]init];
    self.arrRecommendations = [[NSMutableArray alloc]init];
    casesIds = [[NSArray alloc] init];
    searchResults = [[NSArray alloc] init];
    arrayListOfCompaniesName = [[NSMutableArray alloc] init];
    arrayListOfCompaniesId = [[NSMutableArray alloc] init];

    [self configureView];
    
    if ([statsScreenType isEqualToString:@"Statistics"]) {
        
         [self getCaseOfCompany:CustomerData];

    }
    if ([statsScreenType isEqualToString:@"All Cases"]) {

         [self fetchFromDatabase];
         [self getCaseOfCompany:CustomerData];
}
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

}

- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void) DataReload
{
    [self fetchFromDatabase];
    [refresh endRefreshing];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated
{
      [super viewWillAppear:animated];
}

-(void)configureView
{
    
    [self.view endEditing:YES];
    
    searchBar1 = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, IS_IPAD ? 768 : 320, 40)];
    
    searchBar1.placeholder = @"Search ID's";
    
    searchBar1.delegate = self;
    //   [searchBar1 setImage:[UIImage imageNamed:@""] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self.view addSubview:searchBar1];
    for (UIView *searchBarSubview in [searchBar1 subviews]) {
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            @try {
                
                [(UITextField *)searchBarSubview setBorderStyle:UITextBorderStyleLine];
            }
            @catch (NSException * e) {
                // ignore exception
            }
        }
    }
    self.caseTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, searchBar1.frame.size.height+searchBar1.frame.origin.y, deviceWidth, deviceHeight-100) style:UITableViewStylePlain];
    
    self.caseTableView.delegate=self;
    self.caseTableView.dataSource=self;
    self.caseTableView.backgroundColor=[UIColor whiteColor];
    self.caseTableView.rowHeight = IS_IPAD ? 60 : 50.0f;
    self.caseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.caseTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.caseTableView];
    self.view.layer.borderColor = UIColorFromRGB(0xF0F0F0).CGColor;
    
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}

// use this method to search data in list =====================================================================================================================

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] > 0) {
        
        //        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.case_type contains[c] %@", searchText];
        
        NSMutableArray *sResult = @[].mutableCopy;
        
        for (PatrawinCase *cs in casesIds) {
            NSRange r = [cs.case_id rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (r.length > 0) {
                //  NSLog(@"case is's%@", cs.case_id);
                [sResult addObject:cs];
            }
        }
        
        if (search) {
            search = nil;
        }
        
        search = [NSArray arrayWithArray:sResult];
        
        sResult = nil;
    } else {
        search = [NSArray arrayWithArray:casesIds];
    }
    //    [searchResults addObjectsFromArray:[self.casesArray filteredArrayUsingPredicate:resultPredicate]];
    
    [self.caseTableView reloadData];
}

#pragma mark - UISearchField Delegates

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
}

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.case_type contains[c] %@", searchText];
//
//    searchResults = [self.casesArray filteredArrayUsingPredicate:resultPredicate];
//
////    [searchResults addObjectsFromArray:[self.casesArray filteredArrayUsingPredicate:resultPredicate]];
//
//    NSLog(@"Search Array = %@", searchResults);
//}

//- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//{
//    [self filterContentForSearchText:searchText
//                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                      objectAtIndex:[self.searchDisplayController.searchBar
//                                                     selectedScopeButtonIndex]]];
//}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark - Table view Datasource & Delegate Methods 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==_caseTableView)
    {
        return IS_IPAD ? 61 : 71.0f;
    }
    else
        return IS_IPAD ? 55 : 44;
    
    
    // return IS_IPAD ? 61 : 71.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return search.count;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    PatrawinCase *cid=[search objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
      if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
    }
    
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    cell.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    cell.textLabel.text = [NSString stringWithFormat:@"Case:%@",cid.case_id];
    cell.detailTextLabel.text =[NSString stringWithFormat:@"%@",cid.title];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    cell.textLabel.textColor= [UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
    
    cell.textLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 18 : 16.0];
    
    cell.detailTextLabel.textColor=[UIColor darkGrayColor];
    
    cell.detailTextLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 16 : 12.0];

    UIView *_lineDiv=[[UIView alloc] init];
    _lineDiv.frame=CGRectMake(0, IS_IPAD ? 60 : 70.0f, self.view.frame.size.width, 1.0);
    _lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
    [cell addSubview:_lineDiv];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];

    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];

    if(tableView == self.caseTableView)
    {
        //        if (self.casesArray.count==0)
        if (search.count==0)
        {

        }
        else
        {
            obj_CaseVC.strSelectedIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            //          obj_CaseVC.arrayAllCases=[[NSMutableArray alloc]initWithArray:self.casesArray];
            obj_CaseVC.arrayAllCases = [[NSMutableArray alloc]initWithArray:search];

            if ([strKey isEqualToString:@"1"])
            {

            }
            else if ([strKey isEqualToString:@"0"])
            {
                obj_CaseVC.arrRecommendationListBO=arrRecommendations;
                NSLog(@"recomendation list:==%i", obj_CaseVC.arrRecommendationListBO.count);
            }
            else{
                NSLog(@"Logic testing");
            }

            [self.navigationController pushViewController:obj_CaseVC animated:YES];
        }
    }
}


-(void)fetchFromDatabase
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"PetrawinCasesList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    
    NSEntityDescription *entityDesc1 = [NSEntityDescription entityForName:@"RecommendationsList" inManagedObjectContext:context];
    NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
    [request1 setEntity:entityDesc1];
    
    NSError *error1;
    NSArray *fetchedArray1 = [context executeFetchRequest:request1 error:&error1];
    
    responseDict =[NSMutableDictionary new];
    [responseDict setValue:fetchedArray forKey:@"data"];
    [responseDict setValue:fetchedArray1 forKey:@"recommendation_list"];
    
    //NSLog(@"The array is %i", fetchedArray1.count);
    
    if (fetchedArray.count == 0) {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"CaseListdownloaded" object:nil];
        dataBO=[DataSyncBO new];
        [dataBO caseList];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(sortCasesDescending) withObject:self waitUntilDone:YES];
        //  [self fetchCompanyData];
    }
}




-(void)sortCasesDescending
{
    
    [self.casesArray removeAllObjects];
    [self.arrRecommendations removeAllObjects];
    
    caseArray = responseDict[@"data"];
    arrRecommendation=responseDict[@"recommendation_list"];
    
    //Recommendation_TextArray = [caseArray valueForKey:@"Recommendation_Text"];
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ([strKey isEqualToString:@"1"])
    {
        NSMutableArray *unsortedArr = [NSMutableArray array];
        for(PetrawinCasesList *caseDict in caseArray)
        {
            PatrawinCase *patCase = [[PatrawinCase alloc] init];
            patCase.account_cost=caseDict.accountCost;
            patCase.agent_id =caseDict.agentId ;
            patCase.arrived_date =[NSString stringWithFormat:@"%@",caseDict.arrivedDate];
            //patCase.case_id =[NSString stringWithFormat:@"%@",caseDict.caseId];
            patCase.case_id = caseDict.caseId;
            patCase.comment_case =caseDict.caseComment;
            patCase.case_type =caseDict.caseType;
            patCase.comment =caseDict.comment;
            patCase.country_code =caseDict.countrycode;
            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
            patCase.customer_name=caseDict.customerName;
            patCase.deadline=caseDict.deadline;
            patCase.figure=caseDict.figure;
            patCase.figureSmall = caseDict.figuresmall;
            patCase.link=caseDict.link;
            patCase.patent_number =caseDict.patentNo;
            patCase.title =caseDict.title;
            patCase.status =caseDict.status;
            patCase.Recommendation_Text = caseDict.recommendationText;
            
            //            for(RecommendationsList *dictRecommendation in arrRecommendation)
            //            {
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];;
            patCase.objRecommendationListBO=objRecommendationListBO;
            [unsortedArr addObject:patCase];
            //            }
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        self.casesArray = [sortedArray mutableCopy];
        sortedArray = nil;
        
    }
    else if ([strKey isEqualToString:@"0"])
    {
        for (RecommendationsList *dictRecommendation in arrRecommendation)
        {
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = dictRecommendation.recommendation_ID;
            objRecommendationListBO.strRecommendationText = dictRecommendation.recommendation_Text;
            objRecommendationListBO.strRecommendationStatus = dictRecommendation.recommendation_Status;
            [self.arrRecommendations addObject:objRecommendationListBO];
        }
        
        NSMutableArray *unsortedArr = [NSMutableArray array];
        for(PetrawinCasesList *caseDict in caseArray)
        {
            PatrawinCase *patCase = [[PatrawinCase alloc] init];
            patCase.account_cost=caseDict.accountCost;
            patCase.agent_id = caseDict.agentId;
            patCase.arrived_date = [NSString stringWithFormat:@"%@",caseDict.arrivedDate];
            //patCase.case_id = [NSString stringWithFormat:@"%@",caseDict.caseId];
            patCase.case_id = caseDict.caseId;
            patCase.comment_case = caseDict.caseComment;
            patCase.case_type = caseDict.caseType;
            patCase.comment = caseDict.comment;
            patCase.country_code = caseDict.countrycode;
            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
            patCase.customer_name=caseDict.customerName;
            patCase.deadline=caseDict.deadline;
            patCase.figure=caseDict.figure;
            patCase.figureSmall = caseDict.figuresmall;
            patCase.link=caseDict.link;
            patCase.patent_number = caseDict.patentNo;
            patCase.title = caseDict.title;
            patCase.status = caseDict.status;
            patCase.Recommendation_Text = caseDict.recommendationText;
            
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];;
            patCase.objRecommendationListBO=objRecommendationListBO;
            
            [unsortedArr addObject:patCase];
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        self.casesArray = [sortedArray mutableCopy];
        sortedArray = nil;
        
        
    }
    //    else
    //    {
    //        NSMutableArray *unsortedArr = [NSMutableArray array];
    //        for(PetrawinCasesList *caseDict in caseArray)
    //        {
    //            PatrawinCase *patCase = [[PatrawinCase alloc] init];
    //            patCase.account_cost=caseDict.accountCost;
    //            patCase.agent_id =caseDict.agentId;
    //            patCase.arrived_date = [NSString stringWithFormat:@"%@",caseDict.arrivedDate];
    //            //patCase.case_id = [NSString stringWithFormat:@"%@",caseDict.caseId];
    //            patCase.case_id = caseDict.caseId;
    //            patCase.comment_case = caseDict.caseComment;
    //            patCase.case_type = caseDict.caseType;
    //            patCase.comment = caseDict.comment;
    //            patCase.country_code = caseDict.countrycode;
    //            patCase.customer_id=[NSString stringWithFormat:@"%d",[caseDict.customerId intValue]];
    //            patCase.customer_name=caseDict.customerName;
    //            patCase.deadline=caseDict.deadline;
    //            patCase.figure=caseDict.figure;
    //            patCase.figureSmall = caseDict.figuresmall;
    //            patCase.link=caseDict.link;
    //            patCase.patent_number = caseDict.patentNo;
    //            patCase.title = caseDict.title;
    //            patCase.status = caseDict.status;
    //
    //            patCase.Recommendation_Text = caseDict.recommendationText;
    //
    //            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
    //            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[caseDict.recommendationID intValue]];
    //            objRecommendationListBO.strRecommendationText = caseDict.recommendationText;
    //            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[caseDict.recommendationStatus intValue]];
    //            patCase.objRecommendationListBO=objRecommendationListBO;
    //
    //            [unsortedArr addObject:patCase];
    //        }
    //        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
    //        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //        NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    //        self.casesArray = [sortedArray mutableCopy];
    //        sortedArray = nil;
    //    }
    
    searchResults = [NSArray arrayWithArray:self.casesArray];
    
    
    [self.caseTableView reloadData];
    
    //   [self sortCasesDescendings];
}

-(void) getCaseOfCompany:(NSString *)cusId
{
     NSMutableArray *casesId=[[NSMutableArray alloc]init];
    
    
    
    if ([statsScreenType isEqualToString:@"Statistics"]) {
        for(int j=0;j<statisticCases.count;j++)
        {
            
            PatrawinCase *cid=[statisticCases objectAtIndex:j];
            NSLog(@"Recom list 1:%@",cid.Recommendation_Text);
            NSString *hh=cid.customer_id;
            if([cusId isEqualToString:hh])
            {
                [casesId addObject:cid];
                
            }
        }

    }
    
    if ([statsScreenType isEqualToString:@"All Cases"]) {
        for(int j=0;j<self.casesArray.count;j++)
        {
            
            PatrawinCase *cid=[casesArray objectAtIndex:j];
               NSLog(@"Recom list: 2%@",cid.Recommendation_Text);
            NSString *hh=cid.customer_id;
            if([cusId isEqualToString:hh])
            {
                [casesId addObject:cid];
                
            }
        }

    }
    casesIds = [[NSArray alloc]initWithArray:casesId];
    
    
    
    
    NSMutableArray *unsortedArray = [casesIds mutableCopy];
    //[self.casesArray removeAllObjects];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deadline" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    NSMutableArray *sortedwithoutZero = [[NSMutableArray alloc]init];
    for(int i=0;i<sortedArray.count;i++)
    {
        PatrawinCase *c=[sortedArray objectAtIndex:i];
        if(![c.deadline isEqualToString:@""])
        {
            [sortedwithoutZero addObject:c];
        }
    }
    for(int i=0;i<sortedArray.count;i++)
    {
        PatrawinCase *c=[sortedArray objectAtIndex:i];
        if([c.deadline isEqualToString:@""])
        {
            [sortedwithoutZero addObject:c];
        }
    }
    search = [sortedwithoutZero mutableCopy];
    
    
    
    
  //  search=casesIds;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
