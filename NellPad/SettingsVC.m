//
//  HomeVC.m
//  NellPad
//
//  Created by Herry Makker on 12/5/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import "SettingsVC.h"
#import "ChangePasswordController.h"
#import "myProfileViewController.h"
#import <objc/message.h>

@interface SettingsVC ()

@end

@implementation SettingsVC
@synthesize emailForChngePass;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Settings";
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation))
    {
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
            
            
        }
    }
    
    self.view.backgroundColor = UIColorFromRGB(0xF0F0F0);
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize: 18], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x3B98CA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=NO;
    
    
    UIButton *myProfile = [UIButton buttonWithType:UIButtonTypeCustom];
    myProfile.backgroundColor = UIColorFromRGB(0xd3d3d3);
    myProfile.titleLabel.textColor = UIColorFromRGB(0x2a2a2a);
    myProfile.frame=CGRectMake(12, 40, self.view.frame.size.width/2-18,142);
    myProfile.backgroundColor=[UIColor clearColor];
    [myProfile setImage:[UIImage imageNamed:@"profile_1"] forState:UIControlStateNormal];
    [myProfile addTarget:self action:@selector(myProfileClk) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:myProfile];
    
    
    UIButton *btnChangePassword = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChangePassword.backgroundColor = UIColorFromRGB(0xd3d3d3);
    btnChangePassword.titleLabel.textColor = UIColorFromRGB(0x2a2a2a);
    btnChangePassword.backgroundColor=[UIColor clearColor];
    btnChangePassword.frame=CGRectMake(myProfile.frame.size.width+myProfile.frame.origin.x+10, 40, self.view.frame.size.width/2-18,142);
    [btnChangePassword setImage:[UIImage imageNamed:@"change_pass"] forState:UIControlStateNormal];
    [btnChangePassword addTarget:self action:@selector(btnChangePasswordClk) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnChangePassword];
    
    
    UIButton *btnLogout = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLogout.backgroundColor = UIColorFromRGB(0xd3d3d3);
    btnLogout.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:125.0/255.0 blue:222.0/255.0 alpha:1.0];
    btnLogout.frame=CGRectMake(12, myProfile.frame.size.height+myProfile.frame.origin.y+10, self.view.frame.size.width/2-18,142);
    btnLogout.backgroundColor=[UIColor clearColor];
    [btnLogout setImage:[UIImage imageNamed:@"logout_1"] forState:UIControlStateNormal];
    [btnLogout addTarget:self action:@selector(btnLogoutClk) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnLogout];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

    
}

- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];

    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}

//---------------------------------------------------------------------------------------------------------------------------------
-(void)btnLogoutClk
//---------------------------------------------------------------------------------------------------------------------------------
{
    
    UIAlertView * alertviewLogout = [[UIAlertView alloc] initWithTitle:@"Log Out" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm",nil];
    alertviewLogout.tag=334534;
    [alertviewLogout show];
    
}
- (BOOL)shouldAutorotate
{
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait)
    {
        return YES;
    }
    return NO;
}
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

-(void)myProfileClk
{
    
    myProfileViewController *profile=[[myProfileViewController alloc]init];
    [self.navigationController pushViewController:profile animated:YES];
    
    
    
    //    if (myProfile == Nil)
    //    {
    //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    //
    //            myProfileViewController
    //
    //            myProfile = [[myProfileViewController alloc] initWithNibName:@"myProfileViewController_iPad" bundle:[NSBundle mainBundle]];
    //        }else{
    //            myProfile = [[myProfileViewController alloc] initWithNibName:@"myProfileViewController" bundle:[NSBundle mainBundle]];
    //        }
    //
    //        [self.navigationController pushViewController:myProfile animated:YES];
    //    }
}

-(void)logout
{
    
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"logout";
    [inputPram.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    
    [WebserviceManager callWebserviceWithInputParame:inputPram
                                          completion:^(id response, NSError *error) {
                                              if (error) {
                                                  if ([error isErrorOfKindNoNetwork])
                                                      [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                                                  return ;
                                                  
                                                  return;
                                              }
                                              
                                              error = nil;
                                              responseDict = [NSJSONSerialization JSONObjectWithData:response
                                                                                             options:NSJSONReadingMutableContainers
                                                                                               error:&error];
                                              if ([responseDict count])
                                                  [self performSelectorOnMainThread:@selector(logoutScreen) withObject:responseDict waitUntilDone:YES];
                                              
                                          }];
    
    
    
}
#pragma mark -logoutBtnAction
//-----------------------------------------------------------------------------------------------------------------------------
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==334534)
    {
        if (buttonIndex==0)
        {
           
            
        }
        else
            if(buttonIndex ==1)
            {
                
                [self logout];
                
            }
    }
    
}
-(void)logoutScreen{
    [USER_DEFAULTS removeObjectForKey:@"user_status"];
    
    [USER_DEFAULTS removeObjectForKey:@"token"];
    
    [USER_DEFAULTS synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
-(void)btnChangePasswordClk
{
    ChangePasswordController *objchangePasswordVC=[[ChangePasswordController alloc]init];
    objchangePasswordVC.emailForChngePass = emailForChngePass;
    [self.navigationController pushViewController:objchangePasswordVC animated:YES];
}


-(void)loadView
{
    UIView *view;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        view=[[UIView alloc] initWithFrame:CGRectMake(0,0, deviceWidth, 504)];
    else
        view=[[UIView alloc] initWithFrame:CGRectMake(0,0, deviceWidth, deviceHeight)];
    view.backgroundColor=[UIColor whiteColor];
    self.view=view;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
