//
//  ChangePasswordController.m
//  NellPad
//
//  Created by Rakesh Kumar on 12/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "ChangePasswordController.h"
#import <objc/message.h>
#import "Constants.h"
#import "ForgotPasswordVC.h"
#define kOFFSET_FOR_KEYBOARD 95
@interface ChangePasswordController (){
    
    @private
    
    UIScrollView *changeScrollview;
    CGFloat heightofKeyboard;
    UITextField *_txtEmail;
    UITextField *_txtPassword;
    UITextField *_txtConfirmPassword;
    NSDictionary *responseDictionary;
}

@end

@implementation ChangePasswordController
@synthesize arrData,scrollBar,responseDict,emailForChngePass;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title =@"Change Password";
    
    
  NSLog(@" ^^^^^^^^^^^^^^^^^ nnnnnn %@",emailForChngePass);
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation))
    {
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
            
            
        }
    }
    
    
    self.view.backgroundColor = UIColorFromRGB(0xF0F0F0);
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize: 18], NSFontAttributeName, nil]];
   // [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];

//
    arrData=[[NSMutableArray alloc]initWithObjects:@"Email",@"New Password",@"Confirm Password", nil];
//    
//    int Xpos=0;
//    int Ypos=0;
//    
//    self.scrollBar=[[UIScrollView alloc]init];
//    
//    if (IS_IPHONE_5)
//    {
//        scrollBar.frame = CGRectMake(Xpos,Ypos,deviceWidth,deviceHeight);
//    }
//    else
//    {
//        scrollBar.frame = CGRectMake(Xpos, Ypos, deviceWidth, deviceHeight);
//    }
//    self.scrollBar.showsVerticalScrollIndicator=YES;
//    self.scrollBar.scrollEnabled=YES;
//    self.scrollBar.userInteractionEnabled=YES;
//    [self.view addSubview: self.scrollBar];
//    
//    Xpos=24;
//    Ypos=30;
//    
//    for (int i=0; i<[arrData count]; i++)
//    {
//        
//        UILabel *lblTemp=[[UILabel alloc]initWithFrame:CGRectMake(Xpos, IS_IPAD ? Ypos + 25 : Ypos, deviceWidth/2, 25)];
//        lblTemp.backgroundColor=[UIColor clearColor];
//        lblTemp.textAlignment=NSTextAlignmentLeft;
//        lblTemp.textColor=[UIColor blackColor];
//        lblTemp.font= IS_IPAD ? FONT_NORMAL(20) : FONT_BOLD(16.0);
//        [lblTemp setText:[arrData objectAtIndex:i]];
//        [self.scrollBar addSubview:lblTemp];
//        
//        Ypos+=30;
//        
//        txtFieldTemp=[[UITextField alloc]initWithFrame:CGRectMake(Xpos, IS_IPAD ? Ypos + 35: Ypos, deviceWidth-48, IS_IPAD ? 50 : 30)];
//        txtFieldTemp.delegate = self;
//        txtFieldTemp.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//        txtFieldTemp.tag=kTxtFEmail+i;
//        txtFieldTemp.borderStyle=UITextBorderStyleRoundedRect;
//        txtFieldTemp.autocapitalizationType = UITextAutocapitalizationTypeNone;
//        txtFieldTemp.textColor=[UIColor blackColor];
//        txtFieldTemp.keyboardType=UIKeyboardTypeDefault;
//        txtFieldTemp.returnKeyType=UIReturnKeyDone;
//        txtFieldTemp.textAlignment=NSTextAlignmentLeft;
//        txtFieldTemp.backgroundColor=[UIColor clearColor];
//        [ txtFieldTemp setFont:FONT_BOLD(16.0)];
//        [self.scrollBar addSubview: txtFieldTemp];
//        
//        Ypos+= IS_IPAD ? 90 : 45;
//        
//        if (i==1 || i==2)
//        {
//            txtFieldTemp.secureTextEntry = YES;
//
//        }
//        else
//        {
//            txtFieldTemp.secureTextEntry = NO;
//
//        }
//        txtFieldTemp = nil;
//    }
//    
//    Xpos=IS_IPAD?96:24;
//    
//    Ypos+=IS_IPAD?30:0;
//    
//    UIButton *btnChange=[UIButton buttonWithType:UIButtonTypeRoundedRect];
//    btnChange.frame=CGRectMake(Xpos, IS_IPAD ? Ypos + 40 : Ypos, IS_IPAD? 314:163, IS_IPAD?54:40);
//    //[btnChange setBackgroundImage:[UIImage imageNamed: IS_IPAD? @"ChangePasswordBtniPad.png": @"ChangePasswordBtn.png"] forState:UIControlStateNormal];
//    btnChange.backgroundColor=[UIColor clearColor];
//    [btnChange addTarget:self action:@selector(btnChangeClk) forControlEvents:UIControlEventTouchUpInside];
//    [btnChange setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [btnChange setTitle:@"Change Password" forState:UIControlStateNormal];
//    btnChange.titleLabel.font = [UIFont fontWithName:@"Avenir" size:IS_IPAD ? 30 : 19.0];
//     btnChange.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:125.0/255.0 blue:222.0/255.0 alpha:1.0];
//    [self.scrollBar addSubview:btnChange];
//    
//    Xpos+=IS_IPAD? btnChange.frame.size.width+96: btnChange.frame.size.width+26;
//    
//    UIButton *btnCancel=[UIButton buttonWithType:UIButtonTypeRoundedRect];
//    btnCancel.frame=CGRectMake(Xpos, IS_IPAD ? Ypos + 40 : Ypos,IS_IPAD? 164:83, IS_IPAD?54:40);
//   // [btnCancel setBackgroundImage:[UIImage imageNamed: IS_IPAD?@"CancelBtniPad.png": @"CancelBtn.png"] forState:UIControlStateNormal];
//    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
//    btnCancel.titleLabel.font = [UIFont fontWithName:@"Avenir" size:IS_IPAD ? 30 : 19.0];
//    
//    btnCancel.backgroundColor=[UIColor clearColor];
//    [btnCancel addTarget:self action:@selector(btnCancelClk) forControlEvents:UIControlEventTouchUpInside];
//    btnCancel.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:125.0/255.0 blue:222.0/255.0 alpha:1.0];
//    [self.scrollBar addSubview:btnCancel];
//    
//    
//    [self.scrollBar setContentSize:CGSizeMake(deviceWidth,IS_IPHONE_5?0:Ypos+240)];
//    
//    UITapGestureRecognizer *gst=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboardclk)];
//    gst.delegate=self;
//    gst.cancelsTouchesInView=NO;
//    [self.view addGestureRecognizer:gst];

    
    self.view.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];

    
    changeScrollview=[[UIScrollView alloc]init];
    changeScrollview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    changeScrollview.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    changeScrollview.backgroundColor=[UIColor clearColor];
    

    UIImageView *profile=[[UIImageView alloc]init];
    profile.frame=CGRectMake(0, 40, [UIImage imageNamed:@"settingChangePass.png"].size.width, [UIImage imageNamed:@"settingChangePass.png"].size.height);
    profile.backgroundColor=[UIColor clearColor];
    profile.center=CGPointMake(self.view.center.x, 100);
    profile.image=[UIImage imageNamed:@"settingChangePass.png"];
    [changeScrollview addSubview:profile];
    

    UIImageView *emailImageView=[[UIImageView alloc]init];
    emailImageView.frame=CGRectMake(30,profile.frame.size.height+profile.frame.origin.y+50, [UIImage imageNamed:@"settingemailIcon.png"].size.width, [UIImage imageNamed:@"settingemailIcon.png"].size.height);
    emailImageView.image=[UIImage imageNamed:@"settingemailIcon.png"];
    emailImageView.backgroundColor=[UIColor clearColor];
    [changeScrollview addSubview:emailImageView];
    
    
    
   _txtEmail = [self creareTextField:CGRectMake(50,profile.frame.size.height+profile.frame.origin.y+30, self.view.frame.size.width-90, 45) backgroundColor:[UIColor clearColor] placeHolder:@"Email" placeHolderColor:[UIColor darkGrayColor] textColor:[UIColor lightGrayColor]];
    _txtEmail.delegate = self;
    _txtEmail.returnKeyType = UIReturnKeyNext;
    _txtEmail.placeholder=@"Email";
    [_txtEmail setFont:[UIFont fontWithName:helveticaRegular size:15]];
    _txtEmail.tag=kTxtFEmail+0;
    
    
    UIView *_line=[[UIView alloc] init];
    _line.frame=CGRectMake(55, _txtEmail.frame.size.height+_txtEmail.frame.origin.y, _txtEmail.frame.size.width, 1);
    _line.backgroundColor=[UIColor lightGrayColor];
    [changeScrollview addSubview:_line];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _txtEmail.leftView = paddingView;
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    _txtEmail.keyboardType = UIKeyboardTypeEmailAddress;
    [_txtEmail setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtEmail.borderStyle = UITextBorderStyleNone;
    _txtEmail.textAlignment = NSTextAlignmentLeft;
    _txtEmail.text = emailForChngePass;
    _txtEmail.textColor=[UIColor darkGrayColor];
    [changeScrollview addSubview:_txtEmail];
    
    
    _txtEmail.backgroundColor=[UIColor clearColor];
    
    
    UIImageView *passwordImageView=[[UIImageView alloc]init];
    passwordImageView.frame=CGRectMake(30,_txtEmail.frame.size.height+_txtEmail.frame.origin.y+25, [UIImage imageNamed:@"settingpasswordIcon.png"].size.width, [UIImage imageNamed:@"settingpasswordIcon.png"].size.height);
    passwordImageView.image=[UIImage imageNamed:@"settingpasswordIcon.png"];
    passwordImageView.backgroundColor=[UIColor clearColor];
    [changeScrollview addSubview:passwordImageView];
    
    
   _txtPassword = [self creareTextField:CGRectMake(50,_txtEmail.frame.origin.y+ _txtEmail.frame.size.height+10.0, self.view.frame.size.width-90, 45) backgroundColor:[UIColor clearColor] placeHolder:@"Password" placeHolderColor:[UIColor darkGrayColor] textColor:[UIColor lightGrayColor]];
    _txtPassword.delegate = self;
    _txtPassword.returnKeyType = UIReturnKeyNext;
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _txtPassword.leftView = paddingView1;
    _txtPassword.placeholder=@"Password";
    [_txtPassword setFont:[UIFont fontWithName:helveticaRegular size:15]];
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtPassword.keyboardType = UIKeyboardTypeAlphabet;
    _txtPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtPassword.textAlignment = NSTextAlignmentLeft;
    [_txtPassword setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtPassword.secureTextEntry = YES;
    _txtPassword.textColor=[UIColor darkGrayColor];
    [changeScrollview addSubview:_txtPassword];
    
    _txtPassword.backgroundColor=[UIColor clearColor];
    
    _txtPassword.tag=kTxtFEmail+1;
    
    UIView *_line2=[[UIView alloc] init];
    _line2.frame=CGRectMake(55, _txtPassword.frame.size.height+_txtPassword.frame.origin.y, _txtEmail.frame.size.width, 1);
    _line2.backgroundColor=[UIColor lightGrayColor];
    [changeScrollview addSubview:_line2];
    
    
    //--------------
    
    UIImageView *confirmpasswordImageView=[[UIImageView alloc]init];
    confirmpasswordImageView.frame=CGRectMake(30,_txtPassword.frame.size.height+_txtPassword.frame.origin.y+25, [UIImage imageNamed:@"settingpasswordIcon.png"].size.width, [UIImage imageNamed:@"settingpasswordIcon.png"].size.height);
    confirmpasswordImageView.image=[UIImage imageNamed:@"settingpasswordIcon.png"];
    confirmpasswordImageView.backgroundColor=[UIColor clearColor];
    [changeScrollview addSubview:confirmpasswordImageView];
    
    
    _txtConfirmPassword = [self creareTextField:CGRectMake(50,_txtPassword.frame.origin.y+ _txtPassword.frame.size.height+10.0, self.view.frame.size.width-90, 45) backgroundColor:[UIColor clearColor] placeHolder:@"Password" placeHolderColor:[UIColor darkGrayColor] textColor:[UIColor lightGrayColor]];
    _txtConfirmPassword.delegate = self;
    _txtConfirmPassword.returnKeyType = UIReturnKeyDone;
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _txtConfirmPassword.leftView = paddingView2;
    _txtConfirmPassword.placeholder=@"Confirm Password";
    [_txtConfirmPassword setFont:[UIFont fontWithName:helveticaRegular size:15]];
    _txtConfirmPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtConfirmPassword.keyboardType = UIKeyboardTypeAlphabet;
    _txtConfirmPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtConfirmPassword.textAlignment = NSTextAlignmentLeft;
    [_txtConfirmPassword setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtConfirmPassword.secureTextEntry = YES;
    _txtConfirmPassword.textColor=[UIColor darkGrayColor];
    [changeScrollview addSubview:_txtConfirmPassword];
    _txtConfirmPassword.tag=kTxtFEmail+2;
    

    UIView *_line3=[[UIView alloc] init];
    _line3.frame=CGRectMake(55, _txtConfirmPassword.frame.size.height+_txtConfirmPassword.frame.origin.y, _txtEmail.frame.size.width, 1);
    _line3.backgroundColor=[UIColor lightGrayColor];
    [changeScrollview addSubview:_line3];
    

    UIButton *_btnLoginIn = [self createButton:CGRectMake(0, _txtConfirmPassword.frame.size.height+_txtConfirmPassword.frame.origin.y+40, [UIImage imageNamed:@"Button.png"].size.width, [UIImage imageNamed:@"Button.png"].size.height) bckgroundColor:[UIColor clearColor] image:nil title:@"Change Password" font:[UIFont fontWithName:helveticaRegular size:20.0] titleColor:[UIColor blackColor]];
    _btnLoginIn.center=CGPointMake(self.view.center.x, _txtConfirmPassword.frame.size.height+_txtConfirmPassword.frame.origin.y+50);
    [_btnLoginIn addTarget:self action:@selector(changePasswordButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
     _btnLoginIn.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
    [_btnLoginIn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [changeScrollview addSubview:_btnLoginIn];
    
    
    UIButton *_btntCancel = [self createButton:CGRectMake(20, _btnLoginIn.frame.size.height+_btnLoginIn.frame.origin.y+5, _btnLoginIn.frame.size.width, _btnLoginIn.frame.size.height) bckgroundColor:[UIColor clearColor] image:nil title:@"Cancel" font:[UIFont fontWithName:helveticaRegular size:20.0] titleColor:[UIColor blackColor]];
    _btntCancel.center=CGPointMake(self.view.center.x, _btnLoginIn.frame.size.height+_btnLoginIn.frame.origin.y+30);
    [_btntCancel addTarget:self action:@selector(cancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    _btntCancel.backgroundColor=[UIColor colorWithRed:137.0/255.0 green:138.0/255.0 blue:139.0/255.0 alpha:1.0];
    [_btntCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  //  [changeScrollview addSubview:_btntCancel];
    
    
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDidTapped)] ;
    tapRecognizer.numberOfTapsRequired = 1;
    [changeScrollview addGestureRecognizer:tapRecognizer];
    
    
    [self.view addSubview:changeScrollview];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

   // [self callUserProfileWebService];
}


//-(void)callUserProfileWebService{
//    
//    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
//    inputPram.methodType = WebserviceMethodTypePost;
//    inputPram.relativeWebservicePath = @"getUserDetails";
//    [inputPram.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
//    
//    
//    
//    [WebserviceManager callWebserviceWithInputParame:inputPram
//                                          completion:^(id response, NSError *error) {
//                                              if (error) {
//                                                  if ([error isErrorOfKindNoNetwork])
//                                                      [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//                                                  return ;
//                                                  
//                                                  return;
//                                              }
//                                              
//                                              error = nil;
//                                              responseDictionary = [NSJSONSerialization JSONObjectWithData:response
//                                                                                             options:NSJSONReadingMutableContainers
//                                                                                               error:&error];
//                                              if ([responseDictionary count])
//                                                  [self performSelectorOnMainThread:@selector(showProfileData) withObject:responseDictionary waitUntilDone:YES];
//                                              
//                                          }];
//    
//}
//
//
//
//-(void)showProfileData
//{
//    _txtEmail.text = [[[responseDictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"Email"];
//    NSLog(@"txtEmail.text=%@",_txtEmail.text);
//    
//}




- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];

    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
    
}

#pragma mark- scrollViewDidTapped
-(void)scrollViewDidTapped
{
    [self.view endEditing:YES];
}
#pragma mark-viewWillAppear
-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)                                                       name:UIKeyboardWillHideNotification
                                               object:nil];
    
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- viewWillDisappear
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
-(void)changePasswordButtonAction{
    [self btnChangeClk];
}
-(void)cancelButtonAction{
    
}
-(void)hideKeyboardclk
{
    UITextField *TxtFEmail=(UITextField *)[self.view viewWithTag:kTxtFEmail];
    UITextField *TxtFNewPassword=(UITextField *)[self.view viewWithTag:kTxtFEmail+1];
    UITextField *TxtFConfirmPassword=(UITextField *)[self.view viewWithTag:kTxtFEmail+2];
    
    [TxtFEmail resignFirstResponder];
    [TxtFNewPassword resignFirstResponder];
    [TxtFConfirmPassword resignFirstResponder];

}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


//-(void)btnChangeClk
//{
//    BOOL isValid=YES;
//    for (int i=0; i<[arrData count]; i++)
//    {
//        UITextField *TxtF=(UITextField *)[self.view viewWithTag:kTxtFEmail+i];
//        if ([TxtF.text length]==0)
//        {
//            isValid=NO;
//            [[[UIAlertView alloc] initWithTitle:@"NellPat" message:[NSString stringWithFormat:@"Please enter %@",[arrData objectAtIndex:i]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//            break;
//        }
//        if (i==2)
//        {
//            UITextField *TxtPassword=(UITextField *)[self.view viewWithTag:kTxtFEmail+1];
//            
//            if ([TxtF.text isEqualToString:TxtPassword.text])
//            {
//                
//            }
//            
//            else
//            {
//                isValid=NO;
//               [[[UIAlertView alloc]initWithTitle:@"NellPat" message:@"Password does not match." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//                break;
//            }
//        }
//    }
//    
//    
//    if (isValid && [arrData count]>0)
//    {
//        [self CallForChangePassword];
//   
//    }
//
//}



-(void)btnChangeClk
{
    BOOL isValid=YES;
    for (int i=0; i<[arrData count]; i++)
    {
        UITextField *TxtF=(UITextField *)[self.view viewWithTag:kTxtFEmail+i];
        if ([TxtF.text length]==0)
        {
            isValid=NO;
            [[[UIAlertView alloc] initWithTitle:@"Hynell" message:[NSString stringWithFormat:@"Please enter %@",[arrData objectAtIndex:i]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            break;
        }
        if (i==2)
        {
            UITextField *TxtPassword=(UITextField *)[self.view viewWithTag:kTxtFEmail+1];

            if ([TxtF.text isEqualToString:TxtPassword.text])
            {

            }

            else
            {
                isValid=NO;
               [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Password does not match." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                break;
            }
        }
    }


    if (isValid && [arrData count]>0)
    {
        [self CallForChangePassword];

    }

}



-(void)btnCancelClk
{
    UITextField *TxtFEmail=(UITextField *)[self.view viewWithTag:kTxtFEmail];
    UITextField *TxtFNewPassword=(UITextField *)[self.view viewWithTag:kTxtFEmail+1];
    UITextField *TxtFConfirmPassword=(UITextField *)[self.view viewWithTag:kTxtFEmail+2];
    
    [TxtFEmail resignFirstResponder];
    [TxtFNewPassword resignFirstResponder];
    [TxtFConfirmPassword resignFirstResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)CallForChangePassword
{
    
//    UITextField *TxtFEmail=(UITextField *)[self.scrollBar viewWithTag:kTxtFEmail+0];
//    UITextField *TxtFPassword=(UITextField *)[self.scrollBar viewWithTag:kTxtFEmail+1];

    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"changepassword";
    
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    
    NSString *Str = [NSString stringWithUTF8String:[_txtEmail.text cStringUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"%@", Str);
    
    NSLog(@"%@", _txtEmail.text);
    
    NSString *Str1 = [NSString stringWithUTF8String:[_txtPassword.text cStringUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"%@", Str1);
    
    NSLog(@"%@", _txtPassword.text);
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(_txtEmail.text) forKey:@"email"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(_txtPassword.text) forKey:@"password"];

    
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error)
     {
        if (error)
        {
            if ([error isErrorOfKindNoNetwork])
                
                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return;
            
            return;
        }
        
        error = nil;
        self.responseDict =nil;
        self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
        NSString *strMessage=[self.responseDict objectForKey:@"log"];
        
        if ([strMessage isEqualToString:@"Password changed successfully."])
        {
            [CommonMethods showAlertWithTitle:@"Hynell" andMessage:strMessage];

            [self.navigationController popViewControllerAnimated:YES];
        }
        
        else if ([strMessage isEqualToString:@"Try Again."])
        {
            [CommonMethods showAlertWithTitle:@"Hynell" andMessage:strMessage];
        }
        
    }];
    
    
}


- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (IS_IPHONE_5)
    {
        
    }
    else if(IS_IPHONE)
    {
        if (textField.tag==kTxtFEmail+2)
        {
            [self.scrollBar setContentOffset:CGPointMake(0, 100) animated:YES];

        }
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.scrollBar setContentOffset:CGPointZero animated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//---------------------------------------------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UITextField UI
//--------------------------------------------------------------------------------------------------------------------------------------------------
-(UITextField*)creareTextField:(CGRect)frame backgroundColor:(UIColor*)backgroundColor placeHolder:(NSString*)placeHolder placeHolderColor:(UIColor*)placeHolderColor textColor:(UIColor*)textColor
//---------------------------------------------------------------------------------------------------------------------------------------------------
{
    UITextField *_txtField = [[UITextField alloc]initWithFrame:frame];
    _txtField.backgroundColor = [UIColor clearColor];
    _txtField.delegate = self;
    _txtField.returnKeyType = UIReturnKeyNext;
    _txtField.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtField.font=[UIFont fontWithName:helveticaLight size:18];
    _txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _txtField.keyboardType = UIKeyboardTypeEmailAddress;
    _txtField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtField.borderStyle = UITextBorderStyleNone;
    _txtField.textAlignment = NSTextAlignmentLeft;
    _txtField.textColor = textColor;
    return _txtField;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI

-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}
-(void)keyboardWillShow
{
    // Animate the current view out of the way
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        
    {
        
        if (screenSize.height > 480.0f)
            
        {
            heightofKeyboard = 300;
        }
        else{
            heightofKeyboard = 220;
            
        }
    }
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, heightofKeyboard, 0);
    
    [changeScrollview setContentInset:insets];
    [changeScrollview setScrollIndicatorInsets:insets];
    
}
-(void)keyboardWillHide
{
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [changeScrollview setContentInset:insets];
    [changeScrollview setScrollIndicatorInsets:insets];
    
}
#pragma mark- TouchBegan
//-----------------------------------------------------------------------------------------------------------------------------------------------
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.view endEditing:YES];
}

#pragma mark textFieldShouldReturn
/*********************************************** textFieldShouldReturn *****************************************/
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtEmail)
    {
        [_txtPassword becomeFirstResponder];
        return NO;
    }
    if (textField == _txtPassword)
    {
        [_txtConfirmPassword becomeFirstResponder];
        return NO;
    }
    else
    {
        //[self loginButtonAction];
        [self.view endEditing:YES];
    }
    return YES;
}


@end
