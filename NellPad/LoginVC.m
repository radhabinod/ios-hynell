//
//  LogVC.m
//  NellPad
//
//  Created by Herry Makker on 12/5/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import "LoginVC.h"
#import "IPManagementVC.h"
#include <objc/message.h>
#import "DataSyncBO.h"
#import "Constants.h"
#import "ForgotPasswordVC.h"
#import "AppDelegate.h"
#define kOFFSET_FOR_KEYBOARD 95

#define SeviceURL [NSURL URLWithString:@"http://app.hynell.se/hynellapptest/userlogin1"]

@interface LoginVC ()
{
    @private
    NSDictionary *responseForgotMail;
    CGFloat heightofKeyboard;
    UIScrollView *_scrollView;
    UITextField *_txtEmail;
    UITextField *_txtPassword;
    
    UIAlertView *downloadAlert;
    
}

@end

@implementation LoginVC

@synthesize usernameField = _usernameField;
@synthesize passwordField = _passwordField;

int i;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//    {
//        self = [super initWithNibName:@"LoginVC" bundle:nibBundleOrNil];
//        
//        if (self)
//        {
//            // Custom initialization
//        }
//    }
//    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        self = [super initWithNibName:@"LoginVC_iPad" bundle:nibBundleOrNil];
//        
//        if (self)
//        {
//            // Custom initialization
//        }
//    }
//    
//    return self;
//}

-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)                                                       name:UIKeyboardWillHideNotification
                                               object:nil];

}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- viewWillDisappear
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (IBAction) buttonResign:(id) sender
{
    [self.usernameField resignFirstResponder];
    [self.passwordField  resignFirstResponder];
    
    [forgotPasswordView setHidden:YES];
    [loginView setHidden:NO];
    
    [self.forgotEmailField resignFirstResponder];
    self.title = @"Log In";
}

- (void)viewDidLoad
{
    
     //self.view.backgroundColor=[UIColor yellowColor];
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
            
            
        }
    }
    
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
    
    [super viewDidLoad];
	self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
   // [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    self.title = @"Log In";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize: 18], NSFontAttributeName, nil]];
    
    if (IS_IPAD)
    {
        self.usernameField.frame=CGRectMake(169, 20, 450, 50);
        self.passwordField.frame=CGRectMake(169, 130, 450, 50);
        self.forgotEmailField.frame=CGRectMake(32, 49, 450, 50);
    }
    
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UITapGestureRecognizer *gst=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hidekeyboardForgotPassword)];
    gst.delegate=self;
    gst.cancelsTouchesInView=NO;
    [loginView addGestureRecognizer:gst];

        
       _scrollView = [[UIScrollView alloc]init];
        _scrollView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-65);
         _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height-65);
        
        _scrollView.backgroundColor = [UIColor clearColor];
       _scrollView.delegate=self;
        
        UIImageView *locimage=[[UIImageView alloc]init];
        locimage.frame=CGRectMake(0, 0, [UIImage imageNamed:@"NellPat_logo"].size.width, [UIImage imageNamed:@"NellPat_logo"].size.height);
        locimage.center=CGPointMake(self.view.center.x, 100);
        locimage.image=[UIImage imageNamed:@"NellPat_logo"];
        locimage.backgroundColor=[UIColor clearColor];
        [_scrollView addSubview:locimage];
        
        
        UIImageView *emailImageView=[[UIImageView alloc]init];
        emailImageView.frame=CGRectMake(30,locimage.frame.size.height+locimage.frame.origin.y+50, [UIImage imageNamed:@"emailIcon.png"].size.width, [UIImage imageNamed:@"emailIcon.png"].size.height);
        emailImageView.image=[UIImage imageNamed:@"emailIcon.png"];
        emailImageView.backgroundColor=[UIColor clearColor];
        [_scrollView addSubview:emailImageView];

        
       _txtEmail = [self creareTextField:CGRectMake(50,locimage.frame.size.height+locimage.frame.origin.y+30, self.view.frame.size.width-90, 45) backgroundColor:[UIColor clearColor] placeHolder:@"Email" placeHolderColor:[UIColor darkGrayColor] textColor:[UIColor lightGrayColor]];
        _txtEmail.delegate = self;
        _txtEmail.returnKeyType = UIReturnKeyNext;
        _txtEmail.placeholder=@"Email";
        [_txtEmail setFont:[UIFont fontWithName:helveticaRegular size:15]];
        
        
        UIView *_line=[[UIView alloc] init];
        _line.frame=CGRectMake(55, _txtEmail.frame.size.height+_txtEmail.frame.origin.y, _txtEmail.frame.size.width, 1);
    
        _line.backgroundColor=[UIColor lightGrayColor];
    
    [_scrollView addSubview:_line];
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    
    _txtEmail.leftView = paddingView;
    
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
        _txtEmail.keyboardType = UIKeyboardTypeEmailAddress;
        [_txtEmail setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        _txtEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
        _txtEmail.borderStyle = UITextBorderStyleNone;
        _txtEmail.textAlignment = NSTextAlignmentLeft;
      _txtEmail.textColor=[UIColor darkGrayColor];
        _txtEmail.text = @"";
    //_txtEmail.text=@"lennart@arnessons.se";
     //_txtEmail.text=@"peter.kylin@hynell.se";
    
       // _txtEmail.text=@"info@rwlpatents.se";
   // _txtEmail.text=@"eva.qvarnstrom@nellpat.se";
    
        [_scrollView addSubview:_txtEmail];
        
        
        _txtEmail.backgroundColor=[UIColor clearColor];
        
        
        UIImageView *passwordImageView=[[UIImageView alloc]init];
        passwordImageView.frame=CGRectMake(30,_txtEmail.frame.size.height+_txtEmail.frame.origin.y+25, [UIImage imageNamed:@"passwordIcon.png"].size.width, [UIImage imageNamed:@"passwordIcon.png"].size.height);
        passwordImageView.image=[UIImage imageNamed:@"passwordIcon.png"];
        passwordImageView.backgroundColor=[UIColor clearColor];
        [_scrollView addSubview:passwordImageView];
        
        
        
    _txtPassword = [self creareTextField:CGRectMake(50,_txtEmail.frame.origin.y+ _txtEmail.frame.size.height+10.0, self.view.frame.size.width-90, 45) backgroundColor:[UIColor clearColor] placeHolder:@"Password" placeHolderColor:[UIColor darkGrayColor] textColor:[UIColor lightGrayColor]];
        _txtPassword.delegate = self;
        _txtPassword.returnKeyType = UIReturnKeyDone;
        UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
        _txtPassword.leftView = paddingView1;
        _txtPassword.placeholder=@"Password";
        [_txtPassword setFont:[UIFont fontWithName:helveticaRegular size:15]];
        _txtPassword.leftViewMode = UITextFieldViewModeAlways;
        _txtPassword.keyboardType = UIKeyboardTypeAlphabet;
        _txtPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
        _txtPassword.textAlignment = NSTextAlignmentLeft;
        [_txtPassword setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        _txtPassword.secureTextEntry = YES;
        //_txtPassword.text=@"12345";
        _txtPassword.text=@"";
      _txtPassword.textColor=[UIColor darkGrayColor];
        [_scrollView addSubview:_txtPassword];
        
        _txtPassword.backgroundColor=[UIColor clearColor];
        
        
        UIView *_line2=[[UIView alloc] init];
        _line2.frame=CGRectMake(55, _txtPassword.frame.size.height+_txtPassword.frame.origin.y, _txtEmail.frame.size.width, 1);
        _line2.backgroundColor=[UIColor lightGrayColor];
        [_scrollView addSubview:_line2];
        
        UIButton *_btnLoginIn = [self createButton:CGRectMake(0, _txtPassword.frame.size.height+_txtPassword.frame.origin.y+40, [UIImage imageNamed:@"Button.png"].size.width, [UIImage imageNamed:@"Button.png"].size.height) bckgroundColor:[UIColor clearColor] image:nil title:@"Login" font:[UIFont fontWithName:helveticaRegular size:20.0] titleColor:[UIColor blackColor]];
        _btnLoginIn.center=CGPointMake(self.view.center.x, _txtPassword.frame.size.height+_txtPassword.frame.origin.y+50);
        [_btnLoginIn addTarget:self action:@selector(loginButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [_btnLoginIn setBackgroundImage:[UIImage imageNamed:@"Button.png"] forState:UIControlStateNormal];
        [_btnLoginIn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

        [_scrollView addSubview:_btnLoginIn];
        
        
        UIButton *_btntForgotPassword = [self createButton:CGRectMake(150, _btnLoginIn.frame.size.height+_btnLoginIn.frame.origin.y+5, 150, 30) bckgroundColor:[UIColor clearColor] image:nil title:@"Forgot Password?" font:[UIFont fontWithName:helveticaRegular size:17.0] titleColor:[UIColor blackColor]];
         _btntForgotPassword.center=CGPointMake(self.view.center.x, _btnLoginIn.frame.size.height+_btnLoginIn.frame.origin.y+20);
        [_btntForgotPassword addTarget:self action:@selector(forgotPasswordControllerservercall) forControlEvents:UIControlEventTouchUpInside];
        
        _btntForgotPassword.backgroundColor=[UIColor clearColor];
        [_btntForgotPassword setTitleColor:[UIColor colorWithRed:0.0/255.0 green:100.0/255.0 blue:218.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_scrollView addSubview:_btntForgotPassword];
    
        
    
    
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDidTapped)] ;
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    
//    UIImageView *locimage1=[[UIImageView alloc]init];
//    locimage1.frame=CGRectMake(0, 0, [UIImage imageNamed:@"lockIcon.png"].size.width, [UIImage imageNamed:@"lockIcon.png"].size.height);
//    locimage1.center=CGPointMake(self.view.center.x, 110);
//    locimage1.image=[UIImage imageNamed:@"lockIcon.png"];
//    locimage1.backgroundColor=[UIColor clearColor];
//    [_scrollView addSubview:locimage1];
//    
    
    [self.view addSubview:_scrollView];
    
}
-(void)loginButtonAction{
    
    
    [self hitServiceWithaBaseballbat];
    
}
-(void)forgotPasswordControllerservercall{

    [self.view endEditing:YES];
    
    ForgotPasswordVC *obj=[[ForgotPasswordVC alloc]init];
    [self.navigationController pushViewController:obj animated:YES];
    
    
    
}

//---------------------------------------------------------------------------------------------------------------------------------
-(void)scrollViewDidTapped
//---------------------------------------------------------------------------------------------------------------------------------
{
    [self.view endEditing:YES];
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI

-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}

//---------------------------------------------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UITextField UI
//--------------------------------------------------------------------------------------------------------------------------------------------------
-(UITextField*)creareTextField:(CGRect)frame backgroundColor:(UIColor*)backgroundColor placeHolder:(NSString*)placeHolder placeHolderColor:(UIColor*)placeHolderColor textColor:(UIColor*)textColor
//---------------------------------------------------------------------------------------------------------------------------------------------------
{
    UITextField *_txtField = [[UITextField alloc]initWithFrame:frame];
    _txtField.backgroundColor = [UIColor clearColor];
    _txtField.delegate = self;
    _txtField.returnKeyType = UIReturnKeyNext;
    _txtField.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtField.font=[UIFont fontWithName:helveticaLight size:18];
    _txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _txtField.keyboardType = UIKeyboardTypeEmailAddress;
    _txtField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtField.borderStyle = UITextBorderStyleNone;
    _txtField.textAlignment = NSTextAlignmentLeft;
    _txtField.textColor = textColor;
    return _txtField;
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    
//    _usernameField.text = [USER_LOGINID valueForKey:@"UserLoginID"];
//    [forgotPasswordView setHidden:YES];
//    //    self.navigationItem.hidesBackButton=YES;
//    
//    
//#if kShouldLogin
//    //    self.usernameField.text = kUserName;
//    //    self.passwordField.text = kUserPwd;
//#endif
//}
//
//- (BOOL)shouldAutorotate {
//    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
//        return YES;
//    }
//    return NO;
//}
//
//-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientationPortrait;
//}
//
////- (NSUInteger)supportedInterfaceOrientations {
////    return UIInterfaceOrientationMaskPortrait;
////}
//
-(void)hidekeyboardForgotPassword
{
    [self.usernameField resignFirstResponder];
    [self.passwordField  resignFirstResponder];
    
    [forgotPasswordView setHidden:YES];
    [loginView setHidden:NO];
    
    [self.forgotEmailField resignFirstResponder];
    self.title = @"Log in";
    
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self unloadView];
//}
//
//-(void)unloadView
//{
//
//}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - Email Validation

-(void)hitServiceWithaBaseballbat
{
    [self.view endEditing:YES];
    
    BOOL isValidEmail = [CommonMethods validateEmail:_txtEmail.text];
    if((!_txtEmail.text.length) || (!_txtPassword.text.length)) {
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"Please fill email & password for login."];
        return;
    }
    if (isValidEmail)
        [self post];
    else
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"Please enter a valid email address."];
}

-(void)post
{
    
    NSLog(@"Device token =%@",APP_DELEGATE.Devtoken);
    
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"userlogin1";
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(_txtEmail.text) forKey:@"email"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(_txtPassword.text) forKey:@"password"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(APP_DELEGATE.Devtoken) forKey:@"deviceid"];
    
    NSLog(@"inputPram=%@",inputPram);
    NSLog(@"Device token =%@",GET_VALID_STRING(APP_DELEGATE.Devtoken));

    
    
    
    
    [WebserviceManager callWebserviceWithInputParame:inputPram
                                          completion:^(id response, NSError *error) {
                                              if (error) {
                                                  if ([error isErrorOfKindNoNetwork])
                                                      [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                                                  return ;
                                                  
                                                  return;
                                              }
                                              
                                              error = nil;
                                              responseDict = [NSJSONSerialization JSONObjectWithData:response
                                                                                             options:NSJSONReadingMutableContainers
                                                                                               error:&error];
                                              if ([responseDict count])
                                              {
                                                  
                                                  [self performSelectorOnMainThread:@selector(login) withObject:self waitUntilDone:YES];
                                              }
                                              
                                          }];
}

-(void)login
{
    if ([[responseDict objectForKey:@"log"] isEqualToString:@"Login Successful."])
    {
        NSString *strUserStatus=[[[responseDict objectForKey:@"data"] objectAtIndex:0] objectForKey:@"user_status"];
        [USER_DEFAULTS setObject:strUserStatus forKey:@"user_status"];
        [USER_DEFAULTS setObject:[[[responseDict objectForKey:@"data"]lastObject]objectForKey:@"access_token"] forKey:@"token"];
        
        NSString *strAppVersion=[[[responseDict objectForKey:@"data"] objectAtIndex:0] objectForKey:@"ios_version"];
        [USER_DEFAULTS setObject:strAppVersion forKey:@"ios_version"];
        
        NSLog(@"iOS App Version  :%@",strAppVersion);
       
        
        [USER_LOGINID  setObject:_usernameField.text forKey:@"UserLoginID"];
        [USER_LOGINID synchronize];
        [USER_DEFAULTS synchronize];
        
        if ([[USER_DEFAULTS objectForKey:@"user_status"] integerValue] == 1)
        {
            
            [USER_DEFAULTS setObject:@"1" forKey:@"Customer"];
            
            [USER_DEFAULTS synchronize];
        }
        else
        {
            [USER_DEFAULTS setObject:@"0" forKey:@"Customer"];
            
            [USER_DEFAULTS synchronize];
        }
        
        DataSyncBO *dataSyncBo=[DataSyncBO new];
        [dataSyncBo LoginCall];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefs setObject:_txtEmail.text forKey:@"emailForChangePassView"];
        [prefs synchronize];
               
        IPManagementVC *obj_IPManagement_VC = [[IPManagementVC alloc] init];
        obj_IPManagement_VC.emailForChngePass =_txtEmail.text;
        [self.navigationController pushViewController:obj_IPManagement_VC animated:YES];
        
        
        
// set latest version of this app if user wants to download = =  = = = = =  = = = = = = = = == = = = = = = == = = = = = = = == = = = = ====
        
        NSString *strAppVersion1 =[USER_DEFAULTS objectForKey:@"ios_version"];
        float b = [strAppVersion1 floatValue];
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        float a = [version floatValue];

        if ( a < b )
        {
          downloadAlert =  [[UIAlertView alloc]initWithTitle:@"Latest Version" message:[NSString stringWithFormat:@"Download latest version %@ of this application.",strAppVersion1] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Download", nil];
            [downloadAlert show];
        }
        else if ([[USER_DEFAULTS objectForKey:@"ios_version"] integerValue] == [strAppVersion integerValue])
        {
            downloadAlert = nil;
        }
        
        
 //= =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = ====
        
    }
    else
        
    {
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"Please enter correct email and password."];
    }
    
}


#pragma mark - UIAlertView methods for download latest app by its version on "apple store" 

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        
    } else if(buttonIndex == 1)
    {
        NSString *appName = [NSString stringWithString:[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"]];
        NSURL *appStoreURL = [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.com/app/%@",[appName stringByReplacingOccurrencesOfString:@" " withString:@""]]];
        //   itms-apps://itunes.com/app/
        
        [[UIApplication sharedApplication] openURL:appStoreURL];

     //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/"]];
//        //[self refreshTable];
//        // [self.casesArray removeAllObjects];
//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"CaseListdownloaded" object:nil];
//        DataSyncBO *refreshData = [[DataSyncBO alloc]init];
//        [refreshData caseList];
        
    }
}


-(void)forgotPassword
{
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"";
    [inputParams.dict_postParameters setObject:GET_VALID_STRING(self.forgotEmailField.text)
                                        forKey:@"email"];
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error) {
             if ([error isErrorOfKindNoNetwork])
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         }
         error = nil;
         responseForgotMail = [NSJSONSerialization JSONObjectWithData:response
                                                              options:NSJSONReadingMutableContainers
                                                                error:&error];
         [self performSelectorOnMainThread:@selector(submitEmail) withObject:self waitUntilDone:YES];
         
     }];
    
}

-(void)submitEmail
{
    if ([responseDict[@"log"] isEqualToString:@"Password sent successfully."])
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:responseDict[@"log"]];
    else
        [CommonMethods showAlertWithTitle:@"Error!!" andMessage:responseDict[@"error"]];
    
}

- (IBAction)btn_Submit_Clicked:(id)sender
{
    [self.view endEditing:YES];
    
    BOOL success = [CommonMethods validateEmail:self.forgotEmailField.text];
    if (success)
        [self forgotPassword];
    else
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"Please enter a valid email address."];
}
- (IBAction)btnLoginClicked:(id)sender
{
    [self hitServiceWithaBaseballbat];
}
- (IBAction)btnForgotPasswordClicked:(id)sender
{
    self.title = @"Forgot Password";
    forgotPasswordView.hidden = NO;
    loginView.hidden = YES;
    [self.forgotEmailField becomeFirstResponder];
}

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        
    {
        
        if (screenSize.height > 480.0f)
            
        {
            heightofKeyboard = 240;
        }
        else{
            heightofKeyboard = 220;
            
        }
    }
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, heightofKeyboard, 0);
    
    [_scrollView setContentInset:insets];
    [_scrollView setScrollIndicatorInsets:insets];
    
}
-(void)keyboardWillHide
{
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [_scrollView setContentInset:insets];
    [_scrollView setScrollIndicatorInsets:insets];
    
}
#pragma mark- TouchBegan
//-----------------------------------------------------------------------------------------------------------------------------------------------
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.view endEditing:YES];
}

#pragma mark- TextField Delegates
/********************************************** TextField Delegates ******
 *******************************/
-(void)textFieldDidBeginEditing:(UITextField *)textField
//----------------------------------------------------------------------------------------------------------------------------------------
{
    //iPhone 4s
    if (kDeviceHeight==480.0) {
        [UIView animateWithDuration:0.2f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                            // _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y-50, _scrollView.frame.size.width, _scrollView.frame.size.height);
                             
                         }
                         completion:nil];
    }else{
        [UIView animateWithDuration:0.2f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                           // _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y-10, _scrollView.frame.size.width, _scrollView.frame.size.height);
                         }
                         completion:nil];
        
    }
}
#pragma mark textFieldShouldReturn
/*********************************************** textFieldShouldReturn *****************************************/
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtEmail)
    {
        [_txtPassword becomeFirstResponder];
        return NO;
    }
    else
    {
        //[self loginButtonAction];
        [self.view endEditing:YES];
    }
    return YES;
}
@end
