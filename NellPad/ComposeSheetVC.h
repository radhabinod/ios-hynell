//
//  ComposeSheetVC.h
//  NellPad
//
//  Created by Sanjeev Kumar on 12/20/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "User.h"


@class Agent;
@class Customer;
//@class User;
@interface ComposeSheetVC : UIViewController <UITextFieldDelegate,UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>
 {
     NSDictionary *responseDictDropDown;
     NSDictionary *responseDictSendMessage;

     Agent *agent;
     Customer *customer;
     User *loggedInuser;
     IBOutlet UIView *viewDone, *viewDoneiPad;
}
@property(strong,nonatomic) UITableView *tbleUsers;
@property(strong,nonatomic) NSMutableArray *arrUsersList;
@property(nonatomic,strong) NSString *agent_id;
@property(nonatomic,strong) UIView *viewForReply;
@property(nonatomic,strong) NSString *subject;
@property(nonatomic,strong) NSString *user_Type;
@property(strong,nonatomic) NSString *relation_id;
@property(strong,nonatomic) UITextView *txtVSendTo;
@property(strong,nonatomic) UITextView *txtVSubjectField;
@property(strong,nonatomic) UITextView *txtVMessageView;
@property(strong,nonatomic) UIButton *btndropdown;
@property(nonatomic,strong) NSString *strScreenType;
@property(nonatomic,strong) NSString *strSubject;
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) UIView *viewForAgentsListBG;
@property(nonatomic,strong) UIView *viewForAgentsList;
@property(nonatomic,strong) NSString *receiverEmailStr;
@property(nonatomic,strong) NSString *receiverNameStr;
@property(nonatomic,strong) NSString *receiverIDStr;


@end
