//
//  MessageVC.m
//  NellPad
//
//  Created by Sanjeev Kumar on 12/19/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import "MessageVC.h"
#import "ComposeSheetVC.h"
#import "Agent.h"
#import "Message.h"
#import <objc/message.h>
#import "InboxVC.h"




@interface MessageVC ()
{
    UIView * selectedView;
}
@property(nonatomic,strong) NSString *strReciverName;
@property(nonatomic,strong) NSString *strReciverEmail;
@property(nonatomic,strong) NSString *strReciverID;


@end

@implementation MessageVC

@synthesize arrChatMessages,tblViewChat,objMessageBO,sendTo,btnDropDown,replyTextView,replyView,responseDictReplyMessage,payLoadArrayCopy,actionSheetSort,arrUsersList,viewForAgentsListBG,viewForAgentsList,tblUsers,strSelectedAgentID,lblMessageReciever,strReciverName,strReciverEmail,strReciverID;
NSString *TXTFileName;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
            
            
        }
    }
    
    self.strReciverName=[[NSString alloc]init];
    self.strReciverID=[[NSString alloc]init];
    self.strReciverName=[[NSString alloc]init];

    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    
    self.title = @"Message";
    
    self.view.backgroundColor = UIColorFromRGB(0xF0F0F0);
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize: 18], NSFontAttributeName, nil]];
    
    arrChatMessages = [[NSMutableArray alloc]init];
    arrUsersList=[[NSMutableArray alloc]init];
    
    NSString *actionSheetTitle = @"Choose Sorting Type";
    NSString *other1 = @"Sort By Latest";
    NSString *other2 = @"Sort By Oldest";
    NSString *cancelTitle = @"Cancel";
    self.strSelectedAgentID=[[NSString alloc]init];
    actionSheetSort = [[UIActionSheet alloc] initWithTitle:actionSheetTitle delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:other1, other2, nil];
   
    emailBody = [[NSString alloc] init];
    
   [self DesignInterface];
    
 self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];
    
}

- (void) buttonBack
{
     [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [arrChatMessages removeAllObjects];
    [self CallForGetAllConversationOfAMessgae];

    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
    
}



-(void)CallForGetAllConversationOfAMessgae
{
    
    @try {
        
    NSString *strRelationId=self.objMessageBO.strRelation_Id;
    NSString *strConversationId=self.objMessageBO.strConversation_Id;
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"getinboxmessages";
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    [inputParams.dict_postParameters setObject:[NSNumber numberWithInt:[strRelationId intValue]] forKey:@"relation_id"];
    [inputParams.dict_postParameters setObject:[NSNumber numberWithInt:[strConversationId intValue]] forKey:@"conversation_id"];

    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error) {
             if ([error isErrorOfKindNoNetwork])
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         } else {
             error = nil;
             responseChatDict = [NSJSONSerialization JSONObjectWithData:response
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&error];
             [self performSelectorOnMainThread:@selector(ParseAllMessages) withObject:self waitUntilDone:YES];
             
             
             [self sortInboxinAscending];
             [self.tblViewChat reloadData];
             
         }
     }];
    }
    @catch (NSException *exception) {
        NSLog(@"exception == %@", exception);
    }
    @finally {
        
    }

}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}



-(void)ParseAllMessages
{
    NSArray *chatArr ;
    NSArray *payArr = [NSArray array];
    if ([arrChatMessages  count])
    {
        chatArr = [[responseChatDict objectForKey:@"data"] objectForKey:@"messages"];
        if (([chatArr count]) > [arrChatMessages count])
            [arrChatMessages removeAllObjects];
        arrChatMessages = [chatArr mutableCopy];
        chatArr = nil;
    }
    
    
    payArr = [[responseChatDict objectForKey:@"data"]objectForKey:@"messages" ];
    
 
    NSLog(@"Response Dict = %@", responseChatDict);
    
        emailBody = @"";
    strReciverName=[[responseChatDict objectForKey:@"data"]  objectForKey:@"receiver"];
    strReciverEmail=[[responseChatDict objectForKey:@"data"]  objectForKey:@"receiveremail"];
    strReciverID=[[responseChatDict objectForKey:@"data"]  objectForKey:@"receiverid"];

    
    for (NSDictionary *messageDict in payArr)
    {
        Message *message= [[Message alloc] init];
        
        message.strRelation_Id = GET_VALID_STRING(messageDict[@"Chat_Relation_Id"]);
        message.strDateTime = GET_VALID_STRING(messageDict[@"DateTime"]);
        message.strMessage_Id = GET_VALID_STRING(messageDict[@"Message_Id"]);
        message.strSenderName=GET_VALID_STRING(messageDict[@"Name"]);
        message.strRead_Status = GET_VALID_STRING(messageDict[@"Read_Status"]);
        message.strSenderId=GET_VALID_STRING(messageDict[@"SenderId"]);
        message.strSubject = GET_VALID_STRING(messageDict[@"Subject"]);
        
        
        
        message.strMessage = GET_VALID_STRING(messageDict[@"Text_Message"]);
        
        message.you = messageDict[@"you"];

        [arrChatMessages addObject:message];
    }
    
    
    lblMessageReciever.text = [NSString stringWithFormat:@"Conversation with %@", self.strReciverName];
    
//    NSLog(@"receiver nammeeeeee:%@",self.strReciverName);
//    NSLog(@"receiver emaill:%@",self.strReciverEmail);
//    NSLog(@"receiver IDss:%@",self.strReciverID);

    

    [self CallForGetListOfUsers];

}



-(void)DesignInterface
{
    NSString *strConversationId=self.objMessageBO.strConversation_Id;
   //  NSString *strConversationId=@"";

    UIView *_bgview=[[UIView alloc]init];
    _bgview.frame=CGRectMake(0, 0, self.view.frame.size.width, 70);
    _bgview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:_bgview];
    
    
    sendTo =  [[UITextField alloc] initWithFrame:CGRectMake(10, 20, deviceWidth-20, 30)];
    sendTo.userInteractionEnabled = NO;
    sendTo.text = @"   Sort By Oldest";
    sendTo.backgroundColor=[UIColor whiteColor];
    sendTo.textColor = [UIColor darkGrayColor];
    sendTo.font = [UIFont systemFontOfSize:14];
    sendTo.layer.borderWidth = 0.5f;
    sendTo.layer.cornerRadius = 5.0;
    sendTo.layer.borderWidth = 0.5f;
    sendTo.clipsToBounds = YES;
    sendTo.layer.borderColor = UIColorFromRGB(0xA9A9A9).CGColor;
    sendTo.delegate = self;
    [self.view addSubview:sendTo];
    
    btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, deviceWidth-20, 30)];
    //btnDropDown.layer.cornerRadius = 5.0;
    [btnDropDown setBackgroundImage:[UIImage imageNamed:@"searchBox.png"] forState:UIControlStateNormal];
    btnDropDown.clipsToBounds = YES;
    [btnDropDown addTarget:self action:@selector(dropDown) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDropDown];
    
    
    lblMessageReciever = [[UILabel alloc] initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 25)];
    lblMessageReciever.font = [UIFont systemFontOfSize:IS_IPAD ? 17 : 14];
    lblMessageReciever.adjustsFontSizeToFitWidth=YES;
    lblMessageReciever.textAlignment=NSTextAlignmentCenter;
    lblMessageReciever.textColor = UIColorFromRGB(0x4174DA);
    lblMessageReciever.backgroundColor=[UIColor colorWithRed:207.0f/255.0f green:208.0f/255.0f blue:209.0f/255.0f alpha:1.0];
    [self.view addSubview:lblMessageReciever];
    
    
    self.tblViewChat = [[UITableView alloc] initWithFrame:CGRectMake(5, 95, deviceWidth-10,deviceHeight-220) style:UITableViewStylePlain];
    self.tblViewChat.delegate = self;
    self.tblViewChat.dataSource = self;
    self.tblViewChat.backgroundColor= [UIColor clearColor];
    //   self.tblViewChat.backgroundColor=[UIColor colorWithRed:245.0f/255.0f green:246.0f/255.0f blue:247.0f/255.0f alpha:1.0];
    //  self.tblViewChat.backgroundColor=[UIColor yellowColor];
    self.tblViewChat.showsVerticalScrollIndicator=NO;
    self.tblViewChat.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tblViewChat.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.tblUsers.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.tblViewChat];
    

    UIButton* composeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    composeButton.tag = 1;
    if ([strConversationId isEqualToString:@"0"])
    {
        composeButton = nil;
    }
    composeButton.frame = CGRectMake(0, deviceHeight-122, deviceWidth/3+1, 65);
    composeButton.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
    [composeButton addTarget:self action:@selector(bottomButtonsTapped:) forControlEvents:UIControlEventTouchUpInside];
    [composeButton setImage:[UIImage imageNamed:@"compose1.png"] forState:UIControlStateNormal];
    [self.view addSubview:composeButton];
    
    UIButton* shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.tag = 2;
    if ([strConversationId isEqualToString:@"0"])
    {
        shareButton = nil;
    }
    shareButton.frame = CGRectMake(composeButton.frame.size.width+1,  deviceHeight-122, deviceWidth/3+1, 65);
    shareButton.backgroundColor =[UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
    [shareButton setImage:[UIImage imageNamed:@"share1.png"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(bottomButtonsTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: shareButton];
    
    UIButton* replyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    replyButton.tag = 3;
    if ([strConversationId isEqualToString:@"0"])
    {
        replyButton = nil;
    }
    replyButton.frame = CGRectMake(shareButton.frame.origin.x + shareButton.frame.size.width+1,  deviceHeight-122, deviceWidth/3+1, 65);
    replyButton.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
    [replyButton setImage:[UIImage imageNamed:@"reply1.png"] forState:UIControlStateNormal];
    [replyButton addTarget:self action:@selector(bottomButtonsTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: replyButton];
    
    UILabel *buttonsHideLabel = [[UILabel alloc]initWithFrame:CGRectMake(62,deviceHeight-122,self.view.frame.size.width,65)];
    if ([strConversationId isEqualToString:@"0"])
    {
        buttonsHideLabel.text= @"You can't reply to admin.";
    }else{
        buttonsHideLabel.text = nil;
    }
    buttonsHideLabel.textColor= [UIColor lightGrayColor];
    [self.view addSubview:buttonsHideLabel];
}


- (void) sortInboxinDescending
{
    NSMutableArray *unsortedArr;
    
    unsortedArr = [arrChatMessages mutableCopy];
    
    [arrChatMessages removeAllObjects];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"strDateTime" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    arrChatMessages = [sortedArray mutableCopy];
    sortedArray = nil;
    
    [self.tblViewChat reloadData];
}

- (void) sortInboxinAscending
{
    NSMutableArray *unsortedArray = [arrChatMessages mutableCopy];
    [arrChatMessages  removeAllObjects];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"strDateTime" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    arrChatMessages = [sortedArray mutableCopy];
    sortedArray = nil;
    [self.tblViewChat reloadData];
}


- (void) dropDown
{
    [actionSheetSort showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:@"Sort By Latest"])
    {
        sendTo.text = @"   Sort By Latest";
        [self sortInboxinDescending];
    }
    else if ([buttonTitle isEqualToString:@"Sort By Oldest"])
    {
        sendTo.text = @"   Sort By Oldest";
        [self sortInboxinAscending];
    }
    else if ([buttonTitle isEqualToString:@"Cancel"])
    {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
}





- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)openPreviewcontrollerToShare
{
    NSMutableString *txtString=[[NSMutableString alloc]init];
    for (Message *objMessageTemp in arrChatMessages)
    {
        NSMutableArray *arrTemp=[NSMutableArray new];
        
        [arrTemp addObject:objMessageTemp.strDateTime];
        [arrTemp addObject:[NSString stringWithFormat:@"%@:",objMessageTemp.strSenderName]];
        [arrTemp addObject:objMessageTemp.strMessage];
        [txtString appendString:[arrTemp componentsJoinedByString:@" "]];
        
        
        [txtString appendString:@"\n"];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Chat Data.txt"]];
    [fileManager createFileAtPath:fullPath contents:[txtString dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    TXTFileName = [documentsDirectory stringByAppendingPathComponent:@"Chat Data.txt"];
    
    
    QLPreviewController *previewController = [[QLPreviewController alloc] init];
    previewController.dataSource = self;
    //previewController.title = @"ChatData";
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];

    //
//    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor]];
//    
//    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:29.0/255.0f green:155.0/255.0f blue:203.0f/255.0f alpha:1.0]];
    //self.title = @"ChatData";
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
 
    //   self.navigationController.navigationBar.translucent = YES;

    [self presentViewController:previewController animated:NO completion:^(void){
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        
        UIView *view = [[[previewController.view.subviews lastObject] subviews] lastObject];
        if ([view isKindOfClass:[UINavigationBar class]])
        {
            ((UINavigationBar *)view).translucent = NO;
        }
        
    }];
}

- (void)openMail
{
    NSMutableString *txtString=[[NSMutableString alloc]init];
    for (Message *objMessageTemp in arrChatMessages)
    {
        NSMutableArray *arrTemp=[NSMutableArray new];
        
        [arrTemp addObject:objMessageTemp.strDateTime];
        [arrTemp addObject:[NSString stringWithFormat:@"%@:",objMessageTemp.strSenderName]];
        [arrTemp addObject:objMessageTemp.strMessage];
        [txtString appendString:[arrTemp componentsJoinedByString:@" "]];
        
        
        [txtString appendString:@"\n"];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Chat Data.txt"]];
    [fileManager createFileAtPath:fullPath contents:[txtString dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    TXTFileName = [documentsDirectory stringByAppendingPathComponent:@"Chat Data.txt"];

    
//    QLPreviewController *previewController = [[QLPreviewController alloc] init];
//    previewController.dataSource = self;
//    [self presentViewController:previewController animated:YES completion:nil];

   if ([MFMailComposeViewController canSendMail])
   {
       MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
       mailer.mailComposeDelegate = self;
       [mailer setSubject:@""];
       NSArray *toRecipients = @[];
       [mailer setToRecipients:toRecipients];
       [mailer addAttachmentData:[NSData dataWithContentsOfFile:fullPath] mimeType:@".text" fileName:@"Messages"];
       [self presentViewController:mailer animated:YES completion:nil];
   }
   else
   {
       [CommonMethods showAlertWithTitle:NSLocalizedString(@"Alert.Title.NoMail", @"") andMessage:NSLocalizedString(@"Alert.Message.NoMail", @"")];
   }

}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows=0;
    if (tableView==tblViewChat)
    {
        if (arrChatMessages.count==0)
        {
            rows=1;
        }
        else
        rows=[arrChatMessages count];
    }
    else if(tableView==tblUsers)
    {
        rows=[arrUsersList count];
    }
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if (tableView==tblViewChat)
    {
        if (arrChatMessages.count==0)
        {
            height=44.0;
        }
        else
        {
            Message *message = arrChatMessages[indexPath.row];
            CGSize maximumLabelSize = CGSizeMake(180,9999);
            NSString *str=message.strMessage;
            CGSize size = [CommonMethods calculateSizeForMaxSize:maximumLabelSize forText:str WithFont:[UIFont systemFontOfSize:14.0]];
            height = size.height + 30.0;
            
            if (height <=30.0)
            {
                height = 70.0f;
                return height;
            }
  
        }
    }
    else
    {
        height=44;
    }
    return height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
  //  float cellheight = [self tableView:tableView heightForRowAtIndexPath:indexPath];
   // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
   // if (cell == nil)
    //{
     UITableViewCell *   cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //}
    cell.clipsToBounds = YES;
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    cell.backgroundColor=[UIColor clearColor];
    self.tblUsers.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tblViewChat.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (tableView ==tblViewChat)
    {
        
        if (arrChatMessages.count==0)
        {
            UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?360: 140,IS_IPAD? 768: 320, 30)];
            if (responseChatDict.count==0)
            {
                
            }
            
            else
            {
                [lblNoRecord setText:@"No Record Available."];
            }
            
            [lblNoRecord setTextColor:[UIColor lightGrayColor]];
            lblNoRecord.textAlignment = NSTextAlignmentCenter;
            [lblNoRecord setBackgroundColor:[UIColor clearColor]];
            [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
            [cell.contentView addSubview:lblNoRecord];
            cell.backgroundColor=[UIColor clearColor];

        }
        else
        {
            
           // cell.textLabel.backgroundColor = UIColorFromRGB(0xF0F0F0);
            CGSize maxLblMsgSize = CGSizeMake(deviceWidth-80,9999.0);
            Message *message = arrChatMessages[indexPath.row];
            
            UIImageView *imgUser = [[UIImageView alloc] init];
            
            UIImageView *imgchat = [[UIImageView alloc] init];
            
            //chat list
            UILabel *lblChat = [[UILabel alloc] init];
            lblChat.text = message.strMessage;
            lblChat.font = [UIFont systemFontOfSize:14.0f];
            lblChat.backgroundColor = [UIColor clearColor];
            //lblChat.backgroundColor=[UIColor colorWithRed:245.0f/255.0f green:246.0f/255.0f blue:247.0f/255.0f alpha:1.0];
            lblChat.numberOfLines = 0;
            
            
            UILabel *lblDateTime = [[UILabel alloc] init];
            NSArray *separatValue1 = [message.strDateTime componentsSeparatedByString:@" "];
            NSArray *separatValue2 = [[separatValue1 lastObject] componentsSeparatedByString:@":"];
            NSString *dayNight;
            if ([[separatValue2 firstObject] intValue] < 12) {
                dayNight = @"AM";
            }
            else{
                dayNight = @"PM";
            }
            
            NSString *showDate = [NSString stringWithFormat:@"%@ %@",[separatValue1 firstObject],[NSString stringWithFormat:@"%@:%@ %@",[separatValue2 firstObject],[separatValue2 objectAtIndex:1],dayNight]];
            lblDateTime.text = showDate;
            lblDateTime.font = [UIFont systemFontOfSize:10.0f];
            lblDateTime.textColor = [UIColor lightGrayColor];
            lblDateTime.backgroundColor = [UIColor clearColor];
            lblDateTime.numberOfLines = 0;
            
            UILabel *lblUserName = [[UILabel alloc] init];
            lblUserName.text = message.strSenderName;
           // lblUserName.textColor = [UIColor blackColor];
            lblUserName.font = [UIFont systemFontOfSize:10.0f];
            lblUserName.textAlignment=NSTextAlignmentCenter;
            lblUserName.adjustsFontSizeToFitWidth=YES;
            lblUserName.backgroundColor = [UIColor clearColor];
            
            
            CGSize expectedLblMsgSize = [lblChat sizeThatFits:maxLblMsgSize];
            
            UIImageView *bgView = [[UIImageView alloc] init];
            bgView.backgroundColor=[UIColor clearColor];
            
            if ([message.you intValue] == 1)
            {
                //right chat
                
                lblChat.frame = CGRectMake((IS_IPAD?(deviceWidth-46-(expectedLblMsgSize.width + 35.0)):MAX((deviceWidth-54-(expectedLblMsgSize.width + 26.0)),4)), 10.0, expectedLblMsgSize.width + 4.0, expectedLblMsgSize.height);
                lblChat.textColor = [UIColor grayColor];
                lblChat.textAlignment = NSTextAlignmentLeft;
            //    lblChat.backgroundColor = [UIColor redColor];
                

                
                imgUser.frame = CGRectMake(IS_IPAD?(deviceWidth-64):(deviceWidth-62), 4.0f, 40.0f, 40.0f);
                imgUser.image = [UIImage imageNamed:@"client_user"];
                [imgUser setContentMode:UIViewContentModeScaleAspectFit];
               // imgchat.backgroundColor = [UIColor blueColor];
                lblUserName.frame=CGRectMake(IS_IPAD?(deviceWidth-64):(deviceWidth-62), 45, 40, 13);
                imgchat.frame = CGRectMake(((IS_IPAD?(deviceWidth-50-(expectedLblMsgSize.width + 35.0)):(deviceWidth-45-(expectedLblMsgSize.width + 35.0)))), 5.0, expectedLblMsgSize.width + 35.0, expectedLblMsgSize.height + 20.0);
               // lblChat.textColor = [UIColor blackColor];
                imgUser.layer.cornerRadius=40.0f/2;
                //imgUser.layer.borderColor=[UIColor blackColor].CGColor;
               // imgUser.layer.borderWidth=0.5;
                imgUser.clipsToBounds=YES;
                //bgView.backgroundColor= [UIColor greenColor];
                UIImage *myImage = [UIImage imageNamed:@"MessageTextAreaReverse"];
                UIImage *myResizableImage = [myImage resizableImageWithCapInsets:UIEdgeInsetsMake(34, 34, 20, 34)];
                
                int expectedWidth=MAX(expectedLblMsgSize.width, 100);
                NSLog(@"%i",expectedWidth);
                [bgView setImage:myResizableImage];
                
                lblDateTime.frame = CGRectMake(45, expectedLblMsgSize.height+15, deviceWidth-125, 15);
                lblDateTime.textAlignment=NSTextAlignmentRight;
                if (expectedWidth>100) {
                    
                    CGFloat xtemp = MIN(lblChat.frame.origin.x-4, lblDateTime.frame.origin.x + lblDateTime.frame.size.width-70);
                    
                    CGFloat x = (IS_IPAD ? (deviceWidth-50-(expectedWidth + 35.0)) : MAX(xtemp,0));
                    
                bgView.frame =CGRectMake(x, 5.0, MAX(expectedWidth +20,130), expectedLblMsgSize.height + 28.0);
                }
                
                else
                {
                    bgView.frame =CGRectMake(((IS_IPAD?(deviceWidth-50-(expectedWidth + 35.0)):lblDateTime.frame.origin.x+80)), 5.0, MAX(expectedWidth +25,130), expectedLblMsgSize.height + 28.0);
                }
                
            ///    lblDateTime.backgroundColor=[UIColor redColor];
          
                 }
            else
            {
                //left chat
                lblChat.frame = CGRectMake(62.0, 10.0, expectedLblMsgSize.width +2, expectedLblMsgSize.height);
                lblChat.textAlignment = NSTextAlignmentLeft;
                [imgUser setContentMode:UIViewContentModeScaleAspectFill];

                imgUser.frame = CGRectMake(4.0f, 4.0f, 40.0f, 40.0f);
                imgUser.image = [UIImage imageNamed:@"ic_user1"];
                [imgUser setContentMode:UIViewContentModeScaleAspectFill];

                imgUser.layer.cornerRadius=40.0f/2;
                imgUser.clipsToBounds=YES;
                lblUserName.frame=CGRectMake(5, 45, 40, 13);
                lblChat.textColor = [UIColor grayColor];
                // bgView.backgroundColor= [UIColor greenColor];
                imgchat.frame = CGRectMake(30.0, 5.0, expectedLblMsgSize.width + 35.0, expectedLblMsgSize.height + 20.0);

                UIImage *myImage = [UIImage imageNamed:@"MessageTextArea"];
                UIImage *myResizableImage = [myImage resizableImageWithCapInsets:UIEdgeInsetsMake(34, 34, 20, 34)];
                [bgView setImage:myResizableImage];
                int expectedWidth=MAX(expectedLblMsgSize.width, 100);
                
                
                bgView.frame =CGRectMake(((IS_IPAD?(deviceWidth-50-(expectedWidth + 35.0)):(45))), 5.0, MAX(expectedWidth +25,130), expectedLblMsgSize.height + 28.0);

                
                lblDateTime.frame = CGRectMake(60, expectedLblMsgSize.height+15, deviceWidth-125, 15);
                lblDateTime.textAlignment=NSTextAlignmentLeft;
                
                //  bgView.frame =CGRectMake(50.0, 5.0, expectedLblMsgSize.width + 55.0, expectedLblMsgSize.height + 40.0);
               
//                bgView.backgroundColor=[UIColor colorWithRed:208/255.0 green:232/255.0 blue:242/255.0 alpha:1];
//                

//                bgView.backgroundColor=[UIColor whiteColor];
//                bgView.layer.cornerRadius=4.0;
//                bgView.layer.borderColor=[UIColor grayColor].CGColor;
//                bgView.layer.masksToBounds=YES;
//                //  lblDateTime.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 146,  cellheight - 20, 120, 15);
                
            }
            
          //  [cell.contentView addSubview:lblUserName];
            [cell.contentView addSubview:imgUser];
            [cell.contentView addSubview:lblChat];
            [cell.contentView addSubview:bgView];
            [cell.contentView sendSubviewToBack:bgView];
            [cell.contentView addSubview:lblDateTime];
            
        }
  
    }

    else if(tableView ==tblUsers)
    {
        if (self.arrUsersList.count==0)
        {
            UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?360: 140,IS_IPAD? 768: 320, 30)];
            [lblNoRecord setText:@"No Record Available."];
            [lblNoRecord setTextColor:[UIColor lightGrayColor]];
            lblNoRecord.textAlignment = NSTextAlignmentCenter;
            [lblNoRecord setBackgroundColor:[UIColor clearColor]];
            [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
            [cell.contentView addSubview:lblNoRecord];
            cell.backgroundColor=[UIColor clearColor];
        }
        else
        {
            User *objTempuser=self.arrUsersList[indexPath.row];
            cell.textLabel.text = objTempuser.user_name;
            cell.textLabel.textColor = [UIColor darkGrayColor];
            cell.textLabel.font = [UIFont systemFontOfSize:16];
            cell.textLabel.backgroundColor = UIColorFromRGB(0xF0F0F0);
        }
        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView==tblUsers)
    {
        if (self.arrUsersList.count==0)
        {
            
        }
        else
        {
            User *objuser=[self.arrUsersList objectAtIndex:indexPath.row];
            self.strSelectedAgentID=objuser.user_id;
            [self ShowViewForAgentsList:NO];
            [self ShowReplyView];
  
        }
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *objChatMessage=[self.arrChatMessages objectAtIndex:indexPath.row];
    NSString *strMessageId=objChatMessage.strMessage_Id;
    
    [self CallToDeleteMessageOfConversation:strMessageId withIndexPath:indexPath.row];
}

-(void)CallToDeleteMessageOfConversation:(NSString *)MessageId withIndexPath:(NSInteger)indexpathSelectedRow
{
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"deleteMessageByMessageId";
    
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    [inputParams.dict_postParameters setObject:MessageId forKey:@"messageId"];
    
    
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             
             return;
         }
         else
         {
             error = nil;
             self.responseDictReplyMessage = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
             
             
             NSString *strMessage=[[self.responseDictReplyMessage objectForKey:@"data"] objectForKey:@"log"];
             if ([strMessage isEqualToString:@"Message deleted successfully."])
             {
                 [self.arrChatMessages removeObjectAtIndex:indexpathSelectedRow];
                 [tblViewChat reloadData];

             }
             
             
         }
     }];

}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void) textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    [replyTextView removeFromSuperview];
    
    [replyView removeFromSuperview];
}

-(void)CallForGetListOfUsers
{
    [self.arrUsersList removeAllObjects];
    
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    if([strKey isEqualToString:@"1"])
    {
        inputParams.relativeWebservicePath = @"listofagents";
    }
    else if([strKey isEqualToString:@"0"])
    {
        inputParams.relativeWebservicePath = @"pwtestusersofagents";
    }
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         
         if (error) {
             if ([error isErrorOfKindNoNetwork])
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         } else {
             
             error = nil;
             responseDictDropDown = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
             
             [self performSelectorOnMainThread:@selector(ParseListOfUsers) withObject:self waitUntilDone:YES];
             
         }
     }];
    
}

-(void)ParseListOfUsers
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    NSMutableArray *arr = [responseDictDropDown objectForKey:@"data"];
    if([strKey isEqualToString:@"1"])
    {
        
        for (NSDictionary *dict in arr) {
            User *objTempuser = [[User alloc] init];
            objTempuser.user_id = GET_VALID_STRING(dict[@"agent_id"]);
//            objTempuser.user_name = GET_VALID_STRING(dict[@"agent_name"]);
             objTempuser.user_name = GET_VALID_STRING(dict[@"email"]);
            [self.arrUsersList addObject:objTempuser];
        }
    }
    else if([strKey isEqualToString:@"0"])
    {
        for (NSDictionary *dict in arr) {
            User *objTempuser = [[User alloc] init];
            objTempuser.user_id = GET_VALID_STRING(dict[@"User_Id"]);
//            objTempuser.user_name = GET_VALID_STRING(dict[@"Username"]);
            objTempuser.user_name = GET_VALID_STRING(dict[@"Email"]);
            [self.arrUsersList addObject:objTempuser];
        }
        
    }
    [self.tblViewChat reloadData];

}

-(void)ShowViewForAgentsList:(BOOL)isShow
{
    if (self.viewForAgentsListBG)
    {
        [self.viewForAgentsListBG.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.viewForAgentsListBG removeFromSuperview];
        self.viewForAgentsListBG=nil;
    }
    if(isShow)
    {
        
        self.viewForAgentsListBG=[[UIView alloc]initWithFrame:CGRectMake(0, IS_VERSION_GRT_7 ? 20:0, deviceWidth,IS_VERSION_GRT_7? deviceHeight-20:deviceHeight)];
        self.viewForAgentsListBG.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
        [APP_DELEGATE.window addSubview:self.viewForAgentsListBG];
    
        self.viewForAgentsList=[[UIView alloc]initWithFrame:CGRectMake(10,50, deviceWidth-20,CGRectGetHeight(self.view.bounds)-75)];
        self.viewForAgentsList.backgroundColor = [UIColor whiteColor] ;
        [self.viewForAgentsListBG addSubview:self.viewForAgentsList];
        
        
        tblUsers=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, deviceWidth-20, CGRectGetHeight(self.view.bounds)-75) style:UITableViewStylePlain];
        tblUsers.delegate=self;
        tblUsers.dataSource=self;
        tblUsers.backgroundColor=[UIColor clearColor];
        tblUsers.backgroundView=nil;
        tblUsers.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        
        tblViewChat.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        tblUsers.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        [self.viewForAgentsList addSubview:tblUsers];
        
        
        UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCross.frame = CGRectMake(0, 30, 24, 24);
        [btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
        [btnCross addTarget:self action:@selector(btnCross_Clicked) forControlEvents:UIControlEventTouchUpInside];
        [self.viewForAgentsListBG addSubview:btnCross];
        btnCross = nil;
    }
    
    
}
-(void)btnCross_Clicked
{
    [self ShowViewForAgentsList:NO];
}

-(void)ShowReplyView
{
    
    selectedView=[[UIView alloc]init];
    selectedView.frame=CGRectMake(0, 0,self.view.frame.size.width,self.view.frame.size.height);
    selectedView.backgroundColor=[UIColor blackColor];
    selectedView.alpha=0.3f;
    [self.view addSubview:selectedView];
    
    btnDropDown.enabled = NO;
    replyView = [[UIView alloc ] init];
    replyTextView = [[UITextView alloc] init];
    UIButton *replybtn = [UIButton buttonWithType:UIButtonTypeCustom];
     UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
    
    replyView.frame = CGRectMake(20, IS_IPAD?20:(IS_IPHONE_5?20:10), deviceWidth-40,IS_IPAD?deviceHeight-348:(IS_IPHONE_5?deviceHeight-308:deviceHeight-290) );
    replyView.backgroundColor = [UIColor colorWithRed:0.914 green:0.914 blue:0.914 alpha:1];
    
    
    
    replyTextView.frame = CGRectMake(10, 10, deviceWidth-60, IS_IPAD?replyView.frame.size.height-50:(IS_IPHONE_5?replyView.frame.size.height-60:replyView.frame.size.height-70));
    replyTextView.font = [UIFont systemFontOfSize:14];
    replyTextView.userInteractionEnabled = YES;
    replyTextView.text = @"";
    replyTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    replyTextView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //replyTextView.layer.borderColor = UIColorFromRGB(0xA9A9A9).CGColor;
    //replyTextView.layer.borderWidth = 0.5f;
    replyTextView.delegate = self;
    
    btnCross.frame = CGRectMake((deviceWidth-130)/2-85, IS_IPAD?replyTextView.frame.size.height+20:(IS_IPHONE_5?replyTextView.frame.size.height+20:replyTextView.frame.size.height+23), 125,IS_IPAD?29:(IS_IPHONE_5?29:26));
    btnCross.userInteractionEnabled=YES;
    btnCross.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnCross setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnCross setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCross setBackgroundColor:[UIColor lightGrayColor] ];
    //[btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
    [btnCross addTarget:self action:@selector(Cross_Clicked) forControlEvents:UIControlEventTouchUpInside];


    replybtn.frame = CGRectMake((deviceWidth-30)/2+1, IS_IPAD?replyTextView.frame.size.height+20:(IS_IPHONE_5?replyTextView.frame.size.height+20:replyTextView.frame.size.height+23), 125,IS_IPAD?29:(IS_IPHONE_5?29:26));
    replybtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [replybtn setTitle:@"Send" forState:UIControlStateNormal];
    [replybtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    //[replybtn setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
    [replybtn setBackgroundColor:UIColorFromRGB(0x2C76E2) ];
    [replybtn addTarget:self action:@selector(reply) forControlEvents:UIControlEventTouchUpInside];
    
    [replyView addSubview:replyTextView];
    [replyTextView becomeFirstResponder];
    [replyView addSubview:replybtn];
    [self.view addSubview:replyView];
    [replyView addSubview:btnCross];
}

-(void)Cross_Clicked
{
    [replyView removeFromSuperview];
    [selectedView removeFromSuperview];
    btnDropDown.enabled = YES;
}


-(void)reply
{
    NSString *strConversationId=self.objMessageBO.strConversation_Id;
    if ([strConversationId intValue]==0)
    {
        if ([replyTextView.text length]<=0)
        {
           [ [[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please enter message." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show ];
        }
        else
        {
            NSString *strSubject = objMessageBO.strSubject;
            WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
            inputParams.methodType = WebserviceMethodTypePost;
            inputParams.relativeWebservicePath = @"insertMessage";
            
            [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
            [inputParams.dict_postParameters setObject: self.strSelectedAgentID forKey:@"agent_id"];
            [inputParams.dict_postParameters setObject:replyTextView.text forKey:@"textmessage"];
            [inputParams.dict_postParameters setObject:strSubject forKey:@"subject"];
            
            [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
             {
                 
                 if (error) {
                     if ([error isErrorOfKindNoNetwork])
                         [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                     return;
                     
                     return;
                 } else {
                     
                     error = nil;
                     self.responseDictReplyMessage = [NSJSONSerialization JSONObjectWithData:response
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&error];
                     
                     [self performSelectorOnMainThread:@selector(isMessageSent) withObject:self waitUntilDone:YES];
                     
                 }
             }];
            
        }
    }
    else
    {
        if ([replyTextView.text length]<=0)
        {
            [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please enter message." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        else
        {
            WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
            inputParams.methodType = WebserviceMethodTypePost;
            inputParams.relativeWebservicePath = @"inboxreply";
            NSString *strRelationId=objMessageBO.strRelation_Id;
            NSString *strConversationId=self.objMessageBO.strConversation_Id;
            NSString *strSubject=self.objMessageBO.strSubject;
            NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
            
            
            NSString *Str = [NSString stringWithUTF8String:[replyTextView.text cStringUsingEncoding:NSUTF8StringEncoding]];
            NSLog(@"%@", Str);
            
            NSLog(@"%@", replyTextView.text);
            
            
            [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
            [inputParams.dict_postParameters setObject:replyTextView.text forKey:@"textmessage"];
            [inputParams.dict_postParameters setObject:strRelationId forKey:@"relation_id"];
            [inputParams.dict_postParameters setObject:strConversationId forKey:@"conversation_id"];
            [inputParams.dict_postParameters setObject:strSubject forKey:@"subject"];
            
            
            [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
             {
                 
                 if (error) {
                     if ([error isErrorOfKindNoNetwork])
                         [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                     return;
                     
                     return;
                 } else {
                     
                     error = nil;
                     self.responseDictReplyMessage = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
                     
                     [self performSelectorOnMainThread:@selector(isMessageSent) withObject:self waitUntilDone:YES];
                     
                 }
             }];
            
        }

    }
}
-(void)isMessageSent
{
    
    NSString *strConversationId=self.objMessageBO.strConversation_Id;
    
    if ([strConversationId intValue]==0)
    {
        if([self.responseDictReplyMessage[@"data"][@"log"] isEqualToString:@""])
        {
            
        }
        else
        {
            
        NSString *correctString = [NSString stringWithCString:[[[self.responseDictReplyMessage objectForKey:@"data"] objectForKey:@"log"] cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];

        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:correctString];
            
        [replyView removeFromSuperview];
        [replyTextView resignFirstResponder];
 
            
    }
        
        for (UIViewController *objControllr in self.navigationController.viewControllers)
        {
            if ([objControllr isKindOfClass:[InboxVC class]])
            {
                InboxVC *vwCont=(InboxVC *)objControllr;
                [self.navigationController popToViewController:vwCont animated:YES];
            }
            
        }

    }
    else
    {
        if([self.responseDictReplyMessage[@"data"][@"log"] isEqualToString:@""])
        {
            
        }
        else
        {
            
        NSString *correctString = [NSString stringWithCString:[[[self.responseDictReplyMessage objectForKey:@"data"] objectForKey:@"log"] cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
        NSLog(@"%@", correctString);

        NSString *Str = [[self.responseDictReplyMessage objectForKey:@"data"] objectForKey:@"log"];
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:Str];
        [replyView removeFromSuperview];
            replyView.hidden=YES;
            replyView=nil;
           btnDropDown.enabled = YES;
            [selectedView removeFromSuperview];
       // [replyTextView resignFirstResponder];

        }
        
        [arrChatMessages removeAllObjects];
        [self CallForGetAllConversationOfAMessgae];
       

    }
}
- (void)bottomButtonsTapped: (id) sender
{
    if([sender tag] == 1)
    {
        ComposeSheetVC *obj_ComposeVC = [[ComposeSheetVC alloc]init];
        obj_ComposeVC.receiverEmailStr = strReciverEmail;
        obj_ComposeVC.receiverNameStr = strReciverName;
        obj_ComposeVC.receiverIDStr = strReciverID;
        obj_ComposeVC.strScreenType=@"Message";
        obj_ComposeVC.relation_id = self.objMessageBO.strRelation_Id;
        [self.navigationController pushViewController:obj_ComposeVC animated:YES];
    }
    if([sender tag] == 2)
        [self openPreviewcontrollerToShare];
    
    if([sender tag] == 3)
    {
        NSString *strConversationId=self.objMessageBO.strConversation_Id;
        if ([strConversationId intValue]==0)
        {
            [self ShowViewForAgentsList:NO];
            [self ShowReplyView];

        }
        else
        {
            [self ShowReplyView];
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

   // [replyView removeFromSuperview];
    //[replyTextView resignFirstResponder];
    
    btnDropDown.enabled = YES;
}
#pragma mark - QLPreviewControllerDataSource Methods

-(NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
    return 1;
}

-(id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    return  [NSURL fileURLWithPath:TXTFileName];
}

-(void)previewControllerWillDismiss:(QLPreviewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self sortInboxinAscending];
    [self.tblViewChat reloadData];
}

#pragma mark-

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}







@end
