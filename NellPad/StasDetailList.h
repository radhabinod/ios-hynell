//
//  StasDetailList.h
//  NellPat
//
//  Created by Macbook on 7/7/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StasDetailList : NSManagedObject

@property (nonatomic, retain) NSString * accountCost;
@property (nonatomic, retain) NSString * agentId;
@property (nonatomic, retain) NSDate * arrivedDate;
@property (nonatomic, retain) NSString * caseComment;
@property (nonatomic, retain) NSString * caseId;
@property (nonatomic, retain) NSString * caseType;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * corresID;
@property (nonatomic, retain) NSString * countryCode;
@property (nonatomic, retain) NSNumber * customerId;
@property (nonatomic, retain) NSString * customerName;
@property (nonatomic, retain) NSString * deadline;
@property (nonatomic, retain) NSString * figure;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * patentNumber;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * recommendationID;
@property (nonatomic, retain) NSNumber * recommendationStatus;
@property (nonatomic, retain) NSString * recommendationText;
@property (nonatomic, retain) NSString * keyword;
@property (nonatomic, retain) NSString * deadlineCode;

@end
