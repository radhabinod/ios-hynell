//
//  CompaniesListVC.h
//  NellPat
//
//  Created by Ram Kumar on 04/04/16.
//  Copyright © 2016 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CasesVC;
@class PatrawinCase;
@interface CompaniesListVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,UIActionSheetDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
{
    NSMutableDictionary *responseDict;
}
@property(strong, nonatomic) UITableView *caseTableView;
@property(strong, nonatomic) NSMutableArray *casesArray;
@property(nonatomic,strong) NSMutableArray *arrRecommendations;
@property(nonatomic,strong) UIActionSheet *actionSheetSort;
@property(nonatomic,strong) UITextField *textSortingCompany;
@property(nonatomic,strong) UIView *viewForAgentsListBG, *viewForAgentsList;
@property(strong,nonatomic) UITableView *tbleUsers;
@property(nonatomic,strong) NSArray *search;
@property(nonatomic,strong) NSArray *grantedSearch;

@property(nonatomic,strong) NSString *statsString;
@property(nonatomic,strong) NSArray *statsCases;

@property(nonatomic,strong) NSMutableArray *finalArrayValueZeroStat;


-(void)refreshTable;
-(void) DataReload;

-(void)fetchFromDatabase;
@end
