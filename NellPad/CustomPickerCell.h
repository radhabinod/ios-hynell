//
//  CustomPickerCell.h
//  Fieldo
//
//  Created by Herry Makker on 11/14/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPickerCell : UITableViewCell

@property (retain, nonatomic) UIPickerView *pickerView;

@end
