//
//  LinkViewController.m
//  NellPad
//
//  Created by Rakesh Kumar on 30/04/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "LinkViewController.h"

#import <objc/message.h>

@interface LinkViewController ()

@end

@implementation LinkViewController
@synthesize webView,strUrl,screenType;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    if (_isDeadline) {
        self.title = @"Deadline";
        
    }
    else if ([screenType isEqualToString:@"Codes"])
    {
        self.title = @"List Of Codes";

    }
    else if ([screenType isEqualToString:@"Image"]){
        
        self.title = @"Case Image";
    }
    else
    {
    self.title = @"Status";
    }
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
            
            
        }
    }
    
    self.webView = [[UIWebView alloc]init];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        self.webView.frame = CGRectMake(0,0, 320 ,CGRectGetHeight(self.view.bounds));
        
        self.webView.autoresizesSubviews = YES;
        self.webView.autoresizingMask  = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.webView.frame = CGRectMake(0, 0, 768, 976);
    }
    
    
    
    self.webView.delegate = self;
    self.webView.autoresizesSubviews = YES;
    self.webView.scalesPageToFit=YES;
    
    [self.view addSubview:self.webView];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestURL];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

}


- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark - UIWebViewDelegate delegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
