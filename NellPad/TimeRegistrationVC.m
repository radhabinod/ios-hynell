//
//  TimeRegistrationVC.m
//  NellPad
//
//  Created by Sanjeev Kumar on 12/23/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import "TimeRegistrationVC.h"
#import "PatrawinCase.h"
#import "CompanyBO.h"
#import <objc/message.h>
#import "LinkViewController.h"
@class TimeRegistrationVC;

@interface TimeRegistrationVC ()
{
    UISearchBar *searchBar1;
    UISearchDisplayController *searchDisplayController;
    NSArray *search;
    NSArray *searchCodes;

    UITableView *tblForRecommendations;
    
    NSString *searchtextForCases;
    NSString *searchtextForCodes;
}

@end


@implementation TimeRegistrationVC

@synthesize responseDict,arrCasesData,datetimeSelection,arrDataHdng,scrollView,strCaseValue,arrCodesData,keyBoardToolBar,strGlobalCompany_id,strDatePickerSelected,timePicker,viewForCaseData,strViewSelected,viewForPopUp,viewForPopUpBG,strTypeOfData,strCustomerNameValue,strCustomerIdValue,arrSelectedCodes;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.title = @"Time Registration";

    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
        }
    }
    arrSelectedCodes=[[NSMutableArray alloc]init];
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize: 18], NSFontAttributeName, nil]];

    self.arrCasesData=[[NSMutableArray alloc]init];
    self.arrDataHdng=[[NSMutableArray alloc]initWithObjects:@"Case:",@"Code:",@"Customer:",@"Day:",@"Time:", nil];
    self.arrCodesData=[[NSMutableArray alloc]init];
    self.strGlobalCompany_id=[[NSString alloc]init];
    self.strDatePickerSelected=[[NSString alloc]init];
    self.strViewSelected=[[NSString alloc]init];
    self.strTypeOfData=[[NSString alloc]init];
    
    
   // search = [[NSArray alloc]init];

    
    
    if (strCaseValue.length >0)
    {
      //  [self showCustomViewForCase:YES];
        self.strViewSelected=@"CaseView";
    }
    else
    {
//    [self showCustomView:YES];
//        
     ///   [self showCustomViewForCase:YES];
        
        self.strViewSelected=@"NonCaseView";
    }
    [self showCustomView:YES];
    
    
    [self designToolBarForDatePicker];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

}


- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    if (self.strCaseValue.length >0)
    {
        [self CallForGetCodes];
        
    }
    else
    {
        [self CallForGetCases];
        
 }
}

-(void)DesignPickerView
{
    self.datetimeSelection=[[UIDatePicker alloc]initWithFrame:CGRectMake(0, 1200, deviceWidth, 216)];
    self.datetimeSelection.backgroundColor=[UIColor whiteColor];
    [self.datetimeSelection addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    self.datetimeSelection.datePickerMode=UIDatePickerModeDateAndTime;
    [self.view addSubview:self.datetimeSelection];
    
    self.timePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0 ,1200, deviceWidth, 216)];
    self.timePicker.backgroundColor=[UIColor whiteColor];
    self.timePicker.delegate = self;
    self.timePicker.dataSource = self;
    [self.view addSubview:self.timePicker];
}

-(void)designToolBarForDatePicker
{
    if (keyBoardToolBar==nil)
    {
        
        keyBoardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 1200 , deviceWidth, 44)];
        
        UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(toolBarBtnClicked:)];
        btnDone.tag = kbtnDone;
        
        UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(toolBarBtnClicked:)];
        btnCancel.tag = kbtnCancel;
        
        UIBarButtonItem *tempDistance = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        
        keyBoardToolBar.items = [NSArray arrayWithObjects:btnDone,tempDistance,btnCancel, nil];
    }
    else
    {
        [keyBoardToolBar removeFromSuperview];
    }
    
    [self.view addSubview:keyBoardToolBar];
    [self.view bringSubviewToFront:keyBoardToolBar];
}
-(void)showOptionPatch:(BOOL)isShow
{
    if(isShow)
    {
        [UIView animateWithDuration:0.29 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^
         {
             keyBoardToolBar.frame = CGRectMake(0,   CGRectGetHeight(self.view.bounds)-(216)-44, deviceWidth, 44 );
         }completion:nil];
        
    }
    else{
        [UIView animateWithDuration:0.35 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^
         {
             keyBoardToolBar.frame = CGRectMake(0, 1200, deviceWidth, 44);
         }
                         completion:nil];
        
    }
}

-(void)toolBarBtnClicked:(UIButton *)sender
{
    UITextField *TxtFDay=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+3];
    UITextField *TxtFTime=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+4];
    
    [self showPickerView:NO];
    [self showTimerPicker:NO];
    [self showOptionPatch:NO];
    

    switch (sender.tag)
    {
        case kbtnCancel:
            break;
            
        case kbtnDone:
        {
            if ([self.strDatePickerSelected isEqualToString:@"Day"])
            {
                NSDate *date = self.datetimeSelection.date;
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss.SSS"];
                [self showPickerView:NO];
                NSString *strDay=[dateFormat stringFromDate:date];
                TxtFDay.text=strDay;
                
            }
            else if ([self.strDatePickerSelected isEqualToString:@"Counter"])
            {
                NSString *time = [NSString stringWithFormat:@"%@:%@", [NSString stringWithFormat:@"%i", (int)[self.timePicker selectedRowInComponent:0]], [NSString stringWithFormat:@"%i", (int)[self.timePicker selectedRowInComponent:1]*5]];
                
                if ([time isEqualToString:@"0:0"])
                {
                    [CommonMethods showAlertWithTitle:nil andMessage:@"Kindly enter a valid time."];
                    return;
                }
                
                if ([time isEqualToString:@"1:0"] || [time isEqualToString:@"2:0"] ||[time isEqualToString:@"3:0"]||[time isEqualToString:@"4:0"]||[time isEqualToString:@"5:0"]||[time isEqualToString:@"6:0"]||[time isEqualToString:@"7:0"]||[time isEqualToString:@"8:0"]||[time isEqualToString:@"9:0"])
                {
                    TxtFTime.text = [NSString stringWithFormat:@"0%@", time];
                }
                else
                {
                    TxtFTime.text = time;
                }
            }
        }
            break;

        default:
            break;
    }
}

//-(void)updateValuesOfTextFields
//{
//    UITextField *txtFCode=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase];
//    txtFCode.text=self.strCaseValue;
//    
//    UITextField *txtFCustomer=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+2];
//    txtFCustomer.text=self.strCustomerNameValue;
//    
//    self.strGlobalCompany_id=strCustomerIdValue;
//}
-(void)dateChanged
{

}

- (void) showTimerPicker:(BOOL)isShow
{
    BOOL isIOS7 = [[[UIDevice currentDevice] systemVersion] intValue] >= 7 ? YES : NO;
    
    if(isShow)
    {
        [UIView animateWithDuration:0.29 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            self.timePicker.frame = CGRectMake(0, IS_IPHONE_5? (isIOS7? CGRectGetHeight(self.view.bounds)-(216): CGRectGetHeight(self.view.bounds)-(216)):(isIOS7?CGRectGetHeight(self.view.bounds)-216:CGRectGetHeight(self.view.bounds)-(216)), deviceWidth, 216);
        }
                         completion:nil];
        
    }
    else
    {
        [UIView animateWithDuration:0.35 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                              self.timePicker.frame= CGRectMake(0, CGRectGetHeight(self.view.bounds)+100, deviceWidth,isIOS7?300:216);
                         }
                         completion:nil];
        
    }
}


-(void)showPickerView:(BOOL)isShow
{
    BOOL isIOS7 = [[[UIDevice currentDevice] systemVersion] intValue] >= 7 ? YES : NO;
    
    if(isShow)
    {
        [UIView animateWithDuration:0.29 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
        datetimeSelection.frame = CGRectMake(0, IS_IPHONE_5? (isIOS7? CGRectGetHeight(self.view.bounds)-(216): CGRectGetHeight(self.view.bounds)-(216)):(isIOS7?CGRectGetHeight(self.view.bounds)-216:CGRectGetHeight(self.view.bounds)-(216)), deviceWidth, 216);
        }
                         completion:nil];
        
    }
    else
    {
        [UIView animateWithDuration:0.35 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
        self.datetimeSelection.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds)+100, deviceWidth,isIOS7?300:216);
                         }
                         completion:nil];
        
    }
}

//-(void)showCustomViewForCase:(BOOL)isShow
//{
//    if(self.viewForCaseData)
//    {
//        [self.viewForCaseData.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//        [self.viewForCaseData removeFromSuperview];
//        self.viewForCaseData=nil;
//    }
//
//    self.viewForCaseData=[[UIView alloc]initWithFrame:CGRectMake(0, 0, deviceWidth, deviceHeight)];
//    self.viewForCaseData.backgroundColor=[UIColor clearColor];
//    [self.view addSubview:self.viewForCaseData];
//
//    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,deviceWidth,deviceHeight)];
//    self.scrollView.showsVerticalScrollIndicator=NO;
//    self.scrollView.backgroundColor=[UIColor clearColor];
//    self.scrollView.scrollEnabled=YES;
//    self.scrollView.userInteractionEnabled=YES;
//    [self.viewForCaseData addSubview: self.scrollView];
//
//    
//    int Xpos=0;
//    int Ypos=0;
//    
//    Ypos+=30;
//    for (int i=0; i<self.arrDataHdng.count; i++)
//    {
//        Xpos=15;
//
//        UILabel *lbl_HD=[[UILabel alloc]initWithFrame:CGRectMake(Xpos, Ypos, IS_IPAD ? 90 : 70, 40)];
//        lbl_HD.backgroundColor = [UIColor clearColor];
//        lbl_HD.text=[self.arrDataHdng objectAtIndex:i];
//        lbl_HD.font = IS_IPAD ? FONT_BOLD(19.0) : FONT_BOLD(14.0);
//        lbl_HD.textAlignment=NSTextAlignmentCenter;
//        lbl_HD.textColor=UIColorFromRGB(0x4174DA);
//        lbl_HD.textColor=[UIColor blackColor];
//        [self.scrollView addSubview:lbl_HD];
//        
//        Xpos+=lbl_HD.frame.size.width+10;
//        Ypos+= IS_IPAD ? 0 : 8;
//        
//        UITextField *txtFieldName=[[UITextField alloc]init];
//        txtFieldName.delegate = self;
//        txtFieldName.tag=kTxtFieldCase+i;
//        txtFieldName.textColor=[UIColor blackColor];
//        txtFieldName.autocorrectionType=UITextAutocorrectionTypeNo;
//        txtFieldName.borderStyle=UITextBorderStyleRoundedRect;
//        txtFieldName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//        txtFieldName.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
//        txtFieldName.returnKeyType=UIReturnKeyDone;
//        txtFieldName.backgroundColor=[UIColor clearColor];
//        [txtFieldName setFont:IS_IPAD?FONT_NORMAL(15.0): FONT_NORMAL(12.0)];
//        txtFieldName.userInteractionEnabled=NO;
//        [self.scrollView addSubview:txtFieldName];
//        
//        
//        if (i==0) {
//            if (self.strCaseValue) {
//                txtFieldName.text=self.strCaseValue;
//                
//            }
//            
//            
//            
//        }
//        if (i==1)
//        {
//            txtFieldName.frame=CGRectMake(IS_IPAD ? Xpos + 5 : Xpos, Ypos, IS_IPAD ? deviceWidth-140 : deviceWidth-110, IS_IPAD ? 50 : 30);
//            
//            
//            Xpos+=txtFieldName.frame.size.width -30;
//            
//            UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD ? Xpos + 5 : Xpos, Ypos, 30, IS_IPAD ? 50 : 30)];
//            
//            [btn setBackgroundColor:[UIColor clearColor]];
//            btn.tag=kbtnCode;
//            [btn addTarget:self action:@selector(btnActionClicked:) forControlEvents:UIControlEventTouchUpInside];
//            
//            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(Xpos,Ypos, 30, 30)];
//            
//            [image setImage:[UIImage imageNamed:@"arrowBgIcon"]];
//            [self.scrollView addSubview:image];
//           // txtFieldName.backgroundColor = [UIColor redColor];
//            [self.scrollView addSubview:btn];
//            
//            btn=nil;
//            image=nil;
//        }
//        else if (i==3 || i==4)
//        {
//            txtFieldName.frame=CGRectMake(IS_IPAD ? Xpos + 5 : Xpos, Ypos, IS_IPAD ? deviceWidth-140 : deviceWidth-110, IS_IPAD ? 50 : 30);
//            
//            
//            Xpos+=txtFieldName.frame.size.width -30;
//            
//            UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD ? Xpos + 5 : Xpos, Ypos, 30, IS_IPAD ? 50 : 30)];
//            
//            [btn setBackgroundColor:[UIColor clearColor]];
//            btn.tag=kbtnCode+i-1;
//            [btn addTarget:self action:@selector(btnActionClicked:) forControlEvents:UIControlEventTouchUpInside];
//            
//            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(Xpos,Ypos, 30, 30)];
//            if (i==3) {
//                
//            
//            [image setImage:[UIImage imageNamed:@"dayIcon"]];
//            }
//            else
//            {
//                [image setImage:[UIImage imageNamed:@"time	Icon"]];
//                
//            
//            }
//                
//                [self.scrollView addSubview:image];
//            
//            [self.scrollView addSubview:btn];
//            
//            btn=nil;
//            image=nil;
//  
//        }
//        else
//        {
//            txtFieldName.frame=CGRectMake(IS_IPAD ? Xpos + 5 : Xpos, Ypos, IS_IPAD ? deviceWidth-140 : deviceWidth-110, IS_IPAD ? 50 : 30);
//        }
//
//        Ypos+= IS_IPAD ? 100 : 50;
//        lbl_HD=nil;
//        txtFieldName=nil;
//        
//    }
//    
//    Xpos=IS_IPAD?((deviceWidth-200)/2): ((deviceWidth-90)/2);
//    Ypos+=20;
//    
//    UIButton* btnsubmitData = [UIButton buttonWithType:UIButtonTypeCustom];
//    //btnsubmitData.frame = CGRectMake(Xpos, Ypos, IS_IPAD?200:90, IS_IPAD ? 50 : 30);
//     btnsubmitData.frame = CGRectMake(20,Ypos,self.view.frame.size.width-40,50);
//    
//    btnsubmitData.backgroundColor = UIColorFromRGB(0x4174DA);
//    [btnsubmitData setTitle:@"Submit" forState:UIControlStateNormal];
//    btnsubmitData.titleLabel.textAlignment = NSTextAlignmentCenter;
//    btnsubmitData.titleLabel.font = [UIFont boldSystemFontOfSize:18];
//    btnsubmitData.titleLabel.textColor = UIColorFromRGB(0xF0F0F0);
//    btnsubmitData.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
//    
//    [btnsubmitData addTarget:self action:@selector(btnsubmitDataClk) forControlEvents:UIControlEventTouchUpInside];
//    [self.scrollView addSubview:btnsubmitData];
//    
//    [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+150)];
//    
//    [self DesignPickerView];
//    [self updateValuesOfTextFields];
//    
//    UITapGestureRecognizer *gst=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
//    gst.cancelsTouchesInView=NO;
//    gst.delegate=self;
//    [self.view addGestureRecognizer:gst];
//
//
//}


- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


-(void)showCustomView:(BOOL)isShow
{
    if(self.viewForCaseData)
    {
        [self.viewForCaseData.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.viewForCaseData removeFromSuperview];
        self.viewForCaseData=nil;
    }
    
    self.viewForCaseData=[[UIView alloc]initWithFrame:CGRectMake(0, 0, deviceWidth, deviceHeight)];
    self.viewForCaseData.backgroundColor=[UIColor clearColor];
    [self.view addSubview:self.viewForCaseData];
    
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,deviceWidth,deviceHeight)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=[UIColor clearColor];
    self.scrollView.scrollEnabled=YES;
    self.scrollView.userInteractionEnabled=YES;
    [self.viewForCaseData addSubview: self.scrollView];
    
    
    int Xpos=0;
    int Ypos=0;
    
    Ypos+=30;
    for (int i=0; i<self.arrDataHdng.count; i++)
    {
        Xpos=12;
        
        UILabel *lbl_HD=[[UILabel alloc]initWithFrame:CGRectMake(Xpos, Ypos+5, IS_IPAD ? 90 : 70, 40)];
        lbl_HD.backgroundColor = [UIColor clearColor];
        lbl_HD.text=[self.arrDataHdng objectAtIndex:i];
        lbl_HD.font = IS_IPAD ? FONT_BOLD(19.0) : FONT_BOLD(14.0);
        lbl_HD.textAlignment=NSTextAlignmentCenter;
        lbl_HD.textColor=UIColorFromRGB(0x4174DA);
        lbl_HD.textColor=[UIColor blackColor];
        lbl_HD.textAlignment=NSTextAlignmentLeft;
        [self.scrollView addSubview:lbl_HD];
        
        Xpos+=lbl_HD.frame.size.width+10;
        Ypos+= IS_IPAD ? 0 : 8;
        
        UITextField *txtFieldName=[[UITextField alloc]init];
        txtFieldName.delegate = self;
        txtFieldName.tag=kTxtFieldCase+i;
        txtFieldName.textColor=[UIColor blackColor];
        txtFieldName.autocorrectionType=UITextAutocorrectionTypeNo;
        //txtFieldName.borderStyle=UITextBorderStyleRoundedRect;
        txtFieldName.layer.borderWidth=1.0;
        txtFieldName.layer.borderColor=[UIColor lightGrayColor].CGColor;
       // txtFieldName.layer.cornerRadius = 3;
       // txtFieldName.layer.masksToBounds=YES;
        txtFieldName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
       
        txtFieldName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        txtFieldName.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        txtFieldName.returnKeyType=UIReturnKeyDone;
        txtFieldName.backgroundColor=[UIColor clearColor];
        [txtFieldName setFont:IS_IPAD?FONT_NORMAL(15.0):  FONT_NORMAL(12.0)];
        txtFieldName.userInteractionEnabled=NO;
        [self.scrollView addSubview:txtFieldName];

        if (i==0) {
            if (self.strCaseValue) {
                txtFieldName.text=self.strCaseValue;
                
            }
            
            
            
        }
        
        
//        if (i==2)
//        {
//            txtFieldName.frame=CGRectMake(IS_IPAD ? Xpos + 5 : Xpos, Ypos, IS_IPAD ? deviceWidth-140 : deviceWidth-110, IS_IPAD ? 50 : 36);
//
//        }
//
//        else
//        {
            txtFieldName.frame=CGRectMake(IS_IPAD ? Xpos + 5 : Xpos, Ypos, IS_IPAD ? deviceWidth-150 : deviceWidth-110, IS_IPAD ? 50 : 36);
//        }
        
        Xpos+=txtFieldName.frame.size.width;
        
        if (i==2)
        {
            
        }
        else
        {
            UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD ? Xpos-15 : Xpos-33, Ypos+3, 30, IS_IPAD ? 50 : 30)];
            if (i==3) {
                [btn setBackgroundImage:[UIImage imageNamed:@"dayIcon"] forState:UIControlStateNormal];
                
            }
            else if (i==4)
            {
                [btn setBackgroundImage:[UIImage imageNamed:@"timeIcon"] forState:UIControlStateNormal];
                
            }
            else
            {
            [btn setBackgroundImage:[UIImage imageNamed:@"arrowBgIcon"] forState:UIControlStateNormal];
            }
            
            [btn setBackgroundColor:[UIColor clearColor]];
            btn.tag=kbtnCase+i;
            [btn addTarget:self action:@selector(btnActionClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self.scrollView addSubview:btn];
            btn=nil;

        }
        
        Ypos+= IS_IPAD ? 100 : 50;
        lbl_HD=nil;
        txtFieldName=nil;
    }
    
    Xpos=IS_IPAD?((deviceWidth-200)/2): ((deviceWidth-90)/2);
    Ypos+=20;
  
    UIButton* btnsubmitData = [UIButton buttonWithType:UIButtonTypeCustom];
    //btnsubmitData.frame = CGRectMake(Xpos, Ypos, IS_IPAD?200: 90, IS_IPAD ? 50 : 30);
    btnsubmitData.backgroundColor = UIColorFromRGB(0x4174DA);
    [btnsubmitData setTitle:@"Submit" forState:UIControlStateNormal];
    btnsubmitData.titleLabel.textAlignment = NSTextAlignmentCenter;
    btnsubmitData.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    btnsubmitData.titleLabel.textColor = UIColorFromRGB(0xF0F0F0);
    btnsubmitData.frame = CGRectMake(25,Ypos,self.view.frame.size.width-50,40);
    btnsubmitData.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
    
    [btnsubmitData addTarget:self action:@selector(btnsubmitDataClk) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:btnsubmitData];
    
    [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+150)];
    
    
    [self DesignPickerView];
    
    UITapGestureRecognizer *gst=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    gst.cancelsTouchesInView=NO;
    gst.delegate=self;
    [self.view addGestureRecognizer:gst];
}
-(void)hideKeyboard
{
    UITextField *txtFTime=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+4];
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [txtFTime resignFirstResponder];
}

#pragma mark - Button Actions

-(void)btnActionClicked:(UIButton *)sender
{
    switch (sender.tag)
    {
        case kbtnDay:
            [self showTimerPicker:NO];
            self.strDatePickerSelected=@"Day";
            self.datetimeSelection.datePickerMode=UIDatePickerModeDateAndTime;
            [self showPickerView:YES];
            [self showOptionPatch:YES];
            break;
        case kbtnCase:
        {
            self.strTypeOfData=@"Case";
            [self showCustomViewForPopUpForArray:search toShow:YES];
            
        }
            break;

        case kbtnCode:
        {
            self.strTypeOfData=@"Codes";
            UITextField *txtCode=(UITextField *)[self.viewForCaseData viewWithTag:kTxtFieldCase];

            if ([txtCode.text length]==0)
            {
                UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please select case first." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertVw show];
            }
            else
            {
              if ([strViewSelected isEqualToString:@"CaseView"])
              {
                  [self showCustomViewForPopUpForArray:searchCodes toShow:YES];
              }
              else if([strViewSelected isEqualToString:@"NonCaseView"])
              {
                  [self showCustomViewForPopUpForArray:searchCodes toShow:YES];
              }
            }
        }
            break;
        case kbtnTime:
        {
            [self showPickerView:NO];
            self.strDatePickerSelected=@"Counter";
            self.datetimeSelection.datePickerMode=UIDatePickerModeTime;
            [self showTimerPicker:YES];
            [self showOptionPatch:YES];
        }
            break;
            

        default:
            break;
    }
}

-(void)btnsubmitDataClk
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    [self showOptionPatch:NO];
    [self showPickerView:NO];
    [self showTimerPicker:NO];
    UIAlertView *alertView;

    BOOL isValid=YES;

    if ([self.strViewSelected isEqualToString:@"NonCaseView"])
    {
            for (int i=0; i<[arrDataHdng count]; i++)
            {
                UITextField *TxtF=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+i];
                
            if (i==0 || i==1 || i==3 || i==4) {
              
            if ([TxtF.text length]==0)
            {
                isValid=NO;
                
                
             
                alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Please Select %@",[arrDataHdng objectAtIndex:i]] message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               
                        [alertView show];
                
                break;
            }
                }
        }
        
    }
    else if ([self.strViewSelected isEqualToString:@"CaseView"])
    {

        for (int i=1; i<[self.arrDataHdng count]; i++)
        {
            UITextField *TxtF=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+i];
            
              if (i==0 || i==1 || i==3 || i==4) {
                  
            if ([TxtF.text length]==0)
            {
                isValid=NO;
                
                alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Please Select %@",[self.arrDataHdng objectAtIndex:i]] message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                
                
                break;
            }
              }
            

        
        }
        
    }
   
    if (isValid)
    {
        [self CallForTimeRegistration];

    }
}

#pragma mark - CallForTimeRegistration

-(void)CallForTimeRegistration
{
    UITextField *txtFCodes=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+1];
    UITextField *txtFDay=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+3];
    UITextField *txtFTime=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+4];
    UITextField *txtFCase=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase];
    
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"insertTimeRegistration";
    
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    
    NSString *strCaseId=self.strCaseValue;
    
    if ([strViewSelected isEqualToString:@"NonCaseView"])
    {
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtFCase.text) forKey:@"case_nr"];

    }
    else if ([strViewSelected isEqualToString:@"CaseView"])
    {
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(strCaseId) forKey:@"case_nr"];
    }
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtFDay.text) forKey:@"day"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtFTime.text) forKey:@"time"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(self.strGlobalCompany_id) forKey:@"company_id"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtFCodes.text) forKey:@"code"];

    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error)
     {
        if (error)
        {
            if ([error isErrorOfKindNoNetwork])
                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return ;
            
            return;
        }
        
        error = nil;
        responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];

        if ([responseDict count])
        {
            NSString *strMessage=[[responseDict objectForKey:@"data"] objectForKey:@"log"];
            if (strMessage.length>0)
            {
                if ([strMessage isEqualToString:@"Time Registration Successful."])
                {
                    UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    alertVw.tag=786723;
                    [alertVw show];
                }
            }
            
        }
    }];
}

#pragma mark - UIAlertView Delegates

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==786723)
    {
        switch (buttonIndex)
        {
            case 0:
                [self.navigationController popViewControllerAnimated:YES];
                break;
                
            default:
                break;
        }
    }
}
#pragma mark - CallForGetCodes

-(void)CallForGetCodes
{
    [self.arrCodesData removeAllObjects];
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"codesfortimeregistration";
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    NSString *strCaseId=strCaseValue;

    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strCaseId) forKey:@"case_nr"];

    
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
        if (error) {
            if ([error isErrorOfKindNoNetwork])
                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return ;
            
            return;
        }
        
        error = nil;
        responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];

        if ([responseDict count])
        {
            NSArray *arrData=[responseDict objectForKey:@"data"];
            
            //[arrCodesData addObjectsFromArray:arrData];
            for(int i=0;i<arrData.count;i++)
            {
                [arrCodesData addObject:[[arrData objectAtIndex:i] objectForKey:@"code"]];
                searchCodes = [[NSArray alloc]initWithArray:arrCodesData];
            }
            //NSLog(@"array code data:%@",arrCodesData);
        }

        
    }];
    
}


-(void)CallForGetCases
{
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"listofcasesandcodesfortimeregistration";
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];

   [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    
    
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
                                              if (error) {
                                                  if ([error isErrorOfKindNoNetwork])
                                                      [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                                                  return ;
                                                  
                                                  return;
                                              }
                                              
                                              error = nil;
 responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];

        if ([responseDict count])
                                              {
                                                  [self performSelectorOnMainThread:@selector(ParseCaseDatawithDictionary:) withObject:responseDict waitUntilDone:YES];

                                              }
        
                                          }];
}
-(void)ParseCaseDatawithDictionary:(NSDictionary *)dict
{
    [self.arrCasesData removeAllObjects];
    [self.arrCodesData removeAllObjects];
    NSArray *arrayData=[dict objectForKey:@"data"];
    
    for (int i=0; i<arrayData.count; i++)
    {
        PatrawinCase *objPatraCase=[[PatrawinCase alloc]init];
        objPatraCase.account_cost=[[[arrayData objectAtIndex:i] objectForKey:@"account_cost"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"account_cost"]:@"";
        objPatraCase.agent_id=[[[arrayData objectAtIndex:i] objectForKey:@"agent_id"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"agent_id"]:@"";
        objPatraCase.arrived_date=[[[arrayData objectAtIndex:i] objectForKey:@"arrived_date"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"arrived_date"]:@"";
        objPatraCase.case_id=[[[arrayData objectAtIndex:i] objectForKey:@"case_id"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"case_id"]:@"";
        objPatraCase.case_type=[[[arrayData objectAtIndex:i] objectForKey:@"case_type"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"case_type"]:@"";
        objPatraCase.comment=[[[arrayData objectAtIndex:i] objectForKey:@"comment"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"comment"]:@"";
        objPatraCase.country_code=[[[arrayData objectAtIndex:i] objectForKey:@"country_code"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"country_code"]:@"";
        objPatraCase.customer_id=[[[arrayData objectAtIndex:i] objectForKey:@"customer_id"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"customer_id"]:@"";
        objPatraCase.customer_name=[[[arrayData objectAtIndex:i] objectForKey:@"customer_name"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"customer_name"]:@"";
        objPatraCase.deadline=[[[arrayData objectAtIndex:i] objectForKey:@"deadline"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"deadline"]:@"";
        objPatraCase.figure=[[[arrayData objectAtIndex:i] objectForKey:@"figure"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"figure"]:@"";
        objPatraCase.link=[[[arrayData objectAtIndex:i] objectForKey:@"link"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"link"]:@"";
        objPatraCase.patent_number=[[[arrayData objectAtIndex:i] objectForKey:@"patent_number"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"patent_number"]:@"";
        objPatraCase.status=[[[arrayData objectAtIndex:i] objectForKey:@"title"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"title"]:@"";
        objPatraCase.title=[[[arrayData objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSString class]]?[[arrayData objectAtIndex:i] objectForKey:@"status"]:@"";
        

        NSDictionary *dict=[[arrayData objectAtIndex:i] objectForKey:@"codes"];
        NSArray *arrData=[dict objectForKey:@"data"];
        objPatraCase.arrCodesData=[[NSMutableArray alloc]initWithArray:arrData];
        [self.arrCasesData addObject:objPatraCase];
        search = [[NSArray alloc ]initWithArray:arrCasesData];
     
    }
}

-(void)showCustomViewForPopUpForArray:(NSMutableArray *)arrData toShow:(BOOL)isshow
{
    if(self.viewForPopUpBG)
    {
        [self.viewForPopUpBG.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.viewForPopUpBG removeFromSuperview];
        self.viewForPopUpBG=nil;
    }
    if (isshow)
    {
       
        
        self.viewForPopUpBG=[[UIView alloc]initWithFrame:CGRectMake(0, IS_VERSION_GRT_7 ? 20:0, deviceWidth,IS_VERSION_GRT_7? deviceHeight-20:deviceHeight)];
        self.viewForPopUpBG.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
        [APP_DELEGATE.window addSubview:self.viewForPopUpBG];
        
        UIView *blueBar=[[UIView alloc]init];
        blueBar.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
        blueBar.frame=CGRectMake(10, 52, self.view.frame.size.width-20, 40);
        
        searchBar1 = [[UISearchBar alloc] initWithFrame:CGRectMake(10, 92, deviceWidth-20, 35)];
        searchBar1.placeholder = @"Search Cases";
        
        
        if ([strTypeOfData isEqualToString:@"Codes"]) {
            
            searchBar1.text = searchtextForCodes;
            }
        if([strTypeOfData isEqualToString:@"Case"])
        {
            searchBar1.text = searchtextForCases;
            
        }

       // searchBar1.backgroundColor = [UIColor redColor];
        searchBar1.delegate = self;
        //   [searchBar1 setImage:[UIImage imageNamed:@""] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
        [self.viewForPopUpBG addSubview:searchBar1];
        for (UIView *searchBarSubview in [searchBar1 subviews]) {
            if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
                @try {
                    
                    [(UITextField *)searchBarSubview setBorderStyle:UITextBorderStyleLine];
                }
                @catch (NSException * e) {
                    // ignore exception
                }
            }
        }

        
        if ([strTypeOfData isEqualToString:@"Codes"]) {
            
            searchBar1.placeholder = @"Search Codes";
            UIButton *infobtn = [UIButton buttonWithType:UIButtonTypeCustom];
            infobtn.frame = CGRectMake(260, 5, 30, 30);
            [infobtn setImage:[UIImage imageNamed:@"info-top"] forState:UIControlStateNormal];
            [infobtn addTarget:self action:@selector(codesInfoButton) forControlEvents:UIControlEventTouchUpInside];
            [blueBar addSubview:infobtn];
            infobtn=nil;
       }
        
        
        
        
        UILabel *cases=[[UILabel alloc]init];
        cases.frame=CGRectMake(0, 0, blueBar.frame.size.width, 40);
        cases.text= self.strTypeOfData;
        cases.textAlignment=NSTextAlignmentCenter;
        cases.textColor=[UIColor whiteColor];
        [blueBar addSubview:cases];
        
        [self.viewForPopUpBG addSubview:blueBar];
        
        self.viewForPopUp=[[UIView alloc]initWithFrame:CGRectMake(10,125, deviceWidth-20,IS_IPAD ? 870 : deviceHeight-75-60)];
        self.viewForPopUp.backgroundColor = [UIColor whiteColor] ;
        [self.viewForPopUpBG addSubview:self.viewForPopUp];
        
        tblForRecommendations=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, deviceWidth-20, IS_IPAD ? 870 : deviceHeight-75-60) style:UITableViewStylePlain];
        tblForRecommendations.delegate=self;
        tblForRecommendations.dataSource=self;
        
        tblForRecommendations.rowHeight = IS_IPAD ? 60 : 44;
        
        tblForRecommendations.backgroundColor=[UIColor whiteColor];
        tblForRecommendations.backgroundView=nil;
        //tblForRecommendations.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        
        tblForRecommendations.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        [self.viewForPopUp addSubview:tblForRecommendations];
        
        
        UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCross.frame = CGRectMake(0, 30, 24, 24);
        [btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
        [btnCross addTarget:self action:@selector(btnCross_Clicked) forControlEvents:UIControlEventTouchUpInside];
        [self.viewForPopUpBG addSubview:btnCross];
        btnCross = nil;
        
    }
}

-(void)btnCross_Clicked
{
    [self showCustomViewForPopUpForArray:nil toShow:NO];
    [tblForRecommendations reloadData];
}


-(void)codesInfoButton
{
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    
    LinkViewController *objLinkVC=[[LinkViewController alloc]init];
    objLinkVC.strUrl=[NSString stringWithFormat:@"http://app.hynell.se/hynellapptest/hynell/casecodes.php?accesstoken=%@",strAccessToken];
    objLinkVC.screenType = @"Codes";
    [self.navigationController pushViewController:objLinkVC animated:YES];
    
    [self showCustomViewForPopUpForArray:nil toShow:NO];
    
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    }

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([strTypeOfData isEqualToString:@"Codes"]) {
        
        searchtextForCodes = searchBar1.text;
    }
    if([strTypeOfData isEqualToString:@"Case"])
    {
        searchtextForCases = searchBar1.text;
    }
    

}

// use this method to search data in list =====================================================================================================================

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    
    if ([strTypeOfData isEqualToString:@"Codes"])
    {
        
        if ([searchText length] > 0) {
            
            NSMutableArray *sResult = @[].mutableCopy;
            
            for(int i=0;i<arrCodesData.count;i++){
                NSString *codesString = [arrCodesData objectAtIndex:i];
                NSLog(@"codes data %@",codesString);
                NSRange r = [codesString rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (r.length > 0) {
                    [sResult addObject:codesString];
                }
            }
            
            if (searchCodes) {
                searchCodes = nil;
            }
            
            searchCodes = [NSArray arrayWithArray:sResult];
            
            sResult = nil;
        } else {
            searchCodes = [NSArray arrayWithArray:arrCodesData];
        }
        [tblForRecommendations reloadData];


    }
    
    
if([strTypeOfData isEqualToString:@"Case"])
    {
        
    if ([searchText length] > 0) {
        
            NSMutableArray *sResult = @[].mutableCopy;
        
            for (PatrawinCase *cs in arrCasesData) {
            NSRange r = [cs.case_id rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (r.length > 0) {
                 // NSLog(@"case is's%@", cs.case_id);
                [sResult addObject:cs];
            }
        }
        
        if (search) {
            search = nil;
        }
        
        search = [NSArray arrayWithArray:sResult];
        
        sResult = nil;
    } else {
        search = [NSArray arrayWithArray:arrCasesData];
    }
    
        [tblForRecommendations reloadData];

}

 }

#pragma mark - UISearchField Delegates

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
}




#pragma mark - UIPicker Delegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (component==0)
    {
        return 24;
    }
    
    else
    {
        return 12;
    }
    
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [NSString stringWithFormat:@"%i Hours", (int)row];
    }
    
    else
    {
        return [NSString stringWithFormat:@"%i Minutes", (int)row*5];
    }
}


#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger row=0;
    if ([strTypeOfData isEqualToString:@"Codes"])
    {
        if ([strViewSelected isEqualToString:@"NonCaseView"])
        {
            
            if (searchCodes==0)
            {
                row=1;
            }
            else
            row= searchCodes.count;
        }
        else if([strViewSelected isEqualToString:@"CaseView"])
        {
            if (searchCodes.count==0)
            {
                row=1;
            }
            else
            row=[searchCodes count];
        }
    }
    
    else if([strTypeOfData isEqualToString:@"Case"])
    {
        if ([strViewSelected isEqualToString:@"NonCaseView"])
        {
            if (search.count==0)
            {
                row=1;
            }
            else
                row= search.count;

        }
        else if([strViewSelected isEqualToString:@"CaseView"])
        {
            if (search.count==0)
            {
                row=1;
            }
            else
                row= search.count;
   
        }

    }
    return row;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_IPAD ? 60 : 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static  NSString *CellIdentifier=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
    }
    
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UILabel *lbl_NameHD = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 290, 30)];
    lbl_NameHD.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
   // lbl_NameHD.backgroundColor=[UIColor redColor];
    lbl_NameHD.font = IS_IPAD ? FONT_NORMAL(18) : FONT_NORMAL(12.0);
    lbl_NameHD.textAlignment=NSTextAlignmentCenter;
    lbl_NameHD.textColor=[UIColor blackColor];
    [cell.contentView addSubview:lbl_NameHD];
    
    if([strTypeOfData isEqualToString:@"Codes"])
    {
        if ([strViewSelected isEqualToString:@"CaseView"])
        {
            if (searchCodes.count==0)
            {
                UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?380: 230,IS_IPAD? 768: 320, 30)];
                [lblNoRecord setText:@"No Record Available."];
                [lblNoRecord setTextColor:[UIColor lightGrayColor]];
                lblNoRecord.textAlignment = NSTextAlignmentCenter;
                [lblNoRecord setBackgroundColor:[UIColor clearColor]];
                [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
                [cell.contentView addSubview:lblNoRecord];
                cell.backgroundColor=[UIColor clearColor];
            }
            else
            {
                lbl_NameHD.text=[searchCodes objectAtIndex:indexPath.row];
                lbl_NameHD.textAlignment = NSTextAlignmentLeft;

              //  NSDictionary *dict=[self.arrCodesData objectAtIndex:indexPath.row];
                //lbl_NameHD.text=[dict objectForKey:@"code"];
            }
        }
        else if([strViewSelected isEqualToString:@"NonCaseView"])
        {
            if (searchCodes.count==0)
            {
                UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?380: 230,IS_IPAD? 768: 320, 30)];
                [lblNoRecord setText:@"No Record Available."];
                [lblNoRecord setTextColor:[UIColor lightGrayColor]];
                lblNoRecord.textAlignment = NSTextAlignmentCenter;
                [lblNoRecord setBackgroundColor:[UIColor clearColor]];
                [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
                [cell.contentView addSubview:lblNoRecord];
                cell.backgroundColor=[UIColor clearColor];
            }
            else
            {
                
                lbl_NameHD.text=[searchCodes objectAtIndex:indexPath.row];
                lbl_NameHD.textAlignment = NSTextAlignmentLeft;

               // NSLog(@"position: %i",indexPath.row);
               // lbl_NameHD.text=[dict objectForKey:@"code"];

//                NSDictionary *dictTemp=arrCodesData;
//                NSString *a = dictTemp[@"code"];
                
                //lbl_NameHD.text=a;
                
                
            }
        }
    }
    
    else if ([strTypeOfData isEqualToString:@"Case"])
    {
        if (search.count==0)
        {
            UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?380: 230,IS_IPAD? 768: 320, 30)];
            [lblNoRecord setText:@"No Record Available."];
            [lblNoRecord setTextColor:[UIColor lightGrayColor]];
            lblNoRecord.textAlignment = NSTextAlignmentCenter;
            [lblNoRecord setBackgroundColor:[UIColor clearColor]];
            [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
            [cell.contentView addSubview:lblNoRecord];
            cell.backgroundColor=[UIColor clearColor];
            
        }
        else
        {
            PatrawinCase *objTemp=[search objectAtIndex:indexPath.row];
            lbl_NameHD.text=objTemp.case_id;
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([strTypeOfData isEqualToString:@"Codes"])
    {
        [self showCustomViewForPopUpForArray:nil toShow:NO];
        if ([strViewSelected isEqualToString:@"CaseView"])
        {
            if (searchCodes.count==0)
            {
                
            }
            else
            {
              //  NSDictionary *dict=[self.arrCodesData objectAtIndex:indexPath.row];
                UITextField *txtFCode=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+1];
                txtFCode.text=[searchCodes objectAtIndex:indexPath.row];
            }
        }
        else if([strViewSelected isEqualToString:@"NonCaseView"])
        {
            if (searchCodes.count==0)
            {
                
            }
            else
            {
//                NSDictionary *dictTemp=[arrCodesData objectAtIndex:indexPath.row];
                UITextField *txtFCode=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+1];
                txtFCode.text=[searchCodes objectAtIndex:indexPath.row];
            }
        }
        [tblForRecommendations reloadData];

    }
    else if ([strTypeOfData isEqualToString:@"Case"])
    {
        if ([strViewSelected isEqualToString:@"CaseView"])
        {
            if (search.count==0)
            {
                
            }
            else
            {
                [self showCustomViewForPopUpForArray:nil toShow:NO];
                PatrawinCase *objTemp=[search objectAtIndex:indexPath.row];
                UITextField *txtFCases=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase];
                txtFCases.text=objTemp.case_id;
                
                UITextField *txtFCustomer=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+2];
                txtFCustomer.text=objTemp.customer_name;
                
                self.strGlobalCompany_id=objTemp.customer_id;
            }
            
        }
        else if([strViewSelected isEqualToString:@"NonCaseView"])
        {
            if (search.count==0)
            {
                
            }
            else
            {
                [self showCustomViewForPopUpForArray:nil toShow:NO];
                PatrawinCase *objTemp=[search objectAtIndex:indexPath.row];
                UITextField *txtFCases=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase];
                txtFCases.text=objTemp.case_id;
                strCaseValue = objTemp.case_id;
                
                if (strCaseValue.length >0)
                {
                    [self CallForGetCodes];
                    
                }
                
                if (indexPath.row) {
                    UITextField *txtFCode=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+1];
                    txtFCode.text=nil;
                }
                
                
                UITextField *txtFCustomer=(UITextField *)[self.scrollView viewWithTag:kTxtFieldCase+2];
                txtFCustomer.text=objTemp.customer_name;
                
                self.strGlobalCompany_id=objTemp.customer_id;
                
                //arrSelectedCodes=objTemp.arrCodesData;
            }
            
        }
        
        [tblForRecommendations reloadData];

        
    }
    
     }

@end

