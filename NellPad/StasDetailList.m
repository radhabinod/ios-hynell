//
//  StasDetailList.m
//  NellPat
//
//  Created by Macbook on 7/7/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import "StasDetailList.h"


@implementation StasDetailList

@dynamic accountCost;
@dynamic agentId;
@dynamic arrivedDate;
@dynamic caseComment;
@dynamic caseId;
@dynamic caseType;
@dynamic comment;
@dynamic corresID;
@dynamic countryCode;
@dynamic customerId;
@dynamic customerName;
@dynamic deadline;
@dynamic figure;
@dynamic link;
@dynamic patentNumber;
@dynamic status;
@dynamic title;
@dynamic recommendationID;
@dynamic recommendationStatus;
@dynamic recommendationText;
@dynamic keyword;
@dynamic deadlineCode;

@end
