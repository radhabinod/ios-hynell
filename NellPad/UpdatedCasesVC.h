//
//  UpdatedCasesVC.h
//  Hynell
//
//  Created by Ram Kumar on 18/10/16.
//  Copyright © 2016 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdatedCasesVC : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableDictionary *responseDict;
}


@property(strong, nonatomic) UITableView *caseTableView;
@property(strong, nonatomic) NSMutableArray *updateCasesArray;
@property(nonatomic,strong) NSMutableArray *arrRecommendations;

-(void)recentlyUpdateCases;
@end
