//
//  DataSyncBO.m
//  NellPat
//
//  Created by Macbook on 6/10/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import "DataSyncBO.h"
#import "PetrawinCasesList.h"
#import "StasList.h"
#import "StatsLisingController.h"
#import "StasDetailList.h"
#import "RecommendationsList.h"
#import "CompanyList.h"
#import "CompanyBO.h"

@implementation DataSyncBO
{
    
    int start;
    int end;
    int count;
    int noOfCase;
   NSDictionary *responseDict;
   NSMutableArray *caseArray;
    
}

NSArray *statsDetailArray;

-(void)LoginCall
{
    [self deleteAllEntities:@"PetrawinCasesList"];
    [self deleteAllEntities:@"RecommendationsList"];
    [self deleteAllEntities:@"StasList"];
    [self deleteAllEntities:@"StasDetailList"];
    [self deleteAllEntities:@"CompanyList"];
    
//    [self deleteEntitiesWRTSelectedStr:@"StasDetailList" withstr:];
//    [self caseList];
//    [self CallForStaticsData];
//    [self callForListOfCompanies];
}




//-(void)caseList
//{
//    
//    start = 0;
//    end = 100;
//    count=0;
//    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
//    
//    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
//    inputParams.methodType = WebserviceMethodTypePost;
//    inputParams.relativeWebservicePath = @"petrawincasesofagent";
//    
//    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
//    
//    if ([strKey isEqualToString:@"1"])
//    {
//        [inputParams.dict_postParameters setObject:@"2" forKey:@"status_id"];
//    }
//    
//    else if ([strKey isEqualToString:@"0"])
//    {
//        [inputParams.dict_postParameters setObject:@"1" forKey:@"status_id"];
//    }
//    
//    [inputParams.dict_postParameters setObject:[NSString stringWithFormat:@"%d", start] forKey:@"start"];
//    [inputParams.dict_postParameters setObject:[NSString stringWithFormat:@"%d", end] forKey:@"end"];
//    
//    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
//     {
//         if (error)
//         {
//             if ([error isErrorOfKindNoNetwork])
//                 
//                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//             return;
//             
//             return;
//         }
//         
//         else
//         {
//             error = nil;
//             
//             [self deleteAllEntities:@"PetrawinCasesList"];
//             
//             responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
//             caseArray = responseDict[@"data"];
//             
//             noOfCase = [responseDict[@"total_cases"] intValue];
//             
//             NSLog(@"TOTAL**%d",noOfCase);
//             
//             //NSLog(@"cases**%@",caseArray);
//             
//             
//             
//             while (noOfCase>=count) {
//                 if(count>0)
//                 {
//                     [ self updateData];
//                 }
//                 else{
//                     for (int i=0 ;i<caseArray.count;i++) {
//                         
//                         [self ParseAndSaveData:[caseArray objectAtIndex:i]];
//                         count=count+1;
//                         NSLog(@"START %d END %d",start,end);
//                     }
//                     
//                     //             [self deleteAllEntities:@"RecommendationsList"];
//                     NSLog(@"hheeeeee ");
//                     
//                     
//                     
//                     //NSLog(@"main count :%d array size %@ i:%d,start %d end %d",count,caseArray,i,start,end);
//                 }
//                 start=end;
//                 end=end+100;
//                 
//             }
//             
//             NSArray *arrRecommendation = responseDict[@"recommendation_list"];
//             
//             [self deleteAllEntities:@"RecommendationsList"];
//             
//             for (int i=0 ;i<arrRecommendation.count;i++) {
//                 [self ParseAndSaveRecommText:[arrRecommendation objectAtIndex:i]];
//             }
//             
//             [[NSNotificationCenter defaultCenter]postNotificationName:@"CaseListdownloaded" object:nil];
//         }
//     }];
//}








-(void)caseList
{

    start = 0;
    end = 100;
    count=0;
     
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    inputParams.relativeWebservicePath = @"petrawincasesofagent";
    
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    
    if ([strKey isEqualToString:@"1"])
    {
        [inputParams.dict_postParameters setObject:@"2" forKey:@"status_id"];
    }
    
    else if ([strKey isEqualToString:@"0"])
    {
        [inputParams.dict_postParameters setObject:@"1" forKey:@"status_id"];
    }

    [inputParams.dict_postParameters setObject:[NSString stringWithFormat:@"%d", start] forKey:@"start"];
    [inputParams.dict_postParameters setObject:[NSString stringWithFormat:@"%d", end] forKey:@"end"];

    
    //  add hud
    
    //text downloaded cases   0 total cases  100
    
    
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         }
         
         else
         {
             error = nil;
             
             [self deleteAllEntities:@"PetrawinCasesList"];

             responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];

             noOfCase = [responseDict[@"total_cases"] intValue];
             
              caseArray = responseDict[@"data"];
             
             //count =[caseArray count];
             NSLog(@"TOTAL**%d",noOfCase);
             

             
       
                        for (int i=0 ;i<caseArray.count;i++) {
                            // update hud text
                            
                            
                            //text downloaded cases   i total cases  noOfCases
                      
                            
                        [self ParseAndSaveData:[caseArray objectAtIndex:i]];
                            
                        count++;
                            
                        }
             
             
             if (noOfCase >[caseArray count]) {
                 start=end;
                 end=end+100;

                 [self updateData];
                 
             }
             

             NSArray *arrRecommendation = responseDict[@"recommendation_list"];
            
             [self deleteAllEntities:@"RecommendationsList"];
             
             for (int i=0 ;i<arrRecommendation.count;i++) {
                 [self ParseAndSaveRecommText:[arrRecommendation objectAtIndex:i]];
             }
            // }
             
             if ([strKey isEqualToString:@"1"]) {
                 
                  [[NSNotificationCenter defaultCenter]postNotificationName:@"CaseListdownloaded" object:nil];
             }
         
             
         }
     }];
    
}

-(void)updateData
{
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];

    NSLog(@"FUNCTION START %d END %d",start,end);
    
   
    WebserviceInputParameter *inputParams2 = [[WebserviceInputParameter alloc] init];
    inputParams2.methodType = WebserviceMethodTypePost;
    inputParams2.relativeWebservicePath = @"petrawincasesofagent";
    
    [inputParams2.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    
    if ([strKey isEqualToString:@"1"])
    {
        [inputParams2.dict_postParameters setObject:@"2" forKey:@"status_id"];
    }
    
    else if ([strKey isEqualToString:@"0"])
    {
        [inputParams2.dict_postParameters setObject:@"1" forKey:@"status_id"];
    }
    
  [inputParams2.dict_postParameters setObject:[NSString stringWithFormat:@"%d", start] forKey:@"start"];
  [inputParams2.dict_postParameters setObject:[NSString stringWithFormat:@"%d", end] forKey:@"end"];
    
    [WebserviceManager callWebserviceWithInputParame:inputParams2 completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         }
         else{
             
             error = nil;

             responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
             [caseArray addObjectsFromArray:responseDict[@"data"]];
         
         
             NSLog(@"hheeeeee 1");
            
             
             for (int i=start; i<caseArray.count;i++) {
                 
                 
                 // update hud text
                 //text downloaded cases   i total cases  noOfCases
                 
                 
                 [self ParseAndSaveData:[caseArray objectAtIndex:i]];
                 
                 //[self deleteAllEntities:@"PetrawinCasesList"];
                 
                 count++;
                 if (count>=noOfCase) {
                     break;
                 }
                 
             }
             
             start=end;
             end=end+100;
             
             if (noOfCase>count) {
                 
                 
                 [self updateData];
            
             }
             
             else
             {
                 [[NSNotificationCenter defaultCenter]postNotificationName:@"CaseListdownloaded" object:nil];
             }
       }
         
         
     }];
  }




//-(void)caseList
//{
//
//    start = 0;
//    end = 1701;
//    count=0;
//    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
//
//    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
//    inputParams.methodType = WebserviceMethodTypePost;
//    inputParams.relativeWebservicePath = @"petrawincasesofagent";
//
//    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
//
//    if ([strKey isEqualToString:@"1"])
//    {
//        [inputParams.dict_postParameters setObject:@"2" forKey:@"status_id"];
//    }
//
//    else if ([strKey isEqualToString:@"0"])
//    {
//        [inputParams.dict_postParameters setObject:@"1" forKey:@"status_id"];
//    }
//    
//    [inputParams.dict_postParameters setObject:[NSString stringWithFormat:@"%d", start] forKey:@"start"];
//    [inputParams.dict_postParameters setObject:[NSString stringWithFormat:@"%d", end] forKey:@"end"];
//    
//    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
//     {
//         if (error)
//         {
//             if ([error isErrorOfKindNoNetwork])
//                 
//                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//             return;
//             
//             return;
//         }
//         
//         else
//         {
//             error = nil;
//             
//             responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
//            // caseArray = responseDict[@"data"];
//             
//             
//             
//             //[self deleteAllEntities:@"PetrawinCasesList"];
//             //             [self deleteAllEntities:@"RecommendationsList"];
//             
//             
//             
//          //   @try {
//                 
////                 noOfCase = [responseDict[@"total_cases"] intValue];
////                           NSLog(@"TOTAL**%d",noOfCase);
//
////                 while (noOfCase>=count) {
////                     
////                     if(count>0)
////                     {
//////                         start=100;
//////                         end =100;
////                             NSString *strKey= @"2";
////                         
//////                         @try {
//////                             NSString *post = [NSString stringWithFormat:@"accesstoken=%@&status_id=%@&start=%d&end=%d",USR_DEFAULTS(@"token"),strKey,start,end];
//////
//////                             NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//////                             NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
//////                             NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//////                             [request setURL:[NSURL URLWithString:@"http://app.hynell.se/hynellapptest/petrawincasesofagent"]];
//////                             [request setHTTPMethod:@"POST"];
//////                             [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//////                             [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//////                             [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//////                             [request setHTTPBody:postData];
//////                             NSURLSession *session = [NSURLSession sharedSession];
//////                             NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//////                                NSLog(@"requestReply: %@", data);
//////                        
//////                                 if (error)
//////                                 {
//////                                     //                                        if ([error isErrorOfKindNoNetwork])
//////                                     //
//////                                     //                                    [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//////                                     //                                                return;
//////                                     //
//////                                     //                                    return;
//////                                     NSLog(@"session data");
//////                                     
//////                                 }
//////                                 else{
//////                                     
//////                                     NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//////                                     
//////                                     
//////                                     NSLog(@"requestReply: %@", requestReply);
//////                                     
//////                                     // NSLog(@"response get:%@" ,response);
//////                                     
//////                                     
//////                                     //                                 NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
//////                                     //                                 //             caseArray1 = responseDict1[@"data"];
//////                                     //                                 NSArray *caseArray2 = json[@"data"];
//////                                     //
//////                                     //                                              for (int i=0 ;i<caseArray2.count;i++) {
//////                                     //                                                  [self ParseAndSaveData:[caseArray2 objectAtIndex:i]];
//////                                     //                                                  count++;
//////                                     //                                                  //  NSLog(@"data %d and array1 %@ ",i,caseArray1);
//////                                     //                                              }
//////                                     
//////                                 }
//////                             }];
//////                             
//////                             [task resume];
//////
//////                             
//////                         }
//////                         @catch (NSException *exception) {
//////                             //
//////                         }
//////                         @finally {
//////                             
//////                         }
////                         
////                         
////                         
////                          //   NSString *post = [NSString stringWithFormat:@"accesstoken=%@&status_id=%@&start=%d&end=%d",USR_DEFAULTS(@"token"),strKey,start,end];
////                         
////                        
////
////                         
//////                         WebserviceInputParameter *inputParams2 = [[WebserviceInputParameter alloc] init];
//////                         inputParams2.methodType = WebserviceMethodTypePost;
//////                         inputParams2.relativeWebservicePath = @"petrawincasesofagent";
//////                         
//////                         [inputParams2.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
//////                         
//////                         if ([strKey isEqualToString:@"1"])
//////                         {
//////                             [inputParams2.dict_postParameters setObject:@"2" forKey:@"status_id"];
//////                         }
//////                         
//////                         else if ([strKey isEqualToString:@"0"])
//////                         {
//////                             [inputParams2.dict_postParameters setObject:@"1" forKey:@"status_id"];
//////                         }
//////                         
//////                         [inputParams2.dict_postParameters setObject:[NSString stringWithFormat:@"%d", start] forKey:@"start"];
//////                         [inputParams2.dict_postParameters setObject:[NSString stringWithFormat:@"%d", end] forKey:@"end"];
//////                         
//////                         [WebserviceManager callWebserviceWithInputParame:inputParams2 completion:^(id responses, NSError *error)
//////                          {
//////                              if (error)
//////                              {
//////                                  if ([error isErrorOfKindNoNetwork])
//////                                      
//////                                      [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//////                                  return;
//////                                  
//////                                  return;
//////                              }
//////                              else{
//////                                  
//////                                  error = nil;
//////
//////                                  responseDict = [NSJSONSerialization JSONObjectWithData:responses options:NSJSONReadingMutableContainers error:&error];
//////                                  caseArray = responseDict[@"data"];
//////                                  //   NSLog(@"hheeeeee  start %d end %d count %d totalcasen %d",start,end,count,noOfCase);
//////                                  //NSLog(@"hheeeeee%@",caseArray);
//////                                  NSLog(@"hheeeeee 1");
//////                                  
//////                                  
//////                                  for (int i=0 ;i<caseArray.count;i++) {
//////                                      
//////                                      [self ParseAndSaveData:[caseArray objectAtIndex:i]];
//////                                      count++;
//////                                      //                 //  NSLog(@"data %d and array1 %@ ",i,caseArray);
//////                                      //             }
//////                                      
//////                                  }
////////                                  start=end;
////////                                  end=end+100;
//////                                  
//////                                  //  [self deleteAllEntities:@"PetrawinCasesList"];
//////                                  
//////                                  
//////                              }
//////                              
//////                          }];
////                         
////                         
////                     }
////                     
////                     
////                     
////                     else{
//                 
//                         caseArray = responseDict[@"data"];
//
//                         for (int i=0 ;i<caseArray.count;i++)
//                         {
//                             [self ParseAndSaveData:[caseArray objectAtIndex:i]];
//                             
//                             count ++;
//                             
//                             //NSLog(@"data %d and array %@ ",i,caseArray);
//                             
//                         }
////                         start=end;
////                         end=end+100;
//             
//               //  }
//             //    }
//                 
//                 NSArray *arrRecommendation = responseDict[@"recommendation_list"];
//                 
//                 [self deleteAllEntities:@"RecommendationsList"];
//                 
//                 for (int i=0 ;i<arrRecommendation.count;i++) {
//                     [self ParseAndSaveRecommText:[arrRecommendation objectAtIndex:i]];
//                 }
//
//             }
////             @catch (NSException *exception) {
////                 
////                 NSLog(@"exception == %@", exception);
////
////             }
////             @finally {
////                 
////             }
//         
//             
//             
//             
//             
//             [[NSNotificationCenter defaultCenter]postNotificationName:@"CaseListdownloaded" object:nil];
//         
//     }];
//}



//-(void)caseList
//{
//    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
//
//    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
//    inputParams.methodType = WebserviceMethodTypePost;
//    inputParams.relativeWebservicePath = @"petrawincasesofagent";
//
//    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
//
//    if ([strKey isEqualToString:@"1"])
//    {
//        [inputParams.dict_postParameters setObject:@"2" forKey:@"status_id"];
//    }
//
//    else if ([strKey isEqualToString:@"0"])
//    {
//        [inputParams.dict_postParameters setObject:@"1" forKey:@"status_id"];
//    }
//
//    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
//     {
//         if (error)
//         {
//             if ([error isErrorOfKindNoNetwork])
//
//                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//             return;
//
//             return;
//         }
//
//         else
//         {
//             error = nil;
//
//             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
//             NSArray *caseArray = responseDict[@"data"];
//
//             [self deleteAllEntities:@"PetrawinCasesList"];
//             //             [self deleteAllEntities:@"RecommendationsList"];
//
//
//             for (int i=0 ;i<caseArray.count;i++) {
//                 [self ParseAndSaveData:[caseArray objectAtIndex:i]];
//             }
//
//             NSArray *arrRecommendation = responseDict[@"recommendation_list"];
//
//             [self deleteAllEntities:@"RecommendationsList"];
//
//             for (int i=0 ;i<arrRecommendation.count;i++) {
//                 [self ParseAndSaveRecommText:[arrRecommendation objectAtIndex:i]];
//             }
//
//             [[NSNotificationCenter defaultCenter]postNotificationName:@"CaseListdownloaded" object:nil];
//         }
//     }];
//}




- (void)deleteAllEntities:(NSString *)nameEntity
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    
    NSError *error;
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *object in fetchedObjects)
    {
        [context deleteObject:object];
    }
    
    error = nil;
    [context save:&error];
}

-(void)ParseAndSaveData :(NSDictionary *) caseDict
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    PetrawinCasesList *patCase = [NSEntityDescription insertNewObjectForEntityForName:@"PetrawinCasesList" inManagedObjectContext:context];
    patCase.accountCost=caseDict[@"account_cost"];
   // NSNumber *agentId= [NSNumber numberWithInt:[caseDict[@"agent_id"] intValue]];
    patCase.agentId = caseDict[@"agent_id"];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.zzz"];
    NSDate *dateArrived=[formatter dateFromString:caseDict[@"arrived_date"]];
    
    NSLog(@"Case ID ------------------------ %@",caseDict[@"case_id"]);
    
    patCase.arrivedDate = dateArrived;
    //NSNumber *caseId= [NSNumber numberWithInt:[caseDict[@"case_id"] intValue]];
    patCase.caseId =caseDict[@"case_id"];
    patCase.caseComment =caseDict[@"Case_Comment"];
    patCase.caseType =caseDict[@"case_type"];
    patCase.comment =caseDict[@"comment"];
    patCase.countrycode =caseDict[@"country_code"];
    NSNumber *customerId= [NSNumber numberWithInt:[caseDict[@"customer_id"] intValue]];
    patCase.customerId=customerId;
    patCase.customerName=caseDict[@"customer_name"];
    //patCase.pcNewDL=caseDict[@""];
    patCase.figure=caseDict[@"figure"];
    patCase.keyword =caseDict[@"keyword"];
    patCase.deadline=caseDict[@"new_deadline"];
    patCase.deadlineCode=caseDict[@"deadline_code"];
    //patCase.pcNewDL=caseDict[@""];
    patCase.figuresmall = caseDict[@"figuresmall"];
    patCase.link=caseDict[@"link"];
    patCase.patentNo =caseDict[@"patent_number"];
    patCase.title =caseDict[@"title"];
    patCase.status =caseDict[@"status"];
    patCase.recommendationText = caseDict[@"Recommendation_Text"];
    NSNumber *recID= [NSNumber numberWithInt:[caseDict[@"Recommendation_Id"] intValue]];
    patCase.recommendationID=recID;
    NSNumber *recStatus= [NSNumber numberWithInt:[caseDict[@"Recommendation_Status"] intValue]];
    patCase.recommendationStatus=recStatus;
  //  NSLog(@"case id %@",caseDict[@"case_id"]);
    NSError *error;
    [context save : &error];

}

-(void)ParseAndSaveRecommText : (NSDictionary *) dictRecommendation
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    RecommendationsList *recomm_List = [NSEntityDescription insertNewObjectForEntityForName:@"RecommendationsList" inManagedObjectContext:context];
    
    recomm_List.recommendation_ID = dictRecommendation[@"Recommendation_Id"];
    recomm_List.recommendation_Text = dictRecommendation[@"Recommendation_Text"];
    recomm_List.recommendation_Status = dictRecommendation[@"Recommendation_Status"];
    
    NSError *error;
    [context save : &error];

}
//

-(void)CallForStaticsData
{
    NSMutableArray *arrData = [[NSMutableArray alloc]init];
    [arrData removeAllObjects];
    
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"statisticsOnBasesOfAccesstoken";
    
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
        
        if (error)
        {
            if ([error isErrorOfKindNoNetwork])
                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return ;
        }
        
        error = nil;
         NSDictionary *responseDict=nil;
        responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
        staticsArray = responseDict[@"data"];
        
        [self deleteAllEntities:@"StasList"];
        [self deleteAllEntities:@"StasDetailList"];

        for (int i=0 ;i<staticsArray.count;i++) {
            [self ParseAndSaveStat:[staticsArray objectAtIndex:i] withIValue:[NSString stringWithFormat:@"%d",i+1]];
        }
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"GraphListdownloaded" object:nil];
    }];
}

-(void) ParseAndSaveStat :(NSDictionary *) caseDict withIValue:(NSString *)selectedStr
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    StasList *Statics = [NSEntityDescription insertNewObjectForEntityForName:@"StasList" inManagedObjectContext:context];
    
    Statics.name=[[caseDict allKeys] objectAtIndex:0];
    Statics.number=[[caseDict allValues] objectAtIndex:0];

    NSError *error;
    [context save : &error];
    
//    [self CallForStatsData:selectedStr];
}

//

-(void)CallForStatsData:(NSString*)selectedStr
{
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"innerStatisticsOnBasesOfAccesstoken";
    
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    NSString *strTag=selectedStr;
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strTag) forKey:@"case"];
    
    if ([strKey isEqualToString:@"1"])
    {
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"2") forKey:@"status_id"];
    }
    else if ([strKey isEqualToString:@"0"])
    {
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"1") forKey:@"status_id"];
    }
    
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
        if (error) {
            if ([error isErrorOfKindNoNetwork])
                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return ;
        }
        error = nil;
        NSDictionary *responseDict=nil;
        responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];

//    id responseData=[WebserviceManager callWebserviceWithInputParams:inputPram];
//    NSError* error = nil;
//    if ([responseData isKindOfClass:[NSError class]]) {
//        error=(NSError*)responseData;
//        [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//    }
//    else
//    {
//        NSDictionary *responseDict =nil;
//        responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
        statsDetailArray = responseDict[@"data"];
        
        for (int i=0 ;i<statsDetailArray.count;i++) {
            [self ParseAndSaveStatList:[statsDetailArray objectAtIndex:i] withselectedStr:selectedStr];
        }
        
        NSArray *arrRecommendation = responseDict[@"recommendation_list"];
        
        [self deleteAllEntities:@"RecommendationsList"];
        
        for (int i=0 ;i<arrRecommendation.count;i++) {
            [self ParseAndSaveRecommText:[arrRecommendation objectAtIndex:i]];
        }
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"StatsListdownloaded" object:nil];

//    }
//    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
//        if (error) {
//            if ([error isErrorOfKindNoNetwork])
//                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//            return ;
//        }
//        
           }];
}

-(void) ParseAndSaveStatList :(NSDictionary *) caseDict withselectedStr:(NSString*)selectedStr
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    StasDetailList *statsList = [NSEntityDescription insertNewObjectForEntityForName:@"StasDetailList" inManagedObjectContext:context];
    statsList.corresID=selectedStr;
    statsList.accountCost=caseDict[@"account_cost"];
  //  NSNumber *agentID= [NSNumber numberWithInt:[caseDict[@"agent_id"] intValue]];
    statsList.agentId=caseDict[@"agent_id"];
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.zzz"];
    
    NSDate *dateArrived=[formatter dateFromString:caseDict[@"arrived_date"]];
    statsList.arrivedDate = dateArrived;
    //NSNumber *CaseID= [NSNumber numberWithInt:[caseDict[@"case_id"] intValue]];
    statsList.caseId=caseDict[@"case_id"];
    statsList.caseComment=caseDict[@"Case_Comment"];
    statsList.caseType =caseDict[@"case_type"];
    statsList.comment=caseDict[@"comment"];
    statsList.countryCode =caseDict[@"country_code"];
    NSNumber *customerId= [NSNumber numberWithInt:[caseDict[@"customer_id"] intValue]];
    statsList.customerId=customerId;
    statsList.customerName=caseDict[@"customer_name"];
    statsList.figure=caseDict[@"figure"];
    statsList.keyword = caseDict[@"keyword"];
    statsList.deadline =caseDict[@"new_deadline"];
    statsList.deadlineCode=caseDict[@"deadline_code"];
    statsList.link=caseDict[@"link"];
    statsList.patentNumber =caseDict[@"patent_number"];
    statsList.title =caseDict[@"title"];
    statsList.status =caseDict[@"status"];
    statsList.recommendationText = caseDict[@"Recommendation_Text"];
    NSNumber *recID= [NSNumber numberWithInt:[caseDict[@"Recommendation_Id"] intValue]];
    statsList.recommendationID=recID;
    NSNumber *recStatus= [NSNumber numberWithInt:[caseDict[@"Recommendation_Status"] intValue]];
    statsList.recommendationStatus=recStatus;

    //NSLog(@"%@", statsList);
  
    NSError *error;
    [context save : &error];
    
}
//

- (void) callForListOfCompanies
{
    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"listofcompanies";
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error)
     {
         if (error)
         {
             if ([error isErrorOfKindNoNetwork])
                 
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             
             return ;
         }
         error = nil;
         NSDictionary *responseDict =nil;
         responseDict = nil;
         
         responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
         
         NSLog(@"Response Dict = %@", responseDict);
         
        //NSMutableArray *arrayListOfCompaniesId = [[responseDict objectForKey:@"data"] valueForKey:@"company_id"];
         //arrayListOfCompaniesName = [[self.responseDict objectForKey:@"data"] valueForKey:@"company_name"];
         
        NSMutableArray *arrayListOfCompaniesName = responseDict[@"data"];
         
         [self deleteAllEntities:@"CompanyList"];
        
         for (int i=0 ;i<arrayListOfCompaniesName.count;i++) {
             [self ParseAndSaveCompanyName:[arrayListOfCompaniesName objectAtIndex:i]];
         }
         
         [[NSNotificationCenter defaultCenter]postNotificationName:@"CompanyListdownloaded" object:nil];
     }];
}

-(void)ParseAndSaveCompanyName : (NSDictionary *) dictCompanyName
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    CompanyList *companyData = [NSEntityDescription insertNewObjectForEntityForName:@"CompanyList" inManagedObjectContext:context];
    
    companyData.companyID = [dictCompanyName valueForKey:@"company_id"];
    companyData.companyName = [dictCompanyName valueForKey:@"company_name"];

    NSError *error;
    [context save : &error];
}

- (void)deleteEntitiesWRTSelectedStr:(NSString *)nameEntity withstr:(NSString *) selectedStr
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSPredicate *prdecate=[NSPredicate predicateWithFormat:@"corresID == %@",selectedStr];
    fetchRequest.predicate=prdecate;
    
    NSError *error;
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [context deleteObject:object];
    }
    
    error = nil;
    [context save:&error];
}

@end
