//
//  InboxVC.h
//  NellPad
//
//  Created by Herry Makker on 12/9/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Message;
@interface InboxVC : UIViewController<UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate,UIActionSheetDelegate>
@property (nonatomic,strong) UITableView *tblMessages;
@property (nonatomic,strong) UITextField *txtFSendTo;
@property (nonatomic,strong) NSMutableArray *arrMessages;
@property (nonatomic,strong) UIButton *btnDropDown;
@property (nonatomic,strong) NSDictionary *responseDict;
@property(nonatomic,strong) UIActionSheet *actionSheetSort;
@property(nonatomic,strong) UIView *viewForPopUpBG;
@property(nonatomic,strong) UIView *viewForPopUp;

@end
