//
//  CasesVC.h
//  NellPad
//
//  Created by Herry Makker on 12/9/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <QuickLook/QuickLook.h>


#define KlblCountry 2356
#define KlblTitle 4356

#define kBtnCompose 56565
#define kBtnShare 45458
#define kBtnRegistration 45546
#define kBtnUpdate 45455
#define kBtnLeft 5655
#define kBtnRight 8974
#define kBtnFigure 21324
#define kTxtVComment 3000
#define kBtnImage 3001
#define kTxtVRecommendation 34546
#define kStsVRecommendation 34500
#define kBtnSelectRecommendation 98414
#define kbtnApproved 565
#define kbtnDecline 566
#define kbtnWeight 567

#define kbtnRecom1 56666
#define kbtnRecom 56777



@class PatrawinCase;
@interface CasesVC : UIViewController<  UITextViewDelegate,UIWebViewDelegate,MFMailComposeViewControllerDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIAlertViewDelegate,QLPreviewControllerDelegate,QLPreviewControllerDataSource,UIActionSheetDelegate>

@property (strong,nonatomic) NSDictionary *responseDict;
@property(nonatomic,strong) NSMutableArray *arrDataHdng;
@property(nonatomic,strong) NSMutableArray *arrayAllCases;
@property(nonatomic,strong) NSString *strSelectedIndex;
@property(nonatomic,strong) UILabel *lbl_CaseName;
@property(nonatomic,strong) UIView *viewForCaseData;
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) UIView *ViewForRecommendations;
@property(nonatomic,strong) UIView *ViewForRecommendationsBG;
@property(nonatomic,strong) NSArray *arrRecommendationListBO;
@property(nonatomic,strong) NSString *strGlobalRecommendationId;


@property(nonatomic,strong) NSString *recommendation_TexPC;
@property(nonatomic,strong) NSString *casesScreentype;


-(void)setText:(NSString *)RecmStatusID;
-(void)refreshPatrawinCasesSorting;



@end
