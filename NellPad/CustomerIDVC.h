//
//  CustomerIDVC.h
//  NellPat
//
//  Created by Ram Kumar on 05/04/16.
//  Copyright © 2016 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CasesVC;
@class PatrawinCase;
@interface CustomerIDVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,UIActionSheetDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
{
    NSMutableDictionary *responseDict;
}
@property(strong, nonatomic) UITableView *caseTableView;
@property (nonatomic,strong) NSMutableArray *customerID;

@property(strong, nonatomic) NSMutableArray *casesArray;
@property(nonatomic,strong) NSMutableArray *arrRecommendations;
@property(nonatomic,strong) UIActionSheet *actionSheetSort;
@property(nonatomic,strong) UITextField *textSortingCompany;
@property(nonatomic,strong) UIView *viewForAgentsListBG, *viewForAgentsList;
@property(strong,nonatomic) UITableView *tbleUsers;

@property(nonatomic,strong) NSString *statsScreenType;
@property(nonatomic,strong) NSArray *statisticCases;

@end
