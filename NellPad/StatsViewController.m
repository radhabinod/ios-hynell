//
//  StatsViewController.m
//  NellPad
//
//  Created by Rakesh Kumar on 05/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "StatsViewController.h"
#import "StatsLisingController.h"
#import "PatrawinCase.h"
#import <objc/message.h>
#import "StasList.h"
#import "DataSyncBO.h"
#import "PatrawinCasesVC.h"
#import "LinkViewController.h"

@interface StatsViewController ()
{
    int mixper;
    DataSyncBO *dataBO;
}

@end

@implementation StatsViewController
CGFloat const CPDBarWidth = 0.25f;
CGFloat const CPDBarInitialX = 0.5f;

@synthesize hostView,barPlot,responseDict,arrData,scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title=@"Statistics";
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
        
        }
    }
    self.arrData=[[NSMutableArray alloc]init];
    
    self.view.backgroundColor=[UIColor whiteColor];
    [self fetchFromDatabase];

    //[self CallForStaticsData];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];


}

- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];

    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}

-(void) DataReload
{
    [self fetchFromDatabase];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)CallForStaticsInnerDatawithSelectedString:(NSString *)strSelectedTag

{
    if ([strSelectedTag isEqualToString:@"1"])
    {
             PatrawinCasesVC* casesTable = [[PatrawinCasesVC alloc] init];
             [self.navigationController pushViewController:casesTable animated:YES];
    }
    else{
    
    StatsLisingController *objStatsListngContrllr=[[StatsLisingController alloc]init];
    objStatsListngContrllr.strSelectedTag=strSelectedTag;
    objStatsListngContrllr.statsString = @"Statistics";
   //[objStatsListngContrllr refreshTableStats];
    [self.navigationController pushViewController:objStatsListngContrllr animated:YES];
        
    }
}

-(void)CallForStaticsData
{
    [self.arrData removeAllObjects];

    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
    inputPram.methodType = WebserviceMethodTypePost;
    inputPram.relativeWebservicePath = @"statisticsOnBasesOfAccesstoken";
    
    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
    
    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
    
    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
        
        if (error)
        {
            if ([error isErrorOfKindNoNetwork])
                [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
            return ;
        }
        
        error = nil;
        self.responseDict=nil;
        self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
        [self performSelectorOnMainThread:@selector(parseCases:) withObject:self.responseDict waitUntilDone:YES];
    }];
  
}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

-(void)parseCases:(NSDictionary *)dictTemp
{
    NSArray *arrTemp=[dictTemp objectForKey:@"data"];
    //NSArray *arrTemp=responseDict[@"data"];

    [self.arrData addObjectsFromArray:arrTemp];
    if (self.arrData.count>0)
    {
        [self initPlot];
    }
}

#pragma mark - Chart behavior

-(void)initPlot
{
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,deviceWidth,deviceHeight)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=[UIColor clearColor];
    self.scrollView.scrollEnabled=YES;
    self.scrollView.userInteractionEnabled=YES;
    [self.view addSubview:self.scrollView];

    self.hostView=[[CPTGraphHostingView alloc]initWithFrame:CGRectMake(IS_IPAD?40:15,IS_IPAD?60:20, deviceWidth-30,IS_IPAD?deviceHeight:deviceHeight/2)];
  //  self.hostView=[[CPTGraphHostingView alloc]initWithFrame:CGRectMake(40,IS_IPAD?60:40, deviceWidth-80,deviceHeight)];
    self.hostView.backgroundColor=[UIColor redColor];
    self.hostView.userInteractionEnabled=YES;
    [self.scrollView addSubview:self.hostView];
    self.hostView.allowPinchScaling = NO;
    
    int value=[self CallForGetMaximumValueOfBar:self.arrData];
    
    [self configureGraphwithMaxYValue:value];
    [self configurePlots];
    [self configureAxeswithMaxYValue:value];
    
    int Xpos=0;
    int Ypos=0;
    
    Ypos+=self.hostView.frame.origin.y+self.hostView.frame.size.height+5;
    
    
    for (int i=0; i<self.arrData.count; i++)
    {
        Xpos=IS_IPAD?164: 40;
        
        UIImageView *imgBg=[[UIImageView alloc]initWithFrame:CGRectMake(IS_IPAD?164:33, Ypos,IS_IPAD?440:254, 40)];
        // grey-bg.png
        imgBg.image=[UIImage imageNamed:@""];
        [imgBg setBackgroundColor:[UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:0.7f]];
        [self.scrollView addSubview:imgBg];
        [self.view sendSubviewToBack:imgBg];
//        Ypos+=45;
//        
//        UIImageView *imgBg1=[[UIImageView alloc]initWithFrame:CGRectMake(IS_IPAD?164:33, Ypos,IS_IPAD?440:254, 40)];
//        // grey-bg.png
//        imgBg1.image=[UIImage imageNamed:@""];
//        [imgBg1 setBackgroundColor:[UIColor grayColor]];
//        [self.scrollView addSubview:imgBg1];
//        
//        Ypos+=45;
//        
//        UIImageView *imgBg2=[[UIImageView alloc]initWithFrame:CGRectMake(IS_IPAD?164:33, Ypos,IS_IPAD?440:254, 40)];
//        // grey-bg.png
//        imgBg2.image=[UIImage imageNamed:@""];
//        [imgBg2 setBackgroundColor:[UIColor grayColor]];
//        [self.scrollView addSubview:imgBg2];
//        Ypos+=45;
//        
//        UIImageView *imgBg3=[[UIImageView alloc]initWithFrame:CGRectMake(IS_IPAD?164:33, Ypos,IS_IPAD?440:254, 40)];
//        // grey-bg.png
//        imgBg3.image=[UIImage imageNamed:@""];
//        [imgBg3 setBackgroundColor:[UIColor grayColor]];
//        [self.scrollView addSubview:imgBg3];

        
        
        
        StasList *dictTemp=[self.arrData objectAtIndex:i];
        
//        NSArray *arr=[dictTemp allKeys];
//        NSString *str=[arr objectAtIndex:0];
        NSString *strValue=dictTemp.number;//[dictTemp objectForKey:str] ;
        
        UILabel *lbl_NameHD = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos,IS_IPAD?220: 127, 45)];
        lbl_NameHD.backgroundColor = [UIColor clearColor];
        lbl_NameHD.text=[NSString stringWithFormat:@"%@:",dictTemp.name];
        lbl_NameHD.font = FONT_NORMAL(18.0);
        lbl_NameHD.textAlignment=NSTextAlignmentCenter;
        
        
        UILabel *lbl_NameValue;
        if (i==0) {
            lbl_NameValue = [[UILabel alloc] initWithFrame:CGRectMake(IS_IPAD?Xpos+223: Xpos+110, Ypos,IS_IPAD?218: 125, 45)];
            

        }
        else
        {
    lbl_NameValue = [[UILabel alloc] initWithFrame:CGRectMake(IS_IPAD?Xpos+223: Xpos+80, Ypos,IS_IPAD?218: 125, 45)];
        }
        
        
        lbl_NameValue.backgroundColor = [UIColor clearColor];
        lbl_NameValue.text=strValue;
        lbl_NameValue.font = FONT_NORMAL(16.0);
        lbl_NameValue.textAlignment=NSTextAlignmentCenter;
        
        
        UIButton *img = [[UIButton alloc] initWithFrame:CGRectMake(lbl_NameValue.frame.origin.x+lbl_NameValue.frame.size.width+10, Ypos+12,IS_IPAD?40: 20,IS_IPAD?40: 20)];
        [img addTarget:self action:@selector(btnInfoClk) forControlEvents:UIControlEventTouchUpInside];
        
        if (i==1) {
       //     img.image=[UIImage imageNamed:@"infoBlue"];
            [img setBackgroundImage:[UIImage imageNamed:@"infoBlue"] forState:UIControlStateNormal];
        }
        else if(i==2)
        {
            [img setBackgroundImage:[UIImage imageNamed:@"infoRed"] forState:UIControlStateNormal];
            
        }
        else if (i==3)
        {
            [img setBackgroundImage:[UIImage imageNamed:@"infoYellow"] forState:UIControlStateNormal];
            
        }
        
        Xpos=IS_IPAD?164: 33;

        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(Xpos, Ypos, IS_IPAD?440:254, 45)];
        [btn setBackgroundColor:[UIColor clearColor]];
        btn.tag=i;
        btn.titleLabel.font=[UIFont boldSystemFontOfSize:12.0f];
        [btn addTarget:self action:@selector(btnGetStatsDataClk:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.scrollView addSubview:btn];
        [lbl_NameHD setTextAlignment:NSTextAlignmentLeft];
        [lbl_NameValue setTextAlignment:NSTextAlignmentRight];
        
        Ypos+=45;
        [self.scrollView addSubview:lbl_NameHD];
        [self.scrollView addSubview:lbl_NameValue];
        [self.scrollView addSubview:img];

        if (i==0)
        {
            lbl_NameHD.textColor=[UIColor grayColor];
            lbl_NameValue.textColor=[UIColor grayColor];
        }
        else if (i==1)
        {
            lbl_NameHD.textColor=[UIColor grayColor];
            lbl_NameValue.textColor=[UIColor grayColor];
        }

        else if (i==2)
        {
            lbl_NameHD.textColor=[UIColor grayColor];
            lbl_NameValue.textColor=[UIColor grayColor];
        }

        else if (i==3)
        {
            lbl_NameHD.textColor=[UIColor grayColor];
            lbl_NameValue.textColor=[UIColor grayColor];
        }
        else
        {
            lbl_NameHD.textColor=[UIColor grayColor];
            lbl_NameValue.textColor=[UIColor grayColor];
        }

        btn=nil;
        lbl_NameHD=nil;
        lbl_NameValue=nil;
    }
    
    [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+80)];
    
}
-(void)configureGraphwithMaxYValue:(int)MaxmValue
{
    
	CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
	graph.plotAreaFrame.masksToBorder = NO;
    graph.plotAreaFrame.borderLineStyle=nil;
	self.hostView.hostedGraph = graph;
    
    
	[graph applyTheme:[CPTTheme themeNamed:kCPTPlainWhiteTheme ]];
	graph.paddingBottom = 30.0f;
	graph.paddingLeft  = 35.0f;
	graph.paddingTop    = -1.0f;
	graph.paddingRight  = -5.0f;
    

	CGFloat xMin = 0.0f;
	CGFloat xMax = [self.arrData count];
	CGFloat yMin = 0.0f;
	CGFloat yMax =[ [NSNumber numberWithInt:MaxmValue] floatValue];  // should determine dynamically based on max price
    int myYMax = [[NSNumber numberWithInt:MaxmValue] intValue];
    mixper = myYMax / 20;
    while ((myYMax%10) != 0) {
        myYMax = myYMax+1;
    }
    
    yMax = [[NSNumber numberWithInt:myYMax] floatValue];
    
	CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
	plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xMin) length:CPTDecimalFromFloat(xMax)];
	plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin) length:CPTDecimalFromFloat(yMax)];
    
}

-(void)configurePlots
{
	self.barPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
	self.barPlot.identifier = CPDTickerSymbolMSFT;
    self.barPlot.barWidthScale=3.8;
    
	CPTMutableLineStyle *barLineStyle = [[CPTMutableLineStyle alloc] init];
	barLineStyle.lineColor = [CPTColor lightGrayColor];
	barLineStyle.lineWidth = 0.1;
    
	CPTGraph *graph = self.hostView.hostedGraph;
    graph.plotAreaFrame.borderLineStyle=nil;

	CGFloat barX = CPDBarInitialX;
	NSArray *plots = [NSArray arrayWithObjects:self.barPlot,nil];
	for (CPTBarPlot *plot in plots)
    {
		plot.dataSource = self;
		plot.delegate = self;
		plot.barWidth = CPTDecimalFromDouble(CPDBarWidth);
		plot.barOffset = CPTDecimalFromDouble(barX);
		plot.lineStyle = barLineStyle;
		[graph addPlot:plot toPlotSpace:graph.defaultPlotSpace];
		barX += CPDBarWidth;
	}
    
    
    for (int i=0;i<self.arrData.count;i++)
    {
        CPTBarPlot *plot=[plots objectAtIndex:0];
        CPTPlotSpaceAnnotation *priceAnnotation;
        
        static CPTMutableTextStyle *style = nil;
        if (!style)
        {
            style = [CPTMutableTextStyle textStyle];
            style.color= [CPTColor blackColor];
        
            style.fontSize = 0.0f;
      
            style.fontName = @"Helvetica-Bold";
        }
        
        NSNumber *price = [self numberForPlot:plot field:CPTBarPlotFieldBarTip recordIndex:i];
        
        if (!priceAnnotation)
        {
            NSNumber *x = [NSNumber numberWithInt:0];
            NSNumber *y = [NSNumber numberWithInt:0];
            NSArray *anchorPoint = [NSArray arrayWithObjects:x, y, nil];
            priceAnnotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:plot.plotSpace anchorPlotPoint:anchorPoint];
        }
        
       
        CPTTextLayer *textLayer = [[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%@",price] style:style];
        priceAnnotation.contentLayer = textLayer;
        
        
        NSInteger plotIndex = 0;
        
        if ([plot.identifier isEqual:CPDTickerSymbolMSFT] == YES)
        {
            plotIndex = 0;
        }
        
        
        CGFloat x = i + CPDBarInitialX + (plotIndex * CPDBarWidth);
        NSNumber *anchorX = [NSNumber numberWithFloat:x];
        CGFloat y = ([price floatValue]+ mixper);
        NSNumber *anchorY = [NSNumber numberWithFloat:y];
        priceAnnotation.anchorPlotPoint = [NSArray arrayWithObjects:anchorX, anchorY, nil];
        [plot.graph.plotAreaFrame.plotArea addAnnotation:priceAnnotation];
        
        plot=nil;
        priceAnnotation=nil;
        
    }
}



-(void)btnInfoClk
{
    
    LinkViewController *objLinkVC=[[LinkViewController alloc]init];
    objLinkVC.strUrl=@"http://app.hynell.se/casehtml/";
    [self.navigationController pushViewController:objLinkVC animated:YES];
   // objLinkVC.isDeadline=YES;
    
    
}

-(void)configureAxeswithMaxYValue:(int)MaxmValue
{
	// 1 - Configure styles
	CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
	axisTitleStyle.color = [CPTColor blueColor];
	axisTitleStyle.fontName = @"Helvetica-Bold";
	axisTitleStyle.fontSize = 12.0f;
	CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
	axisLineStyle.lineWidth = 2.0f;
	axisLineStyle.lineColor = [[CPTColor blackColor] colorWithAlphaComponent:0.5];
    
	// 2 - Get the graph's axis set
    
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    
	// 3 - Configure the x-axis
    
	axisSet.xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    axisSet.xAxis.labelingPolicy=NO;
	axisSet.xAxis.titleTextStyle = axisTitleStyle;
	axisSet.xAxis.titleOffset = 10.0f;
	axisSet.xAxis.axisLineStyle = axisLineStyle;
    
	// 4 - Configure the y-axis
    
	axisSet.yAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    axisSet.yAxis.labelingPolicy=NO;
	axisSet.yAxis.titleTextStyle = axisTitleStyle;
	axisSet.yAxis.titleOffset = -15.0f;
	axisSet.yAxis.axisLineStyle = axisLineStyle;
    
    NSMutableArray *customTickLocations= [[NSMutableArray alloc]init];
    NSMutableArray *xAxisLabels = [[NSMutableArray alloc]init];
    
    for (int i=0; i<self.arrData.count; i++)
    {
        StasList *dictTemp=[self.arrData objectAtIndex:i];
//        NSArray *arrTemp=[dict allKeys];
//        NSString *str=[arrTemp objectAtIndex:0];
        [customTickLocations addObject:[NSDecimalNumber numberWithInt:i]];

        [xAxisLabels addObject:dictTemp.name];
    }
    NSUInteger labelLocation = 0;
    NSMutableArray *customLabels = [NSMutableArray arrayWithCapacity:[xAxisLabels count]];
    
    for (NSNumber *tickLocation in customTickLocations)
    {
        
      //  axisSet.xAxis.labelTextStyle.color=[CPTColor colorWithComponentRed:103.0f/255.0f green:173.0f/255.0f blue:91.0f/255.0f alpha:1.0f];
        CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
        [textStyle setFontSize:12.0f];
        [textStyle setColor:[CPTColor colorWithCGColor:[[self getColorWithIndex:[tickLocation intValue]] CGColor]]];
        
        
        CPTTextLayer *textLayer = [[CPTTextLayer alloc] initWithText:[xAxisLabels objectAtIndex:labelLocation++]  style:textStyle];
        
   //     CGSize textSize = [textLayer sizeThatFits];
        
        // Calculate the padding needed to center the label under the plot record.
        textLayer.paddingLeft = (deviceWidth-30)/4;
        
        
        
        
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithContentLayer:textLayer];
        [newLabel setAlignment:CPTAlignmentMiddle];
        newLabel.tickLocation = [tickLocation decimalValue];
       // newLabel.offset = axisSet.xAxis.labelOffset + axisSet.xAxis.majorTickLength;
//
        
        
        
        

        NSLog(@"%f>>>>>>>%f",axisSet.xAxis.labelOffset,axisSet.xAxis.labelOffset);
        //newLabel.tickLocation = CPTDecimalFromInt([tickLocation intValue]);

        newLabel.offset = 5;
      //  newLabel.alignment=CPTAlignmentCenter;
        //  newLabel.rotation = M_PI/4;
        [customLabels addObject:newLabel];
        newLabel=nil;
    }
    
    axisSet.xAxis.axisLabels =  [NSSet setWithArray:customLabels];
    
    NSMutableArray *customTickLocations1=[[NSMutableArray alloc]init];
    NSMutableArray *yAxisLabels=[[NSMutableArray alloc]init];
    
    int CurrentValue=0;
    
    for (int i=0; i<11; i++)
    {
        int ValueToPlace=CurrentValue*(i);
        [customTickLocations1 addObject:[NSDecimalNumber numberWithInt:i]];
        [yAxisLabels addObject:[NSString stringWithFormat:@"%d ",ValueToPlace]];
        float tempValue=[[NSString stringWithFormat:@"%d",MaxmValue] floatValue];
        CurrentValue= ceilf(tempValue/10);

    }

    NSUInteger labelLocationy = 0;
    NSMutableArray *customLabelsy = [NSMutableArray arrayWithCapacity:[yAxisLabels count]];
    NSNumber *number=[NSNumber numberWithInt:0];
    int value=[number intValue];

    for (NSNumber *tickLocation in customTickLocations1)
    {
        NSLog(@"%@", tickLocation);
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText: [yAxisLabels objectAtIndex:labelLocationy++] textStyle:axisSet.yAxis.labelTextStyle];
        number=[NSNumber numberWithInt:value];
        newLabel.tickLocation = [number decimalValue] ;
        newLabel.offset = axisSet.yAxis.labelOffset + axisSet.yAxis.majorTickLength;
      //  newLabel.offset = 0.0f;
        
        [customLabelsy addObject:newLabel];
        float tempValue=[[NSString stringWithFormat:@"%d",MaxmValue] floatValue];
        value=value+ceilf(tempValue/10);
        newLabel=nil;
    }
    axisSet.yAxis.axisLabels =  [NSSet setWithArray:customLabelsy];
}


-(int)CallForGetMaximumValueOfBar:(NSArray *)arrTemp
{
    int Max=0;
    
    NSMutableArray *arrValues=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrTemp.count-1; i++)
    {
        StasList *dictTemp=[self.arrData objectAtIndex:i];
        
//        NSArray *arr=[dictTemp allKeys];
//        NSString *str=[arr objectAtIndex:0];
//        NSString *strValue=[dictTemp objectForKey:str];
        [arrValues addObject:dictTemp.number];
    }
    for (NSString * strMaxi in arrValues)
    {
        int currentValue=[strMaxi intValue];
        if (currentValue > Max) {
            Max=currentValue;
        }
    }

    return Max;
}
-(void)btnGetStatsDataClk:(UIButton *)sender
{
    StasList *dictTemp=[self.arrData objectAtIndex:sender.tag];
    
//    NSArray *arr=[dictTemp allKeys];
    NSString *str=dictTemp.name;//[arr objectAtIndex:0];
    
    NSString *strSendValue;
    if ([str isEqualToString:@"Total Cases"])
    {
      strSendValue=@"1";
    }
    else if ([str isEqualToString:@"Granted"])
    {
        strSendValue=@"2";
    }
    else if ([str isEqualToString:@"Deadline"])
    {
        strSendValue=@"3";
    }
    
    else if ([str isEqualToString:@"Pending"])
    {
        strSendValue=@"4";
    }
    
    NSString *strValues=dictTemp.number;//[dictTemp objectForKey:str];
    if ([strValues isEqualToString:@"0"])
    {
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"No cases"];
    }
    else
    {
        [self CallForStaticsInnerDatawithSelectedString:strSendValue];
    }


}

#pragma mark - CPTPlotDataSource methods

-(NSUInteger) numberOfRecordsForPlot:(CPTPlot *) plot
{
	return self.arrData.count;
}

-(NSNumber *) numberForPlot:(CPTPlot *) plot field:(NSUInteger) fieldEnum recordIndex:(NSUInteger) index
{
	if ((fieldEnum == CPTBarPlotFieldBarTip) && (index < [self.arrData count]))
    {
        StasList *dictTemp=[self.arrData objectAtIndex:index];
        NSNumber *number=[NSNumber numberWithInt:dictTemp.number.intValue];
//        NSArray *arr=[dictTemp allKeys];
//        NSString *str=[arr objectAtIndex:0];
        return number;
    }
    
	return [NSDecimalNumber numberWithUnsignedInteger:index];
}
-(CPTFill *)barFillForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)index
{
    CPTColor *areaColor = nil;
    
    switch (index)
    {
        case 0:
            areaColor = [CPTColor colorWithComponentRed:103.0f/255.0f green:173.0f/255.0f blue:91.0f/255.0f alpha:1.0f];
            break;
            
        case 1:
            areaColor = [CPTColor colorWithComponentRed:73.0f/255.0f green:148.0f/255.0f blue:236.0f/255.0f alpha:1.0f];
            break;
            
        case 2:
            areaColor = [CPTColor colorWithComponentRed:226.0f/255.0f green:82.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
            break;
            
        case 3:
            areaColor = [CPTColor colorWithComponentRed:243.0f/255.0f green:195.0f/255.0f blue:68.0f/255.0f alpha:1.0f];
            break;
            
        default:
            areaColor = [CPTColor purpleColor];
            break;
    }
    
    CPTFill *barFill = [CPTFill fillWithColor:areaColor];
    
    return barFill;
}





-(UIColor*)getColorWithIndex : (int)number
{
    UIColor *areaColor;
    switch (number)
    {
        case 0:
            areaColor = [UIColor colorWithRed:103.0f/255.0f green:173.0f/255.0f blue:91.0f/255.0f alpha:1.0f];
            break;
            
        case 1:
            areaColor = [UIColor colorWithRed:73.0f/255.0f green:148.0f/255.0f blue:236.0f/255.0f alpha:1.0f];
            break;
            
        case 2:
            areaColor = [UIColor colorWithRed:226.0f/255.0f green:82.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
            break;
            
        case 3:
            areaColor = [UIColor colorWithRed:243.0f/255.0f green:195.0f/255.0f blue:68.0f/255.0f alpha:1.0f];
            break;
            
        default:
            areaColor = [UIColor purpleColor];
            break;
    }
    
    
    return areaColor;
}



#pragma mark - CPTBarPlotDelegate methods

-(void)barPlot:(CPTBarPlot *)plot barWasSelectedAtRecordIndex:(NSUInteger)index
{
    StasList *dictTemp=[self.arrData objectAtIndex:index];
    
//    NSArray *arr=[dictTemp allKeys];
    NSString *str=dictTemp.name;//[arr objectAtIndex:0];
    
    NSString *strSendValue;
    if ([str isEqualToString:@"Total Cases"])
    {
        strSendValue=@"1";
    }
    else if ([str isEqualToString:@"Granted"])
    {
        strSendValue=@"2";
    }
    else if ([str isEqualToString:@"Deadline"])
    {
        strSendValue=@"3";
    }
    
    else if ([str isEqualToString:@"Pending"])
    {
        strSendValue=@"4";
    }
    
    
    NSString *strValues=dictTemp.number;//[dictTemp objectForKey:str];
    if ([strValues isEqualToString:@"0"])
    {
     [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"No cases"];
    }
    else
    {
        [self CallForStaticsInnerDatawithSelectedString:strSendValue];

    }
}

#pragma mark -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchFromDatabase
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"StasList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    
    responseDict =[NSMutableDictionary new];
    [responseDict setValue:fetchedArray forKey:@"data"];
    //NSLog(@"The array is %i", fetchedArray1.count);
    
    if (fetchedArray.count == 0)
    {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"GraphListdownloaded" object:nil];
        dataBO=[DataSyncBO new];
        [dataBO CallForStaticsData];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(parseCases:) withObject:self.responseDict waitUntilDone:YES];
    }
   
}

@end
