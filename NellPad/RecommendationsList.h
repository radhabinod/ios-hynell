//
//  RecommendationsList.h
//  NellPat
//
//  Created by Macbook on 6/12/15.
//  Copyright (c) 2015 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RecommendationsList : NSManagedObject

@property (nonatomic, retain) NSString * recommendation_ID;
@property (nonatomic, retain) NSString * recommendation_Text;
@property (nonatomic, retain) NSString * recommendation_Status;

@end
