//
//  RecommendationsListBO.h
//  NellPad
//
//  Created by Rakesh Kumar on 15/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecommendationsListBO : NSObject

@property(nonatomic,strong) NSString *strRecommendationId;
@property(nonatomic,strong) NSString *strRecommendationText;
@property(nonatomic,strong) NSString *strRecommendationStatus;


@end
