//
//  CalendarVC.h
//  NellPad
//
//  Created by Herry Makker on 12/5/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebReferenceVC : UIViewController<UIWebViewDelegate>


@property (nonatomic,retain)  UIWebView *webView;
@property (nonatomic,retain)  NSURL *url;


@end
