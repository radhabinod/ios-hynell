//
//  Agent.h
//  NellPad
//
//  Created by Atul Thakur on 3/27/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Agent : NSObject


@property(strong, nonatomic) NSString *agent_id;
@property(strong, nonatomic) NSString *agent_name;

@end
