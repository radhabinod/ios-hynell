
//
//  ComposeSheetVC.m
//  NellPad
//
//  Created by Sanjeev Kumar on 12/20/13.
//  Copyright (c) 2013 Gagan Joshi. All rights reserved.
//

#import "ComposeSheetVC.h"
#import "Agent.h"
#import "Customer.h"
#import "InboxVC.h"
#import "ApplicationConstants.h"

#import <objc/message.h>
@interface ComposeSheetVC ()
{
    UILabel* lblTo;
    UILabel* LblSubject;
    UILabel *lblMessage;
}
@end

@implementation ComposeSheetVC

@synthesize viewForReply,tbleUsers,arrUsersList,subject,user_Type,relation_id,txtVMessageView,txtVSendTo,txtVSubjectField,btndropdown,strScreenType,strSubject,scrollView,viewForAgentsList,viewForAgentsListBG,receiverEmailStr,receiverIDStr,receiverNameStr;
- (id)initWithNibName:(NSString *)nibNameOrNil budropDownTableViewndle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Compose";
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
            
            
        }
    }
    
    [self.navigationController.navigationBar setTranslucent:NO];
    self.view.backgroundColor = UIColorFromRGB(0xF0F0F0);
    
   // [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont systemFontOfSize: 18], NSFontAttributeName, nil]];
    
    UIBarButtonItem *sendButton =  [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"send-compose"]  style:UIBarButtonItemStylePlain target:self action:@selector(sendBtnClk)];
    
    self.navigationItem.rightBarButtonItem = sendButton;
    
    self.arrUsersList = [NSMutableArray array];
    
    [self DesignInterface];
    [self CallForGetListOfUsers];
    
    UITapGestureRecognizer *gst=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    gst.cancelsTouchesInView=NO;
    gst.delegate=self;
    [self.view addGestureRecognizer:gst];

    self.view.backgroundColor=[UIColor colorWithRed:245.0f/255.0f green:246.0f/255.0f blue:247.0f/255.0f alpha:1.0];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];

}

- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
//    [txtVMessageView resignFirstResponder];
//    [txtVSendTo resignFirstResponder];
//    [txtVSubjectField resignFirstResponder];
}

-(void)DesignInterface
{
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,deviceWidth,deviceHeight-64)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.delegate=self;
    self.scrollView.scrollEnabled=YES;
    self.scrollView.userInteractionEnabled=YES;
    self.scrollView.backgroundColor=[UIColor clearColor];
    [self.view addSubview: self.scrollView];

    lblTo = [[UILabel alloc] init];
    lblMessage = [[UILabel alloc] init];
    LblSubject = [[UILabel alloc] init];
    
    txtVSendTo = [[UITextView alloc] init];
    btndropdown = [[UIButton alloc] init];
    txtVSubjectField = [[UITextView alloc] init];
    txtVMessageView = [[UITextView alloc] init];
    
    
    lblTo.frame = CGRectMake(25, 25, 20, 40);
    lblTo.font = [UIFont systemFontOfSize:IS_IPAD ? 17 : 14];
    
    txtVSendTo.frame = CGRectMake(45, 28, self.view.frame.size.width-80, 55);
    txtVSendTo.font = [UIFont systemFontOfSize:IS_IPAD ? 17 : 14];
   // [txtVSendTo setTextAlignment:NSTextAlignmentCenter];
    
    btndropdown.frame = CGRectMake(40, 0, deviceWidth-40,82);
    
    LblSubject.frame = CGRectMake(25, 116, 60, 20);
    LblSubject.font = [UIFont systemFontOfSize:IS_IPAD ? 17 : 14];
    
    txtVSubjectField.frame = CGRectMake(10, txtVSendTo.frame.size.height+txtVSendTo.frame.origin.y+27, self.view.frame.size.width-20, 55);
    txtVSubjectField.font = [UIFont systemFontOfSize:IS_IPAD ? 17 : 14];
   // [txtVSubjectField setTextAlignment:NSTextAlignmentCenter];
    
    lblMessage.frame = CGRectMake(25, 184, deviceWidth-20, 20);
    lblMessage.font = [UIFont systemFontOfSize:IS_IPAD ? 17 : 14];
    
    txtVMessageView.frame = CGRectMake(10, txtVSubjectField.frame.size.height+txtVSubjectField.frame.origin.y+12, self.view.frame.size.width-20,IS_IPAD?(deviceHeight-328):(IS_IPHONE_5?(deviceHeight-240):(170)));
    txtVMessageView.font = [UIFont systemFontOfSize:IS_IPAD ? 17 : 14];
    
    
    if (IS_IPHONE)
    {
        txtVSubjectField.inputAccessoryView = viewDone;
        txtVMessageView.inputAccessoryView = viewDone;
    }
    
    
    lblTo.textColor = UIColorFromRGB(0x4174DA);
    lblTo.textColor=[UIColor darkGrayColor];
    lblTo.text = @"To";
    [self.scrollView addSubview:lblTo];
    
    txtVSendTo.userInteractionEnabled = YES;
    txtVSendTo.editable=NO;
    txtVSendTo.text =@"";
    txtVSendTo.delegate = self;
    txtVSendTo.backgroundColor=[UIColor clearColor];
    [self.scrollView addSubview:txtVSendTo];
    
    UIView *line=[[UIView alloc]init];
    line.frame=CGRectMake(0, txtVSendTo.frame.size.height+txtVSendTo.frame.origin.y+1
                          ,self.view.frame.size.width, 0.5);
    line.backgroundColor=[UIColor lightGrayColor];
    [self.scrollView addSubview:line];

//    [btndropdown addTarget:self action:@selector(dropDown) forControlEvents:UIControlEventTouchUpInside];
//    btndropdown.userInteractionEnabled=YES;
//    btndropdown.backgroundColor=[UIColor yellowColor];
//    [self.scrollView addSubview:btndropdown];

    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(deviceWidth-30, 38, [UIImage imageNamed:@"inboxArrowSmall.png"].size.width, [UIImage imageNamed:@"inboxArrowSmall.png"].size.height)];
    
    [image setImage:[UIImage imageNamed:@"inboxArrowSmall.png"]];
    [self.view addSubview:image];
    
    LblSubject.textColor = UIColorFromRGB(0x4174DA);
    LblSubject.textColor=[UIColor  darkGrayColor];
   // LblSubject.backgroundColor=[UIColor whiteColor];
    LblSubject.text = @"Subject";
    [self.scrollView addSubview:LblSubject];
    
    txtVSubjectField.userInteractionEnabled = YES;
    txtVSubjectField.text = @"";
    txtVSubjectField.delegate = self;
     txtVSubjectField.tag=1999;
    txtVSubjectField.backgroundColor=[UIColor clearColor];
    [self.scrollView addSubview:txtVSubjectField];
    
    UIView *line2=[[UIView alloc]init];
    line2.frame=CGRectMake(0, txtVSubjectField.frame.size.height+txtVSubjectField.frame.origin.y+1
                          ,self.view.frame.size.width, 0.5);
    line2.backgroundColor=[UIColor lightGrayColor];
    [self.scrollView addSubview:line2];
    
    txtVMessageView.userInteractionEnabled = YES;
    txtVMessageView.delegate = self;
    txtVMessageView.scrollEnabled = YES;
    txtVMessageView.tag=2000;
    txtVMessageView.backgroundColor=[UIColor clearColor ];
    [self.scrollView addSubview:txtVMessageView];
    
    lblMessage.textColor = UIColorFromRGB(0x4174DA);
    lblMessage.textColor=[UIColor darkGrayColor];
   //lblMessage.backgroundColor = [UIColor whiteColor];
    lblMessage.text = @"Message";
    [self.scrollView addSubview:lblMessage];
    
   
    if ([self.strScreenType isEqualToString:@"Message"])
    {
        txtVSubjectField.text = @"";
        txtVSendTo.text =[NSString stringWithFormat:@"%@ [%@]", receiverNameStr,receiverEmailStr];
        self.agent_id = receiverIDStr;
        btndropdown = nil;
        [self ShowViewForAgentsList:NO];
        txtVSubjectField.userInteractionEnabled=YES;
    }
    else if([self.strScreenType isEqualToString:@"Cases"])
    {
        txtVSubjectField.text=strSubject;
        txtVSendTo.text = @"";
        txtVSubjectField.userInteractionEnabled=NO;
        LblSubject.text=@"";
    }
    else if([self.strScreenType isEqualToString:@"Inbox"])
    {
        
        txtVSubjectField.text = @"";
        txtVSendTo.text = @"";
        txtVSubjectField.userInteractionEnabled=YES;

    }
    
    [btndropdown addTarget:self action:@selector(dropDown) forControlEvents:UIControlEventTouchUpInside];
    btndropdown.userInteractionEnabled=YES;
//    btndropdown.backgroundColor=[UIColor yellowColor];
    [self.scrollView addSubview:btndropdown];

}


- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

-(void)ShowViewForAgentsList:(BOOL)isShow
{
    if (self.viewForAgentsListBG)
    {
        [self.viewForAgentsListBG.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.viewForAgentsListBG removeFromSuperview];
        self.viewForAgentsListBG=nil;
    }
    if(isShow)
    {
        
        self.viewForAgentsListBG=[[UIView alloc]initWithFrame:CGRectMake(0, IS_VERSION_GRT_7 ? 20:0, deviceWidth,IS_VERSION_GRT_7?deviceHeight-20:deviceHeight)];
        self.viewForAgentsListBG.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
        [APP_DELEGATE.window addSubview:self.viewForAgentsListBG];
        
        UIView *blueBar=[[UIView alloc]init];
        blueBar.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
        blueBar.frame=CGRectMake(10, 49, self.view.frame.size.width-20, 43);
        
        UILabel *cases=[[UILabel alloc]init];
        cases.frame=CGRectMake(0, 0, blueBar.frame.size.width, 40);
        
        
        
        NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
        
        if([strKey isEqualToString:@"1"])
        {
        
            cases.text=@"Agents List";
            
        
        }
        else
        {
            
            cases.text=@"Customers List";
            
        }
        
        cases.textAlignment=NSTextAlignmentCenter;
        cases.textColor=[UIColor whiteColor];
        [blueBar addSubview:cases];
        
        [self.viewForAgentsListBG addSubview:blueBar];
        
        self.viewForAgentsList=[[UIView alloc]initWithFrame:CGRectMake(10,90, deviceWidth-20,CGRectGetHeight(self.view.bounds)-75-60)];
        self.viewForAgentsList.backgroundColor = [UIColor whiteColor] ;
        [self.viewForAgentsListBG addSubview:self.viewForAgentsList];
        
        tbleUsers=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, deviceWidth-20, IS_IPAD ? 870 : deviceHeight-75-80) style:UITableViewStylePlain];
        tbleUsers.delegate=self;
        tbleUsers.dataSource=self;
        
        tbleUsers.rowHeight = IS_IPAD ? 55 : 44;
        
        tbleUsers.backgroundColor=[UIColor clearColor];
        tbleUsers.backgroundView=nil;
        tbleUsers.separatorStyle=UITableViewCellSeparatorStyleNone;
        
        tbleUsers.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        [self.viewForAgentsList addSubview:tbleUsers];
        
        
        UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCross.frame = CGRectMake(2, 34, 24, 24);
        [btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
        [btnCross addTarget:self action:@selector(btnCross_Clicked) forControlEvents:UIControlEventTouchUpInside];
        [self.viewForAgentsListBG addSubview:btnCross];
        btnCross = nil;
    }
    
    
}

-(void)btnCross_Clicked
{
    [self ShowViewForAgentsList:NO];
}

#pragma mark - UITextView Delegate Methods

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

- (void) textViewDidBeginEditing:(UITextView *) textView
{
    if (textView==txtVMessageView)
    {
        [UIView animateWithDuration:0.3f animations:
         ^{
             if (IS_IPHONE_5)
             {
                 txtVMessageView.frame = CGRectMake(10, txtVSubjectField.frame.size.height+txtVSubjectField.frame.origin.y+12, self.view.frame.size.width-20, 328);
             }
             else if([UIScreen mainScreen].bounds.size.height==480.0)
             {
                 txtVMessageView.frame = CGRectMake(10, txtVSubjectField.frame.size.height+txtVSubjectField.frame.origin.y+12, self.view.frame.size.width-20, 328);
                 [self.scrollView setContentOffset:CGPointMake(0, 100) animated:YES];
             }
             else
             {
                 txtVMessageView.frame = CGRectMake(10, txtVSubjectField.frame.size.height+txtVSubjectField.frame.origin.y+12, self.view.frame.size.width-20,328);
             }
         }];

    }
    
    if(textView.tag==1999)
    {
     LblSubject.text =@"";
        
        if ([txtVMessageView.text isEqualToString:@""]) {
            
            lblMessage.text=@"Message";
        }

    }
    else if(textView.tag==2000)
    {
        
    
           lblMessage.text=@"";
        if ([txtVSubjectField.text isEqualToString:@""]) {
            
            LblSubject.text=@"Subject";
        }
    }
}

- (void) textViewDidEndEditing:(UITextView *) textView
{
    if ([[[self.navigationController viewControllers] lastObject] isEqual:self]) {
        if (textView==txtVMessageView)
        {
            if (IS_IPHONE_5)
            {
                txtVMessageView.frame = CGRectMake(10, txtVSubjectField.frame.size.height+txtVSubjectField.frame.origin.y+12, self.view.frame.size.width-20, 328);
            }
            else if([UIScreen mainScreen].bounds.size.height==480.0)
            {
                txtVMessageView.frame = CGRectMake(10, txtVSubjectField.frame.size.height+txtVSubjectField.frame.origin.y+12, self.view.frame.size.width-20, 328);
                [self.scrollView setContentOffset:CGPointMake(0, 100) animated:YES];
            }
            else
            {
                txtVMessageView.frame = CGRectMake(10, txtVSubjectField.frame.size.height+txtVSubjectField.frame.origin.y+12, self.view.frame.size.width-20,328);
            }

            
        }
    }
    if(textView.tag==1999)
    {

            if (txtVSubjectField.text) {
                LblSubject.text = @"";

            } if (!txtVSubjectField.text){

           LblSubject.text = @"Subject";
            }
    }
    else if(textView.tag==2000)
    {
        
                    if (txtVMessageView.text) {

                    lblMessage.text=@"";
                    }
        


        
        if (!txtVMessageView.text) {

        lblMessage.text=@"Message";
        }
        
    }
   
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (IBAction) buttonAccessoryView:(id) sender
{
    [txtVSubjectField resignFirstResponder];
    
    [txtVMessageView resignFirstResponder];
}

-(void)CallForGetListOfUsers
{

    WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
    inputParams.methodType = WebserviceMethodTypePost;
    NSString *strKey = [USER_DEFAULTS valueForKey:@"Customer"];
    if([strKey isEqualToString:@"1"])
    {
        inputParams.relativeWebservicePath = @"listofagents";
    }
    else if([strKey isEqualToString:@"0"])
    {
        inputParams.relativeWebservicePath = @"pwtestusersofagents";
    }
    [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
    [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
     {
         
         if (error) {
             if ([error isErrorOfKindNoNetwork])
                 [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
             return;
             
             return;
         } else {
             
             error = nil;
             responseDictDropDown = [NSJSONSerialization JSONObjectWithData:response
                                                                    options:NSJSONReadingMutableContainers
                                                                      error:&error];
             
             
           //  NSMutableArray *data  = responseDictDropDown[@"data"];
             
//             for (NSDictionary *dict in data) {
//                 emailString = dict[@"Email"];
//                 NSLog(@"email %@",emailString);
//
//             }
             // [self DesignInterface];
             
         //   NSLog(@"Message send list... %@",responseDictDropDown);
             
             [self performSelectorOnMainThread:@selector(ParseListOfUsers) withObject:self waitUntilDone:YES];
             
         }
     }];
   
}

-(void)ParseListOfUsers
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];

    NSMutableArray *arr = [responseDictDropDown objectForKey:@"data"];
    if([strKey isEqualToString:@"1"])
    {
        
        for (NSDictionary *dict in arr) {
            User *objTempuser = [[User alloc] init];
            objTempuser.user_id = GET_VALID_STRING(dict[@"agent_id"]);
            objTempuser.user_name = GET_VALID_STRING(dict[@"agent_name"]);
            objTempuser.user_Email = GET_VALID_STRING(dict [@"email"]);
            
            [self.arrUsersList addObject:objTempuser];
        }
    }
    else if([strKey isEqualToString:@"0"])
    {
        for (NSDictionary *dict in arr) {
            User *objTempuser = [[User alloc] init];
            objTempuser.user_id = GET_VALID_STRING(dict[@"User_Id"]);
            objTempuser.user_name = GET_VALID_STRING(dict[@"Username"]);
            objTempuser.user_Email = GET_VALID_STRING(dict [@"Email"]);
         //   NSLog(@"user email data :%@",objTempuser.user_Email);
            // self.agent_id = objTempuser.user_id;
            [self.arrUsersList addObject:objTempuser];
        }
        
    }
    
  

}

-(void)hideKeyboard
{
    [txtVMessageView resignFirstResponder];
    [txtVSubjectField resignFirstResponder];
}

- (void)dismissComposeSheet
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sendBtnClk
{
    [self sendMessage];
}

-(void)sendMessage
{
    [txtVMessageView resignFirstResponder];
    [txtVSendTo resignFirstResponder];
    [txtVSubjectField resignFirstResponder];
    
    if (self.agent_id.length==0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please select recipient." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else if([txtVSubjectField.text length]<=0)
    {
       [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please enter subject." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else if([txtVMessageView.text length]<=0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please enter message." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
 
    }
    else
    {
        
        NSString *str = txtVMessageView.text;
        WebserviceInputParameter *inputParams = [[WebserviceInputParameter alloc] init];
        inputParams.methodType = WebserviceMethodTypePost;
        inputParams.relativeWebservicePath = @"insertMessage";
        
        
        NSString *Str = [NSString stringWithUTF8String:[txtVSubjectField.text cStringUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"%@", Str);
        
        NSLog(@"%@", txtVSubjectField.text);
        
        NSString *Str1 = [NSString stringWithUTF8String:[str cStringUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"%@", Str1);
        
        NSLog(@"%@", str);
        
        
        [inputParams.dict_postParameters setObject:USR_DEFAULTS(@"token") forKey:@"accesstoken"];
        [inputParams.dict_postParameters setObject: self.agent_id forKey:@"agent_id"];
        [inputParams.dict_postParameters setObject:str forKey:@"textmessage"];
        [inputParams.dict_postParameters setObject:txtVSubjectField.text forKey:@"subject"];
        
        [WebserviceManager callWebserviceWithInputParame:inputParams completion:^(id response, NSError *error)
         {
             
             if (error) {
                 if ([error isErrorOfKindNoNetwork])
                     [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                 return;
                 
                 return;
             } else {
                 
                 error = nil;
                 responseDictSendMessage = [NSJSONSerialization JSONObjectWithData:response
                                                                           options:NSJSONReadingMutableContainers
                                                                             error:&error];
                 
                 [self performSelectorOnMainThread:@selector(isMessageSent) withObject:self waitUntilDone:YES];
                 
             }
         }];

    }
}

-(void)isMessageSent
{
    NSDictionary *dictData = [responseDictSendMessage objectForKey:@"data"];

    if ([[dictData objectForKey:@"log"] isEqualToString:@""])
    {
        
    }
    else
    {
        NSString *correctString = [NSString stringWithCString:[[dictData objectForKey:@"log"] cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
        [CommonMethods showAlertWithTitle:@"Hynell" andMessage:correctString];
        txtVSendTo.text = @"";
        txtVSubjectField.text = @"";
        txtVMessageView.text = @"";
        
        for (UIViewController *objControllr in self.navigationController.viewControllers)
        {
            if ([objControllr isKindOfClass:[InboxVC class]])
            {
                InboxVC *vwCont=(InboxVC *)objControllr;
                [self.navigationController popToViewController:vwCont animated:YES];
            }

        }

    }
//    else
//        [CommonMethods showAlertWithTitle:@"Error!!" andMessage:@"Some error occurred, please try later."];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - UITableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_IPAD ? 55 : 44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.arrUsersList.count==0)
    {
        return 1;
    }
    return [self.arrUsersList count];;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myCell"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    tbleUsers.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (self.arrUsersList.count==0)
    {
        UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?400: 190,IS_IPAD? 768: 320, 30)];
        [lblNoRecord setText:@"No Record Available."];
        [lblNoRecord setTextColor:[UIColor lightGrayColor]];
        lblNoRecord.textAlignment = NSTextAlignmentCenter;
        [lblNoRecord setBackgroundColor:[UIColor clearColor]];
        [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
        [cell.contentView addSubview:lblNoRecord];
        cell.backgroundColor=[UIColor clearColor];

    }
    else
    {
        User *objTempuser = self.arrUsersList[indexPath.row];
        NSString *strName = [NSString stringWithFormat:@"%@ [%@]",GET_VALID_STRING(objTempuser.user_name), GET_VALID_STRING(objTempuser.user_Email)];
        cell.textLabel.text = strName;
        cell.textLabel.textAlignment=NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont systemFontOfSize:IS_IPAD ? 25 : 15];
    }
    
    return cell;
}

- (void) dealloc
{
    txtVMessageView = nil;
    txtVSendTo = nil;
    txtVSubjectField = nil;
    
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tbleUsers)
    {
        if (self.arrUsersList.count==0)
        {
            
        }
        else
        {
            User *objTempuser = self.arrUsersList[indexPath.row];
            txtVSendTo.text = [NSString stringWithFormat:@"%@ [%@]",objTempuser.user_name,objTempuser.user_Email];
            self.agent_id = objTempuser.user_id;
            // lblTo.text=@"";
            [self ShowViewForAgentsList:NO];
        }
    }
}
//- (void)textViewDidBeginEditing:(UITextView *)textView
//{
//    if(textView.tag==2300){
//        if ([textView.text isEqualToString:@"Journal Title"])
//        {
//            textView.text = @"";
//            textView.textColor = [UIColor blackColor]; //optional
//        }
//    }
//    
//    else if(textView.tag==2200)
//    {
//        if ([textView.text isEqualToString:@"Enter text"])
//        {
//            textView.text = @"";
//            textView.textColor = [UIColor blackColor]; //optional
//        }
//    }
//    [textView becomeFirstResponder];
//}
//
//- (void)textViewDidEndEditing:(UITextView *)textView
//{
//    if(textView.tag==2300)
//    {
//        if ([textView.text isEqualToString:@""])
//        {
//            textView.text = @"Journal Title";
//            textView.textColor = [UIColor darkGrayColor]; //optional
//        }
//    }
//    else if(textView.tag==2200)
//    {
//        if ([textView.text isEqualToString:@""])
//        {
//            textView.text = @"Enter text";
//            textView.textColor = [UIColor darkGrayColor]; //optional
//        }
//    }
//    
//    
//    [textView resignFirstResponder];
//}

#pragma mark - ButtonActions

-(void)dropDown
{
    [self ShowViewForAgentsList:YES];
}



@end
