//
//  CustomDetailCell.m
//  Fieldo
//
//  Created by Herry Makker on 11/13/13.
//  Copyright (c) 2013 Herry Makker. All rights reserved.
//



#import "CustomDetailCell.h"

@implementation CustomDetailCell


- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
       // UIImageView *imageView=[[UIImageView alloc] initWithFrame:self.contentView.frame];
       // imageView.image=[UIImage imageNamed:@"label.png"];
       // [self.contentView addSubview:imageView];
        
        _labelKey = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 80, 60)];
        _labelKey.textAlignment = NSTextAlignmentCenter;
        _labelKey.numberOfLines = 0;
        _labelKey.font = [UIFont fontWithName:@"Arial" size:14];
        [self.contentView addSubview:_labelKey] ;
        
        _labelValue= [[UILabel alloc] initWithFrame:CGRectMake(90, 0, 230, 60)];
        _labelValue.textAlignment = NSTextAlignmentCenter;
        _labelValue.numberOfLines = 0;
        _labelValue.font = [UIFont fontWithName:@"Arial" size:14];
        [self.contentView addSubview:_labelValue];
    
    }
    return self;
}

-(void)setLabelKey:(UILabel *)labelKey
{
    if (labelKey !=_labelKey)
    {
        _labelKey = labelKey;
    }
    
}

-(void)setLabelValue:(UILabel *)labelValue
{
    if (labelValue !=_labelValue)
    {
        _labelValue = labelValue;
    }
    
}


@end