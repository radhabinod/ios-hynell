//
//  StatisticsCasesController.m
//  NellPad
//
//  Created by Rakesh Kumar on 06/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "StatisticsCasesController.h"
#import "TimeRegistrationVC.h"
#import "ComposeSheetVC.h"
#import "TimeRegistrationVC.h"
#import "LinkViewController.h"
#import "PatrawinCase.h"
#import "RecommendationsListBO.h"
#import <objc/message.h>
#import "DataSyncBO.h"
#import "StasDetailList.h"

#import "UIImageView+AFNetworking.h"


@interface StatisticsCasesController ()
{
    UIImageView *imageFigure;
    UITextView * txtViewComment;
    int buttonValue;
    int buttonSelected;

}
@end

@implementation StatisticsCasesController
@synthesize arrDataHdng,strSelectedIndex,arrayAllCases,lbl_CaseName,viewForCaseData,scrollView,responseDict,ViewForRecommendations,ViewForRecommendationsBG,strGlobalRecommendationId,arrRecommendationListBO;
NSString *TXTFileName;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Cases";
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
            
        }
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont systemFontOfSize: 16], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
   // [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    self.strGlobalRecommendationId=[[NSString alloc]init];

    [self configureView];
    [self designToolBar];
    
    UITapGestureRecognizer *gst=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hidekeyboard)];
    gst.delegate=self;
    gst.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:gst];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    buttonSelected = 0;

    [self showCustomViewForCase:YES];
}

-(void)hidekeyboard
{
    UITextView *txtf=(UITextView *)[self.scrollView viewWithTag:kTxtVStatsComment];
    [txtf resignFirstResponder];
  //  [self.scrollView setContentOffset:CGPointZero animated:YES];
}

-(void)configureView
{
    UIImageView *imgV=[[UIImageView alloc]initWithFrame:CGRectMake(0, 12, deviceWidth, 37)];
    imgV.image=[UIImage imageNamed:@"top-grey-bg.png"];
    imgV.userInteractionEnabled=YES;
    [self.view addSubview:imgV];
    
    UIButton *btnLeft=[[UIButton alloc]initWithFrame:CGRectMake(10, 15, 18, 30)];
    btnLeft.tag=kBtnStatsLeft;
    [btnLeft setBackgroundImage:[UIImage imageNamed:@"ArrowPrevback.png"] forState:UIControlStateNormal];
    [btnLeft setBackgroundColor:[UIColor clearColor]];
    [btnLeft addTarget:self action:@selector(btnLeftClk) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnLeft];
    
    lbl_CaseName = [[UILabel alloc] initWithFrame:CGRectMake(50, 15, deviceWidth-100, 30)];
    lbl_CaseName.backgroundColor = [UIColor clearColor];
    lbl_CaseName.font = IS_IPAD ? FONT_NORMAL(18) : FONT_NORMAL(14.0);
    lbl_CaseName.textAlignment=NSTextAlignmentCenter;
    lbl_CaseName.adjustsFontSizeToFitWidth=YES;
    lbl_CaseName.textColor=UIColorFromRGB(0x4174DA);
    [self.view addSubview:lbl_CaseName];
    
    
    UIButton *btnRight=[[UIButton alloc]initWithFrame:CGRectMake(deviceWidth-28, 15, 18, 30)];
    [btnRight setBackgroundImage:[UIImage imageNamed:@"arrowMessageinbox.png"] forState:UIControlStateNormal];
    [btnRight setBackgroundColor:[UIColor clearColor]];
    btnRight.tag=kBtnStatsRight;
    [btnRight addTarget:self action:@selector(btnRightclk) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnRight];
    
    
    btnLeft.enabled=[self enableDisableLeftButtons];
    
    btnRight.enabled=[self enableDisableRightButtons];
   
}
-(void)UpdateValuesForCases
{
    [arrDataHdng removeAllObjects];
    arrDataHdng=nil;
    
    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
    
    NSString *strTemp=[objTemp.case_id substringWithRange:NSMakeRange(1, 1)];
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ( [strKey isEqualToString:@"0"])
    {
        if([strTemp isEqualToString:@"2"])
        {
            arrDataHdng=[[NSMutableArray alloc]initWithObjects:@"Country:",@"Title:",@"Customer:", @"Figure:",@"Add Comment:", @"Deadline:",@"Recommendation:", nil];
        }
        else
        {
            arrDataHdng=[[NSMutableArray alloc]initWithObjects:@"Country:",@"Title:",@"Customer:", @"Status:",@"Add Comment:", @"Deadline:",@"Recommendation:", nil];
        }
        
        
    }
    else if ( [strKey isEqualToString:@"1"])
    {
        if([strTemp isEqualToString:@"2"])
        {
            arrDataHdng=[[NSMutableArray alloc]initWithObjects:@"Title:",@"Agent:",@"Figure:", @"Acc. Cost:",@"Comments:", @"Deadline:",@"Recommendation:", nil];
        }
        else
        {
            arrDataHdng=[[NSMutableArray alloc]initWithObjects:@"Title:",@"Agent:",@"Status:", @"Acc. Cost:",@"Comments:", @"Deadline:",@"Recommendation:", nil];
        }
        
        
    }
    
}


#pragma mark-

#pragma mark showCustomView

//-(void)showCustomViewForCase:(BOOL)isShow
//{
//    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
//    
//    [self UpdateValuesForCases];
//    
//    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
//    
//    if(self.viewForCaseData)
//    {
//        [self.viewForCaseData.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//        [self.viewForCaseData removeFromSuperview];
//        self.viewForCaseData=nil;
//    }
//    self.view.backgroundColor=[UIColor whiteColor];
//    
//    if ( [strKey isEqualToString:@"0"])
//    {
//        if (isShow)
//        {
//            self.viewForCaseData=[[UIView alloc]initWithFrame:CGRectMake(0, 50, deviceWidth, deviceHeight-177)];
//            self.viewForCaseData.backgroundColor=[UIColor clearColor];
//            [self.view addSubview:self.viewForCaseData];
//            
//            self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,deviceWidth,deviceHeight-177)];
//            self.scrollView.showsVerticalScrollIndicator=NO;
//            self.scrollView.delegate=self;
//            self.scrollView.scrollEnabled=YES;
//            self.scrollView.userInteractionEnabled=YES;
//            [self.viewForCaseData addSubview: self.scrollView];
//            
//            int Xpos=0;
//            int Ypos=0;
//            Ypos=20;
//            
//            for (int i=0; i<arrDataHdng.count; i++)
//            {
//                Xpos=24;
//                UILabel *lbl_Line = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, deviceWidth-8, 1)];
//                lbl_Line.backgroundColor = [UIColor lightGrayColor];
//                lbl_Line.textColor=[UIColor grayColor];
//                //   [self.scrollView addSubview:lbl_Line];
//                
//                
//                UILabel *lbl_Line1 = [[UILabel alloc] initWithFrame:CGRectMake(14, Ypos, self.view.frame.size.width-28,IS_IPAD?101: 45)];
//                lbl_Line1.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:0.8];
//                lbl_Line1.textColor=[UIColor lightGrayColor];
//                //lbl_Line1.alpha=0.8;
//                [self.scrollView addSubview:lbl_Line1];
//                
//                Ypos+=lbl_Line.frame.size.height;
//                Xpos+=1;
//                
//                UILabel *lbl_HD= [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, IS_IPAD?298: 110,IS_IPAD?100: 50)];
//                lbl_HD.numberOfLines=0;
//                
//                lbl_HD.backgroundColor = [UIColor clearColor];
//                lbl_HD.text=[arrDataHdng objectAtIndex:i];
//                lbl_HD.font = IS_IPAD ? FONT_BOLD(20) : FONT_BOLD(13.0);
//                lbl_HD.textAlignment=NSTextAlignmentLeft;
//                //    lbl_HD.textColor=UIColorFromRGB(0x4174DA);
//                lbl_HD.adjustsFontSizeToFitWidth=YES;
//                [self.scrollView addSubview:lbl_HD];
//                
//                Xpos=IS_IPAD?303: 79;
//                UILabel *lbl_Line2 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1, IS_IPAD?100: 50)];
//                lbl_Line2.backgroundColor = [UIColor lightGrayColor];
//                lbl_Line2.textColor=[UIColor grayColor];
//                ////    [self.scrollView addSubview:lbl_Line2];
//                
//                Xpos+=1;
//                
//                UILabel *lbl_Values = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+15, Ypos, IS_IPAD?449: 200,IS_IPAD?100: 43)];
//                lbl_Values.backgroundColor = [UIColor clearColor];
//                lbl_Values.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
//                lbl_Values.textAlignment=NSTextAlignmentRight;
//                lbl_Values.numberOfLines=0;
//                lbl_Values.tag=KlblTitle+i;
//                lbl_Values.textColor=[UIColor blackColor];
//                [self.scrollView addSubview:lbl_Values];
//    
//                if (i==3)
//                {
//                    UIButton *btn_figure=[[UIButton alloc]initWithFrame:CGRectMake(Xpos, Ypos,IS_IPAD?459: 235,IS_IPAD?100:50)];
//                    [btn_figure setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                    btn_figure.backgroundColor=[UIColor clearColor];
//                    btn_figure.titleLabel.font=IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
//                    btn_figure.tag=kBtnStatsFigure;
//                    [btn_figure addTarget:self action:@selector(btnFigureClk) forControlEvents:UIControlEventTouchUpInside];
//                    [self.scrollView addSubview:btn_figure];
//                    
//                    imageFigure = [[UIImageView alloc] initWithFrame:CGRectMake(175, 180, 50, 30)];
//                    
//                    [self.scrollView addSubview:imageFigure];
//                }
//                
//                if (i==4)
//                {
//                    UITextView *txtViewComment=[[UITextView alloc]initWithFrame:CGRectMake(Xpos, Ypos,IS_IPAD?459:235,IS_IPAD?100:50)];
//                    txtViewComment.delegate=self;
//                    txtViewComment.tag=kTxtVStatsComment;
//                    txtViewComment.returnKeyType=UIReturnKeyDefault;
//                    txtViewComment.keyboardType=UIKeyboardTypeDefault;
//                    txtViewComment.backgroundColor=[UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:1];
//                    txtViewComment.textColor=[UIColor blackColor];
//                    txtViewComment.editable=YES;
//                    txtViewComment.userInteractionEnabled=YES;
//                    [txtViewComment setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
//                    [self.scrollView addSubview: txtViewComment];
//                }
//                else if (i==arrDataHdng.count-1)
//                {
//                    UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+10,IS_IPAD?Ypos+15: Ypos+7.5,IS_IPAD?400:176,IS_IPAD?70: 35)];
//                    txtViewRecommendation.delegate=self;
//                    txtViewRecommendation.tag=kTxtVStatsRecommendation;
//                    txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
//                    txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
//                    txtViewRecommendation.backgroundColor=[UIColor whiteColor];
//                    txtViewRecommendation.textColor=[UIColor blackColor];
//                    txtViewRecommendation.editable=NO;
//                    txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
//                    txtViewRecommendation.layer.cornerRadius=2.0;
//                    txtViewRecommendation.layer.borderWidth=1.0;
//                    txtViewRecommendation.userInteractionEnabled=YES;
//                    [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
//                    [self.scrollView addSubview: txtViewRecommendation];
//                    
//                    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
//                    btn.frame=CGRectMake(IS_IPAD?Xpos+420: Xpos+196,IS_IPAD?Ypos+35.5:Ypos+10.5, 29, 29);
//                    [btn setImage:[UIImage imageNamed:@"list_view.png"] forState:UIControlStateNormal];
//                    [btn setBackgroundColor:[UIColor clearColor]];
//                    btn.tag=kBtnSelectStatsRecommendation;
//                    [btn addTarget:self action:@selector(btnSelectRecommendationClk) forControlEvents:UIControlEventTouchUpInside];
//                    [self.scrollView addSubview:btn];
//                    
//                }
//                
//                Xpos+=lbl_Values.frame.size.width+10;
//                Ypos-=1;
//                
//                UILabel *lbl_Line3 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?101: 51)];
//                lbl_Line3.backgroundColor = [UIColor lightGrayColor];
//                lbl_Line3.textColor=[UIColor redColor];
//           //     [self.scrollView addSubview:lbl_Line3];
//                
//                Ypos+=IS_IPAD?100:50;
//                
//            }
//            UILabel *lbl_LastLine = [[UILabel alloc] initWithFrame:CGRectMake(4, Ypos, deviceWidth-8, 1)];
//            lbl_LastLine.backgroundColor = [UIColor lightGrayColor];
//            lbl_LastLine.textColor=[UIColor grayColor];
//         //   [self.scrollView addSubview:lbl_LastLine];
//            
//            [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+30)];
//            
//            [self getValuesForLabelsForSelectedCase:objTemp];
//        }
//    }
//    else if ( [strKey isEqualToString:@"1"])
//    {
//        if (isShow)
//        {
//            self.viewForCaseData=[[UIView alloc]initWithFrame:CGRectMake(0, 50,  deviceWidth, deviceHeight-177)];
//            self.viewForCaseData.backgroundColor=[UIColor clearColor];
//            [self.view addSubview:self.viewForCaseData];
//            
//            self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0, deviceWidth,deviceHeight-177)];
//            self.scrollView.showsVerticalScrollIndicator=NO;
//            self.scrollView.scrollEnabled=YES;
//            self.scrollView.delegate=self;
//            self.scrollView.userInteractionEnabled=YES;
//            [self.viewForCaseData addSubview: self.scrollView];
//            
//            int Xpos=0;
//            int Ypos=0;
//            Ypos=20;
//            
//            for (int i=0; i<arrDataHdng.count; i++)
//            {
//                Xpos=4;
//                UILabel *lbl_Line = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, deviceWidth-8, 1)];
//                lbl_Line.backgroundColor = [UIColor lightGrayColor];
//                lbl_Line.textColor=[UIColor grayColor];
//                [self.scrollView addSubview:lbl_Line];
//                
//                
//                UILabel *lbl_Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?101: 51)];
//                lbl_Line1.backgroundColor = [UIColor lightGrayColor];
//                lbl_Line1.textColor=[UIColor grayColor];
//                [self.scrollView addSubview:lbl_Line1];
//                
//                Ypos+=lbl_Line.frame.size.height;
//                Xpos+=1;
//                
//                UILabel *lbl_HD= [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, IS_IPAD?298: 74,IS_IPAD?100: 50)];
//                lbl_HD.numberOfLines=0;
//                
//                lbl_HD.backgroundColor = [UIColor clearColor];
//                lbl_HD.text=[arrDataHdng objectAtIndex:i];
//                lbl_HD.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
//                lbl_HD.textAlignment=NSTextAlignmentCenter;
//                lbl_HD.textColor=UIColorFromRGB(0x4174DA);
//                lbl_HD.adjustsFontSizeToFitWidth=YES;
//                [self.scrollView addSubview:lbl_HD];
//                
//                Xpos=IS_IPAD?303: 79;
//                UILabel *lbl_Line2 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1, IS_IPAD?100: 50)];
//                lbl_Line2.backgroundColor = [UIColor lightGrayColor];
//                lbl_Line2.textColor=[UIColor grayColor];
//                [self.scrollView addSubview:lbl_Line2];
//                
//                Xpos+=1;
//                
//                UILabel *lbl_Values = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+5, Ypos, IS_IPAD?449: 225,IS_IPAD?100: 50)];
//                lbl_Values.backgroundColor = [UIColor clearColor];
//                lbl_Values.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
//                lbl_Values.textAlignment=NSTextAlignmentCenter;
//                lbl_Values.numberOfLines=0;
//                lbl_Values.tag=KlblTitle+i;
//                lbl_Values.textColor=[UIColor blackColor];
//                [self.scrollView addSubview:lbl_Values];
//                
//                if (i==2)
//                {
//                    UIButton *btn_figure=[[UIButton alloc]initWithFrame:CGRectMake(Xpos, Ypos, IS_IPAD?459: 235,IS_IPAD?100:50)];
//                    [btn_figure setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                    btn_figure.backgroundColor=[UIColor clearColor];
//                    btn_figure.titleLabel.font=IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
//                    btn_figure.tag=kBtnStatsFigure;
//                    [btn_figure addTarget:self action:@selector(btnFigureClk) forControlEvents:UIControlEventTouchUpInside];
//                    [self.scrollView addSubview:btn_figure];
//                    
//                    imageFigure = [[UIImageView alloc] initWithFrame:CGRectMake(175, 180, 50, 30)];
//                    
//                    [self.scrollView addSubview:imageFigure];
//                }
//                if (i==4)
//                {
//                    UITextView *txtViewComment=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+10, IS_IPAD?Ypos+10:Ypos+5, IS_IPAD?439:215,IS_IPAD?80:40)];
//                    
//                    txtViewComment=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+10, IS_IPAD?Ypos+10:Ypos+5, IS_IPAD?439:215,IS_IPAD?80:40)];
//                    txtViewComment.delegate=self;
//                    txtViewComment.tag=kTxtVStatsComment;
//                    txtViewComment.returnKeyType=UIReturnKeyDefault;
//                    txtViewComment.keyboardType=UIKeyboardTypeDefault;
//                    txtViewComment.backgroundColor=[UIColor clearColor];
//                    txtViewComment.textColor=[UIColor blackColor];
//                    txtViewComment.textAlignment=NSTextAlignmentRight;
//                    txtViewComment.editable=NO;
//                    // txtViewComment.layer.borderColor=[UIColor grayColor].CGColor;
//                    //txtViewComment.layer.cornerRadius=2.0;
//                    // txtViewComment.layer.borderWidth=1.0;
//                    //  [txtViewComment setBackgroundColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f]];
//                    txtViewComment.userInteractionEnabled=YES;
//                    [txtViewComment setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
//                    [self.scrollView addSubview: txtViewComment];
//  }
//                
//                if (i==arrDataHdng.count-1)
//                {
//                    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
//                    
//                    if ([ObjRecommmendationBO.strRecommendationText isEqualToString:@""] || [ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"2"])
//                    {
//                        
//                    }
//                    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"1"] )
//                    {
//                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+10, IS_IPAD?Ypos+15: Ypos+7.5, IS_IPAD?439:215,IS_IPAD?70: 35)];
//                        txtViewRecommendation.delegate=self;
//                        txtViewRecommendation.tag=kTxtVStatsRecommendation;
//                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
//                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
//                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
//                        txtViewRecommendation.textColor=[UIColor blackColor];
//                        txtViewRecommendation.editable=NO;
//                        txtViewRecommendation.userInteractionEnabled=YES;
//                        txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
//                        txtViewRecommendation.layer.cornerRadius=2.0;
//                        txtViewRecommendation.layer.borderWidth=1.0;
//                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
//                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
//                        [self.scrollView addSubview: txtViewRecommendation];
//                    }
//                    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"3"])
//                    {
//                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+5, IS_IPAD?Ypos+15: Ypos+7.5, IS_IPAD?399:175,IS_IPAD?70: 35)];
//                        txtViewRecommendation.delegate=self;
//                        txtViewRecommendation.tag=kTxtVStatsRecommendation;
//                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
//                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
//                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
//                        txtViewRecommendation.textColor=[UIColor blackColor];
//                        txtViewRecommendation.editable=NO;
//                        txtViewRecommendation.userInteractionEnabled=YES;
//                        txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
//                        txtViewRecommendation.layer.cornerRadius=2.0;
//                        txtViewRecommendation.layer.borderWidth=1.0;
//                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
//                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
//                        [self.scrollView addSubview: txtViewRecommendation];
//                        
//                        UIButton *btnApproved=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+409:Xpos+185,IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
//                        [btnApproved setBackgroundColor:[UIColor clearColor]];
//                        [btnApproved setBackgroundImage:[UIImage imageNamed:@"acceptIconDeselected.png"] forState:UIControlStateNormal];
//                        [btnApproved setBackgroundImage:[UIImage imageNamed:@"acceptIconSelected.png"] forState:UIControlStateSelected];
//                        btnApproved.tag=kbtnStatsApproved;
//                        btnApproved.selected=NO;
//                        [btnApproved addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
//                        [self.scrollView addSubview:btnApproved];
//                        
//                        
//                        UIButton *btnDecline=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD?Xpos+434:Xpos+210, IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
//                        [btnDecline setBackgroundColor:[UIColor clearColor]];
//                        [btnDecline setBackgroundImage:[UIImage imageNamed:@"declineIconDeselected.png"] forState:UIControlStateNormal];
//                        [btnDecline setBackgroundImage:[UIImage imageNamed:@"declineIconSelected.png"] forState:UIControlStateSelected];
//                        btnDecline.tag=kbtnStatsDecline;
//                        btnDecline.selected=NO;
//                        [btnDecline addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
//                        [self.scrollView addSubview:btnDecline];
//                    
//                    }
//                    else
//                    {
//                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+5, IS_IPAD?Ypos+15:Ypos+7.5,IS_IPAD?374:150,IS_IPAD?70: 35)];
//                        txtViewRecommendation.delegate=self;
//                        txtViewRecommendation.tag=kTxtVStatsRecommendation;
//                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
//                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
//                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
//                        txtViewRecommendation.textColor=[UIColor blackColor];
//                        txtViewRecommendation.editable=NO;
//                        txtViewRecommendation.userInteractionEnabled=YES;
//                        txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
//                        txtViewRecommendation.layer.cornerRadius=2.0;
//                        txtViewRecommendation.layer.borderWidth=1.0;
//                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
//                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
//                        [self.scrollView addSubview: txtViewRecommendation];
//                        
//                        UIButton *btnApproved=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+384:Xpos+160,IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
//                        [btnApproved setBackgroundColor:[UIColor clearColor]];
//                        [btnApproved setBackgroundImage:[UIImage imageNamed:@"acceptIconDeselected.png"] forState:UIControlStateNormal];
//                        [btnApproved setBackgroundImage:[UIImage imageNamed:@"acceptIconSelected.png"] forState:UIControlStateSelected];
//                        btnApproved.tag=kbtnStatsApproved;
//                        btnApproved.selected=NO;
//                        [btnApproved addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
//                        [self.scrollView addSubview:btnApproved];
//                        
//                        
//                        UIButton *btnDecline=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD?Xpos+409:Xpos+185, IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
//                        [btnDecline setBackgroundColor:[UIColor clearColor]];
//                        [btnDecline setBackgroundImage:[UIImage imageNamed:@"declineIconDeselected.png"] forState:UIControlStateNormal];
//                        [btnDecline setBackgroundImage:[UIImage imageNamed:@"declineIconSelected.png"] forState:UIControlStateSelected];
//                        btnDecline.tag=kbtnStatsDecline;
//                        btnDecline.selected=NO;
//                        [btnDecline addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
//                        [self.scrollView addSubview:btnDecline];
//                        
//                        
//                        UIButton *btnWeight = [[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+434:Xpos+210, IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
//                        [btnWeight setBackgroundColor:[UIColor clearColor]];
//                        [btnWeight setBackgroundImage:[UIImage imageNamed:@"WaitHover.png"] forState:UIControlStateNormal];
//                        [btnWeight setBackgroundImage:[UIImage imageNamed:@"Wait.png"] forState:UIControlStateSelected];
//                        btnWeight.selected=NO;
//                        btnWeight.tag=kbtnStatsWeight;
//                        [btnWeight addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
//                        [self.scrollView addSubview:btnWeight];
//                    }
//                }
//                
//                
//                
//                Xpos+=lbl_Values.frame.size.width+10;
//                Ypos-=1;
//                
//                UILabel *lbl_Line3 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?101: 51)];
//                lbl_Line3.backgroundColor = [UIColor lightGrayColor];
//                lbl_Line3.textColor=[UIColor grayColor];
//                [self.scrollView addSubview:lbl_Line3];
//                
//                Ypos+=IS_IPAD?100: 50;
//                
//            }
//            UILabel *lbl_LastLine = [[UILabel alloc] initWithFrame:CGRectMake(4, Ypos, deviceWidth-8, 1)];
//            lbl_LastLine.backgroundColor = [UIColor lightGrayColor];
//            lbl_LastLine.textColor=[UIColor grayColor];
//            [self.scrollView addSubview:lbl_LastLine];
//            
//            [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+30)];
//            
//            [self getValuesForLabelsForSelectedCase:objTemp];
//        }
//    }
//    
//    
//}



-(void)showCustomViewForCase:(BOOL)isShow
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
  
    [self UpdateValuesForCases];
    
    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
    
    if(self.viewForCaseData)
    {
        [self.viewForCaseData.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.viewForCaseData removeFromSuperview];
        self.viewForCaseData=nil;
    }
    
    //  self.view.backgroundColor=[UIColor yellowColor];
    
    if ( [strKey isEqualToString:@"0"])
    {
        if (isShow)
        {
            self.viewForCaseData=[[UIView alloc]initWithFrame:CGRectMake(0, 60, deviceWidth, deviceHeight-177)];
            self.viewForCaseData.backgroundColor=[UIColor clearColor];
            [self.view addSubview:self.viewForCaseData];
            
            self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,deviceWidth,deviceHeight-177)];
            self.scrollView.showsVerticalScrollIndicator=NO;
            self.scrollView.delegate=self;
            self.scrollView.scrollEnabled=YES;
            self.scrollView.userInteractionEnabled=YES;
            [self.viewForCaseData addSubview: self.scrollView];
            
            int Xpos=0;
            int Ypos=0;
            Ypos=20;
            
            for (int i=0; i<arrDataHdng.count; i++)
            {
                
                Xpos=4;
                
                UILabel *lbl_Line = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, deviceWidth-8, 1)];
                
                UILabel *lbl_Line1 = [[UILabel alloc] initWithFrame:CGRectMake(14, Ypos, self.view.frame.size.width-28,IS_IPAD?101: 45)];
                
                lbl_Line1.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:0.8];
                lbl_Line1.textColor=[UIColor lightGrayColor];
                //lbl_Line1.alpha=0.8;
                [self.scrollView addSubview:lbl_Line1];
                
                
                Ypos+=lbl_Line.frame.size.height;
                Xpos+=1;
                
                UILabel *lbl_HD= [[UILabel alloc] initWithFrame:CGRectMake(Xpos+14, Ypos, IS_IPAD?200: 90,IS_IPAD?100: 50)];
                lbl_HD.numberOfLines=0;
                
                lbl_HD.backgroundColor = [UIColor clearColor];
                lbl_HD.text=[arrDataHdng objectAtIndex:i];
                lbl_HD.font = IS_IPAD ? FONT_BOLD(20) : FONT_BOLD(13.0);
                lbl_HD.textAlignment=NSTextAlignmentLeft;
                //    lbl_HD.textColor=UIColorFromRGB(0x4174DA);
                lbl_HD.adjustsFontSizeToFitWidth=YES;
                [self.scrollView addSubview:lbl_HD];
                
                Xpos=IS_IPAD?303:79;
                
                UILabel *lbl_Line2 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?100: 50)];
                lbl_Line2.backgroundColor = [UIColor lightGrayColor];
                lbl_Line2.textColor=[UIColor grayColor];
                // [self.scrollView addSubview:lbl_Line2];
                
                Xpos+=1;
                
                UILabel *lbl_Values = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+35, Ypos, IS_IPAD?449: 175,IS_IPAD?100: 43)];
                lbl_Values.backgroundColor = [UIColor clearColor];
                lbl_Values.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                lbl_Values.textAlignment=NSTextAlignmentRight;
                lbl_Values.numberOfLines=0;
                lbl_Values.tag= KlblStatsCountry+i;
                lbl_Values.textColor=[UIColor blackColor];
                [self.scrollView addSubview:lbl_Values];
                
                if (i==2)
                {
                    UIButton *btn_figure=[[UIButton alloc]initWithFrame:CGRectMake(Xpos, Ypos,IS_IPAD?459: 235,IS_IPAD?100:50)];
                    [btn_figure setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    btn_figure.backgroundColor=[UIColor clearColor];
                    btn_figure.titleLabel.font=IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                    btn_figure.tag=kBtnStatsFigure;
                    [btn_figure addTarget:self action:@selector(btnFigureClk) forControlEvents:UIControlEventTouchUpInside];
                    [self.scrollView addSubview:btn_figure];
                    
                    imageFigure = [[UIImageView alloc] initWithFrame:CGRectMake(175, 180, 50, 30)];
                    
                    [self.scrollView addSubview:imageFigure];
                }
                
                if (i==3)
                {
                    txtViewComment=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+30, Ypos,IS_IPAD?459:194,IS_IPAD?100:42)];
                    txtViewComment.delegate=self;
                    txtViewComment.tag=kTxtVStatsComment;
                    txtViewComment.returnKeyType=UIReturnKeyDefault;
                    txtViewComment.keyboardType=UIKeyboardTypeDefault;
                    //  txtViewComment.backgroundColor=[UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:1];
                    txtViewComment.textColor=[UIColor blackColor];
                    txtViewComment.editable=YES;
                    txtViewComment.userInteractionEnabled=YES;
                    [txtViewComment setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                    [self.scrollView addSubview: txtViewComment];
                }
                else if (i==arrDataHdng.count-1)
                {
                    UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+35,IS_IPAD?Ypos+15: Ypos+7.5,IS_IPAD?400:156,IS_IPAD?70: 35)];
                    txtViewRecommendation.delegate=self;
                    txtViewRecommendation.tag=kTxtVStatsRecommendation;
                    txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                    txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                    txtViewRecommendation.backgroundColor=[UIColor whiteColor];
                    txtViewRecommendation.textColor=[UIColor blackColor];
                    txtViewRecommendation.editable=NO;
                    txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                    txtViewRecommendation.layer.cornerRadius=2.0;
                    txtViewRecommendation.layer.borderWidth=1.0;
                    txtViewRecommendation.userInteractionEnabled=YES;
                    [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                    [self.scrollView addSubview: txtViewRecommendation];
                    
                    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
                    btn.frame=CGRectMake(IS_IPAD?Xpos+420: Xpos+196,IS_IPAD?Ypos+35.5:Ypos+10.5, 29, 29);
                    [btn setImage:[UIImage imageNamed:@"list_view.png"] forState:UIControlStateNormal];
                    [btn setBackgroundColor:[UIColor clearColor]];
                    btn.tag=kBtnSelectStatsRecommendation;
                    [btn addTarget:self action:@selector(btnSelectRecommendationClk) forControlEvents:UIControlEventTouchUpInside];
                    [self.scrollView addSubview:btn];
                    
                }
                
                Xpos+=lbl_Values.frame.size.width+10;
                Ypos-=1;
                
                UILabel *lbl_Line3 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?101: 51)];
                lbl_Line3.backgroundColor = [UIColor lightGrayColor];
                lbl_Line3.textColor=[UIColor redColor];
                //  [self.scrollView addSubview:lbl_Line3];
                
                Ypos+=IS_IPAD?100:50;
                
            }
            UILabel *lbl_LastLine = [[UILabel alloc] initWithFrame:CGRectMake(4, Ypos, deviceWidth-8, 1)];
            lbl_LastLine.backgroundColor = [UIColor lightGrayColor];
            lbl_LastLine.textColor=[UIColor grayColor];
            //  [self.scrollView addSubview:lbl_LastLine];
            
            [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+30)];
            
            [self getValuesForLabelsForSelectedCase:objTemp];
        }
    }
    else if ( [strKey isEqualToString:@"1"])
    {
        if (isShow)
        {
            self.viewForCaseData=[[UIView alloc]initWithFrame:CGRectMake(0, 50,  deviceWidth, deviceHeight-177)];
            self.viewForCaseData.backgroundColor=[UIColor clearColor];
            [self.view addSubview:self.viewForCaseData];
            
            self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0, deviceWidth,deviceHeight-177)];
            self.scrollView.showsVerticalScrollIndicator=NO;
            self.scrollView.scrollEnabled=YES;
            self.scrollView.delegate=self;
            self.scrollView.userInteractionEnabled=YES;
            [self.viewForCaseData addSubview: self.scrollView];
            
            int Xpos=0;
            int Ypos=0;
            Ypos=20;
            
            for (int i=0; i<arrDataHdng.count; i++)
            {
                Xpos=24;
                UILabel *lbl_Line = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, deviceWidth-8, 1)];
                lbl_Line.backgroundColor = [UIColor lightGrayColor];
                lbl_Line.textColor=[UIColor grayColor];
                //   [self.scrollView addSubview:lbl_Line];
                
                
                UILabel *lbl_Line1 = [[UILabel alloc] initWithFrame:CGRectMake(14, Ypos, self.view.frame.size.width-28,IS_IPAD?101: 45)];
                lbl_Line1.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:0.8];
                lbl_Line1.textColor=[UIColor lightGrayColor];
                //lbl_Line1.alpha=0.8;
                [self.scrollView addSubview:lbl_Line1];
                
                Ypos+=lbl_Line.frame.size.height;
                Xpos+=1;
                
                UILabel *lbl_HD= [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, IS_IPAD?298: 110,IS_IPAD?100: 50)];
                lbl_HD.numberOfLines=0;
                
                lbl_HD.backgroundColor = [UIColor clearColor];
                lbl_HD.text=[arrDataHdng objectAtIndex:i];
                lbl_HD.font = IS_IPAD ? FONT_BOLD(20) : FONT_BOLD(13.0);
                lbl_HD.textAlignment=NSTextAlignmentLeft;
                //    lbl_HD.textColor=UIColorFromRGB(0x4174DA);
                lbl_HD.adjustsFontSizeToFitWidth=YES;
                [self.scrollView addSubview:lbl_HD];
                
                Xpos=IS_IPAD?303: 79;
                UILabel *lbl_Line2 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1, IS_IPAD?100: 50)];
                lbl_Line2.backgroundColor = [UIColor lightGrayColor];
                lbl_Line2.textColor=[UIColor grayColor];
                ////    [self.scrollView addSubview:lbl_Line2];
                
                Xpos+=1;
                
                UILabel *lbl_Values = [[UILabel alloc] initWithFrame:CGRectMake(Xpos+15, Ypos, IS_IPAD?449: 200,IS_IPAD?100: 43)];
                lbl_Values.backgroundColor = [UIColor clearColor];
                lbl_Values.font = IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                lbl_Values.textAlignment=NSTextAlignmentRight;
                lbl_Values.numberOfLines=0;
                lbl_Values.tag=KlblTitle+i;
                lbl_Values.textColor=[UIColor blackColor];
                [self.scrollView addSubview:lbl_Values];
                
                if (i==3)
                {
                    UIButton *btn_figure=[[UIButton alloc]initWithFrame:CGRectMake(Xpos, Ypos, IS_IPAD?459: 235,IS_IPAD?100:50)];
                    [btn_figure setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    btn_figure.backgroundColor=[UIColor clearColor];
                    btn_figure.titleLabel.font=IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0);
                    btn_figure.tag=kBtnStatsFigure;
                    [btn_figure addTarget:self action:@selector(btnFigureClk) forControlEvents:UIControlEventTouchUpInside];
                    [self.scrollView addSubview:btn_figure];
                }
                if (i==5)
                {
                 
                   txtViewComment=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+10, IS_IPAD?Ypos+10:Ypos+5, IS_IPAD?439:215,IS_IPAD?80:40)];
                    txtViewComment.delegate=self;
                    txtViewComment.tag=kTxtVStatsComment;
                    txtViewComment.returnKeyType=UIReturnKeyDefault;
                    txtViewComment.keyboardType=UIKeyboardTypeDefault;
                    txtViewComment.backgroundColor=[UIColor clearColor];
                    txtViewComment.textColor=[UIColor blackColor];
                    txtViewComment.textAlignment=NSTextAlignmentRight;
                    txtViewComment.editable=NO;
                    // txtViewComment.layer.borderColor=[UIColor grayColor].CGColor;
                    //txtViewComment.layer.cornerRadius=2.0;
                    // txtViewComment.layer.borderWidth=1.0;
                    //  [txtViewComment setBackgroundColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f]];
                    txtViewComment.userInteractionEnabled=YES;
                    [txtViewComment setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                    [self.scrollView addSubview: txtViewComment];
                }
                
                if (i==arrDataHdng.count-1)
                {
                    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
                    
                    if ([ObjRecommmendationBO.strRecommendationText isEqualToString:@""] || [ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"2"])
                    {
                        
                    }
                    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"1"])
                    {
                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+40, IS_IPAD?Ypos+15: Ypos+7.5, IS_IPAD?439:374,IS_IPAD?70: 35)];
                        txtViewRecommendation.delegate=self;
                        txtViewRecommendation.tag=kTxtVStatsRecommendation;
                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
                        txtViewRecommendation.textColor=[UIColor blackColor];
                        txtViewRecommendation.editable=NO;
                        txtViewRecommendation.userInteractionEnabled=YES;
                        txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                        //                        txtViewRecommendation.layer.cornerRadius=2.0;
                        //                        txtViewRecommendation.layer.borderWidth=1.0;
                        [txtViewComment setBackgroundColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f]];
                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
                        [self.scrollView addSubview: txtViewRecommendation];
                        
                    }
                    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"3"])
                    {
                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+40, IS_IPAD?Ypos+15: Ypos+7.5, IS_IPAD?399:175,IS_IPAD?75: 35)];
                        txtViewRecommendation.delegate=self;
                        txtViewRecommendation.tag=kTxtVStatsRecommendation;
                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
                        txtViewRecommendation.textColor=[UIColor blackColor];
                        txtViewRecommendation.editable=NO;
                        txtViewRecommendation.userInteractionEnabled=YES;
                        //txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                        //  txtViewRecommendation.layer.cornerRadius=2.0;
                        //  txtViewRecommendation.layer.borderWidth=1.0;
                        [txtViewComment setBackgroundColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f]];
                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
                        [self.scrollView addSubview: txtViewRecommendation];
                        
                        
                        UIButton *btnApproved = [[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+384:Xpos+195,IS_IPAD? Ypos+42:Ypos+9, 25, 25)];
                        buttonValue = 0;
                        //[btnApproved setTitle:@"" forState:UIControlStateNormal];
                        [btnApproved setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [btnApproved setBackgroundImage:[UIImage imageNamed:@"infoBlue"] forState:UIControlStateNormal];
                        [btnApproved addTarget:self action:@selector(selectedActionSheet) forControlEvents:UIControlEventTouchUpInside];
                        [self.scrollView addSubview:btnApproved];
                        
                        
                        //                            UIButton *btnApproved=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+409:Xpos+185,IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
                        //                            [btnApproved setBackgroundColor:[UIColor clearColor]];
                        //                            [btnApproved setBackgroundImage:[UIImage imageNamed:@"acceptIconDeselected.png"] forState:UIControlStateNormal];
                        //                            [btnApproved setBackgroundImage:[UIImage imageNamed:@"acceptIconSelected.png"] forState:UIControlStateSelected];
                        //                            btnApproved.tag=kbtnApproved;
                        //                            btnApproved.selected=NO;
                        //                            [btnApproved addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
                        //                            [self.scrollView addSubview:btnApproved];
                        //
                        //
                        //                            UIButton *btnDecline=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD?Xpos+434:Xpos+210, IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
                        //                            [btnDecline setBackgroundColor:[UIColor clearColor]];
                        //                            [btnDecline setBackgroundImage:[UIImage imageNamed:@"declineIconDeselected.png"] forState:UIControlStateNormal];
                        //                            [btnDecline setBackgroundImage:[UIImage imageNamed:@"declineIconSelected.png"] forState:UIControlStateSelected];
                        //                            btnDecline.tag=kbtnDecline;
                        //                            btnDecline.selected=NO;
                        //                            [btnDecline addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
                        //                            [self.scrollView addSubview:btnDecline];
                        
                        
                    }
                    else
                    {
                        
                        UITextView *txtViewRecommendation=[[UITextView alloc]initWithFrame:CGRectMake(Xpos+45, IS_IPAD?Ypos+15:Ypos+7.5,IS_IPAD?374:150,IS_IPAD?70: 35)];
                        txtViewRecommendation.delegate=self;
                        txtViewRecommendation.textAlignment=NSTextAlignmentRight;
                        txtViewRecommendation.tag=kTxtVStatsRecommendation;
                        txtViewRecommendation.returnKeyType=UIReturnKeyDefault;
                        txtViewRecommendation.keyboardType=UIKeyboardTypeDefault;
                        txtViewRecommendation.backgroundColor=[UIColor clearColor];
                        txtViewRecommendation.textColor=[UIColor blackColor];
                        txtViewRecommendation.editable=NO;
                        txtViewRecommendation.userInteractionEnabled=YES;
                        //  txtViewRecommendation.layer.borderColor=[UIColor grayColor].CGColor;
                        //                        txtViewRecommendation.layer.cornerRadius=2.0;
                        //                        txtViewRecommendation.layer.borderWidth=1.0;
                        [txtViewRecommendation setFont:IS_IPAD ? FONT_NORMAL(20) : FONT_NORMAL(12.0)];
                        txtViewRecommendation.text=ObjRecommmendationBO.strRecommendationText;
                        [self.scrollView addSubview: txtViewRecommendation];
                        
                        
                        //Show Action sheet for button...
                        UIButton *btnApproved = [[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+384:Xpos+195,IS_IPAD? Ypos+42:Ypos+9, 25, 25)];
                        buttonValue = 1;
                        [btnApproved setBackgroundImage:[UIImage imageNamed:@"infoBlue"] forState:UIControlStateNormal];
                        
                        [btnApproved setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [btnApproved addTarget:self action:@selector(selectedActionSheet) forControlEvents:UIControlEventTouchUpInside];
                        [self.scrollView addSubview:btnApproved];
                        
                        
                        
                        //                            [btnApproved setBackgroundImage:[UIImage imageNamed:@"acceptIconDeselected.png"] forState:UIControlStateNormal];
                        //                            [btnApproved setBackgroundImage:[UIImage imageNamed:@"acceptIconSelected.png"] forState:UIControlStateSelected];
                        //                            btnApproved.tag=kbtnApproved;
                        //                            btnApproved.selected=NO;
                        //                            [btnApproved addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
                        //                            [self.scrollView addSubview:btnApproved];
                        
                        
                        //                            UIButton *btnDecline=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD?Xpos+409:Xpos+185, IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
                        //                            [btnDecline setBackgroundColor:[UIColor clearColor]];
                        //                            [btnDecline setBackgroundImage:[UIImage imageNamed:@"declineIconDeselected.png"] forState:UIControlStateNormal];
                        //                            [btnDecline setBackgroundImage:[UIImage imageNamed:@"declineIconSelected.png"] forState:UIControlStateSelected];
                        //                            btnDecline.tag=kbtnDecline;
                        //                            btnDecline.selected=NO;
                        //                            [btnDecline addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
                        //                            [self.scrollView addSubview:btnDecline];
                        //
                        //
                        //                            UIButton *btnWeight = [[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD? Xpos+434:Xpos+210, IS_IPAD? Ypos+42:Ypos+17, 20, 20)];
                        //                            [btnWeight setBackgroundColor:[UIColor clearColor]];
                        //                            [btnWeight setBackgroundImage:[UIImage imageNamed:@"WaitHover.png"] forState:UIControlStateNormal];
                        //                            [btnWeight setBackgroundImage:[UIImage imageNamed:@"Wait.png"] forState:UIControlStateSelected];
                        //                            btnWeight.selected=NO;
                        //                            btnWeight.tag=kbtnWeight;
                        //                            [btnWeight addTarget:self action:@selector(btnActionClk:) forControlEvents:UIControlEventTouchUpInside];
                        //                            [self.scrollView addSubview:btnWeight];
                        
                    }
                }
                
                
                Xpos+=lbl_Values.frame.size.width+10;
                Ypos-=1;
                
                UILabel *lbl_Line3 = [[UILabel alloc] initWithFrame:CGRectMake(Xpos, Ypos, 1,IS_IPAD?101: 51)];
                lbl_Line3.backgroundColor = [UIColor lightGrayColor];
                lbl_Line3.textColor=[UIColor grayColor];
                //    [self.scrollView addSubview:lbl_Line3];
                
                Ypos+=IS_IPAD?100: 50;
                
            }
            
            UILabel *lbl_LastLine = [[UILabel alloc] initWithFrame:CGRectMake(4, Ypos, deviceWidth-8, 1)];
            lbl_LastLine.backgroundColor = [UIColor lightGrayColor];
            lbl_LastLine.textColor=[UIColor grayColor];
            //  [self.scrollView addSubview:lbl_LastLine];
            
            [self.scrollView setContentSize:CGSizeMake(deviceWidth, Ypos+30)];
            
            [self getValuesForLabelsForSelectedCase:objTemp];
            
        }
    }
}




-(void)btnFigureClk
{
    NSString *strKey=[USER_DEFAULTS  valueForKey:@"Customer"];
    
    if ( [strKey isEqualToString:@"1"] )
    {
        if ([[arrDataHdng objectAtIndex:2] isEqualToString:@"Status"])
        {
            PatrawinCase *objTemp=[arrayAllCases objectAtIndex:[strSelectedIndex integerValue]];
            NSString *strWebUrl=objTemp.link;
            
            LinkViewController *objLinkVC=[[LinkViewController alloc]init];
            objLinkVC.strUrl=strWebUrl;
            [self.navigationController pushViewController:objLinkVC animated:YES];
        }
        else if([[arrDataHdng objectAtIndex:2] isEqualToString:@"Figure"])
        {
        }
        
    }
    else if ( [strKey isEqualToString:@"0"])
    {
        if ([[arrDataHdng objectAtIndex:3] isEqualToString:@"Status"])
        {
            PatrawinCase *objTemp=[arrayAllCases objectAtIndex:[strSelectedIndex integerValue]];
            NSString *strWebUrl=objTemp.link;
            
            LinkViewController *objLinkVC=[[LinkViewController alloc]init];
            objLinkVC.strUrl=strWebUrl;
            [self.navigationController pushViewController:objLinkVC animated:YES];
        }
        else if([[arrDataHdng objectAtIndex:3] isEqualToString:@"Figure"])
        {
        }
    }
    
    
}
-(void)btnLeftClk
{
    int SelectedArrow=[self.strSelectedIndex intValue] -1;
    self.strSelectedIndex =[NSString stringWithFormat:@"%d",SelectedArrow];
    
    [self enableDisableLeftButtons];
    [self enableDisableRightButtons];
    [self showCustomViewForCase:YES];
    
}
-(void)btnRightclk
{
    int SelectedArrow=[self.strSelectedIndex intValue]+1;
    self.strSelectedIndex =[NSString stringWithFormat:@"%d",SelectedArrow];
    
    [self enableDisableRightButtons];
    [self enableDisableLeftButtons];
    
    [self showCustomViewForCase:YES];
    
}
#pragma mark- ButtonAction

-(BOOL)enableDisableLeftButtons
{
    UIButton *btnLeft=(UIButton *)[self.view viewWithTag:kBtnStatsLeft];
    btnLeft.enabled=[self.strSelectedIndex  integerValue]<1?NO:YES;
    return btnLeft.enabled;
}
-(BOOL)enableDisableRightButtons
{
    UIButton *btnRight=(UIButton *)[self.view viewWithTag:kBtnStatsRight];
    btnRight.enabled=[self.strSelectedIndex  integerValue]==arrayAllCases.count-1?NO:YES;
    return btnRight.enabled;
}


-(void)designToolBar
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if([strKey isEqualToString:@"0"])
    {
        UIButton *btnCompose = [[UIButton alloc] initWithFrame:CGRectMake(0, deviceHeight-125, deviceWidth/4, 65)];
        btnCompose.tag = kBtnStatsCompose;
        btnCompose.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnCompose addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnCompose setImage:[UIImage imageNamed:@"compose-top"] forState:UIControlStateNormal];
        [self.view addSubview:btnCompose];
        
        UIButton *btnShare = [[UIButton alloc] initWithFrame:CGRectMake(btnCompose.frame.size.width+1, deviceHeight -125, deviceWidth/4, 65)];
        btnShare.tag = kBtnStatsShare;
        btnShare.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnShare addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnShare setImage:[UIImage imageNamed:@"share1"] forState:UIControlStateNormal];
        [self.view addSubview:btnShare];
        
        UIButton *btntempRegistration = [[UIButton alloc] initWithFrame:CGRectMake(btnShare.frame.size.width+1+btnShare.frame.origin.x, deviceHeight-125, deviceWidth/4, 65)];
        btntempRegistration.tag = kBtnStatsRegistration;
        btntempRegistration.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btntempRegistration addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btntempRegistration setImage:[UIImage imageNamed:@"time_registration.png"] forState:UIControlStateNormal];
        [self.view addSubview:btntempRegistration];
        
        UIButton *btnupdate = [[UIButton alloc] initWithFrame:CGRectMake(btntempRegistration.frame.size.width+1+btntempRegistration.frame.origin.x, deviceHeight-125, deviceWidth/4, 65)];
        btnupdate.tag = kBtnStatsUpdate;
        btnupdate.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnupdate addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnupdate setImage:[UIImage imageNamed:@"caseupload"] forState:UIControlStateNormal];
        [self.view addSubview:btnupdate];
    }
    
    else if ([strKey isEqualToString:@"1"])
    {
        UIButton *btnCompose = [[UIButton alloc] initWithFrame:CGRectMake(0,deviceHeight-125,deviceWidth/3+1, 65)];
        btnCompose.tag = kBtnStatsCompose;
        btnCompose.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnCompose addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnCompose setImage:[UIImage imageNamed:@"compose-top"] forState:UIControlStateNormal];
        [self.view addSubview:btnCompose];
        
        UIButton *btnShare = [[UIButton alloc] initWithFrame:CGRectMake(btnCompose.frame.size.width+1,deviceHeight-125, deviceWidth/3+1, 65)];
        btnShare.tag = kBtnStatsShare;
        btnShare.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnShare addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnShare setImage:[UIImage imageNamed:@"share1"] forState:UIControlStateNormal];
        [self.view addSubview:btnShare];
        
        UIButton *btnupdate = [[UIButton alloc] initWithFrame:CGRectMake(btnShare.frame.origin.x + btnShare.frame.size.width+1, deviceHeight-125, deviceWidth/3+1, 65)];
        btnupdate.tag = kBtnStatsUpdate;
        btnupdate.backgroundColor =[UIColor colorWithRed:45.0f/255.0f green:117.0f/255.0f blue:225.0f/255.0f alpha:1.0];
        [btnupdate addTarget:self action:@selector(toolBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnupdate setImage:[UIImage imageNamed:@"caseupload"] forState:UIControlStateNormal];
        [self.view addSubview:btnupdate];
        
    }
    
}
-(void)openMail
{
    PatrawinCase *objCasesTemp=[self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    
    NSMutableString *txtString=[[NSMutableString alloc]init];
    
    [txtString appendString:[NSString stringWithFormat:@"Country: %@",objCasesTemp.country_code]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Title: %@",objCasesTemp.title]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Status: %@",objCasesTemp.status]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Comment: %@",objCasesTemp.comment]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Deadline: %@",objCasesTemp.deadline]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Casesdata.txt"]];
    [fileManager createFileAtPath:fullPath contents:[txtString dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    TXTFileName = [documentsDirectory stringByAppendingPathComponent:@"Casesdata.txt"];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@""];
        NSArray *toRecipients = @[];
        [mailer setToRecipients:toRecipients];
        [mailer addAttachmentData:[NSData dataWithContentsOfFile:fullPath] mimeType:@".text" fileName:@"Casesdata"];
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        [CommonMethods showAlertWithTitle:NSLocalizedString(@"Alert.Title.NoMail", @"") andMessage:NSLocalizedString(@"Alert.Message.NoMail", @"")];
    }
}

-(void)openPreviewControllerToShare
{
    PatrawinCase *objCasesTemp=[self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue]];
    
    NSMutableString *txtString=[[NSMutableString alloc]init];
    
    [txtString appendString:[NSString stringWithFormat:@"Country: %@",objCasesTemp.country_code]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Title: %@",objCasesTemp.title]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Status: %@",objCasesTemp.status]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];  
    
    [txtString appendString:[NSString stringWithFormat:@"Comment: %@",objCasesTemp.comment]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    [txtString appendString:[NSString stringWithFormat:@"Deadline: %@",objCasesTemp.deadline]];
    [txtString appendString:@" "];
    [txtString appendString:@"\n"];
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Casesdata.txt"]];
    [fileManager createFileAtPath:fullPath contents:[txtString dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    TXTFileName = [documentsDirectory stringByAppendingPathComponent:@"Casesdata.txt"];
    
    QLPreviewController *previewController = [[QLPreviewController alloc] init];
    previewController.dataSource = self;
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    [self presentViewController:previewController animated:NO completion:nil];


}
#pragma mark - QLPreviewControllerDataSource Methods

-(NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
    return 1;
}

-(id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    return  [NSURL fileURLWithPath:TXTFileName];
}

-(void)previewControllerWillDismiss:(QLPreviewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark Button Actions -

-(void)toolBarBtnClicked:(UIButton *)sender
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    switch (sender.tag)
    {
        case kBtnStatsCompose:
        {
            ComposeSheetVC *obj_ComposeVC = [[ComposeSheetVC alloc] init];
            obj_ComposeVC.strScreenType =@"Cases";
            
            PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
            obj_ComposeVC.strSubject=objTemp.case_id;
            [self.navigationController pushViewController:obj_ComposeVC animated:YES];
        }
            break;
            
        case kBtnStatsShare:
        {
            [self openPreviewControllerToShare];
        }
            break;
            
        case kBtnStatsRegistration:
        {
            if([strKey isEqualToString:@"0"])
            {
                TimeRegistrationVC *obj_TimeVC = [[TimeRegistrationVC alloc] init];
                PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
                obj_TimeVC.strCaseValue=objTemp.case_id;
                obj_TimeVC.strCustomerIdValue=objTemp.customer_id;
                obj_TimeVC.strCustomerNameValue=objTemp.customer_name;
                [self.navigationController pushViewController:obj_TimeVC animated:YES];
            }
        }
            break;
            
        case kBtnStatsUpdate:
        {
            if ([strKey isEqualToString:@"1"])
            {
                 [self CallForCustomerUpdate];
            }
            else if ([strKey isEqualToString:@"0"])
            {
                 [self CallForAgentUpdate];
            }
        }
            break;
            
        default:
            break;
    }
}

-(void)CallForAgentUpdate
{
    UITextView *txtV=(UITextView *)[self.scrollView viewWithTag:kTxtVStatsRecommendation];
    UITextField *txtfComment=(UITextField *)[self.scrollView viewWithTag:kTxtVStatsComment];

    if ( [txtfComment.text length]<=0)
    {
        UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please add comment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertVw show];
    }
    else if ([txtV.text length]<=0 )
    {
        UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Please select recommendation." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertVw show];
    }
    else
    {
        WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
        inputPram.methodType = WebserviceMethodTypePost;
        inputPram.relativeWebservicePath = @"sendcommentandrecommendation";
        
        PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
        
        objTemp.comment_case=txtfComment.text;
        objTemp.Recommendation_Text=txtV.text;
        
        NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
        
        NSString *Str1 = [NSString stringWithUTF8String:[txtfComment.text cStringUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"%@", Str1);
        
        NSLog(@"%@", txtfComment.text);
        
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(objTemp.customer_id) forKey:@"user_id"];
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(objTemp.case_id) forKey:@"case_id"];
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtfComment.text) forKey:@"comment"];
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(txtV.text) forKey:@"recommendation_text"];
        [inputPram.dict_postParameters setObject:GET_VALID_STRING(self.strGlobalRecommendationId) forKey:@"recommendation_id"];
        
        [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
            if (error)
            {
                if ([error isErrorOfKindNoNetwork])
                    [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                return ;
            }
            
            error = nil;
            self.responseDict=nil;
            self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];

            NSString *strMessage=[[self.responseDict objectForKey:@"data"] objectForKey:@"log"];
            if ([strMessage isEqualToString:@"Sent Successfully."])
            {
                [self fetchCompanyDatawithSelectedBo:objTemp];
                UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alertVw.tag=677;
                [alertVw show];
                
            }
            else if ([strMessage isEqualToString:@"Updated Successfully."])
            {
                UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alertVw.tag=677;
                [alertVw show];
            }

        }];
    }
}

-(void)CallForCustomerUpdate
{
    PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
    RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
    
    if ([ObjRecommmendationBO.strRecommendationText isEqualToString:@""] || [ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"2"])
    {
        [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"No recommendation to update" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else if ([ObjRecommmendationBO.strRecommendationStatus isEqualToString:@"1"])
    {
        [[[UIAlertView alloc]initWithTitle:@"Hynell" message:@"Nothing to update" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else
    {
        
        if(buttonSelected == 0)
        {
            [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"Please select atleast one action for recommendation."];
        }
        else
        {
            WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
            inputPram.methodType = WebserviceMethodTypePost;
            inputPram.relativeWebservicePath = @"updateStatusOfUserRecommendationSendByAgent";
            
            NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
            
            [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
            
            switch (buttonSelected) {
                case 1:
                    [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"1") forKey:@"status"];
                    break;
                    
                case 2:
                    [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"2") forKey:@"status"];
                    break;
                case 3:
                    [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"3") forKey:@"status"];
                    break;
                default:
                    break;
            }
            
            PatrawinCase *objTemp=[ self.arrayAllCases objectAtIndex:[self.strSelectedIndex integerValue] ];
            
            RecommendationsListBO *ObjRecommmendationBO=objTemp.objRecommendationListBO;
            
            [inputPram.dict_postParameters setObject:GET_VALID_STRING(ObjRecommmendationBO.strRecommendationId) forKey:@"Id"];
            
            
            [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
                if (error)
                {
                    if ([error isErrorOfKindNoNetwork])
                        [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
                    return ;
                }
                
                error = nil;
                
                self.responseDict=nil;
                
                self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
                
                NSString *strMessage=[[self.responseDict objectForKey:@"data"] objectForKey:@"log"];
                
                if ([strMessage isEqualToString:@"Status Updated Successfully."])
                {
                    UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"Hynell" message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    alertVw.tag=677;
                    
                    [alertVw show];
                }
            }];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==677)
    {
        switch (buttonIndex)
        {
            case 0:
                [self.navigationController popViewControllerAnimated:YES];
                break;
                
            default:
                break;
        }
    }
}
-(void)btnSelectRecommendationClk
{
    [self ViewForRecomendationsShow:YES];
    
}
-(void)ViewForRecomendationsShow:(BOOL)isShow
{
    if (self.ViewForRecommendationsBG)
    {
        [self.ViewForRecommendationsBG.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.ViewForRecommendationsBG removeFromSuperview];
        self.ViewForRecommendationsBG=nil;
    }
    if(isShow)
    {
        
        self.ViewForRecommendationsBG=[[UIView alloc]initWithFrame:CGRectMake(0, IS_VERSION_GRT_7 ? 20:0, deviceWidth,IS_VERSION_GRT_7? deviceHeight-20:deviceHeight)];
        self.ViewForRecommendationsBG.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
        [APP_DELEGATE.window addSubview:self.ViewForRecommendationsBG];
        
        self.ViewForRecommendations=[[UIView alloc]initWithFrame:CGRectMake(10,50, deviceWidth-20, IS_IPAD ? 870 : deviceHeight-75)];
        self.ViewForRecommendations.backgroundColor = [UIColor whiteColor] ;
        [self.ViewForRecommendationsBG addSubview:self.ViewForRecommendations];
        
        UITableView *tblForRecommendations=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, deviceWidth-20, IS_IPAD ? 870 : deviceHeight-75) style:UITableViewStylePlain];
        tblForRecommendations.delegate=self;
        tblForRecommendations.dataSource=self;
        
        tblForRecommendations.rowHeight = IS_IPAD ? 55 : 44;
        
        tblForRecommendations.backgroundColor=[UIColor clearColor];
        tblForRecommendations.backgroundView=nil;
        tblForRecommendations.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        
        tblForRecommendations.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        [self.ViewForRecommendations addSubview:tblForRecommendations];
        
        UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCross.frame = CGRectMake(0, 30, 24, 24);
        [btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
        [btnCross addTarget:self action:@selector(btnCross_Clicked) forControlEvents:UIControlEventTouchUpInside];
        [self.ViewForRecommendationsBG addSubview:btnCross];
        btnCross = nil;
    }
 }

#pragma mark-

#pragma mark btnCross_Clicked

-(void)btnCross_Clicked
{
    [self ViewForRecomendationsShow:NO];
}

-(void)getValuesForLabelsForSelectedCase:(PatrawinCase *)selectedStatsCase
{
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ( [strKey isEqualToString:@"0"])
    {
        UILabel *lblCountryValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblStatsCountry];
        lblCountryValue.text= [NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.country_code)];
        
        UILabel *lblTitleValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblStatsCountry+1];
        lblTitleValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.title)];
        
        UILabel *lblCustomerValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblStatsCountry+2];
        lblCustomerValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.customer_name)];
        
        lbl_CaseName.text=selectedStatsCase.case_id;
        
        if ([[arrDataHdng objectAtIndex:3] isEqualToString:@"Status"])
        {
            UIButton *btn=(UIButton *)[self.viewForCaseData viewWithTag:kBtnStatsFigure];
            [btn setTitle:GET_VALID_STRING(selectedStatsCase.status) forState:UIControlStateNormal];
        }
        else if([[arrDataHdng objectAtIndex:3] isEqualToString:@"Figure"])
        {
            [imageFigure setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.figure)]]];
        }
        
        UITextView *txtVCommentValue=(UITextView *)[self.viewForCaseData viewWithTag:kTxtVStatsComment];
        txtVCommentValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.comment_case)];
        
        UILabel *lblDeadlineValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblStatsCountry+5];
        lblDeadlineValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.deadline)];
        
        UITextView *txtV=(UITextView *)[self.scrollView viewWithTag:kTxtVStatsRecommendation];
        RecommendationsListBO *recBo=selectedStatsCase.objRecommendationListBO;
        txtV.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(recBo.strRecommendationText)];
        
    }
    else if ( [strKey isEqualToString:@"1"])
    {
        UILabel *lblTitleValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle];
        lblTitleValue.text= [NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.title)];
        
        UILabel *lblAgentValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+1];
        lblAgentValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.agent_id)];
        
        
        lbl_CaseName.text=selectedStatsCase.case_id;
        
        if ([[arrDataHdng objectAtIndex:2] isEqualToString:@"Status"])
        {
            UIButton *btn=(UIButton *)[self.viewForCaseData viewWithTag:kBtnStatsFigure];
            [btn setTitle:GET_VALID_STRING(selectedStatsCase.status) forState:UIControlStateNormal];
        }
        else if([[arrDataHdng objectAtIndex:2] isEqualToString:@"Figure"])
        {
            UILabel *lblStatusValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+2];
            lblStatusValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.status)];
        }
        
        UILabel *lblAccCostValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+3];
        lblAccCostValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.account_cost)];
        
        
        UITextView *txtVCommentValue=(UITextView *)[self.viewForCaseData viewWithTag:kTxtVStatsComment];
        
        if (selectedStatsCase.comment_case.length == 0)
        {
            UITextView *txtVCommentValue=(UITextView *)[self.scrollView viewWithTag:kTxtVStatsComment];
            
            txtVCommentValue.hidden = YES;
        }
        else
        {
            txtVCommentValue=(UITextView *)[self.viewForCaseData viewWithTag:kTxtVStatsComment];
            
            txtVCommentValue.hidden = NO;
            
            txtVCommentValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.comment_case)];
        }
        
        UILabel *lblDeadlineValue=(UILabel *)[self.viewForCaseData viewWithTag:KlblTitle+5];
        lblDeadlineValue.text=[NSString stringWithFormat:@"%@", GET_VALID_STRING(selectedStatsCase.deadline)];
    }
}

#pragma mark - Table view DataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_IPAD ? 55 : 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.arrRecommendationListBO.count==0)
    {
        return 1;
    }
    else
    {
    return self.arrRecommendationListBO.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
    }
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (self.arrRecommendationListBO.count==0)
    {
        UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?400: 230,IS_IPAD? 768: 320, 30)];
        [lblNoRecord setText:@"No Record Available."];
        [lblNoRecord setTextColor:[UIColor lightGrayColor]];
        lblNoRecord.textAlignment = NSTextAlignmentCenter;
        [lblNoRecord setBackgroundColor:[UIColor clearColor]];
        [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
        [cell.contentView addSubview:lblNoRecord];
        cell.backgroundColor=[UIColor clearColor];
    }
    else
    {
        RecommendationsListBO *objRecommendation=self.arrRecommendationListBO[indexPath.row];
        NSString *strRecommendationText2= objRecommendation.strRecommendationText;
        
//        PatrawinCase *objPatraCaseBO=self.arrRecommendationListBO[indexPath.row];
//        NSString *strRecommendationText2= objPatraCaseBO.Recommendation_Text;
        cell.textLabel.text = strRecommendationText2;
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.font = [UIFont systemFontOfSize:IS_IPAD ? 20 : 16];
    }
    
    return cell;
}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark - UITableView Delegates

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.arrRecommendationListBO.count==0)
    {
        
    }
    else
    {
        RecommendationsListBO *objPatraCaseBO=[self.arrRecommendationListBO objectAtIndex:indexPath.row];
        [self ViewForRecomendationsShow:NO];
        
        UITextView *txtV=(UITextView *)[self.scrollView viewWithTag:kTxtVStatsRecommendation];
        txtV.text=objPatraCaseBO.strRecommendationText;
        self.strGlobalRecommendationId=objPatraCaseBO.strRecommendationId;
        
        UIButton *btn=(UIButton *)[self.scrollView viewWithTag:kBtnSelectStatsRecommendation];
        btn.selected=YES;
    }
}

#pragma mark- UITextView Delegates

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (IS_IPHONE)
    {

    UITextView *txtf=(UITextView *)[self.scrollView viewWithTag:kTxtVStatsComment];
    int y=txtf.frame.origin.y-80;
    
    if (textView.tag==kTxtVStatsComment)
    {
        [self.scrollView setContentOffset:CGPointMake(0,IS_IPHONE_5? (y):(y)) animated:YES];
    }
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (IS_IPHONE)
    {

    if ([text isEqualToString:@"\n"])
    {
        [self.scrollView setContentOffset:CGPointZero animated:YES];
        [textView resignFirstResponder];
        return YES;
    }
    }
    return YES;
}

#pragma mark - Scroll View Delegate Methods

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    [self.view endEditing:YES];
}

#pragma mark -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchCompanyDatawithSelectedBo:(PatrawinCase *)selectedcase
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    //Company Data.
    
    NSEntityDescription *entityDesc2 = [NSEntityDescription entityForName:@"StasDetailList" inManagedObjectContext:context];
    NSFetchRequest *request2 = [[NSFetchRequest alloc] init];
    [request2 setEntity:entityDesc2];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"caseId = %@", selectedcase.case_id];
    request2.predicate=predicate;

    NSError *error2;
    NSArray *fetchedArray2 = [context executeFetchRequest:request2 error:&error2];
    
    if (fetchedArray2.count != 0)
    {
        StasDetailList *managedObj=[fetchedArray2 objectAtIndex:0];
        managedObj.caseComment=selectedcase.comment_case;
        managedObj.recommendationText=selectedcase.Recommendation_Text;
        NSError *error;
        [context save : &error];
    }
}




-(void)selectedActionSheet{
    
    UIActionSheet *actionsheet;
    switch (buttonValue)
    {
        case 0:
            actionsheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Accept" otherButtonTitles:@"Decline", nil];
            actionsheet.tag = 0;
            break;
            
        case 1:
            actionsheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Accept" otherButtonTitles:@"Decline",@"Wait", nil];
            actionsheet.tag = 1;
            break;
        default:
            break;
    }
    
    [actionsheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex)
    {
        case 0:
            buttonSelected = 1;
            break;
            
        case 1:
            buttonSelected = 2;
            break;
            
        case 2:
            buttonSelected = 3;
            break;
            
        default:
            break;
    }
    
}

@end

