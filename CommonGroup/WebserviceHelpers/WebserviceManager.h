//
//  WebserviceManager.h
//  NellPad
//
//  Created by apple on 23/03/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

enum _webserviceMethodType
{
    WebServiceMethodTypeGet,
    WebserviceMethodTypePost
};
typedef enum _webserviceMethodType WebserviceMethodType;


@interface WebserviceInputParameter : NSObject

@property (nonatomic, strong) NSString *relativeWebservicePath;
@property (nonatomic, strong) NSMutableDictionary *dict_postParameters;
@property (nonatomic, strong) NSMutableDictionary *dict_getParameters;
@property (nonatomic, assign) WebserviceMethodType methodType;

@end

typedef void(^completionBlock)(id response, NSError *error);

@interface WebserviceManager : NSObject

+(void) callWebserviceWithInputParame:(WebserviceInputParameter *)input completion:(completionBlock)blk;
+(id) callWebserviceWithInputParams:(WebserviceInputParameter *)input;

@end
