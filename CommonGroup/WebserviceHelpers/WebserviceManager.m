//
//  WebserviceManager.m
//  NellPad
//
//  Created by apple on 23/03/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "WebserviceManager.h"


@implementation WebserviceInputParameter

-(NSMutableDictionary *)dict_postParameters
{
    if (_dict_postParameters == nil)
        _dict_postParameters = [[NSMutableDictionary alloc] init];
    
    return _dict_postParameters;
}

-(NSMutableDictionary *)dict_getParameters
{
    if (_dict_getParameters == nil)
        _dict_getParameters = [[NSMutableDictionary alloc] init];
    
    return _dict_getParameters;
}

@end

@implementation WebserviceManager

+(void) callWebserviceWithInputParame:(WebserviceInputParameter *)input completion:(completionBlock)blk
{
    
    if (!IS_NETWORK_AVAILABLE())
    {
        //Error not rechability
        blk(nil,[NSError noNetowrkError]);
        return;
    }
    
    __block completionBlock localBlock = [blk copy];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        SHOW_LOADING();
    });
    
    
    NSString *paramValues = @"";
    NSDictionary *dictValues = input.methodType == WebServiceMethodTypeGet ? input.dict_getParameters : input.dict_postParameters;
    
    for (NSString *key in dictValues)
    {
        if (paramValues.length == 0)
            paramValues = [NSString stringWithFormat:@"%@=%@",key,[dictValues objectForKey:key]];
        else
            paramValues = [NSString stringWithFormat:@"%@&%@=%@",paramValues,key,[dictValues objectForKey:key]];
    }
    
    NSString *fullURL = @"";
    if (input.methodType == WebServiceMethodTypeGet)
    {
        fullURL = [NSString stringWithFormat:@"%@%@?%@",kWS_BaseURL, input.relativeWebservicePath,paramValues];
    }
    else {
        
        fullURL = [NSString stringWithFormat:@"%@%@",kWS_BaseURL,input.relativeWebservicePath];
   
    }
    
    fullURL = [fullURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullURL]];
    if (input.methodType == WebserviceMethodTypePost)
    {
        
        NSData *postData = [paramValues dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
        NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
        [urlRequest setTimeoutInterval:180];
        
        NSString *requestBody = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
    }
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            HIDE_LOADING();
        });
        
        if (err) {
            localBlock(nil, err);
            localBlock = nil;
            return;
        }
        
        localBlock(data, nil);
        localBlock = nil;
        
    }];}



+(id) callWebserviceWithInputParams:(WebserviceInputParameter *)input
{
    
    if (!IS_NETWORK_AVAILABLE())
    {
        //Error not rechability
//        blk(nil,[NSError noNetowrkError]);
        return [NSError noNetowrkError];
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        SHOW_LOADING();
    });
    
    
    NSString *paramValues = @"";
    NSDictionary *dictValues = input.methodType == WebServiceMethodTypeGet ? input.dict_getParameters : input.dict_postParameters;
    
    for (NSString *key in dictValues)
    {
        if (paramValues.length == 0)
            paramValues = [NSString stringWithFormat:@"%@=%@",key,[dictValues objectForKey:key]];
        else
            paramValues = [NSString stringWithFormat:@"%@&%@=%@",paramValues,key,[dictValues objectForKey:key]];
    }
    
    NSString *fullURL = @"";
    if (input.methodType == WebServiceMethodTypeGet)
    {
        fullURL = [NSString stringWithFormat:@"%@%@?%@",kWS_BaseURL, input.relativeWebservicePath,paramValues];
    }
    else {
        fullURL = [NSString stringWithFormat:@"%@%@",kWS_BaseURL,input.relativeWebservicePath];
    }
    
    fullURL = [fullURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullURL]];
    if (input.methodType == WebserviceMethodTypePost)
    {
        
        NSData *postData = [paramValues dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
        NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
        [urlRequest setTimeoutInterval:180];
        
        NSString *requestBody = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        [urlRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
    }
    NSError *err;
    NSURLResponse *response;
    NSData *dataRes=[NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&err];
    dispatch_async(dispatch_get_main_queue(), ^{
        HIDE_LOADING();
    });

    return dataRes;
    
}
@end
