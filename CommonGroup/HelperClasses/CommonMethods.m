//
//  CommonMethods.m
//  BarCodeReader
//
//  Created by Tarun Khosla on 25/04/13.
//
//

#import "CommonMethods.h"
#import <QuartzCore/QuartzCore.h>

@implementation CommonMethods

#pragma mark- show Alert View

+(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show]; 
}   

#pragma mark- get current screen height

+(CGSize)getCurrentScreenHeight {
    CGSize size = [[UIScreen mainScreen]bounds].size; 
    return size;
}

//
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
//
+ (UIImage *)aspectScaleImage:(UIImage *)image toSize:(CGSize)size
{
    if (image.size.height < image.size.width)
    {
        float ratio = size.height/image.size.height;
        CGSize newSize = CGSizeMake(image.size.width*ratio, size.height);
        
        UIGraphicsBeginImageContext(newSize);
        [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    }
    else
    {
        float ratio = size.width/image.size.width;
        CGSize newSize = CGSizeMake(size.width, image.size.height*ratio);
        
        UIGraphicsBeginImageContext(newSize);
        [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    }
    UIImage *aspectScaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return aspectScaledImage;
}

 
+(BOOL) validateEmail:(NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}



#pragma mark- show laoding view with text

+(UIView *)showLoadingViewWithText:(NSString*)loadingText {
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        backView.frame =  CGRectMake(0, 0, 320, 568);
    }
    else {
        backView.frame =  CGRectMake(0, 0, 768, 1024);
        
    }
    backView.tag = 12678;
    //backImageView.exclusiveTouch = FALSE;
    backView.alpha = 0.7;
    backView.backgroundColor = [UIColor blackColor];
    UIView *loadingView = [[UIView alloc] initWithFrame:CGRectMake(60, 200, 200, 40)];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        loadingView.frame =  CGRectMake(60, 155, 200, 40);
    } 
    else { 
        loadingView.frame =  CGRectMake(320, 400, 150, 60); 
    }  
    loadingView.backgroundColor = [UIColor blackColor]; // 238,84,70 //117 166 105
    loadingView.backgroundColor = [UIColor colorWithRed:0.4588 green:0.650 blue:0.411 alpha:1.0];
    loadingView.tag = 1267;
    [loadingView.layer setCornerRadius:10.0];
    [loadingView.layer setBorderWidth:2.0];
    [loadingView.layer setBorderColor:[UIColor clearColor].CGColor];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.frame = CGRectMake(15, 10, 20, 20);
    [activityIndicator startAnimating];
    [loadingView addSubview:activityIndicator];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(45, 0, 150, 40)];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setText:loadingText];
    label.numberOfLines = 2;
    [label setFont:[UIFont boldSystemFontOfSize:13.0]];
    label.minimumScaleFactor = 12.0;
    [label setTextColor:[UIColor whiteColor]];
    [loadingView addSubview:label];
    
    [backView addSubview:loadingView];
    return backView;
}

#pragma mark- set unique password pin
+(void)setUniquePassword:(NSString *)password
{
    [USER_DEFAULTS setObject:password forKey:@"Unique_Pin"];
    [USER_DEFAULTS synchronize];
}

#pragma mark- get unique pin

+(NSString *)getUniquePin {
    if ([[USER_DEFAULTS objectForKey:@"Unique_Pin"] length]) {
        return [USER_DEFAULTS objectForKey:@"Unique_Pin"];
    }
    else {
        return nil;
    }
}

#pragma mark- get top navigation bar with text

+(UIImageView *)getTopBarImageViewWithText:(NSString *)textString {
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    imageView.image = [UIImage imageNamed:@"top_bar.png"];
    if ([textString length] && textString != nil) {
        UILabel *textCenterLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 8, 200, 28)];
        textCenterLabel.textColor = [UIColor whiteColor];
        textCenterLabel.textAlignment = NSTextAlignmentCenter;
        textCenterLabel.backgroundColor = [UIColor clearColor];
        textCenterLabel.font = [UIFont boldSystemFontOfSize:18.0];
        textCenterLabel.text = textString;
        [imageView addSubview:textCenterLabel];
    }
    return imageView;
}

+(CGSize)calculateSizeForMaxSize:(CGSize)maxSize forText:(NSString *)str WithFont:(UIFont *)font
{
//  CGSize expectedLblMsgSize = [str sizeWithFont:font constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];
    CGSize expectedLblMsgSize = [str sizeWithFont:font constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];
    return expectedLblMsgSize;
    
}

@end
