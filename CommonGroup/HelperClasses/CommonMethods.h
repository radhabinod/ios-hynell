//
//  CommonMethods.h
//  iRemedy
//
//  Created by Tarun Khosla on 05/06/13.
//  Copyright (c) 2013 Nikhil Bansal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonMethods : NSObject

+(UIView *)showLoadingViewWithText:(NSString*)loadingText;
+(CGSize)getCurrentScreenHeight;
+(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
+(NSString *)getUniquePin;
+(void)setUniquePassword:(NSString *)password;
+(UIImageView *)getTopBarImageViewWithText:(NSString *)textString;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+ (UIImage *)aspectScaleImage:(UIImage *)image toSize:(CGSize)size;
//+(UIImage *)resizeImage:(UIImage *)image withWidth:(int) width withHeight:(int) height;
+(BOOL) validateEmail:(NSString *) email;
+(CGSize)calculateSizeForMaxSize:(CGSize)maxSize forText:(NSString *)str WithFont:(UIFont *)font;

@end
