//
//  NSError+CustomErrors.h
//  NellPad
//
//  Created by apple on 23/03/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const int ERROR_CODE_NONETWORK;
extern const int ERROR_CODE_EXCEPTION;

@interface NSError (CustomErrors)

-(BOOL) isErrorOfKindNoNetwork;

+(NSError *) noNetowrkError;

@end
