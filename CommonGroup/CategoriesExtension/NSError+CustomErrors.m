//
//  NSError+CustomErrors.m
//  NellPad
//
//  Created by apple on 23/03/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "NSError+CustomErrors.h"

const int ERROR_CODE_NONETWORK = -1001;
const int ERROR_CODE_EXCEPTION = -1002;

@implementation NSError (CustomErrors)

-(BOOL) isErrorOfKindNoNetwork
{
    return self.code == ERROR_CODE_NONETWORK;
}


+(NSError *) noNetowrkError
{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey : NSLocalizedString(@"Alert.Message.NoNetwork", @"") };
    NSError *error = [[NSError alloc] initWithDomain:NSLocalizedString(@"Alert.Message.Title", @"")
                                                code:ERROR_CODE_NONETWORK
                                            userInfo:userInfo];
    
    return error;
}

@end
