//
//  ApplicationConstants.h
//  NellPad
//
//  Created by apple on 23/03/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#ifndef NellPad_ApplicationConstants_h
#define NellPad_ApplicationConstants_h

#import "WebserviceManager.h"
#import "Reachability.h"
#import "NSError+CustomErrors.h"
#import "CommonMethods.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

//Convert RGB From Hex
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

// iOS Versions Checks
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define USR_DEFAULTS(_str_)             [[NSUserDefaults standardUserDefaults]objectForKey:_str_]
#define USER_DEFAULTS                   [NSUserDefaults standardUserDefaults]
#define USER_LOGINID                    [NSUserDefaults standardUserDefaults]

#define SCREEN_WINDOW                   [[[UIApplication sharedApplication] delegate] window]
#define HIDE_LOADING()                  [MBProgressHUD hideHUDForView:SCREEN_WINDOW animated:NO]
#define SHOW_LOADING()                  [MBProgressHUD showHUDAddedTo:SCREEN_WINDOW animated:NO]
#define IS_NETWORK_AVAILABLE()          [[Reachability reachabilityForInternetConnection] currentReachabilityStatus]


#define is_iOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define NUMBER_INT_TO_STRING(_numberObj_) _numberObj_ ? [NSString stringWithFormat:@"%d", [_numberObj_  integerValue]] : @""
#define NUMBER_FLOAT_TO_STRING(_numberObj_) _numberObj_ ? [NSString stringWithFormat:@"%.2f", [_numberObj_ floatValue]] : @""

// DLog will output like NSLog only when the DEBUG variable is set
#ifdef  TARGET_IPHONE_SIMULATOR //DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif

#define kShouldLogin 1
//#define kUserName @"shivali@impingeonline.com" //marwan.semaan@wermlandsoftware.se
//#define kUserPwd @"1234"

#define IS_VALID_STRING(_str_) _str_ && ![_str_ isEqual:[NSNull null]] && ![_str_ isEqualToString:@"(null)"]  ? TRUE : FALSE

#define GET_VALID_STRING(_str_) _str_ && ![_str_ isEqual:[NSNull null]] && ![_str_ isEqualToString:@"(null)"]  ? _str_ : @""

#define GetBundleImagePath(_imgName_,_ext_) [[NSBundle mainBundle] pathForResource:_imgName_ ofType:_ext_]


// these copy of url's

//#define kWS_BaseURL @"http://app.hynell.se/hynellapp/"

//#define kApp_URL @"http://app.hynell.se/"









#define kWS_BaseURL @"http://app.hynell.se/hynellapptest/"

//#define kWS_BaseURL @"http://app.hynell.se/hynellapp/"

#define kApp_URL @"http://app.hynell.se/"


#endif
