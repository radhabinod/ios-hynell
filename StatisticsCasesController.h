//
//  StatisticsCasesController.h
//  NellPad
//
//  Created by Rakesh Kumar on 06/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <QuickLook/QuickLook.h>


#define KlblStatsCountry 2356
#define KlblTitle 6786
#define kBtnStatsCompose 56565
#define kBtnStatsShare 45458
#define kBtnStatsRegistration 45546
#define kBtnStatsUpdate 45455
#define kBtnStatsLeft 5655
#define kBtnStatsRight 8974
#define kBtnStatsFigure 21324
#define kTxtVStatsComment 3445
#define kTxtVStatsRecommendation 98677
#define kBtnSelectStatsRecommendation 12465
#define kbtnStatsApproved 4556
#define kbtnStatsDecline 5677
#define kbtnStatsWeight 5678

@interface StatisticsCasesController : UIViewController<UITextViewDelegate,UIWebViewDelegate, MFMailComposeViewControllerDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIAlertViewDelegate,QLPreviewControllerDataSource,QLPreviewControllerDelegate>

@property(nonatomic,strong) NSMutableArray *arrDataHdng;
@property(nonatomic,strong) NSMutableArray *arrayAllCases;
@property(nonatomic,strong) NSString *strSelectedIndex;
@property(nonatomic,strong) UILabel *lbl_CaseName;
@property(nonatomic,strong) UIView *viewForCaseData;
@property(nonatomic,strong) UIScrollView *scrollView;
@property (strong,nonatomic) NSDictionary *responseDict;
@property(nonatomic,strong) UIView *ViewForRecommendations;
@property(nonatomic,strong) UIView *ViewForRecommendationsBG;
@property(nonatomic,strong) NSArray *arrRecommendationListBO;
@property(nonatomic,strong) NSString *strGlobalRecommendationId;



@end

