//
//  StatsLisingController.m
//  NellPad
//
//  Created by Rakesh Kumar on 05/05/14.
//  Copyright (c) 2014 Gagan Joshi. All rights reserved.
//

#import "StatsLisingController.h"
#import "PatrawinCase.h"
#import "StatisticsCasesController.h"
#import "RecommendationsListBO.h"
#import "StasDetailList.h"
#import "RecommendationsList.h"
#import "CompanyList.h"
#import "CompanyBO.h"
#import "DataSyncBO.h"
#import <objc/message.h>
#import "CasesVC.h"
#import "CompaniesListVC.h"


@interface StatsLisingController ()
{
    UISearchBar *searchBar1;
    NSArray *searchResults;
    NSMutableArray *arrayListOfCompaniesName, *arrayListOfCompaniesId;
    CompanyBO *companyBo;
    UIRefreshControl *refresh;
    DataSyncBO *dataBO;
   CasesVC * obj_CaseVC;
    
    NSString *other5;
    
    NSSet *uniqueCompany;
    NSMutableArray *compArray;
    NSArray *comName;
    NSArray *search;
    
    NSMutableArray *finalArrayValueZeroStat;
}
@end

@implementation StatsLisingController
@synthesize arrStatsData,tblStatsData,actionSheetSort,txtVSortSelection,btnDropDown,arrRecommendations,strSelectedTag,responseDict, textSortingCompany,statsString,searchResults;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    obj_CaseVC  = [[CasesVC alloc] init];
    search = [[NSArray alloc]init];

    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)){
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
        {
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
        }
    }
    
    arrayListOfCompaniesId = [[NSMutableArray alloc] init];
    
    arrayListOfCompaniesName = [[NSMutableArray alloc] init];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0xF0F0F0),NSForegroundColorAttributeName,[UIFont systemFontOfSize: 18], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x4174DA)];
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(0xFFFFFF)];
    
    self.view.layer.borderColor = UIColorFromRGB(0xF0F0F0).CGColor;
    
    
    self.arrStatsData=[[NSMutableArray alloc]init];
    self.arrRecommendations=[[NSMutableArray alloc]init];
    searchResults = [[NSArray alloc] init];
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    NSString *actionSheetTitle = @"Choose Sorting Type";
    NSString *other1 = @"Sort By Latest";
    NSString *other2 = @"Sort By Oldest";
    NSString *other3 = @"Sort By Deadline";
    NSString *other4 = @"All Cases";
    
    if ([strKey isEqualToString:@"0"])
    {
        other5 = @"Sort By Company";
    }
    if ([strKey isEqualToString:@"1"])
    {
        other5 = nil;
    }
    
    
    NSString *cancelTitle = @"Cancel";
    
    actionSheetSort = [[UIActionSheet alloc] initWithTitle:actionSheetTitle delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:other4,other1,other2,other3,other5, nil];
   
    txtVSortSelection = [[UITextField alloc] init];
    txtVSortSelection.frame = CGRectMake(20, 16, deviceWidth-36, 32);
   // txtVSortSelection.layer.cornerRadius = 5.0;
    txtVSortSelection.userInteractionEnabled = NO;
    txtVSortSelection.layer.borderWidth = 1.0f;
    txtVSortSelection.text = @"   Sort By Latest";
    
    txtVSortSelection.font = [UIFont systemFontOfSize:IS_IPAD ? 20 : 14];
    
    txtVSortSelection.textColor = [UIColor darkGrayColor];
    txtVSortSelection.clipsToBounds = YES;
    txtVSortSelection.layer.borderColor = UIColorFromRGB(0xA9A9A9).CGColor;
    txtVSortSelection.delegate = self;
    txtVSortSelection.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    txtVSortSelection.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:txtVSortSelection];
    
    btnDropDown = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btnDropDown.frame = CGRectMake(20, 20, deviceWidth-40, 30);
    btnDropDown.layer.cornerRadius = 5.0;
    
    btnDropDown.clipsToBounds = YES;
    [btnDropDown addTarget:self action:@selector(btndropDownClk) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(deviceWidth-46,18, 28, 28)];
    
    [image setImage:[UIImage imageNamed:@"arrowBgIcon"]];
    
    [self.view addSubview:image];
    
    [self.view addSubview:btnDropDown];
    
    searchBar1 = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 60, IS_IPAD ? 768 : 320, 40)];
    
    searchBar1.placeholder = @"Search Cases";
    
    searchBar1.delegate = self;
    
    [self.view addSubview:searchBar1];
    
    [self.arrRecommendations removeAllObjects];
    
    [self.arrStatsData removeAllObjects];
    
    
     [self fetchFromDatabase : strSelectedTag];
    
   // [self CallForStatsData];
    
    tblStatsData =[[UITableView alloc]initWithFrame:CGRectMake(0, searchBar1.frame.size.height+searchBar1.frame.origin.y, deviceWidth, deviceHeight-160) style:UITableViewStylePlain];
    tblStatsData.delegate=self;
    tblStatsData.dataSource=self;
    tblStatsData.rowHeight = IS_IPAD ? 61 : 71.0f;
    // return IS_IPAD ? 61 : 71.0f;
    tblStatsData.separatorStyle=UITableViewCellSeparatorStyleNone;
    tblStatsData.backgroundColor=[UIColor whiteColor];
    //    tblStatsData.showsHorizontalScrollIndicator=NO;
    //    tblStatsData.showsVerticalScrollIndicator=NO;
    tblStatsData.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:tblStatsData];
    

    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    button.frame = CGRectMake(0, 64, 768, 100);
//    
//    [button addTarget:self action:@selector(resignKeyboard) forControlEvents:UIControlEventTouchUpInside];
//    
//    [button sendSubviewToBack:self.view];
//    
//    [self.view addSubview:button];
    
    refresh = [[UIRefreshControl alloc]init];
    [self.tblStatsData addSubview:refresh];
    [refresh addTarget:self action:@selector(refreshTableStats) forControlEvents:UIControlEventValueChanged];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack) ];
    
    
    UIBarButtonItem *btnRefresh=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"refresh-top"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshAllCases)];
    
    self.navigationItem.rightBarButtonItem=btnRefresh;


}

- (void) buttonBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
    //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}

-(void)refreshAllCases
{
   
    [[[UIAlertView alloc]initWithTitle:@"Update Cases!" message:@"Do you want to update cases?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] show ];
    
}

#pragma mark - uialert view methods==========

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        [refresh endRefreshing];
        
    } else if(buttonIndex == 1) {
        
      //  [self refreshTableStats];
        
        [self.arrStatsData removeAllObjects];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"StatsListdownloaded" object:nil];
        DataSyncBO *refreshData = [[DataSyncBO alloc]init];
        [refreshData deleteEntitiesWRTSelectedStr:@"StasDetailList" withstr:strSelectedTag];
        [refreshData deleteAllEntities:@"CompanyList"];
        [refreshData CallForStatsData:strSelectedTag];
        [self.arrRecommendations removeAllObjects];

        
    }
}



//- (void) resignKeyboard
//{
//    [searchBar1 resignFirstResponder];
//}

-(void)viewWillAppear:(BOOL)animated
{
     [super viewWillAppear:animated];
}

-(void)refreshTableStats
{
    [[[UIAlertView alloc]initWithTitle:@"Update Cases!" message:@"Do you want to update cases?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] show ];
    
//    [self.arrStatsData removeAllObjects];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"StatsListdownloaded" object:nil];
//    DataSyncBO *refreshData = [[DataSyncBO alloc]init];
//    [refreshData deleteEntitiesWRTSelectedStr:@"StasDetailList" withstr:strSelectedTag];
//    [refreshData deleteAllEntities:@"CompanyList"];
//    [refreshData CallForStatsData:strSelectedTag];
//    [self.arrRecommendations removeAllObjects];
    
}

-(void) DataReload
{
    [self fetchFromDatabase:strSelectedTag];
    [refresh endRefreshing];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
//    [dataBO deleteAllEntities:@"RecommendationsList"];
 //   [self fetchCompanyData];
}

//-(void) CompanyReload
//{
//    [self fetchCompanyData];
//    [refresh endRefreshing];
//    [[NSNotificationCenter defaultCenter]removeObserver:self];
//}

//- (void) buttonDropdownCompanyName
//{
//    [self ShowViewForAgentsList:YES];
//}

#pragma mark - UISearchField Delegates

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] > 0) {
        
        //        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.case_type contains[c] %@", searchText];
        
        NSMutableArray *sResult = @[].mutableCopy;
        
        for (PatrawinCase *cs in self.arrStatsData) {
            NSRange r = [cs.case_id rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (r.length > 0) {
                //NSLog(@"%@", cs.case_id);
                [sResult addObject:cs];
            }
        }
        
        if (searchResults) {
            searchResults = nil;
        }
        
        searchResults = [NSArray arrayWithArray:sResult];
        sResult = nil;
        
    } else {
        searchResults = [NSArray arrayWithArray:self.arrStatsData];
    }
    //    [searchResults addObjectsFromArray:[self.casesArray filteredArrayUsingPredicate:resultPredicate]];
    
    [tblStatsData reloadData];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [theSearchBar resignFirstResponder];
}

- (BOOL)shouldAutorotate {
    if ([self interfaceOrientation] != UIInterfaceOrientationPortrait) {
        return YES;
    }
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

-(void)CallForStatsData
{
       NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
       WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
       inputPram.methodType = WebserviceMethodTypePost;
       inputPram.relativeWebservicePath = @"innerStatisticsOnBasesOfAccesstoken";
    
       NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
       NSString *strTag=strSelectedTag;
    
       [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
       [inputPram.dict_postParameters setObject:GET_VALID_STRING(strTag) forKey:@"case"];
    
       if ([strKey isEqualToString:@"1"])
       {
           [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"2") forKey:@"status_id"];
       }
       else if ([strKey isEqualToString:@"0"])
       {
           [inputPram.dict_postParameters setObject:GET_VALID_STRING(@"1") forKey:@"status_id"];
       }
    
       [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error) {
           if (error) {
               if ([error isErrorOfKindNoNetwork])
                   [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
               return ;
           }
           
           error = nil;
           self.responseDict =nil;
           self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
           
           [self assigningDataStats];
   }];
}

-(void)assigningDataStats
{
    NSLog(@"Response Dict = %@", self.responseDict);
    
    NSArray *arrTemp=[self.responseDict objectForKey:@"data"];
    
    NSArray *arrRecommendation=responseDict[@"recommendation_list"];
    
    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
    
    if ([strKey isEqualToString:@"1"])
    {
        //        for (int i=0; i<arrTemp.count; i++)
        //        {
        for (StasDetailList *statList in arrTemp) {
            
            PatrawinCase *objPatraCaseBO=[[PatrawinCase alloc]init];
            
            objPatraCaseBO.account_cost = statList.accountCost;
            objPatraCaseBO.agent_id = statList.agentId;
            objPatraCaseBO.arrived_date = [NSString stringWithFormat:@"%@",statList.arrivedDate];
            //objPatraCaseBO.case_id = [NSString stringWithFormat:@"%d",[statList.caseId intValue]];
            objPatraCaseBO.case_id = statList.caseId;
            objPatraCaseBO.comment_case = statList.caseComment;
            objPatraCaseBO.case_type = statList.caseType;
            objPatraCaseBO.comment = statList.comment;
            objPatraCaseBO.country_code = statList.countryCode;
            objPatraCaseBO.customer_id = [NSString stringWithFormat:@"%d",[statList.customerId intValue]];
            objPatraCaseBO.customer_name = statList.customerName;
            objPatraCaseBO.deadline = statList.deadline;
            objPatraCaseBO.figure = statList.figure;
            objPatraCaseBO.link = statList.link;
            objPatraCaseBO.patent_number = statList.patentNumber;
            objPatraCaseBO.title = statList.title;
            objPatraCaseBO.status = statList.status;
            objPatraCaseBO.keyword = statList.keyword;
            objPatraCaseBO.Recommendation_Text = statList.recommendationText;
            objPatraCaseBO.deadline_code = statList.deadlineCode;
            
            //            objPatraCaseBO.accountCost=[[[arrTemp objectAtIndex:statList] objectForKey:@"account_cost"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:statList] objectForKey:@"account_cost"]:@"";
            //            objPatraCaseBO.agentId=[[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"]:@"";
            //            objPatraCaseBO.arrivedDate=[[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"]:@"";
            //            objPatraCaseBO.caseId=[[[arrTemp objectAtIndex:i] objectForKey:@"case_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_id"]:@"";
            //            objPatraCaseBO.comment_case=[[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"]:@"";
            //            objPatraCaseBO.case_type=[[[arrTemp objectAtIndex:i] objectForKey:@"case_type"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_type"]:@"";
            //            objPatraCaseBO.comment=[[[arrTemp objectAtIndex:i] objectForKey:@"comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"comment"]:@"";
            //            objPatraCaseBO.country_code=[[[arrTemp objectAtIndex:i] objectForKey:@"country_code"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"country_code"]:@"";
            //            objPatraCaseBO.customer_id=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"]:@"";
            //            objPatraCaseBO.customer_name=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"]:@"";
            //            objPatraCaseBO.deadline=[[[arrTemp objectAtIndex:i] objectForKey:@"deadline"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"deadline"]:@"";
            //            objPatraCaseBO.figure=[[[arrTemp objectAtIndex:i] objectForKey:@"figure"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"figure"]:@"";
            //            objPatraCaseBO.link=[[[arrTemp objectAtIndex:i] objectForKey:@"link"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"link"]:@"";
            //            objPatraCaseBO.patent_number=[[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"]:@"";
            //            objPatraCaseBO.status=[[[arrTemp objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"status"]:@"";
            //            objPatraCaseBO.title=[[[arrTemp objectAtIndex:i] objectForKey:@"title"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"title"]:@"";
            
            //            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            //            objRecommendationListBO.strRecommendationId=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Id"] ;
            //            objRecommendationListBO.strRecommendationText=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Text"];
            //            objRecommendationListBO.strRecommendationStatus=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Status"];
            //            objPatraCaseBO.objRecommendationListBO=objRecommendationListBO;
            
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[statList.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = statList.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[statList.recommendationStatus intValue]];
            objPatraCaseBO.objRecommendationListBO=objRecommendationListBO;
            [self.arrStatsData addObject:objPatraCaseBO];
            
            searchResults = [NSArray arrayWithArray:self.arrStatsData];
        }
        
    }
    else if ([strKey isEqualToString:@"0"])
    {
        
        
        for (StasDetailList *statList in arrTemp) {
            
            PatrawinCase *objPatraCaseBO=[[PatrawinCase alloc]init];
            objPatraCaseBO.account_cost = statList.accountCost;
            objPatraCaseBO.agent_id = statList.agentId ;
            objPatraCaseBO.arrived_date = [NSString stringWithFormat:@"%@",statList.arrivedDate];
            //objPatraCaseBO.case_id = [NSString stringWithFormat:@"%d",[statList.caseId intValue]];
            objPatraCaseBO.case_id = statList.caseId;
            objPatraCaseBO.comment_case = statList.caseComment;
            objPatraCaseBO.case_type = statList.caseType;
            objPatraCaseBO.comment = statList.comment;
            objPatraCaseBO.country_code = statList.countryCode;
            objPatraCaseBO.customer_id = [NSString stringWithFormat:@"%d",[statList.customerId intValue]];
            objPatraCaseBO.customer_name = statList.customerName;
            objPatraCaseBO.deadline = statList.deadline;
            objPatraCaseBO.figure = statList.figure;
            objPatraCaseBO.link = statList.link;
            objPatraCaseBO.patent_number = statList.patentNumber;
            objPatraCaseBO.title = statList.title;
            objPatraCaseBO.status = statList.status;
            objPatraCaseBO.keyword = statList.keyword;
            objPatraCaseBO.Recommendation_Text = statList.recommendationText;
            objPatraCaseBO.deadline_code = statList.deadlineCode;
            
            
            //              objPatraCaseBO.Recommendation_Text = statList.recommendationText;
            
            //            objPatraCaseBO.account_cost=[[[arrTemp objectAtIndex:i] objectForKey:@"account_cost"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"account_cost"]:@"";
            //            objPatraCaseBO.agent_id=[[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"]:@"";
            //            objPatraCaseBO.arrived_date=[[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"]:@"";
            //            objPatraCaseBO.case_id=[[[arrTemp objectAtIndex:i] objectForKey:@"case_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_id"]:@"";
            //            objPatraCaseBO.comment_case=[[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"]:@"";
            //            objPatraCaseBO.case_type=[[[arrTemp objectAtIndex:i] objectForKey:@"case_type"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_type"]:@"";
            //            objPatraCaseBO.comment=[[[arrTemp objectAtIndex:i] objectForKey:@"comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"comment"]:@"";
            //            objPatraCaseBO.country_code=[[[arrTemp objectAtIndex:i] objectForKey:@"country_code"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"country_code"]:@"";
            //            objPatraCaseBO.customer_id=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"]:@"";
            //            objPatraCaseBO.customer_name=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"]:@"";
            //            objPatraCaseBO.deadline=[[[arrTemp objectAtIndex:i] objectForKey:@"deadline"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"deadline"]:@"";
            //            objPatraCaseBO.figure=[[[arrTemp objectAtIndex:i] objectForKey:@"figure"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"figure"]:@"";
            //            objPatraCaseBO.link=[[[arrTemp objectAtIndex:i] objectForKey:@"link"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"link"]:@"";
            //            objPatraCaseBO.patent_number=[[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"]:@"";
            //            objPatraCaseBO.status=[[[arrTemp objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"status"]:@"";
            //            objPatraCaseBO.title=[[[arrTemp objectAtIndex:i] objectForKey:@"title"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"title"]:@"";
            
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[statList.recommendationID intValue]];
            objRecommendationListBO.strRecommendationText = statList.recommendationText;
            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[statList.recommendationStatus intValue]];
            objPatraCaseBO.objRecommendationListBO=objRecommendationListBO;
            [self.arrStatsData addObject:objPatraCaseBO];
            
            
            searchResults = [NSArray arrayWithArray:self.arrStatsData];
        }
        
        
        for (RecommendationsList *dictRecommendation in arrRecommendation)
        {
            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
            objRecommendationListBO.strRecommendationId=dictRecommendation.recommendation_ID;
            objRecommendationListBO.strRecommendationText=dictRecommendation.recommendation_Text;
            if (![self.arrRecommendations containsObject:dictRecommendation.recommendation_Text]) {
                [self.arrRecommendations addObject:objRecommendationListBO];
                
            }
            
        }
        
    }
    [self sortCasesDescending];
    
    [self getCompany];
    
    //    if ([strSelectedTag isEqualToString:@"1"])
    //    {
    //        self.title=@"Total Cases";
    //    }
    if ([strSelectedTag isEqualToString:@"2"])
    {
        self.title=@"Granted";
    }
    else if ([strSelectedTag isEqualToString:@"3"])
    {
        self.title=@"Deadline";
    }
    
    else if ([strSelectedTag isEqualToString:@"4"])
    {
        self.title=@"Pending";
    }
}


//-(void)assigningDataStats
//{
//    NSLog(@"Response Dict = %@", self.responseDict);
//    
//    NSArray *arrTemp=[self.responseDict objectForKey:@"data"];
//    
//    NSArray *arrRecommendation=responseDict[@"recommendation_list"];
//    
//    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
//    
//    if ([strKey isEqualToString:@"1"])
//    {
////        for (int i=0; i<arrTemp.count; i++)
////        {
//        for (StasDetailList *statList in arrTemp) {
//            
//          PatrawinCase *objPatraCaseBO=[[PatrawinCase alloc]init];
//          
//            objPatraCaseBO.account_cost = statList.accountCost;
//            objPatraCaseBO.agent_id = statList.agentId;
//            objPatraCaseBO.arrived_date = [NSString stringWithFormat:@"%@",statList.arrivedDate];
//            //objPatraCaseBO.case_id = [NSString stringWithFormat:@"%d",[statList.caseId intValue]];
//            objPatraCaseBO.case_id = statList.caseId;
//            objPatraCaseBO.comment_case = statList.caseComment;
//            objPatraCaseBO.case_type = statList.caseType;
//            objPatraCaseBO.comment = statList.comment;
//            objPatraCaseBO.country_code = statList.countryCode;
//            objPatraCaseBO.customer_id = [NSString stringWithFormat:@"%d",[statList.customerId intValue]];
//            objPatraCaseBO.customer_name = statList.customerName;
//            objPatraCaseBO.deadline = statList.deadline;
//            objPatraCaseBO.figure = statList.figure;
//            objPatraCaseBO.link = statList.link;
//            objPatraCaseBO.patent_number = statList.patentNumber;
//            objPatraCaseBO.title = statList.title;
//            objPatraCaseBO.status = statList.status;
 //          objPatraCaseBO.keyword = statList.keyword;
//
//
////            objPatraCaseBO.accountCost=[[[arrTemp objectAtIndex:statList] objectForKey:@"account_cost"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:statList] objectForKey:@"account_cost"]:@"";
////            objPatraCaseBO.agentId=[[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"]:@"";
////            objPatraCaseBO.arrivedDate=[[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"]:@"";
////            objPatraCaseBO.caseId=[[[arrTemp objectAtIndex:i] objectForKey:@"case_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_id"]:@"";
////            objPatraCaseBO.comment_case=[[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"]:@"";
////            objPatraCaseBO.case_type=[[[arrTemp objectAtIndex:i] objectForKey:@"case_type"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_type"]:@"";
////            objPatraCaseBO.comment=[[[arrTemp objectAtIndex:i] objectForKey:@"comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"comment"]:@"";
////            objPatraCaseBO.country_code=[[[arrTemp objectAtIndex:i] objectForKey:@"country_code"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"country_code"]:@"";
////            objPatraCaseBO.customer_id=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"]:@"";
////            objPatraCaseBO.customer_name=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"]:@"";
////            objPatraCaseBO.deadline=[[[arrTemp objectAtIndex:i] objectForKey:@"deadline"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"deadline"]:@"";
////            objPatraCaseBO.figure=[[[arrTemp objectAtIndex:i] objectForKey:@"figure"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"figure"]:@"";
////            objPatraCaseBO.link=[[[arrTemp objectAtIndex:i] objectForKey:@"link"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"link"]:@"";
////            objPatraCaseBO.patent_number=[[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"]:@"";
////            objPatraCaseBO.status=[[[arrTemp objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"status"]:@"";
////            objPatraCaseBO.title=[[[arrTemp objectAtIndex:i] objectForKey:@"title"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"title"]:@"";
//            
////            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
////            objRecommendationListBO.strRecommendationId=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Id"] ;
////            objRecommendationListBO.strRecommendationText=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Text"];
////            objRecommendationListBO.strRecommendationStatus=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Status"];
////            objPatraCaseBO.objRecommendationListBO=objRecommendationListBO;
//            
//            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
//            objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[statList.recommendationID intValue]];
//            objRecommendationListBO.strRecommendationText = statList.recommendationText;
//            objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[statList.recommendationStatus intValue]];
//            
//            objPatraCaseBO.objRecommendationListBO=objRecommendationListBO;
//           [arrStatsData addObject:objPatraCaseBO];
//            
//            searchResults = [NSArray arrayWithArray:arrStatsData];
//        }
//        
//    }
//    else if ([strKey isEqualToString:@"0"])
//    {
////        for (int i=0; i<arrTemp.count; i++)
////        {
//          for (StasDetailList *statList in arrTemp) {
//              
//            PatrawinCase *objPatraCaseBO=[[PatrawinCase alloc]init];
//            
//              objPatraCaseBO.account_cost = statList.accountCost;
//              objPatraCaseBO.agent_id = statList.agentId ;
//              objPatraCaseBO.arrived_date = [NSString stringWithFormat:@"%@",statList.arrivedDate];
//              //objPatraCaseBO.case_id = [NSString stringWithFormat:@"%d",[statList.caseId intValue]];
//              objPatraCaseBO.case_id = statList.caseId;
//              objPatraCaseBO.comment_case = statList.caseComment;
//              objPatraCaseBO.case_type = statList.caseType;
//              objPatraCaseBO.comment = statList.comment;
//              objPatraCaseBO.country_code = statList.countryCode;
//              objPatraCaseBO.customer_id = [NSString stringWithFormat:@"%d",[statList.customerId intValue]];
//              objPatraCaseBO.customer_name = statList.customerName;
//              objPatraCaseBO.deadline = statList.deadline;
//              objPatraCaseBO.figure = statList.figure;
//              objPatraCaseBO.link = statList.link;
//              objPatraCaseBO.patent_number = statList.patentNumber;
//              objPatraCaseBO.title = statList.title;
//              objPatraCaseBO.status = statList.status;
////              objPatraCaseBO.Recommendation_Text = statList.recommendationText;
 //          objPatraCaseBO.keyword = statList.keyword;
//              
////            objPatraCaseBO.account_cost=[[[arrTemp objectAtIndex:i] objectForKey:@"account_cost"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"account_cost"]:@"";
////            objPatraCaseBO.agent_id=[[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"]:@"";
////            objPatraCaseBO.arrived_date=[[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"]:@"";
////            objPatraCaseBO.case_id=[[[arrTemp objectAtIndex:i] objectForKey:@"case_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_id"]:@"";
////            objPatraCaseBO.comment_case=[[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"]:@"";
////            objPatraCaseBO.case_type=[[[arrTemp objectAtIndex:i] objectForKey:@"case_type"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_type"]:@"";
////            objPatraCaseBO.comment=[[[arrTemp objectAtIndex:i] objectForKey:@"comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"comment"]:@"";
////            objPatraCaseBO.country_code=[[[arrTemp objectAtIndex:i] objectForKey:@"country_code"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"country_code"]:@"";
////            objPatraCaseBO.customer_id=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"]:@"";
////            objPatraCaseBO.customer_name=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"]:@"";
////            objPatraCaseBO.deadline=[[[arrTemp objectAtIndex:i] objectForKey:@"deadline"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"deadline"]:@"";
////            objPatraCaseBO.figure=[[[arrTemp objectAtIndex:i] objectForKey:@"figure"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"figure"]:@"";
////            objPatraCaseBO.link=[[[arrTemp objectAtIndex:i] objectForKey:@"link"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"link"]:@"";
////            objPatraCaseBO.patent_number=[[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"]:@"";
////            objPatraCaseBO.status=[[[arrTemp objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"status"]:@"";
////            objPatraCaseBO.title=[[[arrTemp objectAtIndex:i] objectForKey:@"title"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"title"]:@"";
//              
//              RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
//              objRecommendationListBO.strRecommendationId = [NSString stringWithFormat:@"%d",[statList.recommendationID intValue]];
//              objRecommendationListBO.strRecommendationText = statList.recommendationText;
//              objRecommendationListBO.strRecommendationStatus = [NSString stringWithFormat:@"%d",[statList.recommendationStatus intValue]];
//              objPatraCaseBO.objRecommendationListBO=objRecommendationListBO;
//              [self.arrStatsData addObject:objPatraCaseBO];
//              
//              
//             
//
//            searchResults = [NSArray arrayWithArray:self.arrStatsData];
//        }
//        for (RecommendationsList *dictRecommendation in arrRecommendation)
//        {
//            RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
//            objRecommendationListBO.strRecommendationId=dictRecommendation.recommendation_ID;
//            objRecommendationListBO.strRecommendationText=dictRecommendation.recommendation_Text;
//            if (![self.arrRecommendations containsObject:dictRecommendation.recommendation_Text]) {
//                [self.arrRecommendations addObject:objRecommendationListBO];
//            }
//        }
//    }
//    [self sortCasesDescending];
//    
////    if ([strSelectedTag isEqualToString:@"1"])
////    {
////        self.title=@"Total Cases";
////    }
//     if ([strSelectedTag isEqualToString:@"2"])
//    {
//        self.title=@"Granted";
//    }
//    else if ([strSelectedTag isEqualToString:@"3"])
//    {
//        self.title=@"Deadline";
//    }
//    
//    else if ([strSelectedTag isEqualToString:@"4"])
//    {
//        self.title=@"Pending";
//    }
//}

//-(void)assigningDataCompany:(NSDictionary *)companyDict
//{
//    NSArray *arrCompanyData=[self.responseDict objectForKey:@"data"];
//    NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
//    
//    arrayListOfCompaniesName=[NSMutableArray new];
//    
//    if ([strKey isEqualToString:@"0"])
//    {
//        for (CompanyList *companyList in arrCompanyData) {
//            
//            CompanyBO *objectCompanyBo=[[CompanyBO alloc]init];
//            
//            objectCompanyBo.strCompany_Id = companyList.companyID;
//            objectCompanyBo.strCompany_name = companyList.companyName;
//            
//            [arrayListOfCompaniesName addObject:objectCompanyBo];
//        }
//    }
//}

//#pragma mark - Call For List Of Companies
//
//- (void) callForListOfCompanies
//{
//    WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
//    inputPram.methodType = WebserviceMethodTypePost;
//    inputPram.relativeWebservicePath = @"listofcompanies";
//    NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
//    [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
//    [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error)
//    {
//        if (error)
//        {
//            if ([error isErrorOfKindNoNetwork])
//            
//            [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//            
//            return ;
//        }
//        
//        error = nil;
//
//        self.responseDict = nil;
//        
//        self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
//        
//        NSLog(@"Response Dict = %@", self.responseDict);
//        
//        arrayListOfCompaniesId = [[self.responseDict objectForKey:@"data"] valueForKey:@"company_id"];
//        
////         arrayListOfCompaniesName = [[self.responseDict objectForKey:@"data"] valueForKey:@"company_name"];
//        
//        arrayListOfCompaniesName = self.responseDict[@"data"];
//    }];
//}

-(void)btndropDownClk
{
    [self.view endEditing:YES];
    
        [actionSheetSort showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:@"Sort By Latest"])
    {
        txtVSortSelection.text = @"   Sort By Latest";
       [self sortCasesDescending];
    }
    else if ([buttonTitle isEqualToString:@"Sort By Oldest"])
    {
        txtVSortSelection.text = @"   Sort By Oldest";
        [self sortCasesAscending];
    }
    else if([buttonTitle isEqualToString:@"Sort By Deadline"])
    {
        txtVSortSelection.text = @"   Sort By Deadline";
        [self sortCasesByDeadline];
    }
    
    else if ([buttonTitle isEqualToString:@"All Cases"])
    {
        txtVSortSelection.text = @"   All Cases";

        //[self CallForStatsData];
        [self fetchFromDatabase:strSelectedTag];
    }
    else if ([buttonTitle isEqualToString:@"Sort By Company"])
    {
        textSortingCompany.text = @"   Sort By Company";
        
        CompaniesListVC* companiesTable = [[CompaniesListVC alloc] init];
        companiesTable.grantedSearch=search;
        companiesTable.statsString = statsString;
        companiesTable.finalArrayValueZeroStat=finalArrayValueZeroStat;
        companiesTable.statsCases=self.arrStatsData;
        
      //  NSLog(@"stats cases in statlisting:%@",self.arrStatsData);
        [self.navigationController pushViewController:companiesTable animated:YES];

    }
    else if ([buttonTitle isEqualToString:@"Cancel"])
    {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
}


//-(void)ShowViewForAgentsList:(BOOL)isShow
//{
//    [self.view endEditing:YES];
//    
//    if (self.viewForAgentsListBG)
//    {
//        [self.viewForAgentsListBG.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//        [self.viewForAgentsListBG removeFromSuperview];
//        self.viewForAgentsListBG=nil;
//    }
//    if(isShow)
//    {
//        self.viewForAgentsListBG=[[UIView alloc]initWithFrame:CGRectMake(0, IS_VERSION_GRT_7 ? 20:0, deviceWidth,IS_VERSION_GRT_7?deviceHeight-20:deviceHeight)];
//        self.viewForAgentsListBG.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
//        [APP_DELEGATE.window addSubview:self.viewForAgentsListBG];
//        
//        NSLog(@"values=%f %f %f %f",self.view.bounds.origin.x,self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height);
//        
//        self.viewForAgentsList=[[UIView alloc]initWithFrame:CGRectMake(10,50, deviceWidth-20,CGRectGetHeight(self.view.bounds)-75)];
//        
//        UIView *blueBar=[[UIView alloc]init];
//        blueBar.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:118.0/255.0 blue:226.0/255.0 alpha:1.0];
//        blueBar.frame=CGRectMake(0, 0, deviceWidth-20, 43);
//        
//        UILabel *cases=[[UILabel alloc]init];
//        cases.frame=CGRectMake(0, 0, blueBar.frame.size.width, 40);
//        cases.text=@"Choose Company";
//        cases.textAlignment=NSTextAlignmentCenter;
//        cases.textColor=[UIColor whiteColor];
//        [blueBar addSubview:cases];
//        
//        [self.viewForAgentsList addSubview:blueBar];
//        [self.viewForAgentsList bringSubviewToFront:blueBar];
//        
//        self.viewForAgentsList.backgroundColor = [UIColor whiteColor] ;
//        [self.viewForAgentsListBG addSubview:self.viewForAgentsList];
//        
//        //        CGFloat height= IS_IPAD ? self.arrUsersList.count * 55 : self.arrUsersList.count * 44;
//        //        CGFloat  heightOfTable;
//        //        CGFloat BoundHeight=CGRectGetHeight(self.view.bounds)-60;
//        //        if (height> BoundHeight)
//        //        {
//        //            heightOfTable=BoundHeight;
//        //        }
//        //        else heightOfTable=height;
//        //
//        _tbleUsers=[[UITableView alloc]initWithFrame:CGRectMake(0, 43, deviceWidth-20, IS_IPAD ? 870 : 425) style:UITableViewStylePlain];
//        _tbleUsers.delegate=self;
//        _tbleUsers.dataSource=self;
//        
//        _tbleUsers.rowHeight = IS_IPAD ? 55 : 44;
//        
//        _tbleUsers.backgroundColor=[UIColor clearColor];
//        _tbleUsers.backgroundView=nil;
//        _tbleUsers.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
//        
//        _tbleUsers.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
//        
//        [self.viewForAgentsList addSubview:_tbleUsers];
//        
//        UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
//        btnCross.frame = CGRectMake(0, 30, 24, 24);
//        [btnCross setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
//        [btnCross addTarget:self action:@selector(btnCross_Clicked) forControlEvents:UIControlEventTouchUpInside];
//        [self.viewForAgentsListBG addSubview:btnCross];
//        btnCross = nil;
//    }
//}

//-(void)btnCross_Clicked
//{
//    [self ShowViewForAgentsList:NO];
//}

#pragma mark - Table view Datasource & Delegate Methods 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tblStatsData)
    {
        return IS_IPAD ? 61 : 71.0f;
    }
    else
    return IS_IPAD ? 55 : 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tblStatsData)
    {
    if (self.arrStatsData.count==0)
    {
        return 1;
    }
        return searchResults.count;
    }
    else
    {
        return [arrayListOfCompaniesName count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
    }
       cell.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];

        [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (tableView == tblStatsData)
    {
    if (searchResults.count==0)
    {
        UILabel *lblNoRecord = [[UILabel alloc] initWithFrame:CGRectMake(-10,IS_IPAD?400: 180,IS_IPAD? 768: 320, 30)];
        
        if (responseDict.count==0)
        {
            [lblNoRecord setText:@""];
        }
        else
        {
            [lblNoRecord setText:@"No Record Available."];
        }
        
        [lblNoRecord setTextColor:[UIColor lightGrayColor]];
        lblNoRecord.textAlignment = NSTextAlignmentCenter;
        [lblNoRecord setBackgroundColor:[UIColor clearColor]];
        [lblNoRecord setFont:[UIFont boldSystemFontOfSize:IS_IPAD?20: 17]];
        [cell.contentView addSubview:lblNoRecord];
        cell.backgroundColor=[UIColor clearColor];
    }
    else
    {
        PatrawinCase *objTemp = [searchResults objectAtIndex:indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.textLabel.text=[NSString stringWithFormat:@"Case: %@",GET_VALID_STRING(objTemp.case_id)];
        cell.textLabel.textColor= UIColorFromRGB(0x4174DA);
        cell.textLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 20 : 16.0];
        
        cell.detailTextLabel.text=GET_VALID_STRING(objTemp.title);
        cell.detailTextLabel.textColor=[UIColor darkGrayColor];
        cell.detailTextLabel.font=[UIFont systemFontOfSize:IS_IPAD ? 20 : 12.0];
        
        cell.accessoryView = [[ UIImageView alloc ]
                              initWithImage:[UIImage imageNamed:@"arrowMessage"]];
        
    }
    }
    
//    else
//    {
//        cell.textLabel.font = [UIFont systemFontOfSize:IS_IPAD ? 25 : 15];
//        
//        companyBo=[arrayListOfCompaniesName objectAtIndex:indexPath.row];
//        cell.textLabel.text = (![companyBo.strCompany_name isEqual:[NSNull null]]) ? companyBo.strCompany_name  : @"";
//    }
    
    if (tableView == tblStatsData)
    {
        UIView *_lineDiv=[[UIView alloc] init];
        _lineDiv.frame=CGRectMake(0, IS_IPAD ? 60 : 70.0f, self.view.frame.size.width, 1.0);
        _lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
        [cell addSubview:_lineDiv];

    }
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    
    if (tableView == tblStatsData)
    {
        if (searchResults.count==0)
        {
            
        }
        else
        {
             NSString *strKey=[USER_DEFAULTS valueForKey:@"Customer"];
            
           // [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            obj_CaseVC.strSelectedIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            //          obj_CaseVC.arrayAllCases=[[NSMutableArray alloc]initWithArray:self.casesArray];
            obj_CaseVC.arrayAllCases = [[NSMutableArray alloc]initWithArray:searchResults];
            obj_CaseVC.casesScreentype = @"Statistics";
            
            if ([strKey isEqualToString:@"1"])
            {
                
            }
            else if ([strKey isEqualToString:@"0"])
            {
               obj_CaseVC.arrRecommendationListBO=self.arrRecommendations;
                NSLog(@"%i", obj_CaseVC.arrRecommendationListBO.count);
            }
            [self.navigationController pushViewController:obj_CaseVC animated:YES];
        }
    }

    
//    else
//    {
//        
//        
//        
////=====================================================================================================
////        AppDelegate *appDelegate;
////        appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
////        NSManagedObjectContext *context = [appDelegate managedObjectContext];
////        
////        //Statistics Listing Data.
////        
////        NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"StasDetailList" inManagedObjectContext:context];
////        NSFetchRequest *request = [[NSFetchRequest alloc] init];
////        [request setEntity:entityDesc];
////        
////        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"corresID = %@", strSelectedTag];
////        request.predicate=predicate;
////        
////        NSError *error;
////        NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
////
////        //Company Data.
////        
////        NSEntityDescription *entityDesc2 = [NSEntityDescription entityForName:@"CompanyList" inManagedObjectContext:context];
////        NSFetchRequest *request2 = [[NSFetchRequest alloc] init];
////        [request2 setEntity:entityDesc2];
////        
////        NSError *error2;
////        NSArray *fetchedArray2 = [context executeFetchRequest:request2 error:&error2];
////        [responseDict setValue:fetchedArray2 forKey:@"data"];
////        
////        [self performSelectorOnMainThread:@selector(assigningDataCompany:) withObject:self.responseDict waitUntilDone:YES];
////
////        NSString *str = [NSString stringWithFormat:@"%@", fetchedArray2];
////        
////        if ([str isEqualToString:@"(\n)"])
////        {
////            [CommonMethods showAlertWithTitle:@"NellPat" andMessage:@"No data in this case list."];
////            
////            [self ShowViewForAgentsList:NO];
////        }
////        else
////        {
////            for (StasDetailList *statList in fetchedArray) {
////                
////                PatrawinCase *objPatraCaseBO=[[PatrawinCase alloc]init];
////                
////                objPatraCaseBO.account_cost = statList.accountCost;
////                objPatraCaseBO.agent_id = [NSString stringWithFormat:@"%d",[statList.agentId intValue]];
////                objPatraCaseBO.arrived_date = [NSString stringWithFormat:@"%@",statList.arrivedDate];
////                objPatraCaseBO.case_id = statList.caseId;
////                objPatraCaseBO.comment_case = statList.caseComment;
////                objPatraCaseBO.case_type = statList.caseType;
////                objPatraCaseBO.comment = statList.comment;
////                objPatraCaseBO.country_code = statList.countryCode;
////                objPatraCaseBO.customer_id = [NSString stringWithFormat:@"%d",[statList.customerId intValue]];
////                objPatraCaseBO.customer_name = statList.customerName;
////                objPatraCaseBO.deadline = statList.deadline;
////                objPatraCaseBO.figure = statList.figure;
////                objPatraCaseBO.link = statList.link;
////                objPatraCaseBO.patent_number = statList.patentNumber;
////                objPatraCaseBO.title = statList.title;
////                objPatraCaseBO.status = statList.status;
////                
////                [self.arrStatsData addObject:objPatraCaseBO];
////                
////                [self ShowViewForAgentsList:NO];
////                
////                searchResults = [NSArray arrayWithArray:self.arrStatsData];
////                
////                textSortingCompany.text = [[arrayListOfCompaniesName objectAtIndex:indexPath.row] valueForKey:@"company_name"];
////                
////                [tblStatsData reloadData];
////            }
////        }
////======================================================================================================================================================
//        
//        CompanyBO *compnyBO = [arrayListOfCompaniesName objectAtIndex:indexPath.row];
//        
//        WebserviceInputParameter *inputPram = [[WebserviceInputParameter alloc] init];
//        
//        inputPram.methodType = WebserviceMethodTypePost;
//        
//        inputPram.relativeWebservicePath = @"petrawincasesonbasesofcompanyid";
//        
//        NSString *strAccessToken=[USER_DEFAULTS valueForKey:@"token"];
//        
//        [inputPram.dict_postParameters setObject:GET_VALID_STRING(strAccessToken) forKey:@"accesstoken"];
//        
//        [inputPram.dict_postParameters setObject:compnyBO.strCompany_Id forKey:@"company_id"];
//        
////=========================================comment ths bcoz it will remove cases by selecting company=========================================================
//        
//       // [self.arrStatsData removeAllObjects];
//        
//        [WebserviceManager callWebserviceWithInputParame:inputPram completion:^(id response, NSError *error)
//         {
//             if (error)
//             {
//                 
//                 if ([error isErrorOfKindNoNetwork])
//                     
//                     [CommonMethods showAlertWithTitle:error.domain andMessage:error.localizedDescription];
//                 
//                 return ;
//             }
//             
//             error = nil;
//             
//             self.responseDict = nil;
//             
//             self.responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
//
//             NSLog(@"Response Dict = %@", self.responseDict);
//             
//             NSArray *arrTemp=[self.responseDict objectForKey:@"data"];
//             
//             NSString *str = [NSString stringWithFormat:@"%@", arrTemp];
//             
//             if ([str isEqualToString:@"(\n)"])
//             {
//                 [CommonMethods showAlertWithTitle:@"Hynell" andMessage:@"No data in this case list."];
//                 
//               //  [self ShowViewForAgentsList:NO];
//             }
//             else
//             {
//                 for (int i=0; i<arrTemp.count; i++)
//                 {
////                 for (StasDetailList *statList in arrTemp) {
//
//                     PatrawinCase *objPatraCaseBO=[[PatrawinCase alloc]init];
////
////                     objPatraCaseBO.account_cost = statList.accountCost;
////                     objPatraCaseBO.agent_id = [NSString stringWithFormat:@"%d",[statList.agentId intValue]];
////                     objPatraCaseBO.arrived_date = [NSString stringWithFormat:@"%@",statList.arrivedDate];
//////                     objPatraCaseBO.case_id = [NSString stringWithFormat:@"%d",[statList.caseId intValue]];
////                     objPatraCaseBO.case_id = statList.caseId;
////                     objPatraCaseBO.comment_case = statList.caseComment;
////                     objPatraCaseBO.case_type = statList.caseType;
////                     objPatraCaseBO.comment = statList.comment;
////                     objPatraCaseBO.country_code = statList.countryCode;
////                     objPatraCaseBO.customer_id = [NSString stringWithFormat:@"%d",[statList.customerId intValue]];
////                     objPatraCaseBO.customer_name = statList.customerName;
////                     objPatraCaseBO.deadline = statList.deadline;
////                     objPatraCaseBO.figure = statList.figure;
////                     objPatraCaseBO.link = statList.link;
////                     objPatraCaseBO.patent_number = statList.patentNumber;
////                     objPatraCaseBO.title = statList.title;
////                     objPatraCaseBO.status = statList.status;
//
//                     objPatraCaseBO.account_cost=[[[arrTemp objectAtIndex:i] objectForKey:@"account_cost"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"account_cost"]:@"";
//                     objPatraCaseBO.agent_id=[[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"agent_id"]:@"";
//                     objPatraCaseBO.arrived_date=[[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"arrived_date"]:@"";
//                     objPatraCaseBO.case_id=[[[arrTemp objectAtIndex:i] objectForKey:@"case_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_id"]:@"";
//                     objPatraCaseBO.comment_case=[[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"Case_Comment"]:@"";
//                     objPatraCaseBO.case_type=[[[arrTemp objectAtIndex:i] objectForKey:@"case_type"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"case_type"]:@"";
//                     objPatraCaseBO.comment=[[[arrTemp objectAtIndex:i] objectForKey:@"comment"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"comment"]:@"";
//                     objPatraCaseBO.country_code=[[[arrTemp objectAtIndex:i] objectForKey:@"country_code"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"country_code"]:@"";
//                     objPatraCaseBO.customer_id=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_id"]:@"";
//                     objPatraCaseBO.customer_name=[[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"customer_name"]:@"";
//                     objPatraCaseBO.deadline=[[[arrTemp objectAtIndex:i] objectForKey:@"deadline"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"deadline"]:@"";
//                     objPatraCaseBO.figure=[[[arrTemp objectAtIndex:i] objectForKey:@"figure"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"figure"]:@"";
//                     objPatraCaseBO.link=[[[arrTemp objectAtIndex:i] objectForKey:@"link"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"link"]:@"";
//                     objPatraCaseBO.patent_number=[[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"patent_number"]:@"";
//                     objPatraCaseBO.status=[[[arrTemp objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"status"]:@"";
//                     objPatraCaseBO.title=[[[arrTemp objectAtIndex:i] objectForKey:@"title"] isKindOfClass:[NSString class]]?[[arrTemp objectAtIndex:i] objectForKey:@"title"]:@"";
//                     
//                     RecommendationsListBO *objRecommendationListBO=[[RecommendationsListBO alloc]init];
//                     objRecommendationListBO.strRecommendationId=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Id"] ;
//                     objRecommendationListBO.strRecommendationText=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Text"];
//                     objRecommendationListBO.strRecommendationStatus=[[arrTemp objectAtIndex:i] objectForKey:@"Recommendation_Status"];
//                     objPatraCaseBO.objRecommendationListBO=objRecommendationListBO;
//                     [self.arrStatsData addObject:objPatraCaseBO];
//                     
//                   //  [self ShowViewForAgentsList:NO];
//                     
//                     searchResults = [NSArray arrayWithArray:self.arrStatsData];
//                     
//                     textSortingCompany.text = [[arrayListOfCompaniesName objectAtIndex:indexPath.row] valueForKey:@"company_name"];
//                     
//                     [tblStatsData reloadData];
//             }
//             }
//              NSLog(@"Array Data = %@", arrTemp);
//         }];
//   }
}


-(void)getCompany
{
    NSMutableArray *finalArray = [[NSMutableArray alloc]init];
    finalArrayValueZeroStat = [[NSMutableArray alloc]init];
    
    uniqueCompany= [NSSet setWithArray:[self.arrStatsData valueForKey:@"customer_id"]];
    comName=[[NSMutableArray alloc]init];
    compArray = [[NSMutableArray alloc]init];
    compArray=[[uniqueCompany allObjects] mutableCopy];
    for(int i=0;i<compArray.count;i++)
    {
        for(int j=0;j<self.arrStatsData.count;j++)
        {
            PatrawinCase *cid=[self.arrStatsData objectAtIndex:j];
            NSString *hh=cid.customer_id;
            if([hh isEqualToString:@"0"] || [hh isEqualToString:@""])
                
            {
                [finalArrayValueZeroStat addObject:cid];
                NSLog(@"cid null *****");
                break;
            }
           else if([hh isEqualToString:[compArray objectAtIndex:i] ])
            {
                [finalArray addObject:cid];
                break;
            }
            
                                       
        }
    }
    
    PatrawinCase *cwithzero = [[PatrawinCase alloc]init];
    cwithzero.customer_id=@"0";
    cwithzero.customer_name=@"Other";
    cwithzero.comment=@"";
    cwithzero.comment_case=@"";
    
    cwithzero.keyword=@"";
    cwithzero.agent_id=@"";
    cwithzero.country_code=@"";
    cwithzero.title=@"";
    
    cwithzero.figure=@"";
    cwithzero.deadline=@"";
    cwithzero.Recommendation_Text=@"";
    cwithzero.case_type=@"";
    
    cwithzero.arrived_date=@"";
    cwithzero.case_id=@"";
    cwithzero.link=@"";
    cwithzero.figureSmall=@"";
    
    cwithzero.account_cost=@"";
    cwithzero.patent_number=@"";
    cwithzero.status=@"";
    cwithzero.Recommendation_Status=@"";

    
    comName = [[NSArray alloc]initWithArray:finalArray];
    search=comName;

}


-(void)sortCasesDescending
{
    NSMutableArray *unsortedArr = [NSMutableArray array];
    for(PatrawinCase *patCase in self.arrStatsData)
    {
        [unsortedArr addObject:patCase];
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    self.arrStatsData = [sortedArray mutableCopy];
    searchResults=self.arrStatsData;
    sortedArray = nil;
    
    [tblStatsData reloadData];
}

-(void)sortCasesAscending
{
    NSMutableArray *unsortedArr = [NSMutableArray array];
    for(PatrawinCase *patCase in self.arrStatsData)
    {
        [unsortedArr addObject:patCase];
    }
    [self.arrStatsData removeAllObjects];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"arrived_date" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    self.arrStatsData = [sortedArray mutableCopy];
    searchResults=self.arrStatsData;
    sortedArray = nil;
    
    [tblStatsData reloadData];
}

-(void)sortCasesByDeadline
{
    NSMutableArray *unsortedArr = [NSMutableArray array];
    for(PatrawinCase *patCase in self.arrStatsData)
    {
        [unsortedArr addObject:patCase];
    }
    [self.arrStatsData removeAllObjects];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deadline" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[unsortedArr sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    
    NSMutableArray *sortedwithoutZero = [[NSMutableArray alloc]init];
    for(int i=0;i<sortedArray.count;i++)
    {
        PatrawinCase *c=[sortedArray objectAtIndex:i];
        if(![c.deadline isEqualToString:@""])
        {
            [sortedwithoutZero addObject:c];
        }
    }
    
    self.arrStatsData = [sortedwithoutZero mutableCopy];
    searchResults=self.arrStatsData;
    sortedwithoutZero=nil;
    sortedArray = nil;
    
    [tblStatsData reloadData];
}

#pragma mark -

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Fetching Data.

-(void)fetchFromDatabase : (NSString *)strSelected
{
    AppDelegate *appDelegate;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    //Statistics Listing Data.
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"StasDetailList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"corresID = %@", strSelectedTag];
    request.predicate=predicate;
    
    NSError *error;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    
    //Recommendations List Data.
    
    NSEntityDescription *entityDesc1 = [NSEntityDescription entityForName:@"RecommendationsList" inManagedObjectContext:context];
    NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
    [request1 setEntity:entityDesc1];
    
    NSError *error1;
    NSArray *fetchedArray1 = [context executeFetchRequest:request1 error:&error1];

    responseDict =[NSMutableDictionary new];
    [responseDict setValue:fetchedArray forKey:@"data"];
    [responseDict setValue:fetchedArray1 forKey:@"recommendation_list"];
    
    if (fetchedArray.count == 0)
    {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DataReload) name:@"StatsListdownloaded" object:nil];
        dataBO=[DataSyncBO new];
        [dataBO CallForStatsData:strSelectedTag];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(assigningDataStats) withObject:self.responseDict waitUntilDone:YES];
    }
}



@end


